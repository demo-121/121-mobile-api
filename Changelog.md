# v1.13.0.5
* Add more information when perform activation
* Add log in audit log
* Remove useless console.log
* Restore some of the unit tests

# v1.11.1.9
* Optimize login and check multiple device flow
* Optimize generate RSA key pair flow.
* Add logs for get conflict app docs

# v1.11.1.8
* Update Shield Child application rather than Shield parent application

# v1.11.1.7
* Update get paymentAndSubmissionDoubleCheck scope

# v1.11.1.3
* Check double payment before get payment url
* Fix data sync conflict issue

# v1.11.1.0
* Add UX_{agentId} query
* Receive language parameter when call submit end point

# v1.10.2.14
* Test axa sync gateway admin port
* Check device and version
* Return data sync ids when after activation
* Test openid setting

# v1.10.2.7
* Multiple log in
* Upgrade Maam from v0 to v2

# v1.10.1.6
* Fix pre App is not shown as pending supervisor approval/approved/expired for submitted case issue

# v1.10.1.5
* Fix email can't send large size attactment issue

# v1.10.1.4
* Add logs for submit appStatus error
* Multiple login
* response merge pdf attactment

# v1.10.1.3
* Update online list
* Fix submit 500 error (can't merge pdf)

# v1.10.1.2
* Return pdf and policy document to iOS on submit response

# v1.10.1.1
* Fix submit email can’t send to supervisor and agent issue
* Fix submit email can't send to client issue
* Follow email attactments password protect rule

# v1.9.0.10
* Fix data sync can't sync approval document issue

# v1.9.0.9
* When one of numOfSheild and numOfNonSheild doesn't have enough policy numbers, return empty array. If both do not have enough policy numbers, throw error.
* Check whether policy number exist before doing submit
* Fix FNA can't sync issue
* Fix quick quote pdfs of email should not password protected issue

# v1.9.0.8
* Fix get policy number 400 error
* Add confirmAppPaid end point
* Add data sync recover and delete end point document

# v1.9.0.6
* Fix email can't send issue (FNA/Quotation/PreApplication)

# v1.9.0.5
* Fix dealerGroup can’t show issue

# v1.9.0.4
* Fix can't submit sheild case issue

# v1.9.0.3
### Defects
* Fix can't submit non-sheild case issue

# v1.9.0.2
### Defects
* Fix can't submit non-sheild case issue

# v1.9.0.1
### Defects
* Fix can't submit non-sheild case issue

# v1.9.0.0
### Defects
* Can't send email error

### Features
* Add token verify switch

# v1.0.0.7
### Features
* Add is_token_verify config to make develop easily (developer can send request without any token)

# v1.0.0.6
### Features
* Submission
* Data sync (includes get sync gateway password and get online list)
* Get policy number (non-shield and shield)
* Get payment status (Need design the payment flow again in the future)
* Send error email with time, error message, request-uuid, error-uuid, global variable and OS info
* Add timestamp for morgan lib in order to see the log more clearly
* Add helmet lib for security

### Defects
* lstChgDate data all transfer to number data type in order to get last change date correctly.