openapi: 3.0.0
info:
  title: AXA SG iOS Web Service Documentation
  description: >-
    This is documentation for AXA SG iOS Web Service Project
    (http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/AXA_SG_APP_web_service)
  version: v1.10.1.6
servers:
  - url: 'http://localhost:3000'
    description: Server on local
  - url: 'http://app.ease-sit3.intraeab/'
    description: Server on EAB SIT
  - url: 'https://app.ease-sit.axa.com.sg/'
    description: Server on AXA SIT
  - url: 'https://app.ease-uat.axa.com.sg/'
    description: Server on AXA UAT
tags:
  - name: Auth
    description: For verifying users
  - name: Mock SSO
    description: Mock SSO API
  - name: User
    description: API about users
  - name: Test
    description: Only for test
  - name: Email
    description: API about email
  - name: Payment
    description: API about payment
  - name: Application
    description: API about application
  - name: Data Sync
    description: API about data sync
paths:
  /auth/activate:
    post:
      tags:
        - Auth
      summary: >-
        Registers the agent and installation information during mobile app
        activation
      security:
        - oauth:
            - axa-sg-lifeapi
      requestBody:
        content:
          application/json:
            schema:
              type: object
              properties:
                device_model:
                  type: string
                device_id:
                  type: string
                versionNumber:
                  type: string
                buildNumber:
                  type: string
      responses:
        '200':
          description: successful operation
          content:
            application/json:
              schema:
                type: object
                properties:
                  status:
                    type: string
                  result:
                    type: object
                    properties:
                      user_id:
                        type: string
                      doc_seq:
                        type: number
                      installation_id:
                        type: string
                example:
                  status: success
                  result:
                    user_id: AG90006A
                    doc_seq: 200001
                    installation_id: 8bdd9fab-73b6-4d0c-acce-c6ec122fa5d0
        '400':
          description: Not allow multiplate login
          content:
            application/json:
              schema:
                type: object
                properties:
                  status:
                    type: string
                  message:
                    type: string
              example:
                status: error
                result: system.message.multipleActivation
  /auth/encryptionKey:
    put:
      tags:
        - Auth
      summary: Updates the master key in online DB during mobile app activation
      security:
        - oauth:
            - axa-sg-lifeapi
      requestBody:
        content:
          application/json:
            schema:
              type: object
              properties:
                master_key:
                  type: string
                installation_id:
                  type: string
      responses:
        '200':
          description: successful operation
          content:
            application/json:
              schema:
                type: object
                properties:
                  status:
                    type: string
                  result:
                    type: object
              example:
                status: success
                result: {}
  /auth/login:
    post:
      tags:
        - Auth
      summary: >-
        Authenticates the user by access token and user ID in online mode. In
        addition, it will return master key.
      security:
        - oauth:
            - axa-sg-lifeapi
      requestBody:
        content:
          application/json:
            schema:
              type: object
              properties:
                user_id:
                  type: string
                installation_id:
                  type: string
      responses:
        '200':
          description: successful login with masterKey
          content:
            application/json:
              schema:
                type: object
                properties:
                  status:
                    type: string
                    description: The status of response
                  result:
                    type: object
                    properties:
                      master_key:
                        type: string
                        description: master key of the agent
                example:
                  status: success
                  result:
                    master_key: sdjfksdjfkjs
  /mock/authorize:
    get:
      tags:
        - Mock SSO
      summary: Returns a fake SSO page
      parameters:
        - in: query
          name: response_type
          schema:
            type: string
          required: true
          description: Value must be set to "code"
        - in: query
          name: client_id
          schema:
            type: string
          required: true
          description: Client ID which was assigned when the client was registered
        - in: query
          name: redirect_uri
          schema:
            type: string
          required: true
          description: >-
            This value must match with the redirect URI which was defined when
            the client was registered
        - in: query
          name: scope
          schema:
            type: string
          required: true
          description: Value must be set to "axa-sg-lifeapi"
      responses:
        '200':
          description: >-
            Show a fake SSO page, delivers an authorization code to the
            redirection URI
  /mock/token:
    post:
      tags:
        - Mock SSO
      summary: Returns a fake access token
      requestBody:
        required: true
        content:
          application/x-www-form-urlencoded:
            schema:
              type: object
              properties:
                grant_type:
                  type: string
                code:
                  type: string
                redirect_uri:
                  type: string
      responses:
        '200':
          description: successful operation
          content:
            application/json:
              schema:
                type: object
                properties:
                  access_token:
                    type: string
                  token_type:
                    type: string
                  expires_in:
                    type: number
                  refresh_token:
                    type: string
                  scope:
                    type: string
                example:
                  access_token: ...
                  token_type: Bearer
                  expires_in: 3600
                  refresh_token: ...
                  scope: axa-sg-lifeapi
  '/mock/user/{userId}':
    get:
      tags:
        - User
      summary: Get user info from couchbase server directly
      security:
        - oauth:
            - axa-sg-lifeapi
      parameters:
        - in: path
          name: userId
          schema:
            type: string
          required: true
          description: User ID
      responses:
        '200':
          description: Return user info
          content:
            application/json:
              schema:
                type: object
                properties:
                  status:
                    type: string
                  result:
                    type: object
                    properties:
                      profileId:
                        type: string
                        description: Agent Id
                      name:
                        type: string
                        description: name
                      position:
                        type: string
                        description: position
                      email:
                        type: string
                        description: email
                      otherFields:
                        type: string
                        description: other fields
                example:
                  status: success
                  result:
                    profileId: AG90006A
                    name: dsfjdslkfj
                    position: fjdlskf
                    email: djflkj@ljl.com
                    otherFields: djflkj
  /email/send:
    post:
      tags:
        - Email
      security:
        - oauth:
            - axa-sg-lifeapi
      summary: Send email to agent or client
      requestBody:
        content:
          application/json:
            schema:
              type: object
              required:
                - userType
                - quotationId
                - from
                - to
                - title
              properties:
                userType:
                  type: string
                  description: >-
                    agent or client (Decide which type of pdf attactment
                    password should generate)
                quotationId:
                  type: string
                  description: >-
                    Quotation id (【case1】 If userType == 'agent', password =
                    quotation.agent.agentCode.substr(agentCode.length - 6, 6);
                    【case2】If userType == 'client', password = new
                    Date(quotation.pDob).format('ddmmmyyyy').toUpperCase())
                from:
                  type: string
                  description: Sender email
                to:
                  type: string
                  description: Recipient email
                title:
                  type: string
                  description: Email title
                content:
                  type: string
                  description: Email content
                attachments:
                  type: array
                  items:
                    type: string
                  description: Email attachments
      responses:
        '200':
          description: Return whether the email is be sent
          content:
            application/json:
              schema:
                type: object
                properties:
                  status:
                    type: string
                    description: whether the email is be send
                  result:
                    type: object
                    description: should be empty
                example:
                  status: success
                  result: {}
        '400':
          description: Return lack of required email params
          content:
            application/json:
              schema:
                type: object
                properties:
                  status:
                    type: string
                    description: Show error
                  message:
                    type: string
                    description: Detail message
                example:
                  status: error
                  message: 'userType, quotationId, from, to and title are required'
  '/payment/{docId}/status':
    get:
      tags:
        - Payment
      summary: Get the payment status
      security:
        - oauth:
            - axa-sg-lifeapi
      parameters:
        - in: path
          name: docId
          required: true
          schema:
            type: string
          description: Couchbase document id
      responses:
        '200':
          description: successful operation
          content:
            application/json:
              schema:
                type: object
                properties:
                  status:
                    type: string
                  result:
                    type: object
                    properties:
                      isSuccess:
                        type: boolean
                        description: >-
                          If payment is successful, this value will be set to
                          true and has paymentValue field. If payment is
                          unsuccessful, this value will be set to false and
                          hasn't paymentValue field.
                      paymentValue:
                        type: object
                        description: >-
                          We set payment field of application document to this
                          field.
                      type:
                        type: string
                        description: Type to identify
                example:
                  status: success
                  result:
                    isSuccess: true
                    paymentValue:
                      field1: value1
                      field2: value2
                      field3: value3
                    type: payment
        '400':
          description: Document is not exists in Couchbase
          content:
            application/json:
              schema:
                type: object
                properties:
                  status:
                    type: string
                  message:
                    type: string
                example:
                  status: error
                  message: Doc is not exists.
  /application/submit:
    post:
      tags:
        - Application
      summary: Submit the application
      security:
        - oauth:
            - axa-sg-lifeapi
      requestBody:
        content:
          application/json:
            schema:
              required:
                - docId
              type: object
              properties:
                docId:
                  type: string
              example:
                docId: NB001003-02064
      responses:
        '200':
          description: successful operation
          content:
            application/json:
              schema:
                type: object
                properties:
                  status:
                    type: string
                  result:
                    type: object
                    description: New application object
                    properties:
                      application:
                        type: object
                        description: >-
                          latest application document. If it's shield case, this
                          field will be an array.
                      bundle:
                        type: object
                        description: latest bundele document
                      policyDocument:
                        type: object
                        description: >-
                          latest policy documents. If it's shield case, this
                          field will be an array and include SP document.
                      appPdf:
                        type: string
                        description: >-
                          appPdf base64 string. If it's shield case, this field
                          will be an array with docId, attchId, rev and base64
                          data
                example:
                  status: success
                  result:
                    application:
                      _id: NB001001-00032
                      type: application
                      field1: value1
                      field2: value2
                    bundle:
                      _id: FN001001-00032
                      type: bundle
                      field1: value1
                      field2: value2
                    policyDocument:
                      _id: 101-123456
                      type: approval
                      field1: value1
                      field2: value2
                    appPdf: sdjfkdsljflkjfdsklfjdslk
  /application/getPolicyNumber:
    post:
      tags:
        - Application
      summary: Get policy number
      security:
        - oauth:
            - axa-sg-lifeapi
      requestBody:
        content:
          application/json:
            schema:
              required:
                - numOfShield
                - numOfNonShield
              type: object
              properties:
                numOfShield:
                  type: number
                numOfNonShield:
                  type: number
              example:
                numOfShield: 2
                numOfNonShield: 3
      responses:
        '200':
          description: successful operation
          content:
            application/json:
              schema:
                type: object
                properties:
                  status:
                    type: string
                  result:
                    type: object
                    properties:
                      shieldPolicyNum:
                        type: array
                        items:
                          type: string
                        description: >-
                          A set of shield policy number.1.  When we don’t have
                          enough shield policy numbers but have enough
                          non-shield policy numbers, Mobile-API will return
                          non-shield policy number array and empty shield policy
                          number array. 2.  When we don’t have enough non-shield
                          policy numbers but have enough shield policy numbers,
                          Mobile-API will return shield policy number array and
                          empty non-shield policy number array.3.  When we don’t
                          have enough both non-shield policy numbers and shield
                          policy numbers, Mobile-API will return 500 HTTP error
                          status and ‘Do not have enough policy numbers’
                          message.
                      nonShieldPolicyNum:
                        type: array
                        items:
                          type: string
                        description: >-
                          A set of non-shield policy number. 1.  When we don’t
                          have enough shield policy numbers but have enough
                          non-shield policy numbers, Mobile-API will return
                          non-shield policy number array and empty shield policy
                          number array. 2.  When we don’t have enough non-shield
                          policy numbers but have enough shield policy numbers,
                          Mobile-API will return shield policy number array and
                          empty non-shield policy number array.3.  When we don’t
                          have enough both non-shield policy numbers and shield
                          policy numbers, Mobile-API will return 500 HTTP error
                          status and ‘Do not have enough policy numbers’
                          message.
                      type:
                        type: string
                example:
                  status: success
                  result:
                    shieldPolicyNum:
                      - 301-4648988
                      - 301-4648989
                    nonShieldPolicyNum:
                      - 101-4648988
                      - 101-4648989
                      - 101-4648990
                    type: getPolicyNumber
        '400':
          description: Lack of params
          content:
            application/json:
              schema:
                type: object
                properties:
                  status:
                    type: string
                  message:
                    type: string
                example:
                  status: error
                  message: numOfShield and numOfNonShield are required.
  /application/confirmAppPaid:
    post:
      tags:
        - Application
      summary: Confirm application paid
      security:
        - oauth:
            - axa-sg-lifeapi
      requestBody:
        content:
          application/json:
            schema:
              required:
                - docId
              type: object
              properties:
                docId:
                  type: string
              example:
                docId: NB23432-234324
      responses:
        '200':
          description: Confirm applicationn paid after invalidated
          content:
            application/json:
              schema:
                type: object
                properties:
                  status:
                    type: string
                  result:
                    type: object
                example:
                  status: success
                  result: {}
        '400':
          description: Lack of doc id
          content:
            application/json:
              schema:
                type: object
                properties:
                  status:
                    type: string
                  message:
                    type: string
                example:
                  status: error
                  message: docId is required.
  /dataSync/onlineList:
    get:
      tags:
        - Data Sync
      summary: Get online list before data sync
      security:
        - oauth:
            - axa-sg-lifeapi
      parameters:
        - in: query
          name: agentCode
          schema:
            type: string
          required: true
        - in: query
          name: lstChgDate
          schema:
            type: string
          required: true
      responses:
        '200':
          description: successful operation
          content:
            application/json:
              schema:
                type: object
                properties:
                  status:
                    type: string
                  result:
                    type: object
                    properties:
                      onlineWinObjs:
                        type: object
                        properties:
                          custId:
                            type: string
                          docIds:
                            type: array
                            items:
                              type: string
                            description: sss
                          maxUpdDate:
                            type: string
                      onlineChangedObjs:
                        type: object
                        properties:
                          custId:
                            type: string
                          docIds:
                            type: array
                            items:
                              type: string
                            description: sss
                          maxUpdDate:
                            type: string
                      deletedIds:
                        type: array
                        items:
                          type: object
                          properties:
                            clientId:
                              type: string
                            deletedTime:
                              type: string
                example:
                  status: success
                  result:
                    onlineWinObjs:
                      custId: CP001041-00001
                      docIds:
                        - FN001041-00001
                        - CP001041-00001
                      maxUpdDate: 1539256289170
                    onlineChangedObjs:
                      custId: CP001041-00001
                      docIds:
                        - FN001041-00001
                        - CP001041-00001
                      maxUpdDate: 1539256289170
                    deletedIds:
                      - clientId: CP001001-00001
                        deletedTime: 1540366349729
                      - clientId: CP201001-00101
                        deletedTime: 1540366349728
        '400':
          description: Lack of params
          content:
            application/json:
              schema:
                type: object
                properties:
                  status:
                    type: string
                  message:
                    type: string
                example:
                  status: error
                  message: numOfShield and numOfNonShield are required.
  /dataSync/accessSG:
    post:
      tags:
        - Data Sync
      summary: Get sync gateway password
      security:
        - oauth:
            - axa-sg-lifeapi
      requestBody:
        content:
          application/json:
            schema:
              required:
                - key
              type: object
              properties:
                key:
                  type: string
              example:
                key: >-
                  -----BEGIN RSA PUBLIC
                  KEY-----\nfjdslkfjslkdjfklsdjflksjflksdjfslkjksdjfwierjiewjejwrkfdsfsd\n-----END
                  RSA PUBLIC KEY-----
      responses:
        '200':
          description: successful operation
          content:
            application/json:
              schema:
                type: object
                properties:
                  status:
                    type: string
                  result:
                    type: string
                example:
                  status: success
                  result: jsfkwjekrelwjrklewjrekwrjewkrjeklrejwrkewjrewkljnvsdnl
  /dataSync/saveAudDataSync:
    post:
      tags:
        - Data Sync
      summary: Save online list on oracle
      security:
        - oauth:
            - axa-sg-lifeapi
      requestBody:
        content:
          application/json:
            schema:
              type: object
              required:
                - agentCode
                - deviceUuid
                - syncResult
                - syncDate
              properties:
                trxNo:
                  type: string
                agentCode:
                  type: string
                deviceUuid:
                  type: string
                syncResult:
                  type: string
                syncDate:
                  type: string
                status:
                  type: string
                createUser:
                  type: string
                updUser:
                  type: string
      responses:
        '200':
          description: Return success
          content:
            application/json:
              schema:
                type: object
                properties:
                  status:
                    type: string
                    description: Whether you have successfully save aud data sync to oracle
                  result:
                    type: number
                    description: >-
                      Trx No. If we doesn't provide on request body, we will 
                      generate a new trx No.
                example:
                  status: success
                  result: 23432
  /dataSync/recoverBackupData:
    get:
      tags:
        - Data Sync
      summary: Get the data before data sync to recover
      security:
        - oauth:
            - axa-sg-lifeapi
      parameters:
        - in: query
          name: agentCode
          schema:
            type: string
          required: true
      responses:
        '200':
          description: Return success
          content:
            application/json:
              schema:
                type: object
                properties:
                  status:
                    type: string
                    description: Whether you have successfully get the recover documents
                  result:
                    type: array
                    items:
                      type: object
                      description: A set of recover documents
                      properties:
                        _id:
                          type: string
                        type:
                          type: string
                        field1:
                          type: string
                        field2:
                          type: string
                example:
                  status: success
                  result:
                    - _id: U_AJFLSKDJFL
                      type: agent
                      agentCode: blablabla
                      channel: agent
                    - _id: CP_2342343424
                      type: cust
                      dob: '1999-01-01'
                      cid: '234324234'
        '400':
          description: Lack of agent code
          content:
            application/json:
              schema:
                type: object
                properties:
                  status:
                    type: string
                  message:
                    type: string
                example:
                  status: error
                  message: agentCode is required.
  /dataSync/clearBackupData:
    delete:
      tags:
        - Data Sync
      security:
        - oauth:
            - axa-sg-lifeapi
      summary: Clear backup data after data sync is finish successfully
      parameters:
        - in: query
          name: agentCode
          schema:
            type: string
          required: true
      responses:
        '200':
          description: Return success
          content:
            application/json:
              schema:
                type: object
                properties:
                  status:
                    type: string
                  result:
                    type: object
                example:
                  status: success
                  result: {}
        '400':
          description: Lack of agent code
          content:
            application/json:
              schema:
                type: object
                properties:
                  status:
                    type: string
                  message:
                    type: string
                example:
                  status: error
                  message: agentCode is required.
  /:
    get:
      tags:
        - Test
      summary: Show you that whether you have successfully run the server
      responses:
        '200':
          description: Return success
          content:
            application/json:
              schema:
                type: object
                properties:
                  status:
                    type: string
                    description: Whether you have successfully run the server
                  result:
                    type: object
                    properties:
                      message:
                        type: string
                        description: Detail message
  /get404:
    get:
      tags:
        - Test
      summary: Show 404 err
      responses:
        '404':
          description: Return 404 error when url is incorrect
          content:
            application/json:
              schema:
                type: object
                properties:
                  status:
                    type: string
                  message:
                    type: string
                example:
                  status: error
                  message: Not found
  /get500:
    get:
      tags:
        - Test
      summary: Show 500 error
      responses:
        '500':
          description: Return 500 error when interval error happens
          content:
            application/json:
              schema:
                type: object
                properties:
                  status:
                    type: string
                    description: Whether you have successfully run the server
                  exception:
                    type: object
                    description: Detail error message
                example:
                  status: error
                  exception: {}
components:
  securitySchemes:
    oauth:
      type: oauth2
      flows:
        authorizationCode:
          authorizationUrl: /auth/authorize
          tokenUrl: /auth/token
          scopes:
            axa-sg-lifeapi: mobile
