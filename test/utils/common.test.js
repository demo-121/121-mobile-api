const faker = require('faker');

const commonUtil = require('../../app/utils/common.util');

describe('Common Util', () => {

  it("1+1=2", (done) => {
    expect(1+ 1).toBe(2);
    done();
  })

  // it('[function]getConfig [cause]Success', () => {
  //   expect(commonUtil.getConfig('MAAM_JWKS_URL')).toBe('https://maam-dev.axa.com/dev/jwks');
  // });

  // it('[function]getConfig [cause]ENV != dev and process.env[key] not exists', (done) => {
  //   process.env.NODE_ENV = 'sit';
  //   expect(commonUtil.getConfig('MAAM_JWKS_URL')).toBe(null);
  //   setTimeout(() => {
  //     process.env.NODE_ENV = 'dev';
  //     done();
  //   }, 1000)
  // });

  // it('[function]getConfig [cause]ENV != dev and process.env[key] exists', (done) => {
  //   const fakeValue = faker.internet.url();
  //   process.env.NODE_ENV = 'sit';
  //   process.env.MAAM_JWKS_URL = fakeValue;
  //   expect(commonUtil.getConfig('MAAM_JWKS_URL')).toBe(fakeValue);
  //   setTimeout(() => {
  //     process.env.NODE_ENV = 'dev';
  //     process.env.MAAM_JWKS_URL = '';
  //     done();
  //   }, 1000)
  // });
})