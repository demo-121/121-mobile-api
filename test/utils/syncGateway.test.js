const faker = require('faker');

const syncGatewayUtil = require('../../app/utils/syncGateway.util');

describe('Sync Gateway Util', () => {
  it("1+1=2", (done) => {
    expect(1+ 1).toBe(2);
    done();
  })

  // it('[function]getDocFromSG [cause]Success', (done) => {
  //   const fakeDocId = `U_${faker.random.arrayElement(['AG90001D', 'AG90003M', 'AG90006A', 'S2195576J'])}`;
  //   syncGatewayUtil.getDocFromSG(fakeDocId, (err, result) => {
  //     expect(result._id).toBe(fakeDocId);
  //     done();
  //   })
  // });

  // it('[function]getDocFromSG [cause]Doc not exists', (done) => {
  //   const fakeDocId = faker.random.word();
  //   syncGatewayUtil.getDocFromSG(fakeDocId, (err, result) => {
  //     expect(err.status).toBe('error');
  //     expect(err.exception).toBe(`Doc (${fakeDocId}) is not exists.`);
  //     done();
  //   })
  // });
})