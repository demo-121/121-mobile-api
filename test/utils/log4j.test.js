const faker = require('faker');
const fs = require('fs');

const log4jUtil = require('../../app/utils/log4j.util');

describe('Log4js Util', () => {
  // it('[function]log("all", info) [cause] Success', (done) => {
  //   const fakeInfo = faker.random.words();
  //   log4jUtil.log('all', fakeInfo);
  //   done();
  //   // fs.readFile(process.cwd() + '/logs/records.log', 'utf8', (err, contents) => {
  //   //   expect(contents.indexOf(contents)).not.toBe("-1");
  //   //   done();
  //   // })
  // });

  // it('[function]log("trace", info) [cause] Success', (done) => {
  //   const fakeInfo = faker.random.words();
  //   log4jUtil.log('trace', fakeInfo);
  //   done();
  //   // fs.readFile(process.cwd() + '/logs/records.log', 'utf8', (err, contents) => {
  //   //   expect(contents.indexOf(contents)).not.toBe("-1");
  //   //   done();
  //   // })
  // });

  // it('[function]log("debug", info) [cause] Success', (done) => {
  //   const fakeInfo = faker.random.words();
  //   log4jUtil.log('debug', fakeInfo);
  //   done();
  //   // fs.readFile(process.cwd() + '/logs/records.log', 'utf8', (err, contents) => {
  //   //   expect(contents.indexOf(contents)).not.toBe("-1");
  //   //   done();
  //   // })
  // });

  // it('[function]log("info", info) [cause] Success', (done) => {
  //   const fakeInfo = faker.random.words();
  //   log4jUtil.log('info', fakeInfo);
  //   done();
  //   // fs.readFile(process.cwd() + '/logs/records.log', 'utf8', (err, contents) => {
  //   //   expect(contents.indexOf(contents)).not.toBe("-1");
  //   //   done();
  //   // })
  // });

  // it('[function]log("warn", info) [cause] Success', (done) => {
  //   const fakeInfo = faker.random.words();
  //   log4jUtil.log('warn', fakeInfo);
  //   done();
  //   // fs.readFile(process.cwd() + '/logs/records.log', 'utf8', (err, contents) => {
  //   //   expect(contents.indexOf(contents)).not.toBe("-1");
  //   //   done();
  //   // })
  // });

  // it('[function]log("error", info) [cause] Success', (done) => {
  //   const fakeInfo = faker.random.words();
  //   log4jUtil.log('error', fakeInfo);
  //   done();
  //   // fs.readFile(process.cwd() + '/logs/records.log', 'utf8', (err, contents) => {
  //   //   expect(contents.indexOf(contents)).not.toBe("-1");
  //   //   done();
  //   // })
  // });

  // it('[function]log("fatal", info) [cause] Success', (done) => {
  //   const fakeInfo = faker.random.words();
  //   log4jUtil.log('fatal', fakeInfo);
  //   done();
  //   // fs.readFile(process.cwd() + '/logs/records.log', 'utf8', (err, contents) => {
  //   //   expect(contents.indexOf(contents)).not.toBe("-1");
  //   //   done();
  //   // })
  // });

  // it('[function]log("mark", info) [cause] Success', (done) => {
  //   const fakeInfo = faker.random.words();
  //   log4jUtil.log('mark', fakeInfo);
  //   done();
  //   // fs.readFile(process.cwd() + '/logs/records.log', 'utf8', (err, contents) => {
  //   //   expect(contents.indexOf(contents)).not.toBe("-1");
  //   //   done();
  //   // })
  // });

  // it('[function]log("OFF", info) [cause] Success', (done) => {
  //   const fakeInfo = faker.random.words();
  //   log4jUtil.log('off', fakeInfo);
  //   done();
  //   // fs.readFile(process.cwd() + '/logs/records.log', 'utf8', (err, contents) => {
  //   //   expect(contents.indexOf(contents)).not.toBe("-1");
  //   //   done();
  //   // })
  // });

  // it('[function]log(ELSE CASE, info) [cause] Success', (done) => {
  //   const fakeInfo = faker.random.words();
  //   log4jUtil.log(faker.random.word(), fakeInfo);
  //   done();
  //   // fs.readFile(process.cwd() + '/logs/records.log', 'utf8', (err, contents) => {
  //   //   expect(contents.indexOf(contents)).not.toBe("-1");
  //   //   done();
  //   // })
  // });

  it("1+1=2", (done) => {
    expect(1+ 1).toBe(2);
    done();
  })
});
