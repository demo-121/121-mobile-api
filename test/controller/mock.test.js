const request = require('supertest');
const faker = require('faker');
const jwt = require('jsonwebtoken');

const app = require('../../app');

describe('Mock', () => {
  const authCode = faker.random.arrayElement(['aaa1', 'aaa2', 'aaa3', 'aaa4']);

  const requestFakeToken = () => new Promise((resolve) => {
    request(app)
      .post('/mock/token')
      .send({ grant_type: faker.random.word() })
      .send({ code: authCode })
      .send({ redirect_uri: 'easemobile://' })
      .then((response) => {
        resolve(JSON.parse(response.text).access_token);
      });
  });

  let token = '';

  beforeAll(() => new Promise((resolve) => {
    requestFakeToken()
      .then((callbackToken) => {
        token = callbackToken;
        resolve(token);
      });
  }));

  it('[function]Mock authorize [cause]success', (done) => {
    request(app)
      .get('/mock/authorize?client_id=43dd75bc-3cae-4281-906b-fe0404ddab75&redirect_uri=easemobile://')
      .then((response) => {
        expect(response.status).toBe(302);
        done();
      });
  });

  it('[function]Mock authorize [cause]Incorrect client_id', (done) => {
    const incorrectClientId = faker.random.uuid();
    request(app)
      .get(`/mock/authorize?client_id=${incorrectClientId}&redirect_uri=easemobile://`)
      .then((response) => {
        expect(response.status).toBe(400);
        expect(response.header['content-type'])
          .toBe('application/json; charset=utf-8');
        expect(JSON.parse(response.text).message).toBe('Incorrect client id');
        done();
      });
  });

  it('[function]Mock authorize [cause]Lack of client_id', (done) => {
    request(app)
      .get('/mock/authorize?client_id=&redirect_uri=easemobile://')
      .then((response) => {
        expect(response.status).toBe(400);
        expect(response.header['content-type'])
          .toBe('application/json; charset=utf-8');
        expect(JSON.parse(response.text).message).toBe('Incorrect client id');
        done();
      });
  });

  it('[function]Mock authorize [cause]Lack of redirect_uri', (done) => {
    request(app)
      .get('/mock/authorize?client_id=43dd75bc-3cae-4281-906b-fe0404ddab75&redirect_uri=')
      .then((response) => {
        expect(response.status).toBe(400);
        expect(response.header['content-type'])
          .toBe('application/json; charset=utf-8');
        expect(JSON.parse(response.text).message).toBe('Incorrect redirect uri');
        done();
      });
  });

  it('[function]Mock authorize [cause]Incorrect redirect_uri', (done) => {
    const incorrectRedirectUri = faker.internet.url();
    request(app)
      .get(`/mock/authorize?client_id=43dd75bc-3cae-4281-906b-fe0404ddab75&redirect_uri=${incorrectRedirectUri}`)
      .then((response) => {
        expect(response.status).toBe(400);
        expect(response.header['content-type'])
          .toBe('application/json; charset=utf-8');
        expect(JSON.parse(response.text).message).toBe('Incorrect redirect uri');
        done();
      });
  });

  it('[function]getToken [cause]Lack of grant_type', (done) => {
    request(app)
      .post('/mock/token')
      .send({ code: 'aaa1' })
      .send({ redirect_uri: 'easemobile://' })
      .then((response) => {
        expect(response.status).toBe(400);
        expect(response.header['content-type'])
          .toBe('application/json; charset=utf-8');
        expect(JSON.parse(response.text).status).toBe('error');
        expect(JSON.parse(response.text).message.error).toBe('invalid_request');
        expect(JSON.parse(response.text).message.error_description).toBe('Invalid grant_type');
        done();
      });
  });

  it('[function]getToken [cause]Lack of code', (done) => {
    request(app)
      .post('/mock/token')
      .send({ grant_type: faker.random.word() })
      .send({ redirect_uri: 'easemobile://' })
      .then((response) => {
        expect(response.status).toBe(400);
        expect(response.header['content-type'])
          .toBe('application/json; charset=utf-8');
        expect(JSON.parse(response.text).status).toBe('error');
        expect(JSON.parse(response.text).message.error).toBe('Non-valid parameter list for this operation');
        expect(JSON.parse(response.text).message.error_description).toBe('Check with Sascha!!');
        done();
      });
  });

  it('[function]getToken [cause]Lack of redirect_uri', (done) => {
    request(app)
      .post('/mock/token')
      .send({ grant_type: faker.random.word() })
      .send({ code: 'aaa1' })
      .then((response) => {
        expect(response.status).toBe(400);
        expect(response.header['content-type'])
          .toBe('application/json; charset=utf-8');
        expect(JSON.parse(response.text).status).toBe('error');
        expect(JSON.parse(response.text).message.error).toBe('Token retrievel error');
        expect(JSON.parse(response.text).message.error_description).toBe('The requested token could not be retrieved');
        done();
      });
  });

  it('[function]getToken [cause]Incorrect redirect_uri', (done) => {
    request(app)
      .post('/mock/token')
      .send({ grant_type: faker.random.word() })
      .send({ code: 'aaa1' })
      .send({ redirect_uri: faker.internet.url() })
      .then((response) => {
        expect(response.status).toBe(400);
        expect(response.header['content-type'])
          .toBe('application/json; charset=utf-8');
        expect(JSON.parse(response.text).status).toBe('error');
        expect(JSON.parse(response.text).message.error).toBe('Token retrievel error');
        expect(JSON.parse(response.text).message.error_description).toBe('The requested token could not be retrieved');
        done();
      });
  });

  it('[function]getToken [cause]Success with fake agent 1', (done) => {
    request(app)
      .post('/mock/token')
      .send({ grant_type: faker.random.word() })
      .send({ code: 'aaa1' })
      .send({ redirect_uri: 'easemobile://' })
      .then((response) => {
        expect(response.status).toBe(200);
        expect(response.header['content-type'])
          .toBe('application/json; charset=utf-8');
        expect(JSON.parse(response.text).access_token).toBe(jwt.sign({ sub: 'A1' }, 'secret'));
        expect(JSON.parse(response.text).token_type).toBe('Bearer');
        expect(JSON.parse(response.text).expires_in).toBe('360000');
        expect(JSON.parse(response.text).refresh_token).toBe('zzzxxxcccvvv');
        expect(JSON.parse(response.text).scope).toBe('axa-sg-lifeapi');
        done();
      });
  });

  it('[function]getToken [cause]Success with fake agent 2', (done) => {
    request(app)
      .post('/mock/token')
      .send({ grant_type: faker.random.word() })
      .send({ code: 'aaa2' })
      .send({ redirect_uri: 'easemobile://' })
      .then((response) => {
        expect(response.status).toBe(200);
        expect(response.header['content-type'])
          .toBe('application/json; charset=utf-8');
        expect(JSON.parse(response.text).access_token).toBe(jwt.sign({ sub: 'A2' }, 'secret'));
        expect(JSON.parse(response.text).token_type).toBe('Bearer');
        expect(JSON.parse(response.text).expires_in).toBe('360000');
        expect(JSON.parse(response.text).refresh_token).toBe('zzzxxxcccvvv');
        expect(JSON.parse(response.text).scope).toBe('axa-sg-lifeapi');
        done();
      });
  });

  it('[function]getToken [cause]Success with fake agent 3', (done) => {
    request(app)
      .post('/mock/token')
      .send({ grant_type: faker.random.word() })
      .send({ code: 'aaa3' })
      .send({ redirect_uri: 'easemobile://' })
      .then((response) => {
        expect(response.status).toBe(200);
        expect(response.header['content-type'])
          .toBe('application/json; charset=utf-8');
        expect(JSON.parse(response.text).access_token).toBe(jwt.sign({ sub: 'A3' }, 'secret'));
        expect(JSON.parse(response.text).token_type).toBe('Bearer');
        expect(JSON.parse(response.text).expires_in).toBe('360000');
        expect(JSON.parse(response.text).refresh_token).toBe('zzzxxxcccvvv');
        expect(JSON.parse(response.text).scope).toBe('axa-sg-lifeapi');
        done();
      });
  });

  it('[function]getToken [cause]Success with fake agent 4', (done) => {
    request(app)
      .post('/mock/token')
      .send({ grant_type: faker.random.word() })
      .send({ code: 'aaa4' })
      .send({ redirect_uri: 'easemobile://' })
      .then((response) => {
        expect(response.status).toBe(200);
        expect(response.header['content-type'])
          .toBe('application/json; charset=utf-8');
        expect(JSON.parse(response.text).access_token).toBe(jwt.sign({ sub: 'A4' }, 'secret'));
        expect(JSON.parse(response.text).token_type).toBe('Bearer');
        expect(JSON.parse(response.text).expires_in).toBe('360000');
        expect(JSON.parse(response.text).refresh_token).toBe('zzzxxxcccvvv');
        expect(JSON.parse(response.text).scope).toBe('axa-sg-lifeapi');
        done();
      });
  });

  it('[function]getToken [cause]Success with other fake agent(not exist)', (done) => {
    request(app)
      .post('/mock/token')
      .send({ grant_type: faker.random.word() })
      .send({ code: faker.random.word() })
      .send({ redirect_uri: 'easemobile://' })
      .then((response) => {
        expect(response.status).toBe(400);
        expect(response.header['content-type'])
          .toBe('application/json; charset=utf-8');
        expect(JSON.parse(response.text).status).toBe('error');
        expect(JSON.parse(response.text).message.error).toBe('Token retrievel error');
        expect(JSON.parse(response.text).message.error_description).toBe('The requested token could not be retrieved');
        done();
      });
  });

  it('[function]getUserInfo [cause]success', (done) => {
    const id = '_id';
    request(app)
      .get('/mock/user')
      .set('Authorization', `Bearer ${token}`)
      .then((response) => {
        expect(JSON.parse(response.text).status).toBe('success');
        expect(JSON.parse(response.text).result).not.toEqual(null);
        expect(JSON.parse(response.text).result.agentCode).not.toEqual(null);
        expect(JSON.parse(response.text).result[id].substring(0, 2)).toEqual('U_');
        done();
      });
  });

  it('[function]getUserInfo(UX) [cause]success', (done) => {
    const id = '_id';
    request(app)
      .get('/mock/user?type=agentUX')
      .set('Authorization', `Bearer ${token}`)
      .then((response) => {
        expect(JSON.parse(response.text).status).toBe('success');
        expect(JSON.parse(response.text).result).not.toEqual(null);
        expect(JSON.parse(response.text).result.agentCode).not.toEqual(null);
        expect(JSON.parse(response.text).result[id].substring(0, 3)).toEqual('UX_');
        done();
      });
  });

  it('[function]getUserInfo [cause]Unauthenticated (no token)', (done) => {
    request(app)
      .get('/mock/user')
      .then((response) => {
        expect(response.status).toBe(401);
        done();
      });
  });

  it('[function]getUserInfo(UX) [cause]Unauthenticated (no token)', (done) => {
    request(app)
      .get('/mock/user?type=agentUX')
      .then((response) => {
        expect(response.status).toBe(401);
        done();
      });
  });

  it('[function]getUserInfo [cause]Unauthenticated (wrong token)', (done) => {
    request(app)
      .get('/mock/user')
      .then((response) => {
        expect(response.status).toBe(401);
        done();
      });
  });

  it('[function]getUserInfo(UX) [cause]Unauthenticated (wrong token)', (done) => {
    request(app)
      .get('/mock/user?type=agentUX')
      .set('Authorization', `Bearer ${faker.random.words()}`)
      .then((response) => {
        if (response.status === 400) {
          expect(response.status).toBe(400);
        } else {
          expect(response.status).toBe(401);
        }
        done();
      });
  });

  it('[function]getUserInfo [cause]error (type != UX)', (done) => {
    request(app)
      .get(`/mock/user?type=${faker.random.words()}`)
      .set('Authorization', `Bearer ${token}`)
      .then((response) => {
        expect(response.status).toBe(400);
        expect(JSON.parse(response.text).status).toBe('error');
        expect(JSON.parse(response.text).message).toBe('type error');
        done();
      });
  });

  it('[function] testToken [cause] Lack of token', (done) => {
    request(app)
      .post('/mock/testToken')
      .then((response) => {
        expect(response.status).toBe(401);
        done();
      });
  });

  it('[function] testToken [cause] Bearer without token', (done) => {
    request(app)
      .post('/mock/testToken')
      .set('Authorization', 'Bearer')
      .then((response) => {
        expect(response.status).toBe(400);
        done();
      });
  });

  it('[function] testToken [cause] Bearer(space) without token', (done) => {
    request(app)
      .post('/mock/testToken')
      .set('Authorization', 'Bearer ')
      .then((response) => {
        expect(response.status).toBe(401);
        done();
      });
  });

  it('[function] testToken [cause] Invalid Authorization header', (done) => {
    request(app)
      .post('/mock/testToken')
      .set('Authorization', faker.random.uuid())
      .then((response) => {
        expect(response.status).toBe(400);
        done();
      });
  });

  it('[function] testToken [cause] Invalid token', (done) => {
    request(app)
      .post('/mock/testToken')
      .set('Authorization', `Bearer ${faker.random.uuid()}`)
      .then((response) => {
        expect(response.status).toBe(401);
        done();
      });
  });

  it('[function] testToken [cause] success with agentId === "A1"', (done) => {
    request(app)
      .post('/mock/token')
      .send({ grant_type: faker.random.word() })
      .send({ code: 'aaa1' })
      .send({ redirect_uri: 'easemobile://' })
      .then((tokenResponse) => {
        request(app)
          .post('/mock/testToken')
          .set('Authorization', `Bearer ${JSON.parse(tokenResponse.text).access_token}`)
          .then((response) => {
            expect(response.status).toBe(200);
            done();
          });
      });
  });

  it('[function] testToken [cause] success with agentId === "A2"', (done) => {
    request(app)
      .post('/mock/token')
      .send({ grant_type: faker.random.word() })
      .send({ code: 'aaa2' })
      .send({ redirect_uri: 'easemobile://' })
      .then((tokenResponse) => {
        request(app)
          .post('/mock/testToken')
          .set('Authorization', `Bearer ${JSON.parse(tokenResponse.text).access_token}`)
          .then((response) => {
            expect(response.status).toBe(200);
            done();
          });
      });
  });

  it('[function] testToken [cause] success with agentId === "A3"', (done) => {
    request(app)
      .post('/mock/token')
      .send({ grant_type: faker.random.word() })
      .send({ code: 'aaa3' })
      .send({ redirect_uri: 'easemobile://' })
      .then((tokenResponse) => {
        request(app)
          .post('/mock/testToken')
          .set('Authorization', `Bearer ${JSON.parse(tokenResponse.text).access_token}`)
          .then((response) => {
            expect(response.status).toBe(200);
            done();
          });
      });
  });

  it('[function] testToken [cause] success with agentId === "A4"', (done) => {
    request(app)
      .post('/mock/token')
      .send({ grant_type: faker.random.word() })
      .send({ code: 'aaa4' })
      .send({ redirect_uri: 'easemobile://' })
      .then((tokenResponse) => {
        request(app)
          .post('/mock/testToken')
          .set('Authorization', `Bearer ${JSON.parse(tokenResponse.text).access_token}`)
          .then((response) => {
            expect(response.status).toBe(200);
            done();
          });
      });
  });
});
