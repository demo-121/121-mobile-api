const request = require('supertest');
const faker = require('faker');

const app = require('../../app');
const config = require('../../config/dev.json');

let installationId = '';

describe('Auth', () => {
  const authCode = faker.random.arrayElement(['aaa1', 'aaa2', 'aaa3']);
  let agentId = '';
  switch (authCode) {
    case 'aaa1':
      agentId = 'AG90001D';
      break;
    case 'aaa2':
      agentId = 'AG90003M';
      break;
    case 'aaa3':
      agentId = 'AG90006A';
      break;
    case 'aaa4':
      agentId = 'S2195576J';
      break;
    default:
      break;
  }

  const requestFakeToken = () => new Promise((resolve) => {
    request(app)
      .post('/mock/token')
      .send({ grant_type: faker.random.word() })
      .send({ code: authCode })
      .send({ redirect_uri: 'easemobile://' })
      .then((response) => {
        resolve(JSON.parse(response.text).access_token);
      });
  });

  let token = '';

  beforeAll(() => new Promise((resolve) => {
    requestFakeToken()
      .then((callbackToken) => {
        token = callbackToken;
        resolve(token);
      });
  }));

  it('[function]activateUser [case]success', (done) => {
    request(app)
      .post('/auth/activate')
      .set('Authorization', `Bearer ${token}`)
      .send({ device_model: faker.random.words() })
      .send({ device_id: faker.random.words() })
      .send({ versionNumber: '0.0.1' })
      .send({ buildNumber: '001' })
      .then((response) => {
        expect(response.status).toBe(200);
        expect(response.header['content-type']).toBe('application/json; charset=utf-8');
        expect(JSON.parse(response.text).status).toBe('success');
        expect(JSON.parse(response.text).result.user_id).toBe(agentId);
        installationId = JSON.parse(response.text).result.installation_id;
        done();
      });
  });

  it('[function]activateUser [case]Unauthenticated (no token)', (done) => {
    request(app)
      .post('/auth/activate')
      .send({ device_model: faker.random.words() })
      .send({ device_id: faker.random.words() })
      .send({ versionNumber: '0.0.1' })
      .send({ buildNumber: '001' })
      .then((response) => {
        expect(response.status).toBe(401);
        done();
      });
  });

  it('[function]activateUser [case]Unauthenticated (wrong token)', (done) => {
    request(app)
      .post('/auth/activate')
      .set('Authorization', `Bearer ${faker.random.words()}`)
      .send({ device_model: faker.random.words() })
      .send({ device_id: faker.random.words() })
      .send({ versionNumber: '0.0.1' })
      .send({ buildNumber: '001' })
      .then((response) => {
        if (response.status === 400) {
          expect(response.status).toBe(400);
        } else {
          expect(response.status).toBe(401);
        }
        done();
      });
  });

  it('[function]activateUser [case]error [detail]Lack of device_model', (done) => {
    request(app)
      .post('/auth/activate')
      .set('Authorization', `Bearer ${token}`)
      .send({ device_id: faker.random.words() })
      .send({ versionNumber: '0.0.1' })
      .send({ buildNumber: '001' })
      .then((response) => {
        expect(response.status)
          .toBe(400);
        expect(response.header['content-type'])
          .toBe('application/json; charset=utf-8');
        expect(JSON.parse(response.text).status).toBe('error');
        expect(JSON.parse(response.text).message).toEqual({
          error: 'device_model, device_id, versionNumber, buildNumber required.',
        });
        done();
      });
  });

  it('[function]activateUser [case]error [detail]Lack of device id', (done) => {
    request(app)
      .post('/auth/activate')
      .set('Authorization', `Bearer ${token}`)
      .send({ device_model: faker.random.words() })
      .send({ versionNumber: '0.0.1' })
      .send({ buildNumber: '001' })
      .then((response) => {
        expect(response.status)
          .toBe(400);
        expect(response.header['content-type'])
          .toBe('application/json; charset=utf-8');
        expect(JSON.parse(response.text).status).toBe('error');
        expect(JSON.parse(response.text).message).toEqual({
          error: 'device_model, device_id, versionNumber, buildNumber required.',
        });
        done();
      });
  });

  it('[function]activateUser [case]error [detail]Lack of versionNumber', (done) => {
    request(app)
      .post('/auth/activate')
      .set('Authorization', `Bearer ${token}`)
      .send({ device_model: faker.random.words() })
      .send({ device_id: faker.random.words() })
      .send({ buildNumber: '001' })
      .then((response) => {
        expect(response.status)
          .toBe(400);
        expect(response.header['content-type'])
          .toBe('application/json; charset=utf-8');
        expect(JSON.parse(response.text).status).toBe('error');
        expect(JSON.parse(response.text).message).toEqual({
          error: 'device_model, device_id, versionNumber, buildNumber required.',
        });
        done();
      });
  });

  it('[function]activateUser [case]error [detail]Lack of buildNumber', (done) => {
    request(app)
      .post('/auth/activate')
      .set('Authorization', `Bearer ${token}`)
      .send({ device_model: faker.random.words() })
      .send({ device_id: faker.random.words() })
      .send({ versionNumber: '0.0.1' })
      .then((response) => {
        expect(response.status)
          .toBe(400);
        expect(response.header['content-type'])
          .toBe('application/json; charset=utf-8');
        expect(JSON.parse(response.text).status).toBe('error');
        expect(JSON.parse(response.text).message).toEqual({
          error: 'device_model, device_id, versionNumber, buildNumber required.',
        });
        done();
      });
  });

  it('[function]storeEncryptionKey [case]success', (done) => {
    request(app)
      .put('/auth/encryptionKey')
      .set('Authorization', `Bearer ${token}`)
      .send({ master_key: faker.random.uuid() })
      .send({ installation_id: installationId })
      .then((response) => {
        expect(response.status)
          .toBe(200);
        expect(response.header['content-type'])
          .toBe('application/json; charset=utf-8');
        expect(JSON.parse(response.text))
          .toEqual({
            status: 'success',
            result: {},
          });
        done();
      });
  });

  it('[function]storeEncryptionKey [case]Unauthenticated (no token)', (done) => {
    request(app)
      .put('/auth/encryptionKey')
      .send({ master_key: faker.random.uuid() })
      .send({ installation_id: installationId })
      .then((response) => {
        expect(response.status)
          .toBe(401);
        done();
      });
  });

  it('[function]storeEncryptionKey [case]Unauthenticated (wrong token)', (done) => {
    request(app)
      .put('/auth/encryptionKey')
      .set('Authorization', `Bearer ${faker.random.words()}`)
      .send({ master_key: faker.random.uuid() })
      .send({ installation_id: installationId })
      .then((response) => {
        if (response.status === 400) {
          expect(response.status).toBe(400);
        } else {
          expect(response.status).toBe(401);
        }
        done();
      });
  });

  it('[function]storeEncryptionKey [case]Lack of master key', (done) => {
    request(app)
      .put('/auth/encryptionKey')
      .set('Authorization', `Bearer ${token}`)
      .send({ installation_id: installationId })
      .then((response) => {
        expect(response.status)
          .toBe(400);
        expect(response.header['content-type'])
          .toBe('application/json; charset=utf-8');
        expect(JSON.parse(response.text))
          .toEqual({
            status: 'error',
            message: {
              message: 'master_key and installation_id required.',
            },
          });
        done();
      });
  });

  it('[function]storeEncryptionKey [case]Lack of installation id', (done) => {
    request(app)
      .put('/auth/encryptionKey')
      .set('Authorization', `Bearer ${token}`)
      .send({ master_key: faker.random.uuid() })
      .then((response) => {
        expect(response.status)
          .toBe(400);
        expect(response.header['content-type'])
          .toBe('application/json; charset=utf-8');
        expect(JSON.parse(response.text))
          .toEqual({
            status: 'error',
            message: {
              message: 'master_key and installation_id required.',
            },
          });
        done();
      });
  });

  it('[function]login [case]success', (done) => {
    request(app)
      .post('/auth/login')
      .set('Authorization', `Bearer ${token}`)
      .send({ installation_id: installationId })
      .then((response) => {
        expect(response.status)
          .toBe(200);
        expect(response.header['content-type'])
          .toBe('application/json; charset=utf-8');
        expect(JSON.parse(response.text).status).toBe('success');
        done();
      });
  });

  it('[function]login [case]Unauthenticated (no token)', (done) => {
    request(app)
      .post('/auth/login')
      .send({ installation_id: installationId })
      .then((response) => {
        expect(response.status)
          .toBe(401);
        done();
      });
  });

  it('[function]login [case]Unauthenticated (wrong token)', (done) => {
    request(app)
      .post('/auth/login')
      .set('Authorization', `Bearer ${faker.random.words()}`)
      .send({ installation_id: installationId })
      .then((response) => {
        if (response.status === 400) {
          expect(response.status).toBe(400);
        } else {
          expect(response.status).toBe(401);
        }
        done();
      });
  });

  it('[function]login [case]Lack of installation id', (done) => {
    request(app)
      .post('/auth/login')
      .set('Authorization', `Bearer ${token}`)
      .then((response) => {
        expect(response.status).toBe(400);
        expect(JSON.parse(response.text).status).toBe('error');
        expect(JSON.parse(response.text).message).toEqual({ message: 'installation_id required' });
        done();
      });
  });

  it('[function]login [case]Activation without enter password', (done) => {
    const fakeAuthCode = 'aaa4';
    request(app)
      .post('/mock/token')
      .send({ grant_type: faker.random.word() })
      .send({ code: fakeAuthCode })
      .send({ redirect_uri: 'easemobile://' })
      .then((tokenResponse) => {
        request(app)
          .post('/auth/activate')
          .set('Authorization', `Bearer ${JSON.parse(tokenResponse.text).access_token}`)
          .send({ device_model: faker.random.words() })
          .send({ device_id: faker.random.words() })
          .send({ versionNumber: '0.0.1' })
          .send({ buildNumber: '001' })
          .then((response) => {
            const installationID = JSON.parse(response.text).result.installation_id;
            request(app)
              .post('/auth/login')
              .set('Authorization', `Bearer ${token}`)
              .send({ installation_id: installationID })
              .then((loginResponse) => {
                expect(loginResponse.status).toBe(400);
                expect(JSON.parse(loginResponse.text).status).toBe('error');
                expect(JSON.parse(loginResponse.text).message).toBe('Master key not exist. Plz check if agent fully activation successfully before (activation + set offline password).');
                done();
              });
          });
      });
  });

  // IS_CHECK_VERSION_NUMBER = true, IS_CHECK_BUILD_NUMBER = true
  it('[function]check [case]success (IS_CHECK_VERSION_NUMBER = true, IS_CHECK_BUILD_NUMBER = true, version number > target version number, build number > target build number)', (done) => {
    request(app)
      .post('/auth/check')
      .set('Authorization', `Bearer ${token}`)
      .send({ versionNumber: '1.2.0' })
      .send({
        buildNumber: faker.random.number({
          min: parseInt(config.TARGET_BUILD_NUMBER, 10) + 1,
        }),
      })
      .send({ installationId })
      .then((response) => {
        expect(response.status).toBe(200);
        expect(JSON.parse(response.text).status).toBe('success');
        expect(JSON.parse(response.text).result.isNeedUpgrade).toBe(false);
        done();
      });
  });

  it('[function]check [case]success (IS_CHECK_VERSION_NUMBER = true, IS_CHECK_BUILD_NUMBER = true, version number > target version number, build number = target build number)', (done) => {
    request(app)
      .post('/auth/check')
      .set('Authorization', `Bearer ${token}`)
      .send({ versionNumber: '1.2.0' })
      .send({ buildNumber: config.TARGET_BUILD_NUMBER })
      .send({ installationId })
      .then((response) => {
        expect(response.status).toBe(200);
        expect(JSON.parse(response.text).status).toBe('success');
        expect(JSON.parse(response.text).result.isNeedUpgrade).toBe(false);
        done();
      });
  });

  it('[function]check [case]success (IS_CHECK_VERSION_NUMBER = true, IS_CHECK_BUILD_NUMBER = true, version number > target version number, build number < target build number)', (done) => {
    request(app)
      .post('/auth/check')
      .set('Authorization', `Bearer ${token}`)
      .send({ versionNumber: '1.2.0' })
      .send({
        buildNumber: faker.random.number({
          max: parseInt(config.TARGET_BUILD_NUMBER, 10) - 1,
        }),
      })
      .send({ installationId })
      .then((response) => {
        expect(response.status).toBe(200);
        expect(JSON.parse(response.text).status).toBe('success');
        expect(JSON.parse(response.text).result.isNeedUpgrade).toBe(false);
        done();
      });
  });

  it('[function]check [case]success (IS_CHECK_VERSION_NUMBER = true, IS_CHECK_BUILD_NUMBER = true, version number = target version number, build number > target build number)', (done) => {
    request(app)
      .post('/auth/check')
      .set('Authorization', `Bearer ${token}`)
      .send({ versionNumber: config.TARGET_VERSION_NUMBER })
      .send({
        buildNumber: faker.random.number({ min: parseInt(config.TARGET_BUILD_NUMBER, 10) + 1 }),
      })
      .send({ installationId })
      .then((response) => {
        expect(response.status).toBe(200);
        expect(JSON.parse(response.text).status).toBe('success');
        expect(JSON.parse(response.text).result.isNeedUpgrade).toBe(false);
        done();
      });
  });

  it('[function]check [case]success (IS_CHECK_VERSION_NUMBER = true, IS_CHECK_BUILD_NUMBER = true, version number = target version number, build number = target build number)', (done) => {
    request(app)
      .post('/auth/check')
      .set('Authorization', `Bearer ${token}`)
      .send({ versionNumber: config.TARGET_VERSION_NUMBER })
      .send({ buildNumber: config.TARGET_BUILD_NUMBER })
      .send({ installationId })
      .then((response) => {
        expect(response.status).toBe(200);
        expect(JSON.parse(response.text).status).toBe('success');
        expect(JSON.parse(response.text).result.isNeedUpgrade).toBe(false);
        done();
      });
  });

  it('[function]check [case]success (IS_CHECK_VERSION_NUMBER = true, IS_CHECK_BUILD_NUMBER = true, version number = target version number, build number < target build number)', (done) => {
    request(app)
      .post('/auth/check')
      .set('Authorization', `Bearer ${token}`)
      .send({ versionNumber: config.TARGET_VERSION_NUMBER })
      .send({
        buildNumber: faker.random.number({
          max: parseInt(config.TARGET_BUILD_NUMBER, 10) - 1,
        }),
      })
      .send({ installationId })
      .then((response) => {
        expect(response.status).toBe(200);
        expect(JSON.parse(response.text).status).toBe('success');
        expect(JSON.parse(response.text).result.isNeedUpgrade).toBe(true);
        done();
      });
  });

  it('[function]check [case]success (IS_CHECK_VERSION_NUMBER = true, IS_CHECK_BUILD_NUMBER = true, version number < target version number, build number > target build number)', (done) => {
    request(app)
      .post('/auth/check')
      .set('Authorization', `Bearer ${token}`)
      .send({ versionNumber: '1.0.0' })
      .send({
        buildNumber: faker.random.number({
          min: parseInt(config.TARGET_BUILD_NUMBER, 10) + 1,
        }),
      })
      .send({ installationId })
      .then((response) => {
        expect(response.status).toBe(200);
        expect(JSON.parse(response.text).status).toBe('success');
        expect(JSON.parse(response.text).result.isNeedUpgrade).toBe(true);
        done();
      });
  });

  it('[function]check [case]success (IS_CHECK_VERSION_NUMBER = true, IS_CHECK_BUILD_NUMBER = true, version number < target version number, build number = target build number)', (done) => {
    request(app)
      .post('/auth/check')
      .set('Authorization', `Bearer ${token}`)
      .send({ versionNumber: '1.0.0' })
      .send({ buildNumber: config.TARGET_BUILD_NUMBER })
      .send({ installationId })
      .then((response) => {
        expect(response.status).toBe(200);
        expect(JSON.parse(response.text).status).toBe('success');
        expect(JSON.parse(response.text).result.isNeedUpgrade).toBe(true);
        done();
      });
  });

  it('[function]check [case]success (IS_CHECK_VERSION_NUMBER = true, IS_CHECK_BUILD_NUMBER = true, version number < target version number, build number < target build number)', (done) => {
    request(app)
      .post('/auth/check')
      .set('Authorization', `Bearer ${token}`)
      .send({ versionNumber: '1.0.0' })
      .send({
        buildNumber: faker.random.number({
          max: parseInt(config.TARGET_BUILD_NUMBER, 10) - 1,
        }),
      })
      .send({ installationId })
      .then((response) => {
        expect(response.status).toBe(200);
        expect(JSON.parse(response.text).status).toBe('success');
        expect(JSON.parse(response.text).result.isNeedUpgrade).toBe(true);
        done();
      });
  });

  // IS_CHECK_VERSION_NUMBER = true, IS_CHECK_BUILD_NUMBER = false
  // it('[function]check [case]success (IS_CHECK_VERSION_NUMBER = true, IS_CHECK_BUILD_NUMBER = false, version number > target version number, build number > target build number)', (done) => {
  //   config.IS_CHECK_VERSION_NUMBER = true;
  //   config.IS_CHECK_BUILD_NUMBER = false;
  //   request(app)
  //     .post('/auth/check')
  //     .set('Authorization', `Bearer ${token}`)
  //     .send({ versionNumber: '1.2.0' })
  //     .send({
  //       buildNumber: faker.random.number({
  //         min: parseInt(config.TARGET_BUILD_NUMBER, 10) + 1,
  //       }),
  //     })
  //     .send({ installationId })
  //     .then((response) => {
  //       expect(response.status).toBe(200);
  //       expect(JSON.parse(response.text).status).toBe('success');
  //       expect(JSON.parse(response.text).result.isNeedUpgrade).toBe(false);
  //       done();
  //     });
  // });

  // it('[function]check [case]success (IS_CHECK_VERSION_NUMBER = true, IS_CHECK_BUILD_NUMBER = false, version number > target version number, build number = target build number)', (done) => {
  //   config.IS_CHECK_VERSION_NUMBER = true;
  //   config.IS_CHECK_BUILD_NUMBER = false;
  //   request(app)
  //     .post('/auth/check')
  //     .set('Authorization', `Bearer ${token}`)
  //     .send({ versionNumber: '1.2.0' })
  //     .send({ buildNumber: config.TARGET_BUILD_NUMBER })
  //     .send({ installationId })
  //     .then((response) => {
  //       expect(response.status).toBe(200);
  //       expect(JSON.parse(response.text).status).toBe('success');
  //       expect(JSON.parse(response.text).result.isNeedUpgrade).toBe(false);
  //       done();
  //     });
  // });

  // it('[function]check [case]success (IS_CHECK_VERSION_NUMBER = true, IS_CHECK_BUILD_NUMBER = false, version number > target version number, build number < target build number)', (done) => {
  //   config.IS_CHECK_VERSION_NUMBER = true;
  //   config.IS_CHECK_BUILD_NUMBER = false;
  //   request(app)
  //     .post('/auth/check')
  //     .set('Authorization', `Bearer ${token}`)
  //     .send({ versionNumber: '1.2.0' })
  //     .send({
  //       buildNumber: faker.random.number({
  //         max: parseInt(config.TARGET_BUILD_NUMBER, 10) - 1,
  //       }),
  //     })
  //     .send({ installationId })
  //     .then((response) => {
  //       expect(response.status).toBe(200);
  //       expect(JSON.parse(response.text).status).toBe('success');
  //       expect(JSON.parse(response.text).result.isNeedUpgrade).toBe(false);
  //       done();
  //     });
  // });

  // it('[function]check [case]success (IS_CHECK_VERSION_NUMBER = true, IS_CHECK_BUILD_NUMBER = false, version number = target version number, build number > target build number)', (done) => {
  //   config.IS_CHECK_VERSION_NUMBER = true;
  //   config.IS_CHECK_BUILD_NUMBER = false;
  //   request(app)
  //     .post('/auth/check')
  //     .set('Authorization', `Bearer ${token}`)
  //     .send({ versionNumber: config.TARGET_VERSION_NUMBER })
  //     .send({
  //       buildNumber: faker.random.number({ min: parseInt(config.TARGET_BUILD_NUMBER, 10) + 1 }),
  //     })
  //     .send({ installationId })
  //     .then((response) => {
  //       expect(response.status).toBe(200);
  //       expect(JSON.parse(response.text).status).toBe('success');
  //       expect(JSON.parse(response.text).result.isNeedUpgrade).toBe(false);
  //       done();
  //     });
  // });

  // it('[function]check [case]success (IS_CHECK_VERSION_NUMBER = true, IS_CHECK_BUILD_NUMBER = false, version number = target version number, build number = target build number)', (done) => {
  //   config.IS_CHECK_VERSION_NUMBER = true;
  //   config.IS_CHECK_BUILD_NUMBER = false;
  //   request(app)
  //     .post('/auth/check')
  //     .set('Authorization', `Bearer ${token}`)
  //     .send({ versionNumber: config.TARGET_VERSION_NUMBER })
  //     .send({ buildNumber: config.TARGET_BUILD_NUMBER })
  //     .send({ installationId })
  //     .then((response) => {
  //       expect(response.status).toBe(200);
  //       expect(JSON.parse(response.text).status).toBe('success');
  //       expect(JSON.parse(response.text).result.isNeedUpgrade).toBe(false);
  //       done();
  //     });
  // });

  // it('[function]check [case]success (IS_CHECK_VERSION_NUMBER = true, IS_CHECK_BUILD_NUMBER = false, version number = target version number, build number < target build number)', (done) => {
  //   config.IS_CHECK_VERSION_NUMBER = true;
  //   config.IS_CHECK_BUILD_NUMBER = false;
  //   request(app)
  //     .post('/auth/check')
  //     .set('Authorization', `Bearer ${token}`)
  //     .send({ versionNumber: config.TARGET_VERSION_NUMBER })
  //     .send({
  //       buildNumber: faker.random.number({
  //         max: parseInt(config.TARGET_BUILD_NUMBER, 10) - 1,
  //       }),
  //     })
  //     .send({ installationId })
  //     .then((response) => {
  //       expect(response.status).toBe(200);
  //       expect(JSON.parse(response.text).status).toBe('success');
  //       expect(JSON.parse(response.text).result.isNeedUpgrade).toBe(false);
  //       done();
  //     });
  // });

  // it('[function]check [case]success (IS_CHECK_VERSION_NUMBER = true, IS_CHECK_BUILD_NUMBER = false, version number < target version number, build number > target build number)', (done) => {
  //   config.IS_CHECK_VERSION_NUMBER = true;
  //   config.IS_CHECK_BUILD_NUMBER = false;
  //   request(app)
  //     .post('/auth/check')
  //     .set('Authorization', `Bearer ${token}`)
  //     .send({ versionNumber: '1.0.0' })
  //     .send({
  //       buildNumber: faker.random.number({
  //         min: parseInt(config.TARGET_BUILD_NUMBER, 10) + 1,
  //       }),
  //     })
  //     .send({ installationId })
  //     .then((response) => {
  //       expect(response.status).toBe(200);
  //       expect(JSON.parse(response.text).status).toBe('success');
  //       expect(JSON.parse(response.text).result.isNeedUpgrade).toBe(true);
  //       done();
  //     });
  // });

  // it('[function]check [case]success (IS_CHECK_VERSION_NUMBER = true, IS_CHECK_BUILD_NUMBER = false, version number < target version number, build number = target build number)', (done) => {
  //   config.IS_CHECK_VERSION_NUMBER = true;
  //   config.IS_CHECK_BUILD_NUMBER = false;
  //   request(app)
  //     .post('/auth/check')
  //     .set('Authorization', `Bearer ${token}`)
  //     .send({ versionNumber: '1.0.0' })
  //     .send({ buildNumber: config.TARGET_BUILD_NUMBER })
  //     .send({ installationId })
  //     .then((response) => {
  //       expect(response.status).toBe(200);
  //       expect(JSON.parse(response.text).status).toBe('success');
  //       expect(JSON.parse(response.text).result.isNeedUpgrade).toBe(true);
  //       done();
  //     });
  // });

  // it('[function]check [case]success (IS_CHECK_VERSION_NUMBER = true, IS_CHECK_BUILD_NUMBER = false, version number < target version number, build number < target build number)', (done) => {
  //   config.IS_CHECK_VERSION_NUMBER = true;
  //   config.IS_CHECK_BUILD_NUMBER = false;
  //   request(app)
  //     .post('/auth/check')
  //     .set('Authorization', `Bearer ${token}`)
  //     .send({ versionNumber: '1.0.0' })
  //     .send({
  //       buildNumber: faker.random.number({
  //         max: parseInt(config.TARGET_BUILD_NUMBER, 10) - 1,
  //       }),
  //     })
  //     .send({ installationId })
  //     .then((response) => {
  //       expect(response.status).toBe(200);
  //       expect(JSON.parse(response.text).status).toBe('success');
  //       expect(JSON.parse(response.text).result.isNeedUpgrade).toBe(true);
  //       done();
  //     });
  // });

  // IS_CHECK_VERSION_NUMBER = false, IS_CHECK_BUILD_NUMBER = true

  // IS_CHECK_VERSION_NUMBER = false, IS_CHECK_BUILD_NUMBER = false

  it('[function]check [case]Lack of version number', (done) => {
    request(app)
      .post('/auth/check')
      .set('Authorization', `Bearer ${token}`)
      .send({ buildNumber: '002' })
      .send({ installationId })
      .then((response) => {
        expect(response.status).toBe(400);
        expect(JSON.parse(response.text).status).toBe('error');
        expect(JSON.parse(response.text).message).toBe('version number, build number and installation id are required.');
        done();
      });
  });

  it('[function]check [case]Version number is not follow the format (words)', (done) => {
    request(app)
      .post('/auth/check')
      .set('Authorization', `Bearer ${token}`)
      .send({ versionNumber: faker.random.words() })
      .send({ buildNumber: '002' })
      .send({ installationId })
      .then((response) => {
        expect(response.status).toBe(400);
        expect(JSON.parse(response.text).status).toBe('error');
        expect(JSON.parse(response.text).message).toBe('versionNumber is not follow the format');
        done();
      });
  });

  it('[function]check [case]Lack of build number', (done) => {
    request(app)
      .post('/auth/check')
      .set('Authorization', `Bearer ${token}`)
      .send({ versionNumber: '1.0.0' })
      .send({ installationId })
      .then((response) => {
        expect(response.status).toBe(400);
        expect(JSON.parse(response.text).status).toBe('error');
        expect(JSON.parse(response.text).message).toBe('version number, build number and installation id are required.');
        done();
      });
  });

  it('[function]check [case]Build number is not follow the format (words)', (done) => {
    request(app)
      .post('/auth/check')
      .set('Authorization', `Bearer ${token}`)
      .send({ versionNumber: '1.0.0' })
      .send({ buildNumber: faker.random.words() })
      .send({ installationId })
      .then((response) => {
        expect(response.status).toBe(400);
        expect(JSON.parse(response.text).status).toBe('error');
        expect(JSON.parse(response.text).message).toBe('buildNumber is not follow the format');
        done();
      });
  });

  it('[function]check [case]Lack of installation id', (done) => {
    request(app)
      .post('/auth/check')
      .set('Authorization', `Bearer ${token}`)
      .send({ versionNumber: '1.0.0' })
      .send({ buildNumber: '002' })
      .then((response) => {
        expect(response.status).toBe(400);
        expect(JSON.parse(response.text).status).toBe('error');
        expect(JSON.parse(response.text).message).toBe('version number, build number and installation id are required.');
        done();
      });
  });
});
