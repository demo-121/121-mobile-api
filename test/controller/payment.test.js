const request = require('supertest');
const faker = require('faker');

const app = require('../../app');

describe('Auth', () => {
  const _code = faker.random.arrayElement(['aaa1', 'aaa2', 'aaa3', 'aaa4']);

  // it("[function]getPaymentStatus [case]shield success and PaymentStatus == true", (done) => {
  //   request(app)
  //     .post('/mock/token')
  //     .send({ grant_type: faker.random.word() })
  //     .send({ code: _code })
  //     .send({ redirect_uri: 'easemobile://' })
  //     .then((response) => {
  //       request(app)
  //       .get('/payment/status/SA001002-00177')
  //       .set('Authorization', `Bearer ${JSON.parse(response.text).access_token}`)
  //       .then((response) => {
  //         expect(response.status).toBe(200);
  //         expect(JSON.parse(response.text).status).toBe('success');
  //         expect(JSON.parse(response.text).result.isSuccess).toBe(true);
  //         done();
  //       })
  //     })
  // })

  // it("[function]getPaymentStatus [case]non-shield success and PaymentStatus == true", (done) => {
  //   request(app)
  //     .post('/mock/token')
  //     .send({ grant_type: faker.random.word() })
  //     .send({ code: _code })
  //     .send({ redirect_uri: 'easemobile://' })
  //     .then((response) => {
  //       request(app)
  //         .get('/payment/status/NB001001-01588')
  //         .set('Authorization', `Bearer ${JSON.parse(response.text).access_token}`)
  //         .then((response) => {
  //           expect(response.status).toBe(200);
  //           expect(JSON.parse(response.text).status).toBe('success');
  //           expect(JSON.parse(response.text).result.isSuccess).toBe(true);
  //           done();
  //         })
  //     })
  // });

  // it("[function]getPaymentStatus [case]non-shield success and PaymentStatus == false", (done) => {
  //   request(app)
  //     .post('/mock/token')
  //     .send({ grant_type: faker.random.word() })
  //     .send({ code: _code })
  //     .send({ redirect_uri: 'easemobile://' })
  //     .then((response) => {
  //       request(app)
  //         .get('/payment/status/NB001001-00032')
  //         .set('Authorization', `Bearer ${JSON.parse(response.text).access_token}`)
  //         .then((response) => {
  //           expect(response.status).toBe(200);
  //           expect(JSON.parse(response.text).status).toBe('success');
  //           expect(JSON.parse(response.text).result.isSuccess).toBe(false);
  //           done();
  //         })
  //     })
  // });

  it("1+1=2", (done) => {
    expect(1+ 1).toBe(2);
    done();
  })

  // it("[function]getPaymentStatus [case]application document not exists", (done) => {
  //   request(app)
  //     .post('/mock/token')
  //     .send({ grant_type: faker.random.word() })
  //     .send({ code: _code })
  //     .send({ redirect_uri: 'easemobile://' })
  //     .then((response) => {
  //       const type = faker.random.arrayElement(['NB', 'SA']);
  //       const number = faker.random.number();
  //       request(app)
  //         .get(`/payment/status/${type}${number}`)
  //         .set('Authorization', `Bearer ${JSON.parse(response.text).access_token}`)
  //         .then((response) => {
  //           expect(response.status).toBe(400);
  //           expect(JSON.parse(response.text).status).toBe('error');
  //           expect(JSON.parse(response.text).message).toBe(`Doc (${type}${number}) is not exists.`);
  //           done();
  //         })
  //     })
  // });

  // it("[function]getPaymentStatus [case]document id substring(0,2) != NB || SA", (done) => {
  //   request(app)
  //     .post('/mock/token')
  //     .send({ grant_type: faker.random.word() })
  //     .send({ code: _code })
  //     .send({ redirect_uri: 'easemobile://' })
  //     .then((response) => {
  //       request(app)
  //       .get(`/payment/status/${faker.random.word()}`)
  //       .set('Authorization', `Bearer ${JSON.parse(response.text).access_token}`)
  //       .then((response) => {
  //         expect(response.status).toBe(400);
  //         expect(JSON.parse(response.text).status).toBe('error');
  //         expect(JSON.parse(response.text).message).toBe('docId required or docId format error.');
  //         done();
  //       })
  //     })
  // });
});
