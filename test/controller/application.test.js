const faker = require('faker');
const request = require('supertest');

const app = require('../../app');

describe('Application', () => {
  const _code = faker.random.arrayElement(['aaa1', 'aaa2', 'aaa3', 'aaa4']);
  const numOfShield = faker.random.number({ min: 1, max: 5 });
  const numOfNonShield = faker.random.number({ min: 1, max: 5 });

  it("1+1=2", (done) => {
    expect(1+ 1).toBe(2);
    done();
  })
  
  // it('[function]getPolicyNumber [cause]success', (done) => {
  //   request(app)
  //     .post('/mock/token')
  //     .send({ grant_type: faker.random.word() })
  //     .send({ code: _code })
  //     .send({ redirect_uri: 'easemobile://' })
  //     .then((response) => {
  //       request(app)
  //         .post('/application/getPolicyNumber')
  //         .set('Authorization', `Bearer ${JSON.parse(response.text).access_token}`)
  //         .send(`numOfNonShield=${numOfNonShield}`)
  //         .send(`numOfShield=${numOfShield}`)
  //         .then((response) => {
  //           expect(response.status).toBe(200);
  //           expect(JSON.parse(response.text).result.shieldPolicyNum.length).toBe(numOfShield);
  //           expect(JSON.parse(response.text).result.nonShieldPolicyNum.length).toBe(numOfNonShield);
  //           done();
  //         });
  //     });
  // });

  // it('[function]getPolicyNumber [cause]Unauthentication', (done) => {
  //   request(app)
  //     .post('/application/getPolicyNumber')
  //     .send(`numOfNonShield=${faker.random.number({ min: 1, max: 10 })}`)
  //     .send(`numOfShield=${faker.random.number({ min: 1, max: 10 })}`)
  //     .then((response) => {
  //       expect(response.status).toBe(401);
  //       done();
  //     });
  // });

  // it('[function]getPolicyNumber [cause]numOfShield is not number', (done) => {
  //   request(app)
  //     .post('/mock/token')
  //     .send({ grant_type: faker.random.word() })
  //     .send({ code: _code })
  //     .send({ redirect_uri: 'easemobile://' })
  //     .then((response) => {
  //       request(app)
  //         .post('/application/getPolicyNumber')
  //         .set('Authorization', `Bearer ${JSON.parse(response.text).access_token}`)
  //         .send(`numOfShield=${faker.random.word()}`)
  //         .send(`numOfNonShield=${numOfNonShield}`)
  //         .then((response) => {
  //           expect(response.status).toBe(400);
  //           expect(JSON.parse(response.text).status).toBe('error');
  //           expect(JSON.parse(response.text).message).toBe('numOfShield and numOfNonShield must be integer.');
  //           done();
  //         })
  //     });
  // });

  // it('[function]getPolicyNumber [cause]numOfNonShield is not number', (done) => {
  //   request(app)
  //     .post('/mock/token')
  //     .send({ grant_type: faker.random.word() })
  //     .send({ code: _code })
  //     .send({ redirect_uri: 'easemobile://' })
  //     .then((response) => {
  //       request(app)
  //         .post('/application/getPolicyNumber')
  //         .set('Authorization', `Bearer ${JSON.parse(response.text).access_token}`)
  //         .send(`numOfNonShield=${faker.random.word()}`)
  //         .send(`numOfShield=${numOfShield}`)
  //         .then((response) => {
  //           expect(response.status).toBe(400);
  //           expect(JSON.parse(response.text).status).toBe('error');
  //           expect(JSON.parse(response.text).message).toBe('numOfShield and numOfNonShield must be integer.');
  //           done();
  //         });
  //     });
  // });

  // it('[function]getPolicyNumber [cause]numOfNonShield < 0', (done) => {
  //   request(app)
  //     .post('/mock/token')
  //     .send({ grant_type: faker.random.word() })
  //     .send({ code: _code })
  //     .send({ redirect_uri: 'easemobile://' })
  //     .then((response) => {
  //       request(app)
  //         .post('/application/getPolicyNumber')
  //         .set('Authorization', `Bearer ${JSON.parse(response.text).access_token}`)
  //         .send(`numOfNonShield=${faker.random.number({ min: -99999, max: -1 })}`)
  //         .send(`numOfShield=${numOfShield}`)
  //         .then((response) => {
  //           expect(response.status).toBe(400);
  //           expect(JSON.parse(response.text).status).toBe('error');
  //           expect(JSON.parse(response.text).message).toBe('numOfShield or numOfNonShield must more than 0.');
  //           done();
  //         });
  //     });
  // });

  // it('[function]getPolicyNumber [cause]numOfShield < 0', (done) => {
  //   request(app)
  //     .post('/mock/token')
  //     .send({ grant_type: faker.random.word() })
  //     .send({ code: _code })
  //     .send({ redirect_uri: 'easemobile://' })
  //     .then((response) => {
  //       request(app)
  //         .post('/application/getPolicyNumber')
  //         .set('Authorization', `Bearer ${JSON.parse(response.text).access_token}`)
  //         .send(`numOfNonShield=${numOfNonShield}`)
  //         .send(`numOfShield=${faker.random.number({ min: -99999, max: -1 })}`)
  //         .then((response) => {
  //           expect(response.status).toBe(400);
  //           expect(JSON.parse(response.text).status).toBe('error');
  //           expect(JSON.parse(response.text).message).toBe('numOfShield or numOfNonShield must more than 0.');
  //           done();
  //         });
  //     });
  // });

  // it('[function]getPolicyNumber [cause] get too much nonShield policy number', (done) => {
  //   request(app)
  //     .post('/mock/token')
  //     .send({ grant_type: faker.random.word() })
  //     .send({ code: _code })
  //     .send({ redirect_uri: 'easemobile://' })
  //     .then((response) => {
  //       request(app)
  //         .post('/application/getPolicyNumber')
  //         .set('Authorization', `Bearer ${JSON.parse(response.text).access_token}`)
  //         .send(`numOfNonShield=${faker.random.number({ min: 100, max: 1000 })}`)
  //         .send(`numOfShield=${numOfShield}`)
  //         .then((response) => {
  //           expect(response.status).toBe(500);
  //           expect(JSON.parse(response.text).status).toBe('error');
  //           expect(JSON.parse(response.text).exception.data.message).toBe('Do not have enough policy numbers');
  //           done();
  //         });
  //     });
  // });

  // it('[function]getPolicyNumber [cause]get too much shield policy number', (done) => {
  //   request(app)
  //     .post('/mock/token')
  //     .send({ grant_type: faker.random.word() })
  //     .send({ code: _code })
  //     .send({ redirect_uri: 'easemobile://' })
  //     .then((response) => {
  //       request(app)
  //         .post('/application/getPolicyNumber')
  //         .set('Authorization', `Bearer ${JSON.parse(response.text).access_token}`)
  //         .send(`numOfNonShield=${numOfNonShield}`)
  //         .send(`numOfShield=${faker.random.number({ min: 100, max: 1000 })}`)
  //         .then((response) => {
  //           expect(response.status).toBe(500);
  //           expect(JSON.parse(response.text).status).toBe('error');
  //           expect(JSON.parse(response.text).exception.data.message).toBe('Do not have enough policy numbers');
  //           done();
  //         });
  //     });
  // });

  // it('[function]getPolicyNumber [cause]numOfShield === 0', (done) => {
  //   request(app)
  //     .post('/mock/token')
  //     .send({ grant_type: faker.random.word() })
  //     .send({ code: _code })
  //     .send({ redirect_uri: 'easemobile://' })
  //     .then((response) => {
  //       request(app)
  //         .post('/application/getPolicyNumber')
  //         .set('Authorization', `Bearer ${JSON.parse(response.text).access_token}`)
  //         .send(`numOfNonShield=${numOfNonShield}`)
  //         .send(`numOfShield=0`)
  //         .then((response) => {
  //           expect(response.status).toBe(200);
  //           expect(JSON.parse(response.text).result.shieldPolicyNum.length).toBe(0);
  //           expect(JSON.parse(response.text).result.nonShieldPolicyNum.length).toBe(numOfNonShield);
  //           done();
  //         });
  //     });
  // });

  // it('[function]getPolicyNumber [cause]numOfNonShield === 0', (done) => {
  //   request(app)
  //     .post('/mock/token')
  //     .send({ grant_type: faker.random.word() })
  //     .send({ code: _code })
  //     .send({ redirect_uri: 'easemobile://' })
  //     .then((response) => {
  //       request(app)
  //         .post('/application/getPolicyNumber')
  //         .set('Authorization', `Bearer ${JSON.parse(response.text).access_token}`)
  //         .send(`numOfNonShield=0`)
  //         .send(`numOfShield=${numOfShield}`)
  //         .then((response) => {
  //           expect(response.status).toBe(200);
  //           expect(JSON.parse(response.text).result.shieldPolicyNum.length).toBe(numOfShield);
  //           expect(JSON.parse(response.text).result.nonShieldPolicyNum.length).toBe(0);
  //           done();
  //         });
  //     });
  // });

  // it('[function]getPolicyNumber [cause]numOfNonShield === 0 && numOfShield === 0', (done) => {
  //   request(app)
  //     .post('/mock/token')
  //     .send({ grant_type: faker.random.word() })
  //     .send({ code: _code })
  //     .send({ redirect_uri: 'easemobile://' })
  //     .then((response) => {
  //       request(app)
  //         .post('/application/getPolicyNumber')
  //         .set('Authorization', `Bearer ${JSON.parse(response.text).access_token}`)
  //         .send(`numOfNonShield=0`)
  //         .send(`numOfShield=0`)
  //         .then((response) => {
  //           expect(response.status).toBe(500);
  //           expect(JSON.parse(response.text).status).toBe('error');
  //           done();
  //         });
  //     });
  // });
});
