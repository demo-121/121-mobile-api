const request = require('supertest');
const faker = require('faker');

const app = require('../../app');

describe('Router', () => {
  it("1+1=2", (done) => {
    expect(1+ 1).toBe(2);
    done();
  })

  // it('[router][GET] / [cause] Success', (done) => {
  //   request(app)
  //     .get('/')
  //     .then((response) => {
  //       expect(response.status).toBe(400);
  //       expect(response.header["content-type"])
  //         .toBe('application/json; charset=utf-8');
  //       expect(JSON.parse(response.text))
  //         .toEqual({
  //           status: 'error',
  //           message: ''
  //         });
  //       done();
  //     });
  // });

  // it('[router][GET] /get404 [cause] Status 404', (done) => {
  //   request(app).get('/get404')
  //     .then((response) => {
  //       expect(response.status)
  //         .toBe(404);
  //       done();
  //     })
  // });

  // it('[router][GET] /get500 [cause] Status 500', (done) => {
  //   request(app)
  //     .get('/get500')
  //     .then((response) => {
  //       expect(response.status)
  //         .toBe(500);
  //       expect(response.header["content-type"])
  //         .toBe('application/json; charset=utf-8');
  //       done();
  //     });
  // });

  // it('[router][GET] /get400 [cause]Status 400', (done) => {
  //   request(app)
  //     .get('/get400')
  //     .then((response) => {
  //       expect(response.status)
  //         .toBe(400);
  //       expect(response.header["content-type"])
  //         .toBe('application/json; charset=utf-8');
  //       done();
  //     })
  // });

  // it('[router][GET] /getCustomResponse/:statusCode [cause]Status: :statusCode', (done) => {
  //   const randomStatusCode = faker.random.number({ min: 101, max: 511 });
  //   request(app)
  //     .get('/getCustomResponse/' + randomStatusCode)
  //     .then((response) => {
  //       expect(response.status)
  //         .toBe(randomStatusCode);
  //       expect(response.header['content-type'])
  //         .toBe('application/json; charset=utf-8')
  //       done();
  //     })
  // });
})
