[![Coverage report](http://demo_121.eabgitlab.com/121_mobile_api/unitTest/coverage/badge-statements.svg)](http://demo_121.eabgitlab.com.eabgitlab.com/121_mobile_api/unitTest/coverage/lcov-report/)

Init from http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/AXA_SG_APP_web_service (commit 9dffd144ad70b016cd61c4fc4235527dc07850aa [9dffd14])

## Table of Contents

* [Setup](#setup)
* [Available Scripts](#available-scripts)
  * [Start](#start)
  * [Test](#test)
  * [Lint](#lint)
  * [Test With Coverage Badge](#Test-with-coverage-badge)
* [Documentation](#documentation)

## Setup
Install dependencies
```
npm install
```

## Available Scripts
### start
```
npm run start
```
Run the project

### Test
```
npm run test
```

Runs the [jest](https://github.com/facebook/jest) test runner on your tests.

### Test with coverage badge
```
npm run test:coverage
```

Run the [jest](https://github.com/facebook/jest) test runner on your tests and use [jest-coverage-badges](https://www.npmjs.com/package/jest-coverage-badges) to generate coverage badges which can shown on README preview page.

### Lint
```
npm run lint
```

Run [eslint](https://github.com/eslint/eslint) utility to check whether the code format is correct.

### SIT Start
```
npm run sit:start
```
Run the project on [SIT](https://app.ease-sit3.intraeab/) env.

## Documentation
### Edit documentation.

There are 2 ways to edit documentation:
1. Download [swagger editor](https://github.com/swagger-api/swagger-editor) and import /docs/swagger.yaml to edit the documentation. Don't forget to save after editing and replace previous one.

2. Install swagger viewer plugin on your IDE and edit with viewer.
This plugin is for [VSCode](https://marketplace.visualstudio.com/items?itemName=Arjun.swagger-viewer).
This plugin is for [Webstorm](https://plugins.jetbrains.com/plugin/8347-swagger-plugin).

Note: Please follow the [REST API style](https://www.restapitutorial.com/lessons/httpmethods.html) to design or update documentation


### View Documentation

You can refer to [online version](http://demo_121.eabgitlab.com/121_mobile_api/docs/index.html) (for only last commit)
