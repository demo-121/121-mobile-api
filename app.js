const express = require('express');
const logger = require('morgan');
const log4js = require('log4js');
const cors = require('cors');
const helmet = require('helmet');
const bodyParser = require('body-parser');
const axios = require('axios');

const indexRouter = require('./app/routes/index.route');
const authRouter = require('./app/routes/auth.route');
const mockRouter = require('./app/routes/mock.route');
const emailRouter = require('./app/routes/email.route');
const paymentRouter = require('./app/routes/payment.route');
const applicationRouter = require('./app/routes/application.route');
const dataSyncRouter = require('./app/routes/dataSync.route');
const smsRouter = require('./app/routes/sms.route');
const recognizeRouter = require('./app/routes/recognize.route');

const responseUtil = require('./app/utils/response.util');
const log4jUtil = require('./app/utils/log4j.util');
const commonUtil = require('./app/utils/common.util');
const authUtil = require('./app/utils/auth.util');

// Init express framework
const app = express();

// Init Helmet security lib
// Included features by default:
// 1. dnsPrefetchControl(controls browser DNS prefetching),
// 2. frameguard(prevent clickjacking),
// 3. hidePoweredBy (remove the X-Powered-By header),
// 4. hsts (HTTP Strict Transport Security),
// 5. ieNoOpen (sets X-Download-Options for IE8+)
// 6. noSniff (keep clients from sniffing the MIME type)
// 7. xssFilter(adds some small XSS protections)
// ref to: https://helmetjs.github.io/docs/
app.use(helmet());
app.use(helmet.noCache());

// Init log4js
// level: ALL < TRACE < DEBUG < INFO < WARN < ERROR < FATAL < MARK < OFF
// refer to: Configuration Object on https://log4js-node.github.io/log4js-node/api.html
const log4j = log4js.getLogger();
// log4js.configure({
//   appenders: {
//     everything: { type: 'file', filename: 'logs/records.log' },
//   },
//   categories: {
//     default: { appenders: ['everything'], level: 'debug' },
//   },
// });
log4j.level = 'debug';

// Use middleware
log4jUtil.log('info', 'Initializing middleware');
app.use(logger('[:date[iso]] :method :url :status - :response-time ms :res[content-length]'));
app.use(cors());

// Set maximal request body size
// Express.js maximal request body default value is 100 kb
// When APP request send email with attactments(base64 format), 100kb is not enough.
// The size can be set on global variable so that we can modify it conveniently.
// Refer to: https://www.npmjs.com/package/body-parser#limit
app.use(bodyParser.json({ limit: commonUtil.getConfig('REQUEST_BODY_MAX_SIZE') }));
app.use(bodyParser.urlencoded());

// Init passport lib
authUtil.verifyToken();

// Init router
log4jUtil.log('info', 'Initializing router');
app.use('/', indexRouter);
app.use('/auth', authRouter);
app.use('/mock', mockRouter);
app.use('/email', emailRouter);
app.use('/sms', smsRouter);
app.use('/payment', paymentRouter);
app.use('/application', applicationRouter);
app.use('/dataSync', dataSyncRouter);
app.use('/recognize', recognizeRouter);

if (commonUtil.getConfig('GATEWAY_ADMIN_URL').length > 0 && commonUtil.getConfig('GATEWAY_ADMIN_PORT').length > 0) {
  axios.get(`${commonUtil.getConfig('GATEWAY_ADMIN_URL')}:${commonUtil.getConfig('GATEWAY_ADMIN_PORT')}`).then((response) => {
    if (response && response.status === 200) {
      log4jUtil.log('info', 'Connect sync gateway admin port successfully');
    } else {
      log4jUtil.log('error', `Fail to connect sync gateway admin port, status code=${response.status}`);
    }
  }).catch((error) => {
    if (error.response) {
      // The request was made and the server responded with a status code
      // that falls out of the range of 2xx
      log4jUtil.log('error', `Connect to sync gateway admin port error, data:${JSON.stringify(error.response.data)}`);
      log4jUtil.log('error', `Connect to sync gateway admin port error, status:${error.response.status}`);
      log4jUtil.log('error', `Connect to sync gateway admin port error, headers:${JSON.stringify(error.response.headers)}`);
      log4jUtil.log('error', `Connect to sync gateway admin port error, error.response: ${error.response}`);
    } else if (error.request) {
      // The request was made but no response was received
      // `error.request` is an instance of XMLHttpRequest in the browser and an instance of
      // http.ClientRequest in node.js
      log4jUtil.log('error', `Connect to sync gateway admin port error, error.response: ${error.request}`);
    } else {
      // Something happened in setting up the request that triggered an Error
      log4jUtil.log('error', `Connect to sync gateway admin port error, error.response: ${error.message}`);
    }
  });
} else {
  log4jUtil.log('error', 'GATEWAY_ADMIN_URL and GATEWAY_ADMIN_PORT are required.');
}

// catch 404 and forward to error handler
log4jUtil.log('info', 'Initializing 404 handler');
app.use((req, res) => {
  responseUtil.generalNotFoundResponse(req, res);
});

module.exports = app;
