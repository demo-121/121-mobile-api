const axios = require('axios');
const _ = require('lodash');
const async = require('async');

const responseUtil = require('../utils/response.util');
const syncGatewayUtil = require('../utils/syncGateway.util');
const commonUtil = require('../utils/common.util');
const dao = require('../core/bz/cbDaoFactory/remoteCB');
const applicationDao = require('../core/bz/cbDao/application');
const bDao = require('../core/bz/cbDao/bundle/index');
const ApprovalHandler = require('../core/bz/ApprovalHandler');
const aEmaillHandler = require('../core/bz/ApprovalNotificationHandler');
const CommonFunctions = require('../core/bz/CommonFunctions');
const commonApp = require('../core/bz/handler/application/common');
const log4jUtil = require('../utils/log4j.util');
const appDao = require('../core/bz/cbDao/application');
const application = require('../core/bz/handler/application/shield/application');
const bundleDao = require('../core/bz/cbDao/bundle');
const PDFHandler = require('../core/bz/PDFHandler');
const utils = require('../core/bz/Quote/utils');
const signature = require('../core/bz/handler/application/shield/signature');

module.exports.confirmAppPaid = (req, res, next) => {
  if (req.body.docId) {
    syncGatewayUtil.getDocFromSG(req.body.docId, (err, app) => {
      if (err) {
        responseUtil.generalInternalServerErrorResponse(req, res, next, err);
      } else {
        const tempApplication = _.cloneDeep(app);
        tempApplication.isInitialPaymentCompleted = true;
        tempApplication.lastUpdateDate = (new Date()).getTime();
        if (!tempApplication.isSubmittedStatus) {
          tempApplication.isSubmittedStatus = true;
          tempApplication.applicationSubmittedDate = (new Date()).getTime();
          log4jUtil.log('debug', `Submit invalidated paid application (paid after invalidated): ${application.id}`);
          axios.request({
            baseURL: commonUtil.getConfig('INTERNAL_API_DOMAIN'),
            url: `/ease-api/submitInvalidApp/${tempApplication.id}`,
            method: 'GET',
          }).then((response) => {
            if (response.success) {
              dao.updateViewIndex('main', 'summaryApps');
              responseUtil.generalSuccessResponse(req, res, next, {});
            } else {
              responseUtil.generalInternalServerErrorResponse(req, res, next, `Update Application failure: ${tempApplication.id}`);
            }
          }).catch((error) => {
            if (error.response) {
              // The request was made and the server responded with a status code
              // that falls out of the range of 2xx
              log4jUtil.log('error', error.response.data);
              log4jUtil.log('error', error.response.status);
              log4jUtil.log('error', error.response.headers);
              responseUtil.generalInternalServerErrorResponse(req, res, next, {
                data: JSON.stringify(error.response.data),
                status: error.response.status,
                headers: error.response.headers,
              });
            } else if (error.request) {
              // The request was made but no response was received
              // `error.request` is an instance of XMLHttpRequest in the browser and an instance of
              // http.ClientRequest in node.js
              log4jUtil.log('error', error.request);
              responseUtil.generalInternalServerErrorResponse(
                req,
                res,
                next,
                {
                  request: error.request,
                },
              );
            } else {
              // Something happened in setting up the request that triggered an Error
              log4jUtil.log('error', error.message);
              responseUtil.generalInternalServerErrorResponse(
                req,
                res,
                next,
                {
                  message: error.message,
                },
              );
            }
          });
        } else {
          syncGatewayUtil.updateDocToSG(tempApplication, (updateDocErr, result) => {
            if (updateDocErr) {
              responseUtil.generalInternalServerErrorResponse(req, res, next, err);
            } else {
              dao.updateViewIndex('main', 'summaryApps');
              JSON.stringify(result);
              responseUtil.generalSuccessResponse(req, res, next, {});
            }
          });
        }
      }
    });
  } else {
    responseUtil.generalBadRequestResponse(req, res, next, 'docId is required.');
  }
};

// const getPolicyNumber = (numOfPolicyNumber, isShield) => new Promise((resolve, reject) => {
//   log4jUtil.log('info', `Get policy number, type:${isShield ?
// 'shield' : 'nonShield'}, numOfPolicyNumber: ${numOfPolicyNumber}`);
//   if (Number(numOfPolicyNumber) > 0) {
//     axios.request({
//       baseURL: commonUtil.getConfig('INTERNAL_API_DOMAIN'),
//       url: '/ease-api/policyNumber/assignPolicyNumber',
//       method: 'post',
//       data: {
//         companyCD: isShield ? 3 : 5,
//         requestedPolicyNO: Number(numOfPolicyNumber),
//       },
//     }).then((result) => {
//       if (result.status === 200) {
//         log4jUtil.log('info', `Get policy number, type:${isShield ?
// 'shield' : 'nonShield'}, result: ${result.data.retrievePolicyNumberResponse.policyNO}`);
//         resolve(result.data.retrievePolicyNumberResponse.policyNO);
//       } else {
//         const errorObj = {
//           data: result.data,
//           status: result.status,
//           statusText: result.statusText,
//           headers: result.headers,
//           config: result.config,
//         };
//         log4jUtil.log('error', `[error case1] Get policy number error, ${errorObj}`);
//         reject(errorObj);
//       }
//     }).catch((error) => {
//       if (error.response) {
//         // The request was made and the server responded with a status code
//         // that falls out of the range of 2xx
//         const errorObj = {
//           data: error.response.data,
//           status: error.response.status,
//           headers: error.response.headers,
//         };
//         log4jUtil.log('error', `[error case2] Get policy number error,
// type: ${isShield}, ${JSON.stringify(errorObj)}`);
//         if (errorObj.data.message === 'Do not have enough policy numbers') {
//           resolve([]);
//         } else {
//           reject(errorObj);
//         }
//       } else if (error.request) {
//         // The request was made but no response was received
//         // `error.request` is an instance of XMLHttpRequest in the browser and an instance of
//         // http.ClientRequest in node.js
//         const errorObj = {
//           request: error.request,
//         };
//         log4jUtil.log('error', `[error case3] Get policy number error, ${errorObj}`);
//         reject(errorObj);
//       } else {
//         // Something happened in setting up the request that triggered an Error
//         const errorObj = {
//           message: error.message,
//         };
//         log4jUtil.log('error', `[error case4] Get policy number error, ${errorObj}`);
//         reject(errorObj);
//       }
//     });
//   } else {
//     log4jUtil.log('info', `Get policy number, type:${isShield
// ? 'shield' : 'nonShield'}, result: []`);
//     resolve([]);
//   }
// });

module.exports.getPolicyNumber = (req, res, next) => {
  if (req.body.numOfShield || req.body.numOfNonShield) {
    if (
      Number.isInteger(Number(req.body.numOfShield))
      && Number.isInteger(Number(req.body.numOfNonShield))
    ) {
      if (Number(req.body.numOfShield) >= 0 && Number(req.body.numOfNonShield) >= 0) {
        // let shieldResult = [];
        // getPolicyNumber(req.body.numOfShield, true)
        //   .then((_shieldResult) => {
        //     shieldResult = _shieldResult;
        //     return getPolicyNumber(req.body.numOfNonShield, false);
        //   })
        //   .then((nonShieldResult) => {
        //     if (shieldResult.length === 0 && nonShieldResult.length === 0) {
        //       responseUtil.generalInternalServerErrorResponse(req, res, next, {
        //         data: {
        //           success: false,
        //           status: false,
        //           message: 'Do not have enough policy numbers',
        //         },
        //         status: 500,
        //       });
        //     } else {
        //       responseUtil.generalSuccessResponse(req, res, next, {
        //         shieldPolicyNum: shieldResult,
        //         nonShieldPolicyNum: nonShieldResult,
        //         type: 'getPolicyNumber',
        //       });
        //     }
        //   })
        //   .catch((error) => {
        //     responseUtil.generalInternalServerErrorResponse(req, res, next, error);
        //   });
        responseUtil.generalSuccessResponse(req, res, next, {
          shieldPolicyNum: [
            '301-3166834',
            '301-3166842',
            '301-3166859',
            '301-3166867',
            '301-3166875',
            '301-3166883',
            '301-3166891',
            '301-3166909',
            '301-3166917',
            '301-3166925',
            '301-3166933',
            '301-3166941',
            '301-3166958',
            '301-3166966',
            '301-3166974',
            '301-3166982',
            '301-3166990',
            '301-3167006',
            '301-3167014',
            '301-3167022',
          ],
          nonShieldPolicyNum: [
            '103-3166834',
            '103-3166842',
            '103-3166859',
            '103-3166867',
            '103-3166875',
            '103-3166883',
            '103-3166891',
            '103-3166909',
            '103-3166917',
            '103-3166925',
            '103-3166933',
            '103-3166941',
            '103-3166958',
            '103-3166966',
            '103-3166974',
            '103-3166982',
            '103-3166990',
            '103-3167006',
            '103-3167014',
            '103-3167022',
          ],
          type: 'getPolicyNumber',
        });
      } else {
        log4jUtil.log('error', `numOfShield or numOfNonShield must more than 0. (numOfShield: ${req.body.numOfShield},  numOfNonShield: ${req.body.numOfNonShield})`);
        responseUtil.generalBadRequestResponse(req, res, next, 'numOfShield or numOfNonShield must more than 0.');
      }
    } else {
      log4jUtil.log('error', `numOfShield and numOfNonShield must be integer.(numOfShield: ${req.body.numOfShield},  numOfNonShield: ${req.body.numOfNonShield})`);
      responseUtil.generalBadRequestResponse(req, res, next, 'numOfShield and numOfNonShield must be integer.');
    }
  } else {
    log4jUtil.log('error', `numOfShield and numOfNonShield are required.(numOfShield: ${req.body.numOfShield},  numOfNonShield: ${req.body.numOfNonShield})`);
    responseUtil.generalBadRequestResponse(req, res, next, 'numOfShield and numOfNonShield are required.');
  }
};

const getApplication = (applicationId, cb) => {
  syncGatewayUtil.getDocFromSG(applicationId, (err, result) => {
    if (err) {
      cb(err, null);
    } else {
      cb(null, result);
    }
  });
};

const getAgent = (agentCode, cb) => {
  axios.request({
    url: `/${commonUtil.getConfig('GATEWAY_DBNAME')}/_design/main/_view/agentDetails?key=["01","agentCode","${agentCode}"]`,
    method: 'get',
    baseURL: `${commonUtil.getConfig('GATEWAY_URL')}:${commonUtil.getConfig('GATEWAY_PORT')}`,
    auth: {
      username: commonUtil.getConfig('GATEWAY_USER'),
      password: commonUtil.getConfig('GATEWAY_PW'),
    },
  }).then((response) => {
    if (response.status === 200 && response.data && response.data.total_rows === 1) {
      cb(null, response.data.rows[0].value);
    } else {
      cb('Get agent error', null);
    }
  });
};

const updateViewedList = (app, agentCode) => {
  const clonedApp = _.cloneDeep(app);
  if (_.get(app, 'payment.initialPayment.paymentMethod') && _.get(_.get(app, 'supportDocuments.viewedList'), agentCode)) {
    clonedApp
      .supportDocuments
      .viewedList[agentCode][app.payment.initialPayment.paymentMethod] = false;
  }
  return clonedApp;
};

const id = '_id';
const rev = '_rev';

const mergePdfBase64ToAppFormPdf = (app, pdfStr, callback) => {
  log4jUtil.log('info', 'INFO: mergePdfBase64ToAppFormPdf - start', app[id]);
  // get eApp form and merge with premium payment
  applicationDao.getAppAttachment(app[id], 'appPdf', (appPdf) => {
    if (appPdf.success) {
      if (pdfStr && pdfStr.length > 0) {
        log4jUtil.log('info', 'INFO: mergePdfBase64ToAppFormPdf - pdfString length', pdfStr.length, app[id]);
        // merge Pdf
        CommonFunctions.mergePdfs([appPdf.data, pdfStr], (mergedPdf) => {
          if (mergedPdf) {
            log4jUtil.log('info', 'INFO: mergePdfBase64ToAppFormPdf - mergedPdf not null', app[id]);
            dao.uploadAttachmentByBase64(app[id], 'appPdf', app[rev], mergedPdf, 'application/pdf', (res) => {
              if (res && res.rev && !res.error) {
                const tempApp = _.cloneDeep(app);
                tempApp[rev] = res.rev;
                log4jUtil.log('info', 'INFO: mergePdfBase64ToAppFormPdf - end [RETURN=1]', app[id]);
                callback(tempApp);
              } else {
                log4jUtil.log('error', 'ERROR: mergePdfBase64ToAppFormPdf - end [RETURN=-4]', app[id]);
                callback(false);
              }
            });
          } else {
            log4jUtil.log('error', 'ERROR: mergePdfBase64ToAppFormPdf - end [RETURN=-3]', app[id]);
            callback(false);
          }
        });
      } else {
        log4jUtil.log('error', 'ERROR: mergePdfBase64ToAppFormPdf - end [RETURN=-2]', app[id]);
        callback(app);
      }
    } else {
      log4jUtil.log('error', 'ERROR: mergePdfBase64ToAppFormPdf - end [RETURN=-1]', app[id]);
      callback(false);
    }
  });
};

const genAppFormPdfByPaymentData = (app, lang, callback) => {
  const trigger = {};
  const paymentValues = _.cloneDeep(app.payment);
  const isShowDollarSign = _.get(app, 'applicationForm.values.appFormTemplate.properties.dollarSignForAllCcy', true);
  const ccy = isShowDollarSign ? null : _.get(app, 'quotation.ccy');

  dao.getDoc(commonApp.TEMPLATE_NAME.PAYMENT, (paymentTmpl) => {
    log4jUtil.log('info', 'INFO: genAppFormPdfByPaymentData - replaceAppFormValuesByTemplate start', app[id]);
    commonApp.replaceAppFormValuesByTemplate(
      paymentTmpl,
      app.payment,
      paymentValues,
      trigger,
      [],
      lang,
      ccy,
    );
    log4jUtil.log('info', 'INFO: genAppFormPdfByPaymentData - replaceAppFormValuesByTemplate end', app[id]);

    const reportData = {
      root: {
        payment: paymentValues,
        originalData: app.payment,
        policyOptions: app.quotation.policyOptions,
      },
    };

    const tplFiles = [];
    commonApp.getReportTemplates(tplFiles, ['appform_report_payment_main', 'appform_report_common', 'appform_report_premium_payment'], 0, () => {
      if (tplFiles.length) {
        PDFHandler.getPremiumPaymentPdf(reportData, tplFiles, lang, (pdfStr) => {
          // get eApp form and merge with premium payment
          mergePdfBase64ToAppFormPdf(app, pdfStr, (appResult) => {
            if (appResult && !appResult.error) {
              log4jUtil.log('info', 'INFO: genAppFormPdfByPaymentData - end [RETURN=1]', appResult[id]);
              callback(appResult);
            } else {
              log4jUtil.log('error', 'ERROR: genAppFormPdfByPaymentData - end [RETURN=-2]', _.get(appResult, 'error'));
              callback(false);
            }
          });
        });
      } else {
        log4jUtil.log('error', 'ERROR: genAppFormPdfByPaymentData - end [RETURN=-1]', app[id]);
        callback(false);
      }
    });
  });
};

const responseToIOS = (app, req, res, next) => {
  const responsePromises = [];
  responsePromises.push(
    new Promise((resolve, reject) => {
      syncGatewayUtil.getDocFromSG(
        app.id,
        (getAppErr, applicationResult) => {
          if (getAppErr) {
            reject(getAppErr);
          } else {
            resolve(applicationResult);
          }
        },
      );
    }),
  );
  responsePromises.push(
    new Promise((resolve, reject) => {
      syncGatewayUtil.getDocFromSG(
        app.bundleId,
        (getBundleErr, bundleResult) => {
          if (getBundleErr) {
            reject(getBundleErr);
          } else {
            resolve(bundleResult);
          }
        },
      );
    }),
  );
  responsePromises.push(
    new Promise((resolve, reject) => {
      syncGatewayUtil.getDocFromSG(
        app.policyNumber,
        (policyDocumentErr, policyDoc) => {
          if (policyDocumentErr) {
            reject(policyDocumentErr);
          } else {
            resolve(policyDoc);
          }
        },
      );
    }),
  );
  responsePromises.push(
    new Promise((resolve, reject) => {
      syncGatewayUtil.getDocFromSG(
        app.policyNumber,
        (policyDocumentErr, policyDoc) => {
          if (policyDocumentErr) {
            reject(policyDocumentErr);
          } else {
            resolve(policyDoc);
          }
        },
      );
    }),
  );
  responsePromises.push(
    new Promise((resolve, reject) => {
      syncGatewayUtil.getAttachmentByBase64(
        app.id,
        'appPdf',
        (getAttactmentErr, getAttactmentResult) => {
          if (getAttactmentErr) {
            reject(getAttactmentErr);
          } else {
            resolve({ type: 'attactment', data: getAttactmentResult });
          }
        },
      );
    }),
  );
  Promise.all(responsePromises)
    .then((responseResult) => {
      responseUtil
        .generalSuccessResponse(req, res, next, {
          application: _.find(responseResult, doc => doc.type === 'application'),
          bundle: _.find(responseResult, doc => doc.type === 'bundle'),
          policyDocument: _.find(responseResult, doc => doc.type === 'approval'),
          appPdf: _.find(responseResult, doc => doc.type === 'attactment').data,
        });
    }).catch((error) => {
      responseUtil
        .generalInternalServerErrorResponse(req, res, next, error);
    });
};

const getSubmissionTemplate = (app, cb) => {
  log4jUtil.log('info', 'INFO: getSubmissionTemplate');
  dao.getDoc(commonApp.TEMPLATE_NAME.SUBMISSION, (res) => {
    if (res && !res.error) {
      cb(res);
    } else {
      cb(false);
    }
  });
};

const getECpdPdfByAppFormData = (app, lang, callback) => {
  const initPayMethod = _.get(app, 'payment.initPayMethod');
  log4jUtil.log('info', 'INFO: getECpdPdfByAppFormData - start', app[id]);

  if (['crCard', 'eNets', 'dbsCrCardIpp'].indexOf(initPayMethod) > -1) {
    const formValues = _.cloneDeep(app.applicationForm.values);
    formValues.receiptDate = CommonFunctions.getFormattedDate(app.payment.trxTime, 'dd/MMMM/yyyy');
    formValues.policyNumber = app.policyNumber || app[id];
    formValues.agent = app.quotation.agent;
    const reportData = {
      root: formValues,
    };

    const tplFiles = [];
    commonApp.getReportTemplates(tplFiles, ['appform_report_ecpd_main'], 0, () => {
      if (tplFiles.length) {
        PDFHandler.getECpdPdf(reportData, tplFiles, lang, (pdfStr) => {
          dao.uploadAttachmentByBase64(app[id], 'eCpdPdf', app[rev], pdfStr, 'application/pdf', (res) => {
            if (res && res.rev && !res.error) {
              const tempApp = _.cloneDeep(app);
              tempApp[rev] = res.rev;
              log4jUtil.log('info', 'INFO: getECpdPdfByAppFormData - end [RETURN=1]', app[id]);
              callback(tempApp);
            } else {
              log4jUtil.log('error', 'ERROR: getECpdPdfByAppFormData - end [RETURN=-2]', app[id], tplFiles);
              callback(false);
            }
          });
        });
      } else {
        log4jUtil.log('error', 'ERROR: getECpdPdfByAppFormData - end [RETURN=-1]', app[id], tplFiles);
        callback(false);
      }
    });
  } else {
    log4jUtil.log('info', 'INFO: getECpdPdfByAppFormData - end [RETURN=2]', app[id]);
    callback(app);
  }
};

const getSubmissionValues = (app, submitStatus, failMsg, agent, cb) => {
  log4jUtil.log('info', 'INFO: getSubmissionValues');
  let quot = null;
  const tempSubmitStatus = _.cloneDeep(submitStatus);
  let tempMandDocsAllUploaded = '';
  if (submitStatus === 'SUCCESS') {
    tempMandDocsAllUploaded = 'Y';
  } else if (app.isMandDocsAllUploaded) {
    tempMandDocsAllUploaded = 'Y';
  } else {
    tempMandDocsAllUploaded = 'N';
  }
  const values = {
    agentChannelType: (agent.channel === 'FA' ? 'Y' : 'N'),
    submitStatus: tempSubmitStatus,
    receiveEmailFa: (app.submission && app.submission.receiveEmailFa ? app.submission.receiveEmailFa : ''),
    mandDocsAllUploaded: tempMandDocsAllUploaded,
  };
  let covName;
  quot = app.quotation;
  if (_.get(quot, 'quotType') === 'SHIELD') {
    covName = _.get(app, 'quotation.covName');
  } else {
    const basicPlan = quot.plans.find(plan => (plan.covCode === quot.baseProductCode));
    covName = _.get(basicPlan, 'covName');
  }

  if (submitStatus === 'FAIL') {
    values.failMsg = failMsg;
    values.lastSubmitDateTime = CommonFunctions.getFormattedDateTime(new Date());
  } else if (submitStatus === 'SUCCESS') {
    values.eSubmitDateTime = CommonFunctions.getFormattedDateTime(app.applicationSubmittedDate);
    values.proposerName = quot.pFullName;
    values.planName = covName;
    values.policyNumber = app.policyNumber ? app.policyNumber : '';
  }
  cb({ values });
};

const submitNonShiedCase = (previousApp, agent, _clientProfile, req, res, next) => {
  log4jUtil.log('info', 'Start gen App Form Pdf By Payment Data');
  log4jUtil.log('info', `req.body.lang: ${req.body.lang}`);
  genAppFormPdfByPaymentData(previousApp, req.body.lang ? req.body.lang : '', (newApp) => {
    if (newApp && !newApp.error) {
      getECpdPdfByAppFormData(newApp, req.body.lang ? req.body.lang : '', (app) => {
        if (app && !app.error) {
          log4jUtil.log('warn', `Sumission - app.policyNumber:${app.policyNumber}`);
          if (app.policyNumber) {
            applicationDao.getSubmissionEmailTemplate(agent.channel === 'FA', (result) => {
              if (result && !result.error) {
                // Check whether need to send email
                log4jUtil.log('info', 'getSubmissionEmailTemplate success');
                const now = Date.now();
                const filteredTemplate = _.filter(
                  result.eSubmissionEmails,
                  template => template.productCode.indexOf(app.quotation.baseProductCode) !== -1,
                );
                if (filteredTemplate && filteredTemplate.length !== 0) {
                  log4jUtil.log('info', 'filteredTemplate.length !== 0');
                  const values = 'values';
                  const data = {
                    docId: app[id],
                    agentProfile: app.quotation.agent,
                    values: app.applicationForm[values],
                    clientProfile: _clientProfile,
                  };
                  const emailValues = Object.assign({}, data);
                  const attachmentKeys = 'attachmentKeys';
                  const tempAttachmentKeys = filteredTemplate[0][attachmentKeys];
                  const eCpdIndex = tempAttachmentKeys.indexOf('eCpd');
                  if (['crCard', 'eNets', 'dbsCrCardIpp'].indexOf(_.get(app, 'payment.initPayMethod')) < 0 && eCpdIndex > -1) {
                    tempAttachmentKeys.splice(eCpdIndex, 1);
                  }
                  emailValues.agentName = _.get(data, 'agentProfile.name');
                  emailValues.agentContactNum = _.get(data, 'agentProfile.mobile');
                  emailValues.agentEmail = _.get(data, 'agentProfile.email');
                  emailValues.agentTitle = '';
                  emailValues.clientName = _.get(data, 'clientProfile.fullName');
                  emailValues.clientEmail = _.get(data, 'clientProfile.email');
                  emailValues.clientId = _.get(data, 'clientProfile.cid');
                  emailValues.clientTitle = _.get(data, 'clientProfile.title');
                  emailValues.emailContent = filteredTemplate[0].emailContent;
                  emailValues.attKeys = tempAttachmentKeys;
                  emailValues.policyNumber = app.policyNumber ? app.policyNumber : '';
                  emailValues.submissionDate = CommonFunctions.getFormattedDate(now);
                  emailValues.quotId = app.quotation.id;
                  emailValues.appId = app[id];
                  if (agent.channel === 'FA') {
                    if (values.receiveEmailFa) {
                      emailValues.receiveEmailFa = values.receiveEmailFa;
                    } else {
                      emailValues.receiveEmailFa = 'N';
                    }
                  } else {
                    emailValues.receiveEmailFa = 'Y';
                  }
                  // log4jUtil.log('info', `Submit - emailValues: ${JSON.stringify(emailValues)}`);
                  // Create Approval Case and update applciation status in bundle
                  ApprovalHandler.createApprovalCase(
                    { ids: [app[id]] },
                    { agent },
                    (eAppResult) => {
                      dao.updateViewIndex('main', 'applicationsByAgent');
                      dao.updateViewIndex('main', 'approvalDetails');
                      if (app && app.bundleId) {
                        log4jUtil.log('debug', `application.bundleId:${app.bundleId}`);
                      }
                      if (_clientProfile && _clientProfile.bundle) {
                        log4jUtil.log('debug', `clientProfile.bundle${JSON.stringify(_clientProfile.bundle)}`);
                      }
                      if (_clientProfile && _clientProfile[rev]) {
                        log4jUtil.log('debug', `ClientProfile.rev:${_clientProfile[rev]}`);
                      }
                      if (_clientProfile && _clientProfile.lstChgDate) {
                        log4jUtil.log('debug', `ClientProfile.lstChgDate:${_clientProfile.lstChgDate}`);
                      }
                      bDao.onSubmitApplication(app.pCid, app[id]).then((onSubmissionResult) => {
                        log4jUtil.log('info', `onSubmissionResult: ${JSON.stringify(JSON.stringify(onSubmissionResult))}`);
                        if (onSubmissionResult.ok && eAppResult.success) {
                          // Send email
                          aEmaillHandler.SubmittedCaseNotification(
                            { id: app.policyNumber },
                            { agent },
                          );
                          commonApp.prepareSubmissionEmails(emailValues, { agent }, (resp) => {
                            log4jUtil.log('info', `prepareSubmissionEmailsResult: ${JSON.stringify(resp)}`);
                            const tempApp = app;
                            if (resp && resp.success) {
                              log4jUtil.log('info', 'prepareSubmissionEmails success');
                              tempApp.isSubmittedStatus = true;
                              // app.appStatus = 'SUBMITTED';
                              // TODO: still store appStatus inside application?
                              tempApp.applicationSubmittedDate = new Date().toISOString();
                              if (agent.channel === 'FA') {
                                tempApp.submission = {};
                                tempApp.submission.receiveEmailFa = values.receiveEmailFa;
                              }
                              if (_.get({ agent }, 'agent.agentCode') !== _.get({ agent }, 'agent.managerCode')) {
                                commonApp.transformSuppDocsPolicyFormValues(app);
                              }
                              updateViewedList(app);
                              applicationDao.upsertApplication(
                                tempApp[id],
                                app,
                                (upsertAppRes) => {
                                  if (upsertAppRes && !upsertAppRes.error) {
                                    log4jUtil.log('info', 'INFO: appFormSubmission - (3) prepareSubmissionTemplateValues');
                                    getSubmissionTemplate(app, (tmpl) => {
                                      if (tmpl) {
                                        const template = _.cloneDeep(tmpl);
                                        getSubmissionValues(app, 'SUCCESS', '', agent, (resValues) => {
                                          if (resValues) {
                                            bDao.getCurrentBundle(app.pCid).then((bundle) => {
                                              log4jUtil.log('info', bundle);
                                              bDao.updateStatus(
                                                app.pCid,
                                                bDao.BUNDLE_STATUS.SUBMIT_APP,
                                              ).then((newBundle) => {
                                                log4jUtil.log('info', newBundle);
                                                responseToIOS(
                                                  app,
                                                  req,
                                                  res,
                                                  next,
                                                  template,
                                                  values,
                                                );
                                              });
                                            });
                                          } else {
                                            log4jUtil.log('error', 'Fail to get submission values to display');
                                            responseUtil.generalInternalServerErrorResponse(req, res, next, 'Fail to get submission values to display');
                                          }
                                        });
                                      } else {
                                        log4jUtil.log('error', 'Fail to get template');
                                        responseUtil.generalInternalServerErrorResponse(req, res, next, 'Fail to get template');
                                      }
                                    });
                                  } else {
                                    log4jUtil.log('error', 'Fail to update status');
                                    responseUtil.generalInternalServerErrorResponse(req, res, next, 'Fail to update status');
                                  }
                                },
                              );
                            } else {
                              log4jUtil.log('info', 'prepareSubmissionEmails error');
                              tempApp.isFailSubmit = false;
                              applicationDao.upsertApplication(
                                tempApp[id], app,
                                (upsertAppRes) => {
                                  if (upsertAppRes && !upsertAppRes.error) {
                                    syncGatewayUtil.getDocFromSG(
                                      upsertAppRes.id,
                                      (getAppErr, applicationResult) => {
                                        if (!getAppErr && applicationResult) {
                                          syncGatewayUtil.getDocFromSG(
                                            upsertAppRes.bundleId,
                                            (getBundleErr, bundleResult) => {
                                              if (!getBundleErr && bundleResult) {
                                                responseUtil.generalSuccessResponse(
                                                  req,
                                                  res,
                                                  next,
                                                  {
                                                    application: applicationResult,
                                                    bundle: bundleResult,
                                                  },
                                                );
                                              } else {
                                                responseUtil.generalInternalServerErrorResponse(
                                                  req,
                                                  res,
                                                  next,
                                                  getBundleErr,
                                                );
                                              }
                                            },
                                          );
                                        } else {
                                          responseUtil.generalInternalServerErrorResponse(
                                            req,
                                            res,
                                            next,
                                            getAppErr,
                                          );
                                        }
                                      },
                                    );
                                  } else {
                                    responseUtil.generalInternalServerErrorResponse(req, res, next, 'Fail to send email');
                                  }
                                },
                              );
                            }
                          });
                        } else {
                          responseUtil.generalInternalServerErrorResponse(req, res, next, 'Fail to create/ update Bundle');
                        }
                      }).catch((err) => {
                        JSON.stringify(err);
                        responseUtil.generalInternalServerErrorResponse(req, res, next, 'Fail to create/ update Bundle');
                      });
                    },
                  );
                } else {
                  log4jUtil.log('error', 'filteredTemplate.length == 0');
                  responseUtil.generalInternalServerErrorResponse(req, res, next, 'Fail to get email template from product');
                }
              } else {
                log4jUtil.log('error', 'getSubmissionEmailTemplate error');
                responseUtil.generalInternalServerErrorResponse(req, res, next, 'Fail to get email template');
              }
            });
          } else {
            responseUtil.generalBadRequestResponse(req, res, next, 'Policy number not found');
          }
        } else {
          responseUtil.generalInternalServerErrorResponse(
            req,
            res,
            next,
            'getECpdPdfByAppFormData error',
          );
        }
      });
    } else {
      responseUtil.generalInternalServerErrorResponse(
        req,
        res,
        next,
        'genAppFormPdfByPaymentData error',
      );
    }
  });
};

const createMasterApprovalCase = (masterId, session, now) => new Promise((resolve) => {
  ApprovalHandler.createApprovalCase({
    ids: [masterId],
    isShieldMaster: true,
    now,
  }, session, (eAppResult) => {
    if (eAppResult.success) {
      resolve({ success: true });
    } else {
      resolve({ success: false });
    }
  });
});

const createApprovalCase = (childAppId, session, currentTime) => new Promise((resolve, reject) => {
  ApprovalHandler.createApprovalCase({
    ids: [childAppId],
    isShield: true,
    currentTime,
  }, session, (eAppResult) => {
    if (eAppResult.success) {
      resolve({ success: true });
    } else {
      reject(new Error({ success: false }));
    }
  });
});

const createApprovalCases = (app, session, callback) => {
  // Create Approval Cases
  const promises = [];
  const childIds = _.get(app, 'childIds');
  const currentTime = (new Date()).toISOString();
  // Create Master approval docs
  promises.push(createMasterApprovalCase(app[id], session, currentTime));
  // Create child approval docs
  _.each(childIds, (ID) => {
    promises.push(createApprovalCase(ID, session, currentTime));
  });
  return Promise.all(promises).then((results) => {
    JSON.stringify(results);
    callback(null, { success: true });
  }, (rejectReason) => {
    callback(`ERROR in createApprovalCases with Reject Reason,  ${rejectReason}`);
  }).catch((error) => {
    log4jUtil.log('error', `ERROR in createApprovalCases ${error}`);
    callback(`ERROR in createApprovalCases, ${error}`);
  });
};

const saveParentSubmissionValuestoChild = (parentApplication, updateObj, callback) => {
  const childIds = _.get(parentApplication, 'childIds');
  application.getChildApplications(childIds).then((childApplications) => {
    const updAppPromises = childApplications.map((app) => {
      const tempApp = _.assignIn(app, updateObj);
      // app.payment.trxStatus = _.get(parentApplication, 'payment.trxStatus', '');
      // app.payment.trxRemark = _.get(parentApplication, 'payment.trxRemark', '');
      // app.payment.trxAmount = _.get(parentApplication, 'payment.trxAmount', '');
      // app.payment.trxCcy = _.get(parentApplication, 'payment.trxCcy', '');
      // app.payment.trxTime = _.get(parentApplication, 'payment.trxTime', '');
      // app.payment.trxPayType = _.get(parentApplication, 'payment.trxPayType', '');
      // app.payment.receiptNo = _.get(parentApplication, 'payment.receiptNo', '');
      // app.payment.trxStatusRemark = _.get(parentApplication, 'payment.trxStatusRemark', '');
      // app.payment.trxEnquiryStatus = _.get(parentApplication, 'payment.trxEnquiryStatus', '');
      // app.payment.isInitialPaymentCompleted =
      // _.get(parentApplication, 'payment.isInitialPaymentCompleted', '');

      // // Change the trxAmount from total in Parent Application to child Cash portion
      // app.payment.trxAmount = app.payment.cashPortion;
      return new Promise((resolve, reject) => {
        syncGatewayUtil.updateDocToSG(tempApp, (err, result) => {
          if (err) {
            reject(new Error(`Fail to update application ${JSON.stringify(err)}`));
          } else {
            resolve(result);
          }
        });
        // appDao.updApplication(app.id, app, (res) => {
        //   if (res && !res.error) {
        //     resolve(res);
        //   } else {
        //     reject(new Error(`Fail to update application ${_.get(res, 'error')}`));
        //   }
        // });
      });
    });

    return Promise.all(updAppPromises).then((results) => {
      JSON.stringify(results);
      callback(null, parentApplication);
    }, (rejectReason) => {
      callback(`ERROR in _saveParentPaymenttoChild with Reject Reason,  ${rejectReason}`);
    }).catch((error) => {
      log4jUtil.log('error', `ERROR in _saveParentPaymenttoChild ${error}`);
      callback(`ERROR in _saveParentPaymenttoChild, ${error}`);
    });
  }).catch((error) => {
    log4jUtil.log('error', `ERROR in _saveParentPaymenttoChild: ${error}`);
  });
};

const sendApprovalSubmittedCaseNotification = (app, session, callback) => {
  // Send email
  const masterEapprovalId = ApprovalHandler.getMasterApprovalIdFromMasterApplicationId(app.id);
  aEmaillHandler.SubmittedCaseNotification({ id: masterEapprovalId }, session);
  callback(null, app);
};

const submitApplication = (data, app, session, cb) => {
  const submissionValues = _.get(data, 'changedValues.submission');
  async.waterfall([
    (callback) => {
      // update bundle
      log4jUtil.log('info', 'Start Submission - Update Bundle');
      bundleDao.onSubmitApplication(app.pCid, app[id]).then((result) => {
        if (result && result.ok) {
          // Success update bundle
          callback(null, { success: true });
        } else {
          callback('Fail to create/ update Bundle ');
        }
      }).catch((Err) => {
        log4jUtil.log('error', 'ERROR _submitApplication in update bundle promise', Err);
        callback(`Fail to create/ update Bundle ${Err}`);
      });
    }, (previousResult, callback) => {
      // update application
      log4jUtil.log('info', 'Start Submission - Update Application');
      const tempApp = _.cloneDeep(app);
      if (_.get(session, 'agent.agentCode') !== _.get(session, 'agent.managerCode')) {
        commonApp.transformSuppDocsPolicyFormValues(tempApp);
      }
      if (_.get(tempApp, 'payment.initialPayment.paymentMethod') && _.get(_.get(tempApp, 'supportDocuments.viewedList'), session.agentCode)) {
        tempApp.supportDocuments.viewedList[
          session.agent.agentCode
        ][
          tempApp.payment.initialPayment.paymentMethod
        ] = false;
      }

      if (session.agent.channel === 'FA') {
        tempApp.submission = submissionValues;
      }

      tempApp.isSubmittedStatus = true;
      tempApp.submitStatus = 'SUCCESS';
      tempApp.applicationSubmittedDate = new Date().toISOString();
      application.updateApplicationCompletedStep(tempApp);

      // Declare which fields need to update to child
      const updateObjforChild = {
        submission: tempApp.submission,
        isSubmittedStatus: tempApp.isSubmittedStatus,
        submitStatus: tempApp.submitStatus,
        applicationSubmittedDate: tempApp.applicationSubmittedDate,
      };

      appDao.upsertApplication(tempApp[id], tempApp, (res) => {
        if (res && !res.error) {
          // isMandDocsAllUploaded may change to false
          // in transformSuppDocsPolicyFormValues,
          // always return isMandDocsAllUploaded = true to web in submission success case
          tempApp.isMandDocsAllUploaded = true;
          callback(null, [tempApp, updateObjforChild]);
        } else {
          callback('Fail to Update application');
        }
      });
    }, (result, callback) => {
      log4jUtil.log('info', 'Start Submission - Save Parent Submission Values to Child');
      saveParentSubmissionValuestoChild(result[0], result[1], callback);
    }, (pApplication, callback) => {
      log4jUtil.log('info', 'Start Submission - Update Status');
      bundleDao.updateStatus(pApplication.pCid, bundleDao.BUNDLE_STATUS.SUBMIT_APP).then((res) => {
        JSON.stringify(res);
        log4jUtil.log('info', 'INFO: Shield appFormSubmission - update bundle', pApplication[id]);
        callback(null, pApplication);
      }).catch((error) => {
        log4jUtil.log('error', 'ERROR: Shield appFormSubmission - end [RETURN=1] fails :', pApplication[id], error);
        callback('Failed to update Bundle ');
      });
    },
  ], (error, modifiedApplication) => {
    if (error) {
      log4jUtil.log('error', `ERROR in _submitApplication ${error}`);
      cb(`Fail to _submitApplication ${error}`);
    } else {
      cb(null, modifiedApplication);
    }
  });
};

const prepareSubmissionEmail = (data, app, session, cb) => {
  const receiveEmailFa = _.get(data, 'changedValues.submission.receiveEmailFa', 'N');
  const emailValues = Object.assign({}, data);
  const iCidMapping = _.get(app, 'iCidMapping');
  // Prepare Submission Email
  async.waterfall([
    (callback) => {
      log4jUtil.log('info', 'Start Submission - Get Submission Email Template');
      appDao.getSubmissionEmailTemplate(session.agent.channel === 'FA', (result) => {
        if (result && !result.error) {
          // Check whether need to send email
          const now = Date.now();
          const filteredTemplate = _.filter(
            result.eSubmissionEmails,
            template => template.productCode.indexOf(app.quotation.baseProductCode) !== -1,
          );
          if (filteredTemplate && filteredTemplate.length !== 0) {
            log4jUtil.log('info', 'INFO: _prepareSubmissionEmail filteredTemplate', app.quotation.baseProductCode, session.agent.channel, app.id);
            const { attachmentKeys } = filteredTemplate[0];
            const appPdfAttachment = _.get(app, '_attachments');
            _.each(appPdfAttachment, (value, key) => {
              if (key.indexOf(commonApp.PDF_NAME.eAPP) > -1) {
                attachmentKeys.push(key);
              }
            });
            emailValues.agentName = _.get(data, 'agentProfile.name');
            emailValues.agentContactNum = _.get(data, 'agentProfile.mobile');
            emailValues.agentEmail = _.get(data, 'agentProfile.email');
            emailValues.agentTitle = '';
            emailValues.clientName = _.get(data, 'clientProfile.fullName');
            emailValues.clientEmail = _.get(data, 'clientProfile.email');
            emailValues.clientId = _.get(data, 'clientProfile.cid');
            emailValues.clientTitle = _.get(data, 'clientProfile.title');
            emailValues.emailContent = filteredTemplate[0].emailContent;
            emailValues.attKeys = attachmentKeys;
            emailValues.policyNumber = commonApp.getAllPolicyIdsStrFromMaster(iCidMapping);
            emailValues.submissionDate = CommonFunctions.getFormattedDate(now);
            emailValues.quotId = app.quotation.id;
            emailValues.appId = app[id];
            emailValues.receiveEmailFa = session.agent.channel === 'FA' ? receiveEmailFa : 'Y';
            emailValues.isShield = true;
            callback(null, emailValues);
          } else {
            callback('Fail to get email template from product');
          }
        }
      });
    }, (eValues, callback) => {
      log4jUtil.log('info', 'Start Submission - Prepare Submission Emails');
      commonApp.prepareSubmissionEmails(eValues, session, (resp) => {
        if (resp && resp.success) {
          callback(null, app);
        } else {
          callback('Fail to prepareSubmissionEmails');
        }
      });
    },
  ], (err, callbackApplication) => {
    if (err) {
      cb(`Fail in _prepareSubmissionEmail, ${err}`);
    } else {
      cb(null, callbackApplication);
    }
  });
};

const startSubmitSheid = (data, app, session, cb) => {
  log4jUtil.log('info', 'INFO: appFormSubmission - (2) doSubmission', app.id);
  const appId = app[id];
  async.waterfall([
    (callback) => {
      log4jUtil.log('info', 'Start Submission - Create Approval Cases');
      createApprovalCases(app, session, callback);
    }, (result, callback) => {
      // Update View
      log4jUtil.log('info', 'Start Submission - Update Views');
      dao.updateViewIndex('main', 'applicationsByAgent');
      dao.updateViewIndex('main', 'approvalDetails');
      callback(null, { success: true });
    }, (result, callback) => {
      // Submit the Case
      log4jUtil.log('info', 'Start Submission - Submit Application');
      if (app && app.bundleId) {
        log4jUtil.log('debug', `application.bundleId:${app.bundleId}`);
      }
      if (data && data.clientProfile && data.clientProfile.bundle) {
        log4jUtil.log('debug', `clientProfile.bundle:${JSON.stringify(data.clientProfile.bundle)}`);
      }
      if (data && data.clientProfile && data.clientProfile[rev]) {
        log4jUtil.log('debug', `ClientProfile.rev:${data.clientProfile[rev]}`);
      }
      if (data && data.clientProfile && data.clientProfile.lstChgDate) {
        log4jUtil.log('debug', `ClientProfile.lstChgDate:${data.clientProfile.lstChgDate}`);
      }
      submitApplication(data, app, session, callback);
    }, (modifiedApplication, callback) => {
      // Send Approval Email
      log4jUtil.log('info', 'Start Submission - Send Approval Notification');
      sendApprovalSubmittedCaseNotification(modifiedApplication, session, callback);
    }, (modifiedApplication, callback) => {
      // Send Submission Pos Email
      log4jUtil.log('info', 'Start Submission - Send Pos Email');
      prepareSubmissionEmail(data, modifiedApplication, session, callback);
    }], (err, callbackApplication) => {
    if (err) {
      log4jUtil.log('error', `ERROR startSubmission: ${appId}, ${err}`);
      cb('Fail in start Submission', null);
    } else {
      // Return modified application
      cb(null, callbackApplication);
    }
  });
};

const genAppFormPdfPaymentPageByData = (app, lang, callback) => {
  const trigger = {};
  const paymentValues = _.cloneDeep(app.payment);
  const ccy = _.get(app, 'applicationForm.values.proposer.extra.ccy');

  dao.getDoc(commonApp.TEMPLATE_NAME.PAYMENT_SHIELD, (paymentTmpl) => {
    log4jUtil.log('info', 'INFO: _genAppFormPdfPaymentPageByData - replaceAppFormValuesByTemplate start', app[id]);
    commonApp.replaceAppFormValuesByTemplate(
      paymentTmpl,
      app.payment,
      paymentValues,
      trigger,
      [],
      lang,
      ccy,
    );
    log4jUtil.log('info', 'INFO: _genAppFormPdfPaymentPageByData - replaceAppFormValuesByTemplate end', app[id]);
    const ccySign = utils.getCurrencySign(ccy);
    if (paymentValues.totCPFPortion > 0) {
      paymentValues.totCPFPortion = utils.getCurrency(paymentValues.totCPFPortion, ccySign, 2);
    }
    if (paymentValues.totCashPortion > 0) {
      paymentValues.totCashPortion = utils.getCurrency(paymentValues.totCashPortion, ccySign, 2);
    }
    if (paymentValues.totMedisave > 0) {
      paymentValues.totMedisave = utils.getCurrency(paymentValues.totMedisave, ccySign, 2);
    }

    const reportData = {
      root: {
        reportData: paymentValues,
        originalData: app.payment,
      },
    };

    reportData.root.reportData.pCid = app.pCid;

    new Promise((resolve, reject) => {
      dao.getDoc(commonApp.TEMPLATE_NAME.APP_FORM_MAPPING, (mapping) => {
        if (mapping && !mapping.error && mapping[app.quotation.baseProductCode]) {
          resolve(mapping[app.quotation.baseProductCode].paymentTemplate);
        } else {
          reject(new Error(`Fail to get ${commonApp.TEMPLATE_NAME.APP_FORM_MAPPING}`));
        }
      });
    }).then((paymentTemplate) => {
      const tplFiles = [];
      commonApp.getReportTemplates(
        tplFiles,
        [paymentTemplate.main, ...paymentTemplate.template],
        0,
        () => {
          if (tplFiles.length) {
            PDFHandler.getPremiumPaymentPdf(reportData, tplFiles, lang, (pdfStr) => {
              callback(pdfStr);
            });
          } else {
            log4jUtil.log('error', 'ERROR: _genAppFormPdfPaymentPageByData - end [RETURN=-101]', app[id]);
            callback(false);
          }
        },
      );
    }).catch((error) => {
      log4jUtil.log('info', 'ERROR: _genAppFormPdfPaymentPageByData - end [RETURN=-100]', app[id], error);
      callback(false);
    });
  });
};

const preparePaymentPDf = (app, lang, callback) => {
  // TODO: need uncomment after payment works
  genAppFormPdfPaymentPageByData(app, lang, (pdfStr) => {
    if (pdfStr && pdfStr.length > 0) {
      callback(null, pdfStr);
    } else {
      callback('Fail to generate Payment page for App Form');
    }
  });
  // callback(null, '');
};

const mergeAppFormPdf = (app, pdfStr, appformPdfStrs, callback) => {
  log4jUtil.log('info', `INFO: _mergeAppFormPdf - start${app[id]}`);
  async.waterfall([
    (cb) => {
      // let getPdfPromise = [];
      // getPdfPromise.push(new Promise((resolve, reject) => {
      //   appDao.getAppAttachment(app._id, commonApp.PDF_NAME.eAPP, (appPdf) => {
      //     if (appPdf.success) {
      //       resolve(appPdf);
      //     } else {
      //       reject(new Error('Fail to get attachment for proposer'));
      //     }
      //   });
      // }));
      // getPdfPromise = getPdfPromise.concat(app.iCids.map((iCid, i) => {
      //   return new Promise((resolve, reject) => {
      //     appDao.getAppAttachment(
      //  app._id, _signature.getAppFormAttachmentName(iCid), (appPdf) => {
      //       if (appPdf.success) {
      //         resolve(appPdf);
      //       } else {
      //         reject(new Error('Fail to get attachment for insured ' + i + ' ' + iCid));
      //       }
      //     });
      //   });
      // }));

      // Promise.all(getPdfPromise).then((appPdfs) => {
      //   cb(null, appPdfs);
      // }).catch((error) => {
      //   logger.error('ERROR: _mergeAppFormPdf', app._id, error);
      //   cb(error);
      // });
      cb(null, appformPdfStrs);
    },
    (appPdfs, cb) => {
      const mergePdfPromise = [];
      const phAppPdf = appPdfs[0];

      mergePdfPromise.push(new Promise((resolve, reject) => {
        CommonFunctions.mergePdfs([phAppPdf.data, pdfStr], (mergedPdf) => {
          if (mergedPdf) {
            log4jUtil.log('info', 'INFO: _mergeAppFormPdf - mergedPdf', '0', 'done', app[id]);
            resolve(mergedPdf);
          } else {
            reject(new Error('Fail to merge pdf for proposer'));
          }
        });
      }));

      for (let i = 1; i < appPdfs.length; i += 1) {
        const laAppPdf = appPdfs[i];
        mergePdfPromise.push(new Promise((resolve, reject) => {
          CommonFunctions.mergePdfs([phAppPdf.data, laAppPdf.data, pdfStr], (mergedPdf) => {
            if (mergedPdf) {
              log4jUtil.log('info', 'INFO: _mergeAppFormPdf - mergedPdf', i, 'done', app[id]);
              resolve(mergedPdf);
            } else {
              reject(new Error(`Fail to merge pdf for insured ${i}`));
            }
          });
        }));
      }

      Promise.all(mergePdfPromise).then((mergedPdfs) => {
        cb(null, mergedPdfs);
      }).catch((error) => {
        log4jUtil.log('error', 'ERROR: _mergeAppFormPdf', app[id], error);
        cb(error);
      });
    },
    (mergedPdfs, cb) => {
      const uploadInfoList = [];

      for (let i = 0; i < mergedPdfs.length; i += 1) {
        const uploadInfo = {
          attachName: '',
          mergedPdf: mergedPdfs[i],
        };

        if (i === 0) {
          uploadInfo.attachName = commonApp.PDF_NAME.eAPP;
        } else {
          const iCid = app.iCids[i - 1];
          uploadInfo.attachName = signature.getAppFormAttachmentName(iCid);
        }
        uploadInfoList.push(uploadInfo);
      }
      const uploadAttachment = (currApp, index, cbk) => {
        const { attachName, mergedPdf } = uploadInfoList[index];
        dao.uploadAttachmentByBase64(currApp[id], attachName, currApp[rev], mergedPdf, 'application/pdf', (res) => {
          if (res && res.rev && !res.error) {
            const tempCurrApp = currApp;
            tempCurrApp[rev] = res.rev;
            cbk(null, tempCurrApp, index + 1);
          } else {
            cbk(`Fail to upload Pdf attachment ${index}`);
          }
        });
      };

      const uploadFunctions = [];
      _.forEach(mergedPdfs, (item, i) => {
        if (i > 0) {
          uploadFunctions.push(uploadAttachment);
        }
      });

      async.waterfall([
        (cbk) => {
          uploadAttachment(app, 0, cbk);
        },
        ...uploadFunctions,
      ], (err, newApp, index) => {
        if (err) {
          JSON.stringify(index);
          cb(`ERROR: _mergeAppFormPdf - uploadAttachment ${err}`);
        } else {
          cb(null, newApp);
        }
      });
    },
  ], (err, newApp) => {
    if (err) {
      log4jUtil.log('error', 'ERROR: _mergeAppFormPdf - end [RETURN=-100]', app[id], err);
      callback('Fail to merge AppForm Pdf');
    } else {
      log4jUtil.log('info', 'INFO: _mergeAppFormPdf - end [RETURN=100]', app[id]);
      callback(null, newApp);
    }
  });
};

// const rollbackSubmission = (app, template, rollbackPdfs, rollbackDocs, cb) => {
//   async.waterfall([
//     (callback) => {
//       // rollback pdf
//       const cidMappingArr = _.get(app, 'iCids');
//       _rollbackPdf(app, rollbackPdfs, cidMappingArr, undefined, 0, callback);
//     }, (result, callback) => {
//       // rollback json
//       _rollbackDoc(app, rollbackDocs, callback);
//     },
//   ], (error, result) => {
//     if (error) {
//       cb(`ERROR in rollbacksubmission ${error}`);
//     } else {
//       app.submitStatus = 'FAIL';
//       cb({ success: false, application: app, template });
//     }
//   });
// };

const submitShiedCase = (app, agent, _clientProfile, req, res, next) => {
  const data = {
    appId: app[id],
    agentProfile: agent,
    clientProfile: _clientProfile,
    changedValues: {},
  };
  let appformPdfstrs;
  // let rollbackDoc = '';
  async.waterfall([
    (callback) => {
      // GetPdf Doc for rollback
      let getPdfPromise = [];
      getPdfPromise.push(new Promise((resolve, reject) => {
        appDao.getAppAttachment(app[id], commonApp.PDF_NAME.eAPP, (appPdf) => {
          if (appPdf.success) {
            resolve(appPdf);
          } else {
            reject(new Error('Fail to get attachment for proposer'));
          }
        });
      }));
      getPdfPromise = getPdfPromise.concat(
        app.iCids.map((iCid, i) => new Promise((resolve, reject) => {
          appDao.getAppAttachment(app[id], signature.getAppFormAttachmentName(iCid), (appPdf) => {
            if (appPdf.success) {
              resolve(appPdf);
            } else {
              reject(new Error(`Fail to get attachment for insured ${i} ${iCid}`));
            }
          });
        })),
      );

      Promise.all(getPdfPromise).then((appPdfs) => {
        appformPdfstrs = appPdfs;
        callback(null, { success: true });
      }).catch((error) => {
        log4jUtil.log('error', 'ERROR: prepare rollback in submission', app[id], error);
        callback(error);
      });
    }, (result, callback) => {
      // Get application, bundle, approval json
      const docArr = [_.get(app, 'bundleId'), app.id];
      const promiseArr = [];
      docArr.forEach((obj) => {
        promiseArr.push(new Promise((resolve, reject) => {
          JSON.stringify(reject);
          dao.getDoc(obj, (doc) => {
            resolve(doc);
          });
        }));
      });
      Promise.all(promiseArr).then((getDocresult) => {
        // rollbackDoc = getDocresult;
        JSON.stringify(getDocresult);
        callback(null, { success: true });
      }).catch((error) => {
        log4jUtil.log('info', 'Error in getAllDoc->Promise.all: ', error);
        callback(`Error in getAllDoc for rollback ${error}`);
      });
    }, (result, callback) => {
      // Prepare the payment Pdf
      // TODO: uncomment the preparePaymentPDf function
      preparePaymentPDf(app, 'en', callback);
    }, (pdfStr, callback) => {
      // merge to all AppForm Pdf
      mergeAppFormPdf(app, pdfStr, appformPdfstrs, callback);
    }, (newApp, callback) => {
      // Start Submission
      startSubmitSheid(data, app, { agent }, callback);
    }], (err, callbackApplication) => {
    if (err) {
      log4jUtil.log('error', `ERROR submission: ${app[id]}, ${err}`);

      // TO-DO: handle rollback application, and fail message
      // msg 1: Supervisor detail not available. Case cannot be submitted.
      // msg 2: Submission Fail. Please try again later

      // Call Roll Back here
      // rollbackSubmission(app, submissionTemplate, appformPdfstrs, rollbackDoc, cb);
      responseUtil.generalInternalServerErrorResponse(req, res, next, err);
    } else {
      // Return template and modified application
      // const frozenTemplate = _.cloneDeep(submissionTemplate);
      // commonApp.frozenTemplate(frozenTemplate);
      const promiseArr = [];
      // Get all application documents
      _.forEach(app.childIds, (childApp) => {
        promiseArr.push(new Promise((resolve, reject) => {
          JSON.stringify(reject);
          syncGatewayUtil.getDocFromSG(childApp, (getChildAppErr, doc) => {
            if (getChildAppErr) {
              reject(getChildAppErr);
            } else {
              resolve(doc);
            }
          });
        }));
      });

      JSON.stringify(callbackApplication);

      Promise.all(promiseArr).then((childResults) => {
        // Get master application document
        syncGatewayUtil.getDocFromSG(app[id], (getDocErr, result) => {
          if (getDocErr) {
            responseUtil.generalInternalServerErrorResponse(req, res, next, err);
          } else {
            // Get all attactments on master applicaiton document
            const attachment = '_attachments';
            const attachmentPromiseAr = [];
            _.each(result[attachment], (value, key) => {
              JSON.stringify(value);
              if (key.substring(0, 6) === 'appPdf') {
                attachmentPromiseAr.push(new Promise((resolve, reject) => {
                  syncGatewayUtil.getAttachmentByBase64(
                    app[id],
                    key,
                    (getAttactmentErr, getAttactmentResult) => {
                      if (getAttactmentErr) {
                        reject(getAttactmentErr);
                      } else {
                        resolve({
                          docId: app[id],
                          attchId: key,
                          rev: app[rev],
                          data: getAttactmentResult,
                        });
                      }
                    },
                  );
                }));
              }
            });
            Promise.all(attachmentPromiseAr).then((allAttactment) => {
              childResults.push(result);
              // Get bundle document
              syncGatewayUtil.getDocFromSG(app.bundleId, (getBundleErr, bundleResult) => {
                if (getBundleErr) {
                  responseUtil.generalInternalServerErrorResponse(req, res, next, getBundleErr);
                } else {
                  const policyPromiseArr = [];
                  // Get all policy documents
                  _.forEach(app.iCidMapping[app.pCid], (mapping) => {
                    policyPromiseArr.push(new Promise((resolve, reject) => {
                      JSON.stringify(reject);
                      syncGatewayUtil.getDocFromSG(
                        mapping.policyNumber,
                        (getChildAppErr, doc) => {
                          if (getChildAppErr) {
                            reject(getChildAppErr);
                          } else {
                            resolve(doc);
                          }
                        },
                      );
                    }));
                  });
                  JSON.stringify(callbackApplication);
                  Promise.all(policyPromiseArr).then((policyDocs) => {
                    const spDocId = `SP${app[id].substring(2)}`;
                    syncGatewayUtil.getDocFromSG(spDocId, (getSPErr, getSPResult) => {
                      if (getSPErr) {
                        log4jUtil.log('error', `Get SP Err:${JSON.stringify(getSPErr)}`);
                        responseUtil.generalInternalServerErrorResponse(req, res, next, getSPErr);
                      } else {
                        policyDocs.push(getSPResult);
                        // Response result to iOS
                        responseUtil.generalSuccessResponse(req, res, next, {
                          application: childResults,
                          bundle: bundleResult,
                          policyDocument: policyDocs,
                          appPdf: allAttactment,
                        });
                      }
                    });
                  });
                }
              });
            }).catch((error) => {
              responseUtil.generalInternalServerErrorResponse(req, res, next, error);
            });
          }
        });
      }).catch((error) => {
        responseUtil.generalInternalServerErrorResponse(req, res, next, error);
      });
    }
  });
};

module.exports.submit = (req, res, next) => {
  log4jUtil.log('info', `Submit docId:${req.body.docId}`);
  log4jUtil.log('info', 'Start get application document');
  getApplication(req.body.docId, (getApplicationErr, app) => {
    if (getApplicationErr) {
      log4jUtil.log('error', `getApplicationErr:${JSON.stringify(getApplicationErr)}`);
      responseUtil.generalInternalServerErrorResponse(req, res, next, getApplicationErr);
    } else {
      log4jUtil.log('info', 'Get application document successfully');
      log4jUtil.log('info', 'Start get agent document');
      getAgent(app.quotation.agent.agentCode, (getAgentErr, agent) => {
        if (getAgentErr) {
          log4jUtil.log('error', `getAgentErr:${JSON.stringify(getAgentErr)}`);
          responseUtil.generalInternalServerErrorResponse(req, res, next, getAgentErr);
        } else {
          log4jUtil.log('info', 'Get agent document successfully');
          log4jUtil.log('info', 'Start get cp document');
          syncGatewayUtil.getDocFromSG(app.pCid, (getClientProfileErr, _clientProfile) => {
            if (getClientProfileErr) {
              log4jUtil.log('error', `getClientProfileErr:${JSON.stringify(getClientProfileErr)}`);
              responseUtil.generalInternalServerErrorResponse(req, res, next, getClientProfileErr);
            } else if (req.body.docId.substring(0, 2) === 'NB') {
              log4jUtil.log('info', 'Start submit non-shield case');
              submitNonShiedCase(app, agent, _clientProfile, req, res, next);
            } else {
              log4jUtil.log('info', 'Start submit shield case');
              submitShiedCase(app, agent, _clientProfile, req, res, next);
            }
          });
        }
      });
    }
  });
};
