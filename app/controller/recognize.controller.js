const fs = require('fs');
const request = require('request');
const responseUtil = require('../utils/response.util');
const log4jUtil = require('../utils/log4j.util');
const commonUtil = require('../utils/common.util');

module.exports.idRecognize = (req, res, next) => {
  const formData = {
    country: req.body.country,
    idCard: {
      value: fs.createReadStream(req.file.path),
      options: {
        filename: req.file.originalname,
        contentType: req.file.mimetype,
      },
    },
  };
  log4jUtil.log('url =', req.body.url);
  log4jUtil.log('country =', req.body.country);
  // request.post({ url: 'http://eab-ocr-api.intraeab/documentation/recognize/id', formData },
  request.post({ url: `${commonUtil.getConfig('OCR_BASE_URL')}/documentation/recognize/id`, formData },
    (err, httpResponse, body) => {
      fs.unlinkSync(`./uploads/${req.file.filename}`);
      if (err) {
        return responseUtil.generalInternalServerErrorResponse(req, res, next, err);
      }
      log4jUtil.log('Upload successful!  Server responded with:', body);
      log4jUtil.log('httpResponse.status:', httpResponse.statusCode);
      if (httpResponse.statusCode === 200) {
        return responseUtil.generalSuccessResponse(req, res, next, JSON.parse(body).result);
      }
      return responseUtil.generalInternalServerErrorResponse(req, res, next, 'ocr api error');
    });
};

module.exports.idRecognizeBase64 = (req, res, next) => {
  const base64Data = req.body.idCard.replace(/^data:image\/\w+;base64,/, '');
  const dataBuffer = Buffer.from(base64Data, 'base64');
  const filename = `${Date.now()}image.png`;
  fs.writeFile(`uploads/${filename}`, dataBuffer, (err) => {
    // log4jUtil.log('info', 'Start get payment url.');

    if (err) {
      log4jUtil.log('info', 'OCR :: ERR1');
      log4jUtil.log('info', err);
      log4jUtil.log('info', JSON.stringify(err));
      return responseUtil.generalInternalServerErrorResponse(req, res, next, err);
    }
    const formData = {
      country: 'HK',
      idCard: fs.createReadStream(`uploads/${filename}`),
    };
    request.post({ url: `${commonUtil.getConfig('OCR_BASE_URL')}/documentation/recognize/id`, formData },
      (err1, httpResponse, body) => {
        fs.unlinkSync(`./uploads/${filename}`);
        if (err1) {
          log4jUtil.log('info', 'OCR :: ERR2');
          log4jUtil.log('info', err1);
          log4jUtil.log('info', JSON.stringify(err1));
          return responseUtil.generalInternalServerErrorResponse(req, res, next, err1);
        }
        // log4jUtil.log('info', 'Upload successful!  Server responded with:${body}');
        // log4jUtil.log('httpResponse.status:', httpResponse.statusCode);
        log4jUtil.log('info', 'OCR :: INFO');
        log4jUtil.log('info', body);
        log4jUtil.log('info', JSON.stringify(body));
        if (httpResponse.statusCode === 200) {
          return responseUtil.generalSuccessResponse(req, res, next, JSON.parse(body).result);
        }
        return responseUtil.generalInternalServerErrorResponse(req, res, next, 'ocr api error');
      });
    return '';
  });
};
