const _ = require('lodash');
const moment = require('moment');

const responseUtil = require('../utils/response.util');
const emailUtil = require('../utils/email.util');
const log4jUtil = require('../utils/log4j.util');

module.exports.sendEmail = (req, res, next) => {
  if (req.body.from && req.body.to && req.body.title && req.body.userType) {
    if (req.body.userType === 'agent' || req.body.userType === 'client') {
      if ((req.body.userType === 'agent' && req.body.agentCode) || (req.body.userType === 'client' && req.body.dob)) {
        if (emailUtil.validate(req.body.from) && emailUtil.validate(req.body.to)) {
          log4jUtil.log('info', `from:${req.body.from}, to: ${req.body.to}, title: ${req.body.title}, userType: ${req.body.userType}, agentCode: ${req.body.agentCode}, dob: ${req.body.dob}`);
          const setPasswordForPdfPromiseArr = [];
          _.forEach(req.body.attachments, (attachment, key) => {
            if (attachment.isProtectBase) {
              setPasswordForPdfPromiseArr.push(new Promise((resolve, reject) => {
                let password = '';
                if (req.body.userType === 'agent') {
                  const code = req.body.agentCode;
                  password = code.substr(code.length - 6, 6);
                } else {
                  password = moment(req.body.dob).format('DDMMMYYYY').toUpperCase();
                }
                log4jUtil.log('info', `Pdf attactment ${attachment.fileName} password: ${password}`);
                emailUtil.setPasswordForPdf(
                  attachment.data,
                  password,
                  (setPasswordForPdfErr, setPasswordForPdfResult) => {
                    if (setPasswordForPdfErr) {
                      reject(setPasswordForPdfErr);
                    } else {
                      req.body.attachments[key].data = setPasswordForPdfResult;
                      resolve(setPasswordForPdfResult);
                    }
                  },
                );
              }));
            }
          });

          Promise.all(setPasswordForPdfPromiseArr).then((setPasswordForPdfsResult) => {
            JSON.stringify(setPasswordForPdfsResult);
            emailUtil.send({
              from: req.body.from,
              to: req.body.to,
              title: req.body.title,
              content: req.body.content,
              attachments: req.body.attachments,
            }, (err, result) => {
              if (err) {
                responseUtil.generalInternalServerErrorResponse(req, res, next, err);
              } else {
                JSON.stringify(result);
                responseUtil.generalSuccessResponse(req, res, next, {});
              }
            });
          }).catch((error) => {
            responseUtil.generalInternalServerErrorResponse(req, res, next, error);
          });
        } else {
          responseUtil.generalBadRequestResponse(req, res, next, 'incorrect from or to email format');
        }
      } else {
        responseUtil.generalBadRequestResponse(req, res, next, 'userType is not match to agentCode or dob');
      }
    } else {
      responseUtil.generalBadRequestResponse(req, res, next, 'userType error');
    }
  } else {
    responseUtil.generalBadRequestResponse(req, res, next, 'from, to, title and userType are required');
  }
};
