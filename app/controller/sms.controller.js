const responseUtil = require('../utils/response.util');
const log4jUtil = require('../utils/log4j.util');
const smsUtil = require('../utils/sms.util');

module.exports.sendSMS = (req, res, next) => {
  if (req.body.mobileNumber && req.body.content) {
    smsUtil.send(req.body.mobileNumber, req.body.content, (err, result) => {
      if (err) {
        responseUtil.generalInternalServerErrorResponse(req, res, next, err);
      } else {
        responseUtil.generalSuccessResponse(req, res, next, result);
      }
    });
  } else {
    log4jUtil.log('error', 'mobileNumber and content are required.');
    responseUtil.generalBadRequestResponse(req, res, next, 'mobileNumber and content are required.');
  }
};
