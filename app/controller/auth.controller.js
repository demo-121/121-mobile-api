const axios = require('axios');
const _ = require('lodash');
const compareVersion = require('compare-versions');
const nodeRSA = require('node-rsa');

const responseUtil = require('../utils/response.util');
const commonUtil = require('../utils/common.util');
const log4jUtil = require('../utils/log4j.util');
const syncGatewayUtil = require('../utils/syncGateway.util');
const dao = require('../core/bz/cbDaoFactory/remoteCB');

module.exports.getPublicKey = (req, res, next) => {
  axios.request({
    url: '/api/mobile/queryRSAKeyPair',
    method: 'post',
    baseURL: `${commonUtil.getConfig('INTERNAL_API_DOMAIN')}`,
    params: {
      agentId: req.user.id
    },
  }).then((response) => {
    if (response.status === 200) {
      if (response.data.publicKey && response.data.privateKey) {
        responseUtil.generalSuccessResponse(req, res, next, {
          publicKey: response.data.publicKey,
        });
      } else {
        // Generate RSA 1024 key pair
        const key = new nodeRSA();
        key.generateKeyPair(1024);
        const publicKey = key.exportKey('public');
        log4jUtil.log('info', `Public key: ${publicKey}`);
        const privateKey = key.exportKey('private');
        log4jUtil.log('info', `Private key: ${privateKey}`);
        // Store RSA 1024 key pair
        log4jUtil.log('info',  'Start store the key pair to oracle');
        axios.request({
          url: '/api/mobile/storeRSAKeyPair',
          method: 'post',
          baseURL: `${commonUtil.getConfig('INTERNAL_API_DOMAIN')}`,
          params: {
            userId: req.user.id,
            privateKey: privateKey,
            publicKey: publicKey,
          },
        }).then((response) => {
          if (response.status === 200) {
            responseUtil.generalSuccessResponse(req, res, next, {
              publicKey: publicKey,
            });
          } else {
            log4jUtil.log('error', 'Store key pair error');
            responseUtil.generalInternalServerErrorResponse(req, res, next, 'Store private key error');
          }
        }).catch((error) => {
          if (error.response) {
            // The request was made and the server responded with a status code
            // that falls out of the range of 2xx
            log4jUtil.log('error', `Store key pair error, data:${JSON.stringify(error.response.data)}`);
            log4jUtil.log('error', `Store key pair error, status:${error.response.status}`);
            log4jUtil.log('error', `Store key pair error, headers:${JSON.stringify(error.response.headers)}`);
            log4jUtil.log('error', `Store key pair error, error.response: ${error.response}`);
            responseUtil.generalInternalServerErrorResponse(req, res, next, JSON.stringify(error.response.data));
          } else if (error.request) {
            // The request was made but no response was received
            // `error.request` is an instance of XMLHttpRequest in the browser and an instance of
            // http.ClientRequest in node.js
            log4jUtil.log('error', `Store key pair error, error.response: ${error.request}`);
            responseUtil.generalInternalServerErrorResponse(req, res, next, JSON.stringify(error.request));
          } else {
            // Something happened in setting up the request that triggered an Error
            log4jUtil.log('error', `Store key pair error, error.response: ${error.message}`);
            responseUtil.generalInternalServerErrorResponse(req, res, next, JSON.stringify(error.request));
          }
        });
      }
    } else {
      log4jUtil.log('error', 'Query private key error');
      log4jUtil.log('error', `response.status: ${response.status}`);
      log4jUtil.log('error', `response.data: ${JSON.stringify(response.data)}`);
      responseUtil.generalInternalServerErrorResponse(req, res, next, 'Query private key error');
    }
  }).catch((error) => {
    if (error.response) {
      // The request was made and the server responded with a status code
      // that falls out of the range of 2xx
      log4jUtil.log('error', `Query key pair error, data:${JSON.stringify(error.response.data)}`);
      log4jUtil.log('error', `Query key pair error, status:${error.response.status}`);
      log4jUtil.log('error', `Query key pair error, headers:${JSON.stringify(error.response.headers)}`);
      log4jUtil.log('error', `Query key pair error, error.response: ${error.response}`);
      responseUtil.generalInternalServerErrorResponse(req, res, next, JSON.stringify(error.response.data));
    } else if (error.request) {
      // The request was made but no response was received
      // `error.request` is an instance of XMLHttpRequest in the browser and an instance of
      // http.ClientRequest in node.js
      log4jUtil.log('error', `Query key pair error, error.response: ${error.request}`);
      responseUtil.generalInternalServerErrorResponse(req, res, next, JSON.stringify(error.request));
    } else {
      // Something happened in setting up the request that triggered an Error
      log4jUtil.log('error', `Query key pair error, error.response: ${error.message}`);
      responseUtil.generalInternalServerErrorResponse(req, res, next, JSON.stringify(error.request));
    }
  });
};

const versionCompare = (currentNumber, targetNumber) => {
  const compareVersionResult = compareVersion(currentNumber, targetNumber);
  if (compareVersionResult >= 0) {
    return true;
  }
  return false;
};

const checkIfNeedUpgrade = (currentVersionNumber, currentBuildNumber) => {
  const isCheckVersionNumber = commonUtil.getConfig('IS_CHECK_VERSION_NUMBER');
  const isCheckBuildNumber = commonUtil.getConfig('IS_CHECK_BUILD_NUMBER');
  const targetVersionNumber = commonUtil.getConfig('TARGET_VERSION_NUMBER');
  const targetBuildNumber = commonUtil.getConfig('TARGET_BUILD_NUMBER');
  let isEqualToOrMoreThanTargetVersionNumber = true;
  let isEqualToOrMoreThanTargetBuildNumber = true;
  if (isCheckVersionNumber && isCheckVersionNumber.toString() === 'true') {
    isEqualToOrMoreThanTargetVersionNumber = versionCompare(
      currentVersionNumber,
      targetVersionNumber
    );
  }
  if (isCheckBuildNumber && isCheckBuildNumber.toString() === 'true') {
    const current = parseInt(currentBuildNumber, 10);
    const target = parseInt(targetBuildNumber,10);
    isEqualToOrMoreThanTargetBuildNumber = current >= target;
  }
  if (isEqualToOrMoreThanTargetVersionNumber) {
    if (currentVersionNumber === targetVersionNumber) {
      if (isEqualToOrMoreThanTargetBuildNumber) {
        return false;
      }
      return true;
    }
    return false;
  }
  return true;
};

async function getAndPutAttactments(rows, cb) {
  let i = 0;
  const revs = {};
  log4jUtil.log('info', 'Start getAndPutAttactments');
  for (;i < rows.length; i += 1) {
    if (_.get(revs, rows[i].id)) {
      await new Promise((resolve, reject) => {
        syncGatewayUtil.getAttachmentByBase64(rows[i].id, rows[i].value.name, (getAttErr, getAttResult) => {
          if (getAttErr) {
            reject(getAttErr);
          } else {
            const rev = '_rev';
            dao.uploadAttachmentByBase64(
              rows[i].id,
              rows[i].value.name,
              _.get(revs, rows[i].id),
              getAttResult,
              rows[i].value.attactment.content_type,
              (uploadAttResult) => {
                if (uploadAttResult.ok) {
                  revs[rows[i].id] = uploadAttResult.rev;
                  resolve(uploadAttResult);
                } else {
                  reject(uploadAttResult);
                }
              }
            );
          }
        });
      });
    } else {
      await new Promise((resolve, reject) => {
        log4jUtil.log('info', `Start get doc. rows[i].id = ${rows[i].id}`);
        syncGatewayUtil.getDocFromSG(rows[i].id, (getDocErr, getDocResult) => {
          log4jUtil.log('info', 'Get doc successfully');
          syncGatewayUtil.getAttachmentByBase64(rows[i].id, rows[i].value.name, (getAttErr, getAttResult) => {
            if (getAttErr) {
              reject(getAttErr);
            } else {
              const rev = '_rev';
              dao.uploadAttachmentByBase64(
                rows[i].id,
                rows[i].value.name,
                getDocResult[rev],
                getAttResult,
                rows[i].value.attactment.content_type,
                (uploadAttResult) => {
                  if (uploadAttResult.ok) {
                    revs[rows[i].id] = uploadAttResult.rev;
                    resolve(uploadAttResult);
                  } else {
                    reject(uploadAttResult);
                  }
                }
              );
            }
          });
        });
      });
    }
    if (i === rows.length - 1) {
      log4jUtil.log('info', `Call back to updateRev function. i = ${i}`);
      cb();
    }
  }
}

const getSessionCookie = (idToken, cb) => {
  log4jUtil.log('info', 'Start post _session to get session cookie');
  axios.request({
    baseURL: `${commonUtil.getConfig('GATEWAY_URL')}:${commonUtil.getConfig('GATEWAY_PORT')}`,
    url: `/${commonUtil.getConfig('GATEWAY_DBNAME')}/_session`,
    method: 'POST',
    headers: {
      Authorization: `Bearer ${idToken}`,
    },
  }).then((getSessionCookieResult) => {
    if (getSessionCookieResult && getSessionCookieResult.status === 200) {
      cb(null, getSessionCookieResult);
    } else {
      cb('Get session cookie error', null);
    }
  }).catch((error) => {
    if (error.response) {
      // The request was made and the server responded with a status code
      // that falls out of the range of 2xx
      log4jUtil.log('error', `Get session cookie error, data:${JSON.stringify(error.response.data)}`);
      log4jUtil.log('error', `Get session cookie error, status:${error.response.status}`);
      log4jUtil.log('error', `Get session cookie error, headers:${JSON.stringify(error.response.headers)}`);
      log4jUtil.log('error', `Get session cookie error, error.response: ${error.response}`);
      cb('Create channel user error', null);
    } else if (error.request) {
      // The request was made but no response was received
      // `error.request` is an instance of XMLHttpRequest in the browser and an instance of
      // http.ClientRequest in node.js
      log4jUtil.log('error', `Get session cookie error, error.response: ${error.request}`);
      cb('Create channel user error', null);
    } else {
      // Something happened in setting up the request that triggered an Error
      log4jUtil.log('error', `Get session cookie error, error.response: ${error.message}`);
      cb('Create channel user error', null);
    }
  });
};

const createChannelUser = (channelUserName, agentCode, cb) => {
  axios.request({
    baseURL: `${commonUtil.getConfig('GATEWAY_ADMIN_URL')}:${commonUtil.getConfig('GATEWAY_ADMIN_PORT')}`,
    url: `/${commonUtil.getConfig('GATEWAY_DBNAME')}/_user/${channelUserName}`,
    method: 'PUT',
    data: {
      name: channelUserName,
      admin_channels: [`${agentCode}_private_data`, 'agent_hierarchy'],
      disabled: false,
    },
  }).then((createChannelUserResult) => {
    if (createChannelUserResult && createChannelUserResult.status === 200) {
      cb(null, createChannelUserResult);
    } else {
      cb('Create channel user error', null);
    }
  }).catch((error) => {
    if (error.response) {
      // The request was made and the server responded with a status code
      // that falls out of the range of 2xx
      log4jUtil.log('error', `Create channel user error, data:${JSON.stringify(error.response.data)}`);
      log4jUtil.log('error', `Create channel user error, status:${error.response.status}`);
      log4jUtil.log('error', `Create channel user error, headers:${JSON.stringify(error.response.headers)}`);
      log4jUtil.log('error', `Create channel user error, error.response: ${error.response}`);
      cb('Create channel user error', null);
    } else if (error.request) {
      // The request was made but no response was received
      // `error.request` is an instance of XMLHttpRequest in the browser and an instance of
      // http.ClientRequest in node.js
      log4jUtil.log('error', `Create channel user error, error.response: ${error.request}`);
      cb('Create channel user error', null);
    } else {
      // Something happened in setting up the request that triggered an Error
      log4jUtil.log('error', `Create channel user error, error.response: ${error.message}`);
      cb('Create channel user error', null);
    }
  });
};

const updateRev = (agentId, idToken, cb) => {
  // If doc.attactment.revpos > doc._rev, we can't sync data.
  // Solution: Get each attactments and put again
  log4jUtil.log('info', `Start get agent document from sync gateway: U_${agentId}`);
  syncGatewayUtil.getDocFromSG(`U_${agentId}`, (getAgentErr, getAgentResult) => {
    if (getAgentResult && getAgentResult.agentCode) {
      log4jUtil.log('info', 'Get agent document from sync gateway successfully');
      log4jUtil.log('info', `Start get agentDocuments view, key=["01", "${getAgentResult.agentCode}"]`);
      // Get need sync data
      axios.request({
        url: `/${commonUtil.getConfig('GATEWAY_DBNAME')}/_design/dataSync/_view/agentDocuments?key=["01","${getAgentResult.agentCode}"]`,
        method: 'get',
        baseURL: `${commonUtil.getConfig('GATEWAY_URL')}:${commonUtil.getConfig('GATEWAY_PORT')}`,
        auth: {
          username: commonUtil.getConfig('GATEWAY_USER'),
          password: commonUtil.getConfig('GATEWAY_PW'),
        },
      }).then((getAgentDocumentsResult) => {
        if (
          getAgentDocumentsResult
          && getAgentDocumentsResult.data
          && getAgentDocumentsResult.data.rows
        ) {
          const syncDocIds = [''];
          syncGatewayUtil.getView('main', 'products', '', (err, result) => {
            if (result.rows && result.rows.length > 0) {
              _.forEach(result.rows, (row) => {
                if (row && (
                  row.id.indexOf(commonUtil.getConfig('PRODUCT_DATA_SYNC_KEY_WORD').toUpperCase()) > -1
                  || row.id.indexOf(commonUtil.getConfig('PRODUCT_DATA_SYNC_KEY_WORD').toLowerCase()) > -1)                
                ) {
                  syncDocIds.push(row.id);
                }
              });
            }
            _.forEach(getAgentDocumentsResult.data.rows, (doc) => {
              syncDocIds.push(doc.id);
            });
            cb(null, {
              syncDocumentIds: syncDocIds,
            });
          });
          
          // if (idToken) {
          //   getSessionCookie(idToken, (getSessionErr, getSessionCookieResult) => {
          //     if (getSessionErr) {
          //       cb(getSessionErr, null);
          //     } else {
          //       log4jUtil.log('info', 'Get session cookie successfully');
          //       if (getSessionCookieResult.headers && getSessionCookieResult.headers['set-cookie'] && getSessionCookieResult.headers['set-cookie'][0]) {
          //         if (
          //           getSessionCookieResult.data.userCtx
          //           && getSessionCookieResult.data.userCtx.name
          //         ) {
          //           createChannelUser(
          //             getSessionCookieResult.data.userCtx.name,
          //             getAgentResult.agentCode,
          //             (createChannelUserErr, createChannelUserResult) => {
          //               if (createChannelUserResult && createChannelUserResult.status === 200) {
          //               // Update attactment revpos
          //                 log4jUtil.log('info', 'Start get notMatchRevDocuments view');
          //                 axios.request({
          //                   url: `/${commonUtil.getConfig('GATEWAY_DBNAME')}/_design/dataSync/_view/notMatchRevDocuments?key=["01","${getAgentResult.agentCode}"]&stale=false`,
          //                   method: 'get',
          //                   baseURL: `${commonUtil.getConfig('GATEWAY_URL')}:${commonUtil.getConfig('GATEWAY_PORT')}`,
          //                   auth: {
          //                     username: commonUtil.getConfig('GATEWAY_USER'),
          //                     password: commonUtil.getConfig('GATEWAY_PW'),
          //                   },
          //                 }).then((notMatchRevDocResponse) => {
          //                   if (notMatchRevDocResponse.data && notMatchRevDocResponse.data.rows) {
          //                     log4jUtil.log('info', 'Get notMatchRevDocuments view successfully');
          //                     if (notMatchRevDocResponse.data.rows.length === 0) {
          //                       log4jUtil.log('debug', 'notMatchRevDocResponse.data.rows.length: 0');
          //                       cb(null, {
          //                         syncDocumentIds: syncDocIds,
          //                         cookie: getSessionCookieResult.headers['set-cookie'][0],
          //                       });
          //                     } else {
          //                       log4jUtil.log('debug', `notMatchRevDocResponse.data.rows.length:${notMatchRevDocResponse.data.rows.length}`);
          //                       getAndPutAttactments(notMatchRevDocResponse.data.rows, () => {
          //                         cb(null, {
          //                           syncDocumentIds: syncDocIds,
          //                           cookie: getSessionCookieResult.headers['set-cookie'][0],
          //                         });
          //                       });
          //                     }
          //                   } else {
          //                     log4jUtil.log('error', 'Get notMatchRevDocuments view faile');
          //                     cb('Get notMatchRevDocuments view faile', null);
          //                   }
          //                 });
          //               } else {
          //                 log4jUtil.log('error', 'Create channel user error');
          //                 cb(createChannelUserErr, null);
          //               }
          //             },
          //           );
          //         } else {
          //           log4jUtil.log('error', 'Get channel user name error');
          //           cb('Get channel user name error', null);
          //         }
          //       } else {
          //         log4jUtil.log('error', 'Get cookie error');
          //         cb('Get cookie error', null);
          //       }
          //     }
          //   });
          // } else {
          //   cb(null, {
          //     syncDocumentIds: syncDocIds,
          //     cookie: null,
          //   });
          // }
          // log4jUtil.log('info', 'Start get notMatchRevDocuments view');
          // axios.request({
          //   url: `/${commonUtil.getConfig('GATEWAY_DBNAME')}/_design/dataSync/_view/notMatchRevDocuments?key=["01","${getAgentResult.agentCode}"]&stale=false`,
          //   method: 'get',
          //   baseURL: `${commonUtil.getConfig('GATEWAY_URL')}:${commonUtil.getConfig('GATEWAY_PORT')}`,
          //   auth: {
          //     username: commonUtil.getConfig('GATEWAY_USER'),
          //     password: commonUtil.getConfig('GATEWAY_PW'),
          //   },
          //   }).then((notMatchRevDocResponse) => {
          //     if (notMatchRevDocResponse.data && notMatchRevDocResponse.data.rows) {
          //       log4jUtil.log('info', 'Get notMatchRevDocuments view successfully');
          //       if (notMatchRevDocResponse.data.rows.length === 0) {
          //         log4jUtil.log('debug', 'notMatchRevDocResponse.data.rows.length: 0');
          //         cb(null, {
          //           syncDocumentIds: syncDocIds,
          //         });
          //       } else {
          //         log4jUtil.log('debug', `notMatchRevDocResponse.data.rows.length:${notMatchRevDocResponse.data.rows.length}`);
          //         getAndPutAttactments(notMatchRevDocResponse.data.rows, () => {
          //           cb(null, {
          //             syncDocumentIds: syncDocIds,
          //           });
          //         });
          //       }
          //     } else {
          //       log4jUtil.log('error', 'Get notMatchRevDocuments view faile');
          //       cb('Get notMatchRevDocuments view faile', null);
          //     }
          //   });
          
        } else {
          log4jUtil.log('error', 'Get agentDocuments view error');
          cb('Get agentDocuments view error');
        }
      }).catch((error) => {
        cb(error, null);
      });
    } else {
      log4jUtil.log('error', `Get agent document from sync gateway error: ${JSON.stringify(getAgentErr)}`);
      cb(getAgentErr, null);
    }
  });
};

module.exports.check = (req, res, next) => {
  responseUtil.generalSuccessResponse(req, res, next, {
    isNeedUpgrade: false,
    isDeactiveDevice: false,
  });
  // if (req.body.versionNumber && req.body.buildNumber && req.body.installationId) {
  //   log4jUtil.log('info', `Version number: ${req.body.versionNumber}`);
  //   log4jUtil.log('info', `Build number: ${req.body.buildNumber}`);
  //   log4jUtil.log('info', `Installation id: ${req.body.installationId}`);
  //   const versionNumberRegex = /^\d+.\d+.\d+$/;
  //   const buildNumberRegex = /^\d+$/;
  //   if (versionNumberRegex.test(req.body.versionNumber) && versionNumberRegex.test(commonUtil.getConfig('TARGET_VERSION_NUMBER'))) {
  //     if (buildNumberRegex.test(req.body.buildNumber) && buildNumberRegex.test(commonUtil.getConfig('TARGET_BUILD_NUMBER'))) {
  //       axios.request({
  //         url: '/api/mobile/getInstallationInfo',
  //         method: 'post',
  //         baseURL: `${commonUtil.getConfig('INTERNAL_API_DOMAIN')}`,
  //         params: {
  //           userId: req.user.id,
  //           installationId: req.body.installationId,
  //         },
  //       }).then((response) => {
  //         if (response.status === 200 && response.data.status === 'A') {
  //           responseUtil.generalSuccessResponse(req, res, next, {
  //             isNeedUpgrade: checkIfNeedUpgrade(req.body.versionNumber, req.body.buildNumber),
  //             isDeactiveDevice: false,
  //           });
  //         } else {
  //           responseUtil.generalSuccessResponse(req, res, next, {
  //             isNeedUpgrade: checkIfNeedUpgrade(req.body.versionNumber, req.body.buildNumber),
  //             isDeactiveDevice: true,
  //           });
  //         }
  //       }).catch((error) => {
  //         if (error.response) {
  //           log4jUtil.log('error', `data::${JSON.stringify(error.response.data)}`);
  //           log4jUtil.log('error', `status:${error.response.status}`);
  //           log4jUtil.log('error', `headers:${JSON.stringify(error.response.headers)}`);
  //           log4jUtil.log('error', `error.response: ${error.response}`);
  //           // The request was made and the server responded with a status code
  //           // that falls out of the range of 2xx
  //           responseUtil.generalInternalServerErrorResponse(req, res, next, {
  //             data: error.response.data,
  //             status: error.response.status,
  //             headers: error.response.headers,
  //           });
  //         } else if (error.request) {
  //           log4jUtil.log('error', `error.response: ${error.request}`);
  //           // The request was made but no response was received
  //           // `error.request` is an instance of XMLHttpRequest in the browser and an instance of
  //           // http.ClientRequest in node.js
  //           responseUtil.generalInternalServerErrorResponse(req, res, next, error.request);
  //         } else {
  //           log4jUtil.log('error', `error.response: ${error.message}`);
  //           // Something happened in setting up the request that triggered an Error
  //           responseUtil.generalInternalServerErrorResponse(req, res, next, error.message);
  //         }
  //         log4jUtil.log('error', `Config:${error.config}`);
  //       });
  //     } else {
  //       log4jUtil.log('error', 'buildNumber is not follow the format');
  //       responseUtil.generalBadRequestResponse(req, res, next, 'buildNumber is not follow the format');
  //     }
  //   } else {
  //     log4jUtil.log('error', 'versionNumber is not follow the format');
  //     responseUtil.generalBadRequestResponse(req, res, next, 'versionNumber is not follow the format');
  //   }
  // } else {
  //   log4jUtil.log('error', 'version number, build number and installation id are required.');
  //   responseUtil.generalBadRequestResponse(req, res, next, 'version number, build number and installation id are required.');
  // }
};

module.exports.activateUser = (req, res, next) => {
  const reqBody = req.body;
  log4jUtil.log('info', `[7]req.body: ${JSON.stringify(req.body)}`);
  if (reqBody.device_model && reqBody.device_id && reqBody.versionNumber && reqBody.buildNumber) {
    log4jUtil.log('info', '[7a]reqBody.device_model && reqBody.device_id exists');
    log4jUtil.log('info', `[8]Start request EASE-API, baseURL: ${commonUtil.getConfig('INTERNAL_API_DOMAIN')}, agentId:${req.user.id}`);
    axios.request({
      url: '/api/mobile/register',
      method: 'post',
      baseURL: `${commonUtil.getConfig('INTERNAL_API_DOMAIN')}`,
      params: {
        agentId: `${req.user.id}`,
        deviceId: reqBody.device_id,
        deviceModel: reqBody.device_model,
        versionNumber: reqBody.versionNumber,
        buildNumber: reqBody.buildNumber,
        type: 'activation',
        brand: reqBody.brand,
        carrier: reqBody.carrier,
        deviceCountry: reqBody.deviceCountry,
        deviceLocale: reqBody.deviceLocale,
        deviceName: reqBody.deviceName,
        fontScale: reqBody.fontScale,
        freeDiskStorage: reqBody.freeDiskStorage,
        ipAddress: reqBody.ipAddress,
        installReferrer: reqBody.installReferrer,
        instanceID: reqBody.instanceID,
        macAddress: reqBody.macAddress,
        manufacturer: reqBody.manufacturer,
        maxMemory: reqBody.maxMemory,
        phoneNumber: reqBody.phoneNumber,
        serialNumber: reqBody.serialNumber,
        systemVersion: reqBody.systemVersion,
        timezone: reqBody.timezone,
        storageSize: reqBody.storageSize,
        totalMemory: reqBody.totalMemory,
        userAgent: reqBody.userAgent,
        is24Hour: reqBody.is24Hour,
        isEmulator: reqBody.isEmulator,
        isPinOrFingerprintSet: reqBody.isPinOrFingerprintSet,
        isTablet: reqBody.isTablet,
        hasNotch: reqBody.hasNotch,
        isLandscape: reqBody.isLandscape,
        deviceType: reqBody.deviceType,
        systemName: reqBody.systemName,
        firstInstallTime: reqBody.firstInstallTime
      },
    }).then((response) => {
      log4jUtil.log('info', '[8a]Response case1');
      if (response.status === 200) {
        log4jUtil.log('info', `[9a]Response status == 200, response.data: ${JSON.stringify(response.data)}`);
        updateRev(req.user.id, reqBody.id_token, (updateRevErr, updateRevResult) => {
          if (updateRevErr) {
            log4jUtil.log('error', `updateRevErr:${JSON.stringify(updateRevErr)}`);
            responseUtil.generalInternalServerErrorResponse(
              req,
              res,
              next,
              JSON.stringify(updateRevErr),
            );
          } else {
            log4jUtil.log('info', 'Update revision number successfully');
            responseUtil.generalSuccessResponse(req, res, next, {
              user_id: `${req.user.id}`,
              doc_seq: response.data.docSeq,
              installation_id: response.data.installationId,
              isChangeDevice: response.data.isChangeDevice ? response.data.isChangeDevice : false,
              cookie: updateRevResult.cookie,
              syncDocumentIds: updateRevResult.syncDocumentIds,
            });
          }
        });
      } else {
        log4jUtil.log('warn', `[9b]Response status != 200, status code=${response.status}`);
        responseUtil.generalInternalServerErrorResponse(req, res, next, { error: 'internal error' });
      }
    }).catch((error) => {
      log4jUtil.log('error', '[8b]Response case2');
      if (error.response) {
        log4jUtil.log('error', '[8b]Response case21');

        log4jUtil.log('error', `[8b1]data::${JSON.stringify(error.response.data)}`);
        log4jUtil.log('error', `[8b1]status:${error.response.status}`);
        log4jUtil.log('error', `[8b1]headers:${JSON.stringify(error.response.headers)}`);

        log4jUtil.log('error', `[8b1]error.response: ${error.response}`);
        // The request was made and the server responded with a status code
        // that falls out of the range of 2xx
        responseUtil.generalInternalServerErrorResponse(req, res, next, {
          data: error.response.data,
          status: error.response.status,
          headers: error.response.headers,
        });
      } else if (error.request) {
        log4jUtil.log('error', '[8b]Response case22');
        log4jUtil.log('error', `[8b2]Error case2, error.response: ${error.request}`);
        // The request was made but no response was received
        // `error.request` is an instance of XMLHttpRequest in the browser and an instance of
        // http.ClientRequest in node.js
        responseUtil.generalInternalServerErrorResponse(req, res, next, error.request);
      } else {
        log4jUtil.log('error', '[8b]Response case23');
        log4jUtil.log('error', `[8b3]Error case3, error.response: ${JSON.stringify(error.message)}`);
        // Something happened in setting up the request that triggered an Error
        responseUtil.generalInternalServerErrorResponse(req, res, next, error.message);
      }
      log4jUtil.log('error', `[9]Config:${error.config}`);
    });
    // updateRev(req.user.id, reqBody.id_token, (updateRevErr, updateRevResult) => {
    //   responseUtil.generalSuccessResponse(req, res, next, {
    //     user_id: `${req.user.id}`,
    //     doc_seq: 200001,
    //     installation_id: '8bdd9fab-73b6-4d0c-acce-c6ec122fa5d0',
    //     isChangeDevice: false,
    //     cookie: 'SyncGatewaySession=e8d21dbd23468cf30efcc368785e3bbe7b5fe9a9; Path=/axasgsit; Expires=Mon, 10 Dec 2018 18:13:43 GMT',
    //     syncDocumentIds: updateRevResult.syncDocumentIds,
    //   });
    //});
  } else {
    log4jUtil.log('warn', '[7a]reqBody.device_model, reqBody.device_id, reqBody.versionNumber, reqBody.buildNumber not  exists');
    responseUtil.generalBadRequestResponse(req, res, next, { error: 'device_model, device_id, versionNumber, buildNumber required.' });
  }
};

module.exports.storeEncryptionKey = (req, res, next) => {
  responseUtil.generalSuccessResponse(req, res, next, {});
  // if (req.body.master_key && req.body.installation_id) {
  //   axios.request({
  //     url: '/api/mobile/updateMasterKey',
  //     method: 'post',
  //     baseURL: `${commonUtil.getConfig('INTERNAL_API_DOMAIN')}`,
  //     params: {
  //       userId: req.user.id,
  //       master_key: req.body.master_key,
  //       installation_id: req.body.installation_id,
  //     },
  //   }).then((response) => {
  //     if (response.status === 200) {
  //       responseUtil.generalSuccessResponse(req, res, next, {});
  //     }
  //   }).catch((error) => {
  //     if (error.response) {
  //       // The request was made and the server responded with a status code
  //       // that falls out of the range of 2xx
  //       responseUtil.generalInternalServerErrorResponse(req, res, next, {
  //         data: error.response.data,
  //         status: error.response.status,
  //         headers: error.response.headers,
  //       });
  //     } else if (error.request) {
  //       // The request was made but no response was received
  //       // `error.request` is an instance of XMLHttpRequest in the browser and an instance of
  //       // http.ClientRequest in node.js
  //       responseUtil.generalInternalServerErrorResponse(req, res, next, error.request);
  //     } else {
  //       // Something happened in setting up the request that triggered an Error
  //       responseUtil.generalInternalServerErrorResponse(req, res, next, error.message);
  //     }
  //   });
  // } else {
  //   responseUtil.generalBadRequestResponse(req, res, next, { message: 'master_key and installation_id required.' });
  // }
};

const saveLoginAuditLog = (reqBody) => {
  axios.request({
    url: '/api/mobile/saveLoginAuditLog',
      method: 'post',
      baseURL: `${commonUtil.getConfig('INTERNAL_API_DOMAIN')}`,
      params: {
        installationId: reqBody.installation_id,
        deviceId: reqBody.uuid,
        deviceModel: reqBody.deviceModel,
        versionNumber: reqBody.versionNumber,
        buildNumber: reqBody.buildNumber,
        deviceCountry: reqBody.deviceCountry,
        brand: reqBody.brand,
        carrier: reqBody.carrier,
        deviceLocale: reqBody.deviceLocale,
        deviceName: reqBody.deviceName,
        firstInstallTime: reqBody.firstInstallTime,
        fontScale: reqBody.fontScale,
        freeDiskStorage: reqBody.freeDiskStorage,
        ipAddress: reqBody.ipAddress,
        installReferrer: reqBody.installReferrer,
        instanceID: reqBody.instanceID,
        macAddress: reqBody.macAddress,
        manufacturer: reqBody.manufacturer,
        maxMemory: reqBody.maxMemory,
        phoneNumber: reqBody.phoneNumber,
        serialNumber: reqBody.serialNumber,
        systemName: reqBody.systemName,
        systemVersion: reqBody.systemVersion,
        timezone: reqBody.timezone,
        storageSize: reqBody.storageSize,
        totalMemory: reqBody.totalMemory,
        userAgent: reqBody.userAgent,
        is24Hour: reqBody.is24Hour,
        isEmulator: reqBody.isEmulator,
        isPinOrFingerprintSet: reqBody.isPinOrFingerprintSet,
        isTablet: reqBody.isTablet,
        hasNotch: reqBody.hasNotch,
        isLandscape: reqBody.isLandscape,
        deviceType: reqBody.deviceType
      },
  }).then(result => {
    console.log(result);
  }).catch(e => {
    console.log(e);
  });
};

module.exports.login = (req, res, next) => {
  responseUtil.generalSuccessResponse(req, res, next, {
    master_key: 'b35b53259468A7690A136e4b3f9660d1',
    cookie: 'SyncGatewaySession=e8d21dbd23468cf30efcc368785e3bbe7b5fe9a9; Path=/axasgsit; Expires=Mon, 10 Dec 2018 18:13:43 GMT',
  });
  // log4jUtil.log('info', `request userId: ${req.user.id}`);
  // log4jUtil.log('info', `request installation_id: ${req.body.installation_id}`);
  // if (req.body.installation_id) {
  //   axios.request({
  //     url: '/api/mobile/getInstallationInfo',
  //     method: 'post',
  //     baseURL: `${commonUtil.getConfig('INTERNAL_API_DOMAIN')}`,
  //     params: {
  //       userId: req.user.id,
  //       installationId: req.body.installation_id,
  //     },
  //   }).then((response) => {
  //     log4jUtil.log('info', `getInstallationInfo response status: ${response.status}`);
  //     log4jUtil.log('info', `getInstallationInfo response data: ${JSON.stringify(response.data)}`);
  //     if (response.status === 200) {
  //       if(response.data.masterKey) {
  //         if (req.body.id_token) {
  //           log4jUtil.log('info', `request id_token: ${req.body.id_token}`);
  //           getSessionCookie(req.body.id_token, (getSessionCookieErr, getSessionCookieResult) => {
  //             if (getSessionCookieErr) {
  //               log4jUtil.log('error', `getSessionCookie error: ${getSessionCookieErr}`);
  //               responseUtil.generalInternalServerErrorResponse(req, res, next, getSessionCookieErr);
  //             } else {
  //               responseUtil.generalSuccessResponse(req, res, next, {
  //                 master_key: response.data.masterKey,
  //                 cookie: getSessionCookieResult.headers['set-cookie'][0],
  //               });
  //             }
  //           });
  //         } else {
  //           responseUtil.generalSuccessResponse(req, res, next, {
  //             master_key: response.data.masterKey
  //           });
  //         }
  //       } else {
  //         log4jUtil.log('error', 'Master key not exist. Plz check if agent fully activation successfully before (activation + set offline password).');
  //         responseUtil.generalBadRequestResponse(req, res, next, 'Master key not exist. Plz check if agent fully activation successfully before (activation + set offline password).');
  //       }
  //     } else {
  //       log4jUtil.log('error', 'response status != 200');
  //       responseUtil.generalInternalServerErrorResponse(req, res, next, response);
  //     }
  //   }).catch((error) => {
  //     if (error.response) {
  //       // The request was made and the server responded with a status code
  //       // that falls out of the range of 2xx
  //       log4jUtil.log('error', `data: ${JSON.stringify(error.response.data)}`);
  //       log4jUtil.log('error', `status: ${error.response.status}`);
  //       log4jUtil.log('error', `headers: ${JSON.stringify(error.response.headers)}`);
  //       responseUtil.generalInternalServerErrorResponse(req, res, next, {
  //         data: error.response.data,
  //         status: error.response.status,
  //         headers: error.response.headers,
  //       });
  //     } else if (error.request) {
  //       // The request was made but no response was received
  //       // `error.request` is an instance of XMLHttpRequest in the browser and an instance of
  //       // http.ClientRequest in node.js
  //       log4jUtil.log('error', `request: ${JSON.stringify(error.request)}`);
  //       responseUtil.generalInternalServerErrorResponse(req, res, next, error.request);
  //     } else {
  //       // Something happened in setting up the request that triggered an Error
  //       log4jUtil.log('error', `error:message: ${JSON.stringify(error.message)}`);
  //       responseUtil.generalInternalServerErrorResponse(req, res, next, error.message);
  //     }
  //   });
  // } else {
  //   responseUtil.generalBadRequestResponse(req, res, next, { message: 'installation_id required' });
  // }
};
