const _ = require('lodash');
const axios = require('axios');

const responseUtil = require('../utils/response.util');
const syncGatewayUtil = require('../utils/syncGateway.util');
const log4jUtil = require('../utils/log4j.util');
const commonUtil = require('../utils/common.util');

const getApplication = appId => new Promise((resolve, reject) => {
  syncGatewayUtil.getDocFromSG(appId, (err, result) => {
    if (err) {
      reject(err);
    } else {
      resolve(result);
    }
  });
});

const getChildApplications = (childIds) => {
  const getAppPromises = childIds.map(childId => getApplication(childId));
  return Promise.all(getAppPromises);
};

const transformParentPaymenttoChildPayment = (payment, childAppId) => {
  const parentPayment = _.cloneDeep(payment);
  let childPayment = parentPayment;
  const cpfAccNo = _.get(payment, 'cpfMedisaveAccDetails[0].cpfAccNo');
  let paymentChildObj = {};
  const cpfisoaBlock = {
    idCardNo: cpfAccNo,
  };
  const cpfissaBlock = {
    idCardNo: cpfAccNo,
  };
  const srsBlock = {
    idCardNo: cpfAccNo,
  };

  _.each(childPayment.premiumDetails, (premiumDetail) => {
    if (premiumDetail.applicationId === childAppId) {
      paymentChildObj = premiumDetail;
    }
  });
  childPayment.cpfisoaBlock = cpfisoaBlock;
  childPayment.cpfissaBlock = cpfissaBlock;
  childPayment.srsBlock = srsBlock;

  childPayment = _.assignIn(childPayment, paymentChildObj);
  return _.omit(childPayment, ['cpfMedisaveAccDetails', 'premiumDetails', 'applicationId']);
};

const saveParentPaymenttoChild = (parentApplication, callback) => {
  const childIds = _.get(parentApplication, 'childIds');
  const payment = _.get(parentApplication, 'payment');
  const response = parentApplication;
  getChildApplications(childIds).then((childApplications) => {
    const updAppPromises = childApplications.map(app => new Promise((resolve, reject) => {
      response.payment = transformParentPaymenttoChildPayment(payment, app.id);
      response.payment.trxAmount = _.get(app, 'payment.cashPortion');
      syncGatewayUtil.updateDocToSG(app, (err, result) => {
        if (err) {
          reject(err);
        } else {
          resolve(result);
        }
      });
    }));

    return Promise.all(updAppPromises).then(() => {
      callback(null, parentApplication);
    }, (rejectReason) => {
      callback(`ERROR in saveParentPaymenttoChild with Reject Reason,  ${rejectReason}`);
    }).catch((error) => {
      callback(`ERROR in _saveParentPaymenttoChild, ${error}`);
    });
  });
};

module.exports.getPaymentUrl = (req, res, next) => {
  if (
    req.body.trxNo
    && req.body.appId
    && req.body.policyNo
    && req.body.amount
    && req.body.currency
    && req.body.method
    && (req.body.isShield !== undefined)
  ) {
    log4jUtil.log('info', 'Start get payment url.');
    log4jUtil.log('info', `trxNo: ${req.body.trxNo}`);
    log4jUtil.log('info', `appId: ${req.body.appId}`);
    log4jUtil.log('info', `policyNo: ${req.body.policyNo}`);
    log4jUtil.log('info', `amount: ${req.body.amount}`);
    log4jUtil.log('info', `currency: ${req.body.currency}`);
    log4jUtil.log('info', `method: ${req.body.method}`);
    log4jUtil.log('info', `isShield: ${req.body.isShield}`);
    log4jUtil.log('info', `lob: ${req.body.lob}`);
    log4jUtil.log('info', `relatedPolicyNumber: ${JSON.stringify(req.body.relatedPolicyNumber)}`);
    log4jUtil.log('info', `relatedApplicationNumber: ${JSON.stringify(req.body.relatedApplicationNumber)}`);
    axios.request({
      baseURL: commonUtil.getConfig('INTERNAL_API_DOMAIN'),
      method: 'POST',
      url: '/ease-api/wirecard/getPaymentPath',
      data: {
        trxNo: req.body.trxNo,
        appId: req.body.appId,
        policyNo: req.body.policyNo,
        amount: req.body.amount,
        currency: req.body.currency,
        method: req.body.method,
        isShield: req.body.isShield,
        lob: req.body.lob,
        relatedPolicyNumber: req.body.relatedPolicyNumber,
        relatedApplicationNumber: req.body.relatedApplicationNumber,
        isMobile: true,
      },
    }).then((response) => {
      if (response.status === 200) {
        responseUtil.generalSuccessResponse(req, res, next, response.data);
      } else {
        log4jUtil.log('error', `response.status: ${response.status}`);
        log4jUtil.log('error', `response.data: ${JSON.stringify(response.data)}`);
        responseUtil.generalInternalServerErrorResponse(
          req,
          res,
          next,
          JSON.stringify(response),
        );
      }
    }).catch((error) => {
      if (error.response) {
        log4jUtil.log('error', `error.response.data::${JSON.stringify(error.response.data)}`);
        log4jUtil.log('error', `error.response.status:${error.response.status}`);
        log4jUtil.log('error', `error.response.headers:${JSON.stringify(error.response.headers)}`);

        log4jUtil.log('error', `error.response: ${error.response}`);
        // The request was made and the server responded with a status code
        // that falls out of the range of 2xx
        responseUtil.generalInternalServerErrorResponse(req, res, next, {
          data: error.response.data,
          status: error.response.status,
          headers: error.response.headers,
        });
      } else if (error.request) {
        log4jUtil.log('error', `error.response: ${error.request}`);
        // The request was made but no response was received
        // `error.request` is an instance of XMLHttpRequest in the browser and an instance of
        // http.ClientRequest in node.js
        responseUtil.generalInternalServerErrorResponse(req, res, next, error.request);
      } else {
        log4jUtil.log('error', `error.response: ${error.message}`);
        // Something happened in setting up the request that triggered an Error
        responseUtil.generalInternalServerErrorResponse(req, res, next, error.message);
      }
      log4jUtil.log('error', `Config:${error.config}`);
    });
  } else {
    log4jUtil.log('error', 'trxNo, appId, policyNo, amount, currency, method, isShield are required.');
    responseUtil.generalBadRequestResponse(req, res, next, 'trxNo, appId, policyNo, amount, currency, method, isShield are required.');
  }
};

module.exports.getPaymentStatusFromPost = (req, res, next) => {
  if (req.body.docId && (req.body.docId.substring(0, 2) === 'NB' || req.body.docId.substring(0, 2) === 'SA')) {
    syncGatewayUtil.getDocFromSG(req.body.docId, (getDocErr, getDocResult) => {
      if (getDocErr) {
        if (getDocErr.exception) {
          responseUtil.generalBadRequestResponse(req, res, next, getDocErr.exception);
        } else {
          responseUtil.generalInternalServerErrorResponse(req, res, next, getDocErr);
        }
      } else if (getDocResult && getDocResult.payment) {
        const responseObj = getDocResult;
        const status = getDocResult.payment.trxStatus;
        const trxStatusRemark = ((status === 'I') ? 'O' : status);
        if (trxStatusRemark !== getDocResult.payment.trxStatusRemark) {
          responseObj.payment.trxStatusRemark = trxStatusRemark;
          syncGatewayUtil.updateDocToSG(responseObj, (updateErr, updateResult) => {
            // Non-shield
            if (req.body.docId.substring(0, 2) === 'NB') {
              if (updateErr) {
                responseUtil.generalSuccessResponse(req, res, next, {
                  isSuccess: false,
                  type: 'payment',
                });
              } else {
                JSON.stringify(updateResult);
                responseUtil.generalSuccessResponse(req, res, next, {
                  isSuccess: true,
                  paymentValues: responseObj,
                  type: 'payment',
                });
              }
            // Shield
            } else {
              saveParentPaymenttoChild(getDocResult, (err, result) => {
                JSON.stringify(result);
                if (err) {
                  responseUtil.generalSuccessResponse(req, res, next, {
                    isSuccess: false,
                    application: getDocResult,
                    type: 'payment',
                  });
                } else {
                  responseUtil.generalSuccessResponse(req, res, next, {
                    isSuccess: true,
                    application: getDocResult,
                    type: 'payment',
                  });
                }
              });
            }
          });
        } else if (req.body.docId.substring(0, 2) === 'NB') {
          responseUtil.generalSuccessResponse(req, res, next, {
            isSuccess: true,
            paymentValues: responseObj,
            type: 'payment',
          });
        } else {
          // Shield
          saveParentPaymenttoChild(getDocResult, (err, result) => {
            JSON.stringify(result);
            if (err) {
              responseUtil.generalSuccessResponse(req, res, next, {
                isSuccess: false,
                application: getDocResult,
                type: 'payment',
              });
            } else {
              responseUtil.generalSuccessResponse(req, res, next, {
                isSuccess: true,
                application: getDocResult,
                type: 'payment',
              });
            }
          });
        }
      } else {
        responseUtil.generalSuccessResponse(req, res, next, {
          isSuccess: false,
          type: 'payment',
        });
      }
    });
  } else {
    responseUtil.generalBadRequestResponse(req, res, next, 'docId required or docId format error.');
  }
};

module.exports.getPaymentStatus = (req, res, next) => {
  if (req.params.docId && (req.params.docId.substring(0, 2) === 'NB' || req.params.docId.substring(0, 2) === 'SA')) {
    syncGatewayUtil.getDocFromSG(req.params.docId, (getDocErr, getDocResult) => {
      if (getDocErr) {
        if (getDocErr.exception) {
          responseUtil.generalBadRequestResponse(req, res, next, getDocErr.exception);
        } else {
          responseUtil.generalInternalServerErrorResponse(req, res, next, getDocErr);
        }
      } else if (getDocResult && getDocResult.payment) {
        const responseObj = getDocResult;
        const status = getDocResult.payment.trxStatus;
        const trxStatusRemark = ((status === 'I') ? 'O' : status);
        if (trxStatusRemark !== getDocResult.payment.trxStatusRemark) {
          responseObj.payment.trxStatusRemark = trxStatusRemark;
          syncGatewayUtil.updateDocToSG(responseObj, (updateErr, updateResult) => {
            // Non-shield
            if (req.params.docId.substring(0, 2) === 'NB') {
              if (updateErr) {
                responseUtil.generalSuccessResponse(req, res, next, {
                  isSuccess: false,
                  type: 'payment',
                });
              } else {
                JSON.stringify(updateResult);
                responseUtil.generalSuccessResponse(req, res, next, {
                  isSuccess: true,
                  paymentValues: responseObj,
                  type: 'payment',
                });
              }
            // Shield
            } else {
              saveParentPaymenttoChild(getDocResult, (err, result) => {
                JSON.stringify(result);
                if (err) {
                  responseUtil.generalSuccessResponse(req, res, next, {
                    isSuccess: false,
                    application: getDocResult,
                    type: 'payment',
                  });
                } else {
                  responseUtil.generalSuccessResponse(req, res, next, {
                    isSuccess: true,
                    application: getDocResult,
                    type: 'payment',
                  });
                }
              });
            }
          });
        } else if (req.params.docId.substring(0, 2) === 'NB') {
          responseUtil.generalSuccessResponse(req, res, next, {
            isSuccess: true,
            paymentValues: responseObj,
            type: 'payment',
          });
        } else {
          // Shield
          saveParentPaymenttoChild(getDocResult, (err, result) => {
            JSON.stringify(result);
            if (err) {
              responseUtil.generalSuccessResponse(req, res, next, {
                isSuccess: false,
                application: getDocResult,
                type: 'payment',
              });
            } else {
              responseUtil.generalSuccessResponse(req, res, next, {
                isSuccess: true,
                application: getDocResult,
                type: 'payment',
              });
            }
          });
        }
      } else {
        responseUtil.generalSuccessResponse(req, res, next, {
          isSuccess: false,
          type: 'payment',
        });
      }
    });
  } else {
    responseUtil.generalBadRequestResponse(req, res, next, 'docId required or docId format error.');
  }
};
