const axios = require('axios');
const _ = require('lodash');
const NodeRSA = require('node-rsa');
const moment = require('moment');

const responseUtil = require('../utils/response.util');
const commonUtil = require('../utils/common.util');
const log4jUtil = require('../utils/log4j.util');
const syncGatewayUtil = require('../utils/syncGateway.util');

const getConflictAppDocs = (agentCode, lastSyncTime) => new Promise((resolve, reject) => {
  log4jUtil.log('info', 'Start get view');
  const lastSyncTimeInInteger = parseInt(lastSyncTime, 10);
  log4jUtil.log('info', `Get paymentAndSubmissionDoubleCheck view baseURL: ${commonUtil.getConfig('GATEWAY_URL')}:${commonUtil.getConfig('GATEWAY_PORT')}`);
  log4jUtil.log('info', `Get paymentAndSubmissionDoubleCheck view url: /${commonUtil.getConfig('GATEWAY_DBNAME')}/_design/dataSync/_view/paymentAndSubmissionDoubleCheck?startkey=["01","${agentCode}",${lastSyncTime}]&endkey=["01","${agentCode}",${moment().valueOf()}]&stale=false`);
  axios.request({
    baseURL: `${commonUtil.getConfig('GATEWAY_URL')}:${commonUtil.getConfig('GATEWAY_PORT')}`,
    url: `/${commonUtil.getConfig('GATEWAY_DBNAME')}/_design/dataSync/_view/paymentAndSubmissionDoubleCheck?startkey=["01","${agentCode}",${lastSyncTime}]&endkey=["01","${agentCode}",${moment().valueOf()}]&stale=false`,
    method: 'get',
    auth: {
      username: commonUtil.getConfig('GATEWAY_USER'),
      password: commonUtil.getConfig('GATEWAY_PW'),
    },
  }).then((response) => {
    if (response.status === 200 && response.data.total_rows > 0) {
      log4jUtil.log('info', `Get view successfully and total rows > 0, total_rows = ${response.data.total_rows}`);
      const conflictArray = [];
      _.forEach(response.data.rows, (doc, index) => {
        if (
          (
            doc.value.payment
            && doc.value.payment.onlinePaymentUpdateTimeStamp
            && moment(doc.value.payment.onlinePaymentUpdateTimeStamp).unix()
              > moment(lastSyncTimeInInteger).unix()
          )
          || (
            doc.value.applicationSubmittedDate
              && moment(doc.value.applicationSubmittedDate).unix()
              > moment(lastSyncTimeInInteger).unix()
          )
        ) {
          conflictArray.push(doc);
        }
        if (index === response.data.rows.length - 1) {
          resolve(conflictArray);
        }
      });
    } else {
      log4jUtil.log('info', 'Get view successfully and total rows not more than 0');
      resolve([]);
    }
  }).catch((error) => {
    if (error.response) {
      // The request was made and the server responded with a status code
      // that falls out of the range of 2xx
      log4jUtil.log('error', JSON.stringify(error.response.data));
      log4jUtil.log('error', error.response.status);
      log4jUtil.log('error', JSON.stringify(error.response.headers));
      reject(new Error({
        data: error.response.data,
        status: error.response.status,
        headers: error.response.headers,
      }));
    } else if (error.request) {
      // The request was made but no response was received
      // `error.request` is an instance of XMLHttpRequest in the browser and an instance of
      // http.ClientRequest in node.js
      log4jUtil.log('error', JSON.stringify(error.request));
      reject(new Error({
        request: error.request,
      }));
    } else {
      // Something happened in setting up the request that triggered an Error
      log4jUtil.log('error', 'Error', JSON.stringify(error.message));
      reject(new Error({
        message: error.message,
      }));
    }
  });
});

const compareConflictByClientId = (pCid, winObjClientId) => new Promise((resolve) => {
  if (pCid === winObjClientId) {
    resolve(winObjClientId);
  } else {
    resolve();
  }
});

module.exports.checkOnlinePaymentAndSubmission = (req, res, next) => {
  if (req.body.lastSyncTime && req.body.agentCode && req.body.clientId) {
    getConflictAppDocs(req.body.agentCode, req.body.lastSyncTime)
      .then((conflictDocs) => {
        if (conflictDocs.length > 0) {
          const promiseArr = [];
          _.forEach(conflictDocs, (doc) => {
            promiseArr.push(compareConflictByClientId(req.body.clientId, doc.value.pCid));
          });
          Promise
            .all(promiseArr)
            .then((response) => {
              responseUtil.generalSuccessResponse(req, res, next, {
                isWebDataConflict: response.filter(promiseResult => !!promiseResult).length > 0,
              });
            })
            .catch((error) => {
              responseUtil.generalInternalServerErrorResponse(req, res, next, error);
            });
        } else {
          responseUtil.generalSuccessResponse(req, res, next, {
            isWebDataConflict: false,
          });
        }
      })
      .catch((error) => {
        responseUtil.generalInternalServerErrorResponse(req, res, next, error);
      });
  } else {
    responseUtil.generalBadRequestResponse(req, res, next, 'lastSyncTime agentCode and clientId are required');
  }
};

module.exports.getUplineProfileDocId = (req, res, next) => {
  if (req.query.upline2Code) {
    syncGatewayUtil.getView('main', 'agents', `["01","${req.query.upline2Code}"]`, (err, result) => {
      if (err) {
        responseUtil.generalInternalServerErrorResponse(req, res, next, err);
      } else if (result.rows) {
        const docIds = [];
        _.forEach(result.rows, (doc) => {
          docIds.push(doc.id);
        });
        responseUtil.generalSuccessResponse(req, res, next, docIds);
      } else {
        responseUtil.generalInternalServerErrorResponse(req, res, next, 'View result error');
      }
    });
  } else {
    responseUtil.generalBadRequestResponse(req, res, next, 'upline2Code is required');
  }
};

module.exports.saveAudDataSync = (req, res, next) => {
  if (req.body.agentCode && req.body.deviceUuid && req.body.syncResult && req.body.syncDate) {
    axios.request({
      baseURL: commonUtil.getConfig('INTERNAL_API_DOMAIN'),
      url: '/api/mobile/saveAudDataSync',
      method: 'post',
      data: {
        trxNo: req.body.trxNo,
        agentCode: req.body.agentCode,
        deviceUuid: req.body.deviceUuid,
        syncResult: req.body.syncResult,
        syncDate: req.body.syncDate,
        status: req.body.status,
        createUser: req.body.createUser,
        updUser: req.body.updUser,
      },
    }).then((response) => {
      if (response.status === 200) {
        responseUtil.generalSuccessResponse(req, res, next, response.data);
      } else {
        responseUtil.generalInternalServerErrorResponse(req, res, next, response);
      }
    }).catch((error) => {
      if (error.response) {
        log4jUtil.log('error', `Error case1, data::${JSON.stringify(error.response.data)}`);
        log4jUtil.log('error', `Error case1, status:${error.response.status}`);
        log4jUtil.log('error', `Error case1,  headers:${JSON.stringify(error.response.headers)}`);
        // The request was made and the server responded with a status code
        // that falls out of the range of 2xx
        responseUtil.generalInternalServerErrorResponse(req, res, next, {
          data: error.response.data,
          status: error.response.status,
          headers: error.response.headers,
        });
      } else if (error.request) {
        log4jUtil.log('error', `Error case2, error.response: ${error.request}`);
        // The request was made but no response was received
        // `error.request` is an instance of XMLHttpRequest in the browser and an instance of
        // http.ClientRequest in node.js
        responseUtil.generalInternalServerErrorResponse(req, res, next, error.request);
      } else {
        log4jUtil.log('error', `Error case3, error.response: ${error.message}`);
        // Something happened in setting up the request that triggered an Error
        responseUtil.generalInternalServerErrorResponse(req, res, next, error.message);
      }
    });
  } else {
    responseUtil.generalBadRequestResponse(req, res, next, 'agentCode, deviceUuid, syncResult, syncDate fields are required');
  }
};

const filterMaxUpdDate = (datas) => {
  const response = datas;
  _.forEach(response.esubAndEpayCustIds, (data, key) => {
    response.esubAndEpayCustIds[key].docIds = _.sortBy(data.docIds, ['lstChgDate']);
    if (response.esubAndEpayCustIds[key].docIds.length > 0) {
      response.esubAndEpayCustIds[key].maxUpdDate = _.findLast(
        response.esubAndEpayCustIds[key].docIds,
        o => o,
      ).lstChgDate;
      const newDocIds = [];
      _.forEach(response.esubAndEpayCustIds[key].docIds, (docId) => {
        newDocIds.push(docId.id);
      });
      response.esubAndEpayCustIds[key].docIds = newDocIds;
    }
  });
  _.forEach(response.otherCustIds, (data, key) => {
    response.otherCustIds[key].docIds = _.sortBy(data.docIds, ['lstChgDate']);
    if (response.otherCustIds[key].docIds.length > 0) {
      response.otherCustIds[key].maxUpdDate = _.findLast(response.otherCustIds[key].docIds, o => o)
        .lstChgDate;
      const newDocIds = [];
      _.forEach(response.otherCustIds[key].docIds, (docId) => {
        newDocIds.push(docId.id);
      });
      response.otherCustIds[key].docIds = newDocIds;
    }
  });
  return response;
};

const queryEachRelatedDocIdByCustId = (datas, allResults, filtedResult) => {
  const response = datas;
  _.forEach(allResults, (result) => {
    _.forEach(datas.esubAndEpayCustIds, (data) => {
      if (
        data.custId === result.value.pCid
        || data.custId === result.value.cid
        || data.custId === result.value.customerId) {
        if (result.id) {
          data.docIds.push({ id: result.id, lstChgDate: result.value.lstChgDate });
        } else {
          data.docIds.push({ id: result.id, lstChgDate: result.lstChgDate });
        }
      }
    });
  });
  _.forEach(filtedResult, (result) => {
    _.forEach(datas.otherCustIds, (data) => {
      if (result.type === 'cust' && data.custId === result.cid) {
        data.docIds.push({ id: result.id, lstChgDate: result.lstChgDate });
      } else if (result.type === 'bundle' && data.custId === result.pCid) {
        data.docIds.push({ id: result.id, lstChgDate: result.lstChgDate });
      } else if ((result.type === 'pda' || result.type === 'na' || result.type === 'fe') && data.custId === result.pCid) {
        data.docIds.push({ id: result.id, lstChgDate: result.lstChgDate });
      } else if (result.type === 'quotation' && data.custId === result.pCid) {
        data.docIds.push({ id: result.id, lstChgDate: result.lstChgDate });
      } else if (result.type === 'application' && data.custId === result.pCid) {
        data.docIds.push({ id: result.id, lstChgDate: result.lstChgDate });
      } else if (result.type === 'masterApplication' && data.custId === result.pCid) {
        data.docIds.push({ id: result.id, lstChgDate: result.lstChgDate });
      } else if (result.type === 'masterApproval' && data.custId === result.customerId) {
        data.docIds.push({ id: result.id, lstChgDate: result.lstChgDate });
      } else if (result.type === 'approval' && data.custId === result.customerId) {
        data.docIds.push({ id: result.id, lstChgDate: result.lstChgDate });
      }
    });
  });
  return response;
};

const groupByCustId = (datas) => {
  const response = {
    esubAndEpayCustIds: [],
    otherCustIds: [],
  };
  // Format to [{custId: '1234', docIds: []}, {custId: '234', docIds: []}]
  _.forEach(datas.esubAndEpayCustIds, (data) => {
    response.esubAndEpayCustIds.push({ custId: data, docIds: [], maxUpdDate: null });
  });
  _.forEach(datas.otherCustIds, (data) => {
    response.otherCustIds.push({ custId: data, docIds: [], maxUpdDate: null });
  });
  return response;
};

const filterUniqueSubmitStatus = (datas) => {
  const result = {
    esubAndEpayCustIds: [],
    otherCustIds: [],
  };
  _.forEach(datas.esubAndEpayCustIds, (data) => {
    if (!_.includes(result.esubAndEpayCustIds, data)) {
      result.esubAndEpayCustIds.push(data);
    }
  });
  _.forEach(datas.otherCustIds, (data) => {
    if (!_.includes(result.otherCustIds, data) && !_.includes(result.esubAndEpayCustIds, data)) {
      result.otherCustIds.push(data);
    }
  });
  return result;
};

const filterSubmitStatus = (datas) => {
  const result = {
    esubAndEpayCustIds: [],
    otherCustIds: [],
  };
  _.forEach(datas, (data) => {
    if (data.type === 'application' || data.type === 'masterApplication') {
      if (data.isSubmittedStatus) {
        if (data.pCid) {
          result.esubAndEpayCustIds.push(data.pCid);
        }
      } else if (data.payment
        && (
          data.payment.initPayMethod === 'crCard'
          || data.payment.initPayMethod === 'eNets'
          || data.payment.initPayMethod === 'dbsCrCardIpp')
        && data.trxStatus === 'Y'
        && data.trxTime) {
        if (data.pCid) {
          result.esubAndEpayCustIds.push(data.pCid);
        }
      } else {
        result.otherCustIds.push(data.pCid);
      }
    } else if (data.type === 'cust' && data.cid) {
      result.otherCustIds.push(data.cid);
    } else if ((data.type === 'masterApproval' || data.type === 'approval') && data.customerId) {
      result.otherCustIds.push(data.customerId);
    } else if (data.pCid) {
      result.otherCustIds.push(data.pCid);
    }
  });
  return result;
};

const filterNotNull = (datas) => {
  const resultsWithoutNull = [];
  _.forEach(datas, (result) => {
    if (result.id) {
      resultsWithoutNull.push(result);
    }
  });
  return resultsWithoutNull;
};

const filterLastUpd = (data, particularLastUpd) => {
  if (data.value
    && data.value.lstChgDate
    && (data.value.lstChgDate > particularLastUpd)
  ) {
    return {
      id: data.id,
      lstChgDate: data.value.lstChgDate ? Number(data.value.lstChgDate) : '',
      type: data.value.type ? data.value.type : '',
      pCid: data.value.pCid ? data.value.pCid : '',
      cid: data.value.cid ? data.value.cid : '',
      customerId: data.value.customerId ? data.value.customerId : '',
      payment: data.value.payment ? data.value.payment : {},
      isSubmittedStatus: data.value.isSubmittedStatus ? data.value.isSubmittedStatus : '',
    };
  }
  return {};
};

const getDeletedIds = (agentCode, particularLstChgDate, cb) => {
  axios.request({
    baseURL: `${commonUtil.getConfig('GATEWAY_URL')}:${commonUtil.getConfig('GATEWAY_PORT')}`,
    url: `/${commonUtil.getConfig('GATEWAY_DBNAME')}/_design/main/_view/agentDetails?key=["01","agentCode","${agentCode}"]`,
    method: 'get',
    auth: {
      username: commonUtil.getConfig('GATEWAY_USER'),
      password: commonUtil.getConfig('GATEWAY_PW'),
    },
  }).then((response) => {
    if (response.status === 200 && response.data.total_rows === 1) {
      if (response.data.rows[0].value.profileId) {
        syncGatewayUtil.getDocFromSG(`U_${response.data.rows[0].value.profileId}_DELETEDID`, (getDeleteObjErr, deletedObj) => {
          if (getDeleteObjErr) {
            cb(null, []);
          } else {
            const filteredDeletedObj = [];
            _.forEach(deletedObj.deletedIds, (deletedItem) => {
              if (Number(deletedItem.deletedTime)
              && Number(deletedItem.deletedTime) >= particularLstChgDate) {
                filteredDeletedObj.push(deletedItem);
              }
            });
            cb(null, filteredDeletedObj);
          }
        });
      } else {
        cb('Can not get profileId on agent document', null);
      }
    } else {
      cb(`Get agents view error, repsonse.status = "${response.status}"`, null);
    }
  }).catch((error) => {
    if (error.response) {
      // The request was made and the server responded with a status code
      // that falls out of the range of 2xx
      log4jUtil.log('error', JSON.stringify(error.response.data));
      log4jUtil.log('error', error.response.status);
      log4jUtil.log('error', JSON.stringify(error.response.headers));
      cb({
        data: error.response.data,
        status: error.response.status,
        headers: error.response.headers,
      });
    } else if (error.request) {
      // The request was made but no response was received
      // `error.request` is an instance of XMLHttpRequest in the browser and an instance of
      // http.ClientRequest in node.js
      log4jUtil.log('error', JSON.stringify(error.request));
      cb({ request: error.request }, null);
    } else {
      // Something happened in setting up the request that triggered an Error
      log4jUtil.log('error', 'Error', JSON.stringify(error.message));
      cb({ message: error.message }, null);
    }
  });
};

const filterConflictDocs = (winObjs, conflictpCids) => {
  log4jUtil.log('info', 'filterConflictDocs');
  return _.forEach(winObjs, (winObj) => {
    _.set(winObj, 'isForceDownload', conflictpCids.indexOf(winObj.custId) > -1);
  });
};

module.exports.getOnlineList = (req, res, next) => {
  if (req.query.agentCode && req.query.lstChgDate) {
    log4jUtil.log('info', `Start get online list, agentCode=${req.query.agentCode},lstChgDate=${req.query.lstChgDate}`);
    // if (moment(parseInt(req.query.lstChgDate, 10)).isValid()) {
    log4jUtil.log('info', 'Start get view');
    axios.get(`${commonUtil.getConfig('GATEWAY_URL')}:${commonUtil.getConfig('GATEWAY_PORT')}/${commonUtil.getConfig('GATEWAY_DBNAME')}/_design/dataSync/_view/getCPRelatedInfomation?key=["01","${req.query.agentCode}"]&stale=false`, {
      auth: {
        username: commonUtil.getConfig('GATEWAY_USER'),
        password: commonUtil.getConfig('GATEWAY_PW'),
      },
    }).then((response) => {
      if (response
              && response.data
              && response.data.total_rows
              && response.data.total_rows > 0) {
        log4jUtil.log('info', 'Step1 of getOnlineList');
        const results = _.map(
          response.data.rows, data => filterLastUpd(data, parseInt(req.query.lstChgDate, 10)),
        );
        log4jUtil.log('info', 'Step2 of getOnlineList');
        const resultsWithoutNull = filterNotNull(results);
        log4jUtil.log('info', 'Step3 of getOnlineList');
        const resultsWithSubmitStatus = filterSubmitStatus(resultsWithoutNull);
        log4jUtil.log('info', 'Step4 of getOnlineList');
        const uniqueResultsWithSubmitStatus = filterUniqueSubmitStatus(resultsWithSubmitStatus);
        log4jUtil.log('info', 'Step5 of getOnlineList');
        const groupResult = groupByCustId(uniqueResultsWithSubmitStatus);
        log4jUtil.log('info', 'Step6 of getOnlineList');
        const groupResultWithRelatedDocId = queryEachRelatedDocIdByCustId(
          groupResult,
          response.data.rows,
          resultsWithoutNull,
        );
        log4jUtil.log('info', 'Step7 of getOnlineList');
        const resultsWithMaxUpdDate = filterMaxUpdDate(groupResultWithRelatedDocId);
        log4jUtil.log('info', 'Step8 of getOnlineList');
        getDeletedIds(
          req.query.agentCode,
          req.query.lstChgDate,
          (getDeletedIdsErr, getDeletedIdsResult) => {
            if (getDeletedIdsErr) {
              responseUtil.generalInternalServerErrorResponse(req, res, next, getDeletedIdsErr);
            } else {
              getConflictAppDocs(req.query.agentCode, parseInt(req.query.lstChgDate, 10))
                .then((conflictDocs) => {
                  const conflictpCids = _.map(conflictDocs, doc => _.get(doc, 'value.pCid'));
                  /**
                   * Move the ChangeObjs to WinObj
                   * when the customer Ids are in the array of conflict Docs
                   *  */
                  const changedobjWithConflictIds = _.filter(resultsWithMaxUpdDate.otherCustIds,
                    customer => conflictpCids.indexOf(customer.custId) > -1);
                  const changedobjWOConflictIds = _.filter(resultsWithMaxUpdDate.otherCustIds,
                    customer => conflictpCids.indexOf(customer.custId) === -1);
                  const syncDocIds = [{
                    docIds: [''],
                    maxUpdDate: parseInt(moment().format('x'), 10),
                    isForceDownload: true,
                  }];
                  syncGatewayUtil.getView('main', 'products', '', (err, result) => {
                    if (result.rows && result.rows.length > 0) {
                      _.forEach(result.rows, (row) => {
                        if (row && (
                          row.id.indexOf(commonUtil.getConfig('PRODUCT_DATA_SYNC_KEY_WORD').toUpperCase()) > -1
                            || row.id.indexOf(commonUtil.getConfig('PRODUCT_DATA_SYNC_KEY_WORD').toLowerCase()) > -1)
                        ) {
                          syncDocIds.push({
                            docIds: [row.id],
                            maxUpdDate: parseInt(moment().format('x'), 10),
                            isForceDownload: true,
                          });
                        }
                      });
                    }
                    const responseBody = {
                      onlineWinObjs: _.union(syncDocIds, filterConflictDocs(
                        _.union(changedobjWithConflictIds,
                          resultsWithMaxUpdDate.esubAndEpayCustIds),
                        conflictpCids,
                      )),
                      onlineChangedObjs: changedobjWOConflictIds,
                      deletedIds: getDeletedIdsResult,
                    };
                    responseUtil.generalSuccessResponse(req, res, next, responseBody);
                  });
                })
                .catch((error) => {
                  responseUtil.generalInternalServerErrorResponse(req, res, next, error);
                });
            }
          },
        );
      } else {
        const syncDocIds = [{
          docIds: [''],
          maxUpdDate: parseInt(moment().format('x'), 10),
          isForceDownload: true,
        }];
        syncGatewayUtil.getView('main', 'products', '', (err, result) => {
          if (result.rows && result.rows.length > 0) {
            _.forEach(result.rows, (row) => {
              if (row && (                
                row.id.indexOf(commonUtil.getConfig('PRODUCT_DATA_SYNC_KEY_WORD').toUpperCase()) > -1
                  || row.id.indexOf(commonUtil.getConfig('PRODUCT_DATA_SYNC_KEY_WORD').toLowerCase()) > -1)                
              ) {
                syncDocIds.push({
                  docIds: [row.id],
                  maxUpdDate: parseInt(moment().format('x'), 10),
                  isForceDownload: true,
                });
              }
            });
          }
          responseUtil.generalSuccessResponse(req, res, next, {
            onlineWinObjs: syncDocIds,
            onlineChangedObjs: [],
          });
        });
      }
    }).catch((error) => {
      if (error.response) {
        // The request was made and the server responded with a status code
        // that falls out of the range of 2xx
        responseUtil.generalInternalServerErrorResponse(req, res, next, {
          data: error.response.data,
          status: error.response.status,
          headers: error.response.headers,
        });
      } else if (error.request) {
        // The request was made but no response was received
        // `error.request` is an instance of XMLHttpRequest in the browser and an instance of
        // http.ClientRequest in node.js
        responseUtil.generalInternalServerErrorResponse(req, res, next, {
          request: error.request,
        });
      } else {
        // Something happened in setting up the request that triggered an Error
        responseUtil.generalInternalServerErrorResponse(req, res, next, {
          message: error.message,
        });
      }
    });
    // } else {
    //   responseUtil.generalBadRequestResponse(req, res, next, 'lstChgDate format error');
    // }
  } else {
    responseUtil.generalBadRequestResponse(req, res, next, 'agentCode and lstChgDate required');
  }
};

module.exports.getSGPassword = (req, res, next) => {
  if (req.body.key) {
    log4jUtil.log('info', `req.body.key: ${req.body.key}`);
    const key = new NodeRSA(`-----BEGIN RSA PUBLIC KEY-----\n${req.body.key}\n-----END RSA PUBLIC KEY-----`);
    const pwd = commonUtil.getConfig('GATEWAY_PW');
    log4jUtil.log('info', `SGPassword:${pwd}`);
    const encrypted = key.encrypt(pwd, 'base64');
    log4jUtil.log('info', `Encrypted password:${encrypted}`);
    // responseUtil.generalSuccessResponse(req, res, next, encrypted);
    responseUtil.generalSuccessResponse(req, res, next, '');
  } else {
    responseUtil.generalBadRequestResponse(req, res, next, 'key required');
  }
};
