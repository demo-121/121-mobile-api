const jwt = require('jsonwebtoken');

const responseUtil = require('../utils/response.util');
const { getDocFromSG } = require('../utils/syncGateway.util');

module.exports.authorize = (req, res, next) => {
  const params = req.query;
  if (params.client_id !== '43dd75bc-3cae-4281-906b-fe0404ddab75') {
    responseUtil.generalBadRequestResponse(req, res, next, 'Incorrect client id');
  } else if (encodeURIComponent(params.redirect_uri) !== 'easemobile%3A%2F%2F') {
    responseUtil.generalBadRequestResponse(req, res, next, 'Incorrect redirect uri');
  } else {
    responseUtil.generalRedirectResponse(req, res, next, encodeURI(`http://axa_sg_mobile_application_group.eabgitlab.com/AXA_SG_APP_web_service/mock/login.html?response_type=${params.response_type}&client_id=${params.client_id}&redirect_uri=${params.redirect_uri}&scope=${params.scope}`));
  }
};

module.exports.getToken = (req, res, next) => {
  const params = req.body;
  if (!params.grant_type) {
    responseUtil.generalBadRequestResponse(req, res, next, {
      error: 'invalid_request',
      error_description: 'Invalid grant_type',
    });
  } else if (!params.code) {
    responseUtil.generalBadRequestResponse(req, res, next, {
      error: 'Non-valid parameter list for this operation',
      error_description: 'Check with Sascha!!',
    });
  } else if (params.redirect_uri !== 'easemobile://') {
    responseUtil.generalBadRequestResponse(req, res, next, {
      error: 'Token retrievel error',
      error_description: 'The requested token could not be retrieved',
    });
  } else {
    let isError = true;
    if (params.code.substring(0, 9) === 'tempCode_') {
      isError = false;
      res.json({
        access_token: jwt.sign({ sub: `${params.code.substring(9)}` }, 'secret'),
        token_type: 'Bearer',
        expires_in: '360000',
        refresh_token: 'zzzxxxcccvvv',
        scope: 'axa-sg-lifeapi',
      });
    }
    if (isError) {
      responseUtil.generalBadRequestResponse(req, res, next, {
        error: 'Token retrievel error',
        error_description: 'The requested token could not be retrieved',
      });
    }
    // switch (params.code) {
    //   case 'aaa1':
    //     uidValue = 'A1';
    //     break;
    //   case 'aaa2':
    //     uidValue = 'A2';
    //     break;
    //   case 'aaa3':
    //     uidValue = 'A3';
    //     break;
    //   case 'aaa4':
    //     uidValue = 'A4';
    //     break;
    //   default:
    //     isError = true;
    //     responseUtil.generalBadRequestResponse(req, res, next, {
    //       error: 'Token retrievel error',
    //       error_description: 'The requested token could not be retrieved',
    //     });
    //     break;
    // }
    // if (!isError) {
    //   res.json({
    //     access_token: jwt.sign({ sub: uidValue }, 'secret'),
    //     token_type: 'Bearer',
    //     expires_in: '360000',
    //     refresh_token: 'zzzxxxcccvvv',
    //     scope: 'axa-sg-lifeapi',
    //   });
    // }
  }
};

module.exports.getUserInfo = (req, res, next) => {
  if (req.query.type && req.query.type === 'agentUX') {
    getDocFromSG(`UX_${req.user.id}`, (err, result) => {
      if (err) {
        responseUtil.generalBadRequestResponse(req, res, next, err);
      } else {
        responseUtil.generalSuccessResponse(req, res, next, result);
      }
    });
  } else if (req.query.type && req.query.type !== 'agentUX') {
    responseUtil.generalBadRequestResponse(req, res, next, 'type error');
  } else {
    getDocFromSG(`U_${req.user.id}`, (err, result) => {
      if (err) {
        responseUtil.generalBadRequestResponse(req, res, next, err);
      } else {
        responseUtil.generalSuccessResponse(req, res, next, result);
      }
    });
  }
};

// Just for unit test. Test fake account by passport lib.
module.exports.testToken = (req, res, next) => {
  responseUtil.generalSuccessResponse(req, res, next, 'Test token successfully');
};
