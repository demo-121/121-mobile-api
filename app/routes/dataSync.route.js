const express = require('express');
const passport = require('passport');

const router = express.Router();
const dataSyncController = require('../controller/dataSync.controller');
const authUtil = require('../utils/auth.util');
const responseUtil = require('../utils/response.util');

router.post('/checkOnlinePaymentAndSubmission', passport.authenticate('bearer', { session: false }), (req, res, next) => {
  authUtil.decryptRequestBody(req, (decryptRequestBodyErr, decryptedRequestBody) => {
    if (decryptRequestBodyErr) {
      responseUtil.generalBadRequestResponse(req, res, next, decryptRequestBodyErr);
    } else {
      dataSyncController.checkOnlinePaymentAndSubmission(decryptedRequestBody, res, next);
    }
  });
});

router.get('/onlineList', passport.authenticate('bearer', { session: false }), (req, res, next) => {
  dataSyncController.getOnlineList(req, res, next);
});

router.post('/accessSG', passport.authenticate('bearer', { session: false }), (req, res, next) => {
  authUtil.decryptRequestBody(req, (decryptRequestBodyErr, decryptedRequestBody) => {
    if (decryptRequestBodyErr) {
      responseUtil.generalBadRequestResponse(req, res, next, decryptRequestBodyErr);
    } else {
      dataSyncController.getSGPassword(decryptedRequestBody, res, next);
    }
  });
});

router.post('/saveAudDataSync', passport.authenticate('bearer', { session: false }), (req, res, next) => {
  authUtil.decryptRequestBody(req, (decryptRequestBodyErr, decryptedRequestBody) => {
    if (decryptRequestBodyErr) {
      responseUtil.generalBadRequestResponse(req, res, next, decryptRequestBodyErr);
    } else {
      dataSyncController.saveAudDataSync(decryptedRequestBody, res, next);
    }
  });
});

router.get('/getUplineProfileDocId', passport.authenticate('bearer', { session: false }), (req, res, next) => {
  dataSyncController.getUplineProfileDocId(req, res, next);
});

module.exports = router;
