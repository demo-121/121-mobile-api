const express = require('express');
const passport = require('passport');

const paymentController = require('../controller/payment.controller');
const authUtil = require('../utils/auth.util');
const responseUtil = require('../utils/response.util');

const router = express.Router();

router.post('/url', passport.authenticate('bearer', { session: false }), (req, res, next) => {
  authUtil.decryptRequestBody(req, (decryptRequestBodyErr, decryptedRequestBody) => {
    if (decryptRequestBodyErr) {
      responseUtil.generalBadRequestResponse(req, res, next, decryptRequestBodyErr);
    } else {
      paymentController.getPaymentUrl(decryptedRequestBody, res, next);
    }
  });
});

router.get('/:docId/status', passport.authenticate('bearer', { session: false }), (req, res, next) => {
  paymentController.getPaymentStatus(req, res, next);
});

router.post('/status', passport.authenticate('bearer', { session: false }), (req, res, next) => {
  authUtil.decryptRequestBody(req, (decryptRequestBodyErr, decryptedRequestBody) => {
    if (decryptRequestBodyErr) {
      responseUtil.generalBadRequestResponse(req, res, next, decryptRequestBodyErr);
    } else {
      paymentController.getPaymentStatusFromPost(decryptedRequestBody, res, next);
    }
  });
});

module.exports = router;
