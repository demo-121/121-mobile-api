const express = require('express');
const passport = require('passport');

const router = express.Router();
const authController = require('../controller/auth.controller');
const authUtil = require('../utils/auth.util');
const responseUtil = require('../utils/response.util');

router.post('/activate', passport.authenticate('bearer', { session: false }), (req, res, next) => {
  authUtil.decryptRequestBody(req, (decryptRequestBodyErr, decryptedRequestBody) => {
    if (decryptRequestBodyErr) {
      responseUtil.generalBadRequestResponse(req, res, next, decryptRequestBodyErr);
    } else {
      authController.activateUser(decryptedRequestBody, res, next);
    }
  });
});

router.put('/encryptionKey', passport.authenticate('bearer', { session: false }), (req, res, next) => {
  authUtil.decryptRequestBody(req, (decryptRequestBodyErr, decryptedRequestBody) => {
    if (decryptRequestBodyErr) {
      responseUtil.generalBadRequestResponse(req, res, next, decryptRequestBodyErr);
    } else {
      authController.storeEncryptionKey(decryptedRequestBody, res, next);
    }
  });
});

router.post('/login', passport.authenticate('bearer', { session: false }), (req, res, next) => {
  authUtil.decryptRequestBody(req, (decryptRequestBodyErr, decryptedRequestBody) => {
    if (decryptRequestBodyErr) {
      responseUtil.generalBadRequestResponse(req, res, next, decryptRequestBodyErr);
    } else {
      authController.login(decryptedRequestBody, res, next);
    }
  });
});

router.post('/check', passport.authenticate('bearer', { session: false }), (req, res, next) => {
  authUtil.decryptRequestBody(req, (decryptRequestBodyErr, decryptedRequestBody) => {
    if (decryptRequestBodyErr) {
      responseUtil.generalBadRequestResponse(req, res, next, decryptRequestBodyErr);
    } else {
      authController.check(decryptedRequestBody, res, next);
    }
  });
});

router.get('/publicKey', passport.authenticate('bearer', { session: false }), (req, res, next) => {
  authController.getPublicKey(req, res, next);
});

module.exports = router;
