const express = require('express');
const passport = require('passport');

const smsController = require('../controller/sms.controller');
const authUtil = require('../utils/auth.util');
const responseUtil = require('../utils/response.util');

const router = express.Router();

router.post('/send', passport.authenticate('bearer', { session: false }), (req, res, next) => {
  authUtil.decryptRequestBody(req, (decryptRequestBodyErr, decryptedRequestBody) => {
    if (decryptRequestBodyErr) {
      responseUtil.generalBadRequestResponse(req, res, next, decryptRequestBodyErr);
    } else {
      smsController.sendSMS(decryptedRequestBody, res, next);
    }
  });
});

module.exports = router;
