const express = require('express');
const multer = require('multer');

const upload = multer({ dest: 'uploads/' });
const recognizeController = require('../controller/recognize.controller');

const router = express.Router();

router.post('/id', upload.single('idCard'), (req, res, next) => {
  recognizeController.idRecognizeBase64(req, res, next);
});

module.exports = router;
