const express = require('express');

const router = express.Router();

const responseUtil = require('../utils/response.util');

router.get('/', (req, res, next) => {
  responseUtil.generalBadRequestResponse(req, res, next, '');
});

router.get('/get500', (req, res, next) => {
  responseUtil.generalInternalServerErrorResponse(req, res, next, { error: 'Just test 500 error' });
});

router.get('/get400', (req, res, next) => {
  responseUtil.generalBadRequestResponse(req, res, next, { error: 'Just test 400 error' });
});

router.get('/getCustomResponse/:statusCode', (req, res, next) => {
  responseUtil.customResponse(req, res, next, req.params.statusCode, { error: 'Just test get customResponse' });
});

module.exports = router;
