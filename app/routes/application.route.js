const express = require('express');
const passport = require('passport');

const router = express.Router();
const applicationController = require('../controller/application.controller');
const authUtil = require('../utils/auth.util');
const responseUtil = require('../utils/response.util');

router.post('/getPolicyNumber', passport.authenticate('bearer', { session: false }), (req, res, next) => {
  authUtil.decryptRequestBody(req, (decryptRequestBodyErr, decryptedRequestBody) => {
    if (decryptRequestBodyErr) {
      responseUtil.generalBadRequestResponse(req, res, next, decryptRequestBodyErr);
    } else {
      applicationController.getPolicyNumber(decryptedRequestBody, res, next);
    }
  });
});

router.post('/submit', passport.authenticate('bearer', { session: false }), (req, res, next) => {
  authUtil.decryptRequestBody(req, (decryptRequestBodyErr, decryptedRequestBody) => {
    if (decryptRequestBodyErr) {
      responseUtil.generalBadRequestResponse(req, res, next, decryptRequestBodyErr);
    } else {
      applicationController.submit(decryptedRequestBody, res, next);
    }
  });
});

router.post('/confirmAppPaid', passport.authenticate('bearer', { session: false }), (req, res, next) => {
  authUtil.decryptRequestBody(req, (decryptRequestBodyErr, decryptedRequestBody) => {
    if (decryptRequestBodyErr) {
      responseUtil.generalBadRequestResponse(req, res, next, decryptRequestBodyErr);
    } else {
      applicationController.confirmAppPaid(decryptedRequestBody, res, next);
    }
  });
});

module.exports = router;
