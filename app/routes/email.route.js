const express = require('express');
const passport = require('passport');

const router = express.Router();
const emailController = require('../controller/email.controller');
const authUtil = require('../utils/auth.util');
const responseUtil = require('../utils/response.util');

router.post('/send', passport.authenticate('bearer', { session: false }), (req, res, next) => {
  authUtil.decryptRequestBody(req, (decryptRequestBodyErr, decryptedRequestBody) => {
    if (decryptRequestBodyErr) {
      responseUtil.generalBadRequestResponse(req, res, next, decryptRequestBodyErr);
    } else {
      emailController.sendEmail(decryptedRequestBody, res, next);
    }
  });
});

module.exports = router;
