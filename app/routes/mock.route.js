const express = require('express');
const passport = require('passport');

const mockController = require('../controller/mock.controller');
const authUtil = require('../utils/auth.util');
const responseUtil = require('../utils/response.util');

const router = express.Router();

router.get('/authorize', (req, res, next) => {
  mockController.authorize(req, res, next);
});

router.post('/token', (req, res, next) => {
  mockController.getToken(req, res, next);
});

router.get('/test', (req, res, next) => {
  mockController.test(req, res, next);
});

router.get('/user/:userId', passport.authenticate('bearer', { session: false }), (req, res, next) => {
  authUtil.decryptRequestBody(req, (decryptRequestBodyErr, decryptedRequestBody) => {
    if (decryptRequestBodyErr) {
      responseUtil.generalBadRequestResponse(req, res, next, decryptRequestBodyErr);
    } else {
      mockController.getUserInfo(decryptedRequestBody, res, next);
    }
  });
});

router.get('/user', passport.authenticate('bearer', { session: false }), (req, res, next) => {
  authUtil.decryptRequestBody(req, (decryptRequestBodyErr, decryptedRequestBody) => {
    if (decryptRequestBodyErr) {
      responseUtil.generalBadRequestResponse(req, res, next, decryptRequestBodyErr);
    } else {
      mockController.getUserInfo(decryptedRequestBody, res, next);
    }
  });
});

router.post('/testToken', passport.authenticate('bearer', { session: false }), (req, res, next) => {
  mockController.testToken(req, res, next);
});

module.exports = router;
