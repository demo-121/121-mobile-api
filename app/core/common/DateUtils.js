const isDate = require('lodash/isDate');
const moment = require('moment');

const getAttainedAge = function (targetDate, dob) {
  const birthDate = new Date(dob.getFullYear(), dob.getMonth(), dob.getDate());
  const tDate = new Date(targetDate.getFullYear(), targetDate.getMonth(), targetDate.getDate());

  let age = tDate.getFullYear() - birthDate.getFullYear();
  let m = tDate.getMonth() - birthDate.getMonth();
  let d = tDate.getDate() - birthDate.getDate();
  var monthNum = age * 12 + tDate.getMonth() - birthDate.getMonth();
  var dayNum = Math.round((tDate.getTime() - birthDate.getTime()) / (24 * 3600000));
  if (m < 0 || (m === 0 && d < 0)) {
    age--;
  }
  if (d < 0) {
    monthNum--;
  }
  return {
    year: age,
    month: monthNum,
    day: dayNum
  };
};
module.exports.getAttainedAge = getAttainedAge;

/**
 * Get the nearest age on a specific date.
 * e.g. attained age: 19.2 -> nearest age 19
 *      attained age: 19.8 -> nearest age 20
 *
 * @param {Date} targetDate target date, will only use the date part
 * @param {Date} dob date of birth
 */
const getNearestAge = function (targetDate, dob) {
  var tYr = targetDate.getFullYear();
  var tDate = new Date(tYr, targetDate.getMonth(), targetDate.getDate());

  var dobYr = dob.getFullYear();
  var dobMonth = dob.getMonth();
  var dobDay = dob.getDate();

  var tyBirthday = new Date(tYr, dobMonth, dobDay);
  var prevDob = (tDate - tyBirthday) >= 0 ? tyBirthday : new Date(tYr - 1, dobMonth, dobDay);
  var nextDob = (tDate - tyBirthday) < 0 ? tyBirthday : new Date(tYr + 1, dobMonth, dobDay);

  var lastDiff = Math.abs(tDate - prevDob);
  var nextDiff = Math.abs(tDate - nextDob);
  var finalDob = lastDiff > nextDiff ? nextDob : prevDob;

  return finalDob.getFullYear() - dobYr;
};
module.exports.getNearestAge = getNearestAge;

/**
 * Get the age on next birthday for a specific date.
 * e.g. attained age: 19.2 -> nearest age 20
 *      attained age: 19.8 -> nearest age 20
 *
 * @param {Date} targetDate target date, will only use the date part
 * @param {Date} dob date of birth
 */
const getAgeNextBirthday = function (targetDate, dob) {
  var tYr = targetDate.getFullYear();
  var tDate = new Date(tYr, targetDate.getMonth(), targetDate.getDate());

  var dobYr = dob.getFullYear();
  var dobMonth = dob.getMonth();
  var dobDay = dob.getDate();

  var tyBirthday = new Date(tYr, dobMonth, dobDay);
  var nextDob = (tDate - tyBirthday) < 0 ? tyBirthday : new Date(tYr + 1, dobMonth, dobDay);

  return nextDob.getFullYear() - dobYr;
};
module.exports.getAgeNextBirthday = getAgeNextBirthday;

/**
 * Confirmed with AXA, T + 183 is within 0 - 6 months, T + 184 is not. Same for leap year.
 */
const checkIsWithinSixMonthOld = function (targetDate, dob) {
  const days = getAttainedAge(targetDate, dob).day;
  return days < 184 && days >= 0;
};
module.exports.checkIsWithinSixMonthOld = checkIsWithinSixMonthOld;

/**
 * Get collection of ages by dob.
 *
 * @param {*} currentDate
 * @param {*} dob
 */
module.exports.getAges = function (currentDate, dob) {
  return Object.assign({
    nearestAge: getNearestAge(currentDate, dob),
    nextAge: getAgeNextBirthday(currentDate, dob)
  }, getAttainedAge(currentDate, dob));
};

module.exports.formatDate = function (date) {
  return moment(date).format('YYYY-MM-DD');
};

module.exports.formatDateWithTime = function (date) {
  if (isDate(date)) {
    return date.format('yyyy-mm-dd, HH:MM');
  }else return date;
};

module.exports.formatDateWithTimeSecond = function (date) {
  if (isDate(date)) {
    return date.format('dd/mm/yyyy, HH:MM:ss');
  }else return date;
};

module.exports.formatDateForApproval = function (date) {
  if (isDate(date)) {
    return date.format('dd/mm/yyyy HH:MM');
  }else return date;
};

module.exports.formatDateTimeWithPresent = function (date, present) {
  if (isDate(date)) {
    return date.format(present);
  }else return date;
};

module.exports.parseDate = function (dateStr) {
  return new Date(dateStr);
};

module.exports.formatDatetime = function (date) {
  return date.toISOString();
};

module.exports.parseDatetime = function (dateStr) {
  return new Date(dateStr);
};

module.exports.truncDate = function (date) {
  return moment(date).startOf('day').toDate();
};

module.exports.dayDiff = function (date1, date2) {
  return Math.abs(moment(date1).diff(date2, 'day'));
};
