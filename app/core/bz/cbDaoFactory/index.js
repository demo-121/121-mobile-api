const cbDaoFactory = require('./remoteCB');

let db = null;

module.exports.create = () => {
  // the file is included from main project
  // e.g. /axa-sg-app/app/cbDaoFactory/index.js
  db = cbDaoFactory;
  return db;
};
