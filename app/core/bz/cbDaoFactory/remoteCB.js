// var cb = require('couchbase');
const http = require('http');
// var dbConfig = require('../dbconfig');
const fs = require('fs');
const nPath = require('path');
const request = require('request');
const axios = require('axios');
const _ = require('lodash');
const stream = require('stream');
const fileHandler = require('../FileHandler');

const fnaPdfName = 'fnaReport';
const proposalPdfName = 'proposal';
const appPdfName = 'appPdf';
const logger = global.logger || console;
const RemoteUtils = require('../utils/RemoteUtils');
const syncGatewayUtil = require('../../../utils/syncGateway.util');
const commonUtil = require('../../../utils/common.util');

// var ViewQuery = cb.ViewQuery;
// var cluster = null;
// var bucket = null;
// testing tag
module.exports.init = function (callback) {
  // if (!bucket) {
  //   cluster = new cb.Cluster('couchbase://'+dbConfig.cbUrl);
  //   bucket = cluster.openBucket(dbConfig.cbName, dbConfig.cbPw);
  // }
  getDoc('sysParameter', (data) => {
    if (data && !data.error) {
      const rev = data._rev;
      data._id = '';
      data._rev = '';
      global.config = _.merge(global.config, data, { fileKey: rev });
      callback({ success: true });
    } else {
      callback({ success: false, error: data.error });
    }
  });
};

module.exports.createView = function (ddname, view, callback) {
  // logger.log('create view:');
  updToSG('PUT', `_design/${encodeURIComponent(ddname)}`, 'application/json', view, (res) => {
    // logger.log('create view result:', res);
    callback(res);
  });
};

const getViewRange = function (ddname, vname, start, end, params, callback) {
  // var query = ViewQuery.range(start, end, true);
  // bucket.query(query, function(err, result) {
  //   if (err) {
  //     callback(false)
  //   } else {
  //     callback(result);
  //   }
  // });
  const query = {
    startkey: start || null,
    endkey: end || null,
  };

  if (params) {
    for (const p in params) {
      query[p] = params[p];
    }
  }

  // get session
  try {
    const updateIndex = false; // ddname == 'quotation' || ddname == 'application';
    queryFromSG('GET', `_design/${ddname}/_view/${vname}`, query, updateIndex, (res) => {
      if (typeof callback === 'function') {
        callback(res);
      }
    });
  } catch (ex) {
    callback(false);
  }
};
module.exports.getViewRange = getViewRange;

module.exports.getViewByKeys = (ddname, vname, keys, params, batchSize = 40) => {
  let rows = [];
  let totalCnt = 0;
  let promise = Promise.resolve();
  for (let i = 0; i < keys.length; i += batchSize) {
    const batchKeys = _.slice(keys, i, i + batchSize);
    const batchParams = Object.assign({}, params, {
      keys: `[${_.join(batchKeys, ',')}]`,
    });
    promise = promise.then(() => new Promise((resolve) => {
      getViewRange(ddname, vname, null, null, batchParams, (result) => {
        if (result && !result.error) {
          totalCnt += result.total_rows;
          rows = rows.concat(result.rows);
        }
        resolve();
      });
    }));
  }
  return promise.then(() => ({
    total_rows: totalCnt,
    rows,
  }));
};

module.exports.updateViewIndex = (ddname, vname, callback) => { // get session
  try {
    const updateIndex = true;
    // change to be background process
    const backUpdateView = () => {
      const startTime = new Date();
      queryFromSG('GET', `_design/${ddname}/_view/${vname}`, '', updateIndex, (res) => {
        const elapsedTimeMs = new Date() - startTime;
        logger.log(`View update completed: ${vname}, Elapsed ${elapsedTimeMs}ms`);
        if (typeof callback === 'function') {
          callback(res);
        }
      });
    };
    setTimeout(backUpdateView, 5);
  } catch (ex) {
    if (typeof callback === 'function') {
      callback(false);
    }
    logger.error('ERROR:::::: updateViewIndex::::::::::', ex);
  }
};

const getDoc = (docId, callback) => {
  // bucket.get(docId, function(err, result) {
  //   if (err) {
  //     callback(false)
  //   } else {
  //     callback(result);
  //   }
  // });
  // for dev only
  // if (global.NODE_ENV == 'development') {
  //   try {
  //     path = global.rootPath + '/CB_resources/files/' + docId + '.json';
  //     var doc = JSON.parse(fs.readFileSync(path, 'utf8'));
  //     logger.log('INFO: get file from local path:', path);
  //     callback(doc);
  //     return;
  //   } catch (e) {
  //     // logger.log('EXCEPTION: fail to get file from local path:', path, e.stack || e);
  //     logger.error('load doc e:', __dirname, path);
  //   }
  // }
  // // --- end for dev only
  // getFromSG('GET', docId, callback);
  syncGatewayUtil.getDocFromSG(docId, (err, result) => {
    callback(result);
  });
};
module.exports.getDoc = getDoc;

module.exports.getDocFromCacheFirst = (id, callback) => {
  syncGatewayUtil.getDocFromSG(id, (err, result) => {
    callback(result);
  });
  // if (!global.config.useDocCache || !global.documentCache[id] || (global.documentCache[id].ts + (global.config.docCacheTimeout || 60000)) < (new Date()).getTime()) {
  //   getDoc(id, (result) => {
  //     if (result && (result._id || !result.error)) {
  //       global.documentCache[id] = {
  //         doc: result,
  //         ts: (new Date()).getTime(),
  //       };
  //       callback(result);
  //     } else {
  //       callback(false);
  //     }
  //   });
  // } else {
  //   callback(global.documentCache[id].doc);
  // }
};

module.exports.updDoc = (docId, data, cb) => {
  // console.log('data:' + JSON.stringify(data));
  // data._id =  data.proposalNumber;
  syncGatewayUtil.updateDocToSG(data, (err,  result)  => {
    if (err) {
      cb(err, null);
    } else {
      cb(null, result);
    }
  });
  // updToSG('PUT', encodeURIComponent(docId), 'application/json', data, callback);
  // bucket.replace(docId, data, callback);
};

module.exports.delDoc = function (docId, callback) {
  getFromSG('DELETE', docId, callback);
  // bucket.remove(docId, callback);
};

module.exports.delDocWithRev = function (docId, rev, callback) {
  deleteFromSG('DELETE', docId, rev, true, callback);
};

module.exports.getAttachment = function (docId, attName, callback) {
  getAttachmentByBase64(`${docId}/${attName}`, callback);
};

module.exports.getBinaryAttachment = function (docId, attName, callback) {
  getAttachmentByBinary(`${docId}/${attName}`, callback);
};

module.exports.getBinaryDocument = function (name, callback) {
  getAttachmentByBinary(name, callback);
};

module.exports.setAttachment = function (docId, attName, mime, data, callback) {
  updToSG('PUT', `${encodeURIComponent(docId)}/${encodeURIComponent(attName)}`, mime, data, callback);
};

module.exports.delAttachment = function (docId, attName, callback) {
  getFromSG('DELETE', `${docId}/${attName}`, callback);
};

// by base64
const getAttachmentByBase64 = function (name, cb) {
  const options = getOptions('GET', name);
  // const uid = RemoteUtils.SecureRandom(8);
  // const startTime = new Date();

  axios.request({
    baseURL: options.hostname + ':' + options.port,
    url: options.path,
    method: options.method,
    responseType: 'arraybuffer',
    auth: {
      username: commonUtil.getConfig('GATEWAY_USER'),
      password: commonUtil.getConfig('GATEWAY_PW'),
    },
  }).then((response) => {
    const data = Buffer.from(response.data, 'binary').toString('base64');
    cb({ success: true,  data});
  }).catch((error) => {
    cb({ success: false});
  });
  
  // const req = http.request(options, (res) => {
  //   let data = '';
  //   res.setEncoding('binary');
  //   res.on('data', (chunk) => {
  //     data += chunk;
  //   });
  //   res.on('end', () => {
  //     const elapsedTimeMs = new Date() - startTime;
  //     let dataLength = 0;
  //     try {
  //       dataLength = Math.round((data.length / 1048576) * Math.pow(10, 4)) / Math.pow(10, 4);
  //     } catch (ex) {
  //       dataLength = 'error';
  //     }
  //     logger.log(`INFO: getAttachmentByBase64 ${uid}: End: ${dataLength}MB: Elapsed ${elapsedTimeMs}ms`);
  //     if (typeof cb === 'function') {
  //       if (data) {
  //         try {
  //           // error
  //           data = JSON.parse(data);
  //           cb(data);
  //         } catch (e) {
  //           data = new Buffer(data, 'binary').toString('base64');
  //           cb({
  //             success: true,
  //             data,
  //           });
  //         }
  //       } else {
  //         cb(false);
  //       }
  //     }
  //   });
  // }).on('error', (e) => {
  //   logger.error('SG Error: ', e);
  //   cb({ error: e });
  // });
  // req.end();
};

// by binary
const getAttachmentByBinary = function (name, cb) {
  const options = getOptions('GET', name);
  const uid = RemoteUtils.SecureRandom(8);
  logger.log(`INFO: getAttachmentByBinary: ${uid}: Options: `, options.path);
  const startTime = new Date();
  console.log('getAttachmentByBinary');
  const req = http.request(options, (res) => {
    const data = '';
    res.on('data', (chunk) => {
      cb(chunk);
    });
    res.on('end', () => {
      const elapsedTimeMs = new Date() - startTime;
      logger.log(`INFO: getAttachmentByBinary: ${uid}: End: Elapsed ${elapsedTimeMs}ms`);
      cb(false);
    });
  }).on('error', (e) => {
    logger.error('SG Error: ', e);
    cb({ error: e });
  });
  req.end();
};

const getFromSG = function (method, name, cb) {
  if (name && typeof name !== 'string') {
    logger.log('ERROR: invalid doc name:', name);
    cb(false);
    return;
  }
  const options = getOptions(method, name);
  const uid = RemoteUtils.SecureRandom(8);
  logger.log(`INFO: getFromSG ${uid}: Options: ${options.path}`);
  const startTime = new Date();
  console.log('getFromSG');
  const req = http.request(options, (res) => {
    let resp = '';
    res.setEncoding('utf8');
    res.on('data', (chunk) => {
      resp += chunk;
    });
    res.on('end', () => {
      const elapsedTimeMs = new Date() - startTime;
      let dataLength = 0;
      try {
        dataLength = Math.round((resp.length / 1048576) * Math.pow(10, 4)) / Math.pow(10, 4);
      } catch (ex) {
        dataLength = 'error';
      }
      logger.log(`INFO: getFromSG End: ${uid}: ${dataLength}MB: Elapsed ${elapsedTimeMs}ms`);
      try {
        if (typeof cb === 'function') {
          if (resp) {
            resp = JSON.parse(resp);
            cb(resp);
          } else {
            // logger.log('Error: getFromSG non-string resp?', resp);
            cb(false);
          }
        }
      } catch (e) {
        logger.error('get doc failure:', resp, options, e);
        cb(false);
      }
    });
  }).on('error', (e) => {
    logger.log('SG Error: ', e);
    cb(false);
  });
  req.end();
};

const deleteFromSG = function (method, name, rev, updateIndex, cb) {
  if (name && typeof name !== 'string') {
    logger.log('ERROR: invalid doc name:', name);
    cb(false);
    return;
  }
  const options = getOptions(method, `${encodeURIComponent(name)}?rev=${rev}&stale=${updateIndex ? 'false' : 'ok'}`);
  const uid = RemoteUtils.SecureRandom(8);
  logger.log(`INFO: deleteFromSG ${uid}: Options: `, options.path);
  const startTime = new Date();
  console.log('deleteFromSG');
  const req = http.request(options, (res) => {
    let resp = '';
    res.setEncoding('utf8');
    res.on('data', (chunk) => {
      resp += chunk;
    });
    res.on('end', () => {
      const elapsedTimeMs = new Date() - startTime;
      let dataLength = 0;
      try {
        dataLength = Math.round((resp.length / 1048576) * Math.pow(10, 4)) / Math.pow(10, 4);
      } catch (ex) {
        dataLength = 'error';
      }
      logger.log(`INFO: deleteFromSG ${uid}: End: , Elapsed ${elapsedTimeMs}ms: ${dataLength}MB`);
      if (typeof cb === 'function') {
        if (resp) {
          resp = JSON.parse(resp);
          cb(resp);
        } else {
          // logger.log('Error: deleteFromSG non-string resp?', resp);
          cb(false);
        }
      }
    });
  }).on('error', (e) => {
    logger.log('SG Error: ', e);
  });
  req.end();
};

module.exports.updFileToSG = function (docId, attchId, rev, filename, mime, cb) {
  const options = getOptions('PUT', `${encodeURIComponent(docId)}/${encodeURIComponent(attchId)}?rev=${rev}`, {
    'Content-Type': mime,
    'Content-Transfer-Encoding': 'binary',
  });
  const uid = RemoteUtils.SecureRandom(8);
  logger.log(`INFO: updFileToSG: ${uid}: Options: `, options.path);

  axios.request({
    baseURL: commonUtil.getConfig('')
  })

  const root = global.root || __dirname;
  const filePath = nPath.join(global.rootPath, global.config.tempFolder, filename);
  try {
    fileHandler.decryptFile(filePath, () => {
      const startTime = new Date();
      const req = http.request(options, (res) => {
        let resp = '';
        res.on('data', (chunk) => {
          resp += chunk;
        });
        res.on('end', () => {
          const elapsedTimeMs = new Date() - startTime;
          logger.log(`INFO: updFileToSG ${uid}: End: Elapsed ${elapsedTimeMs}ms`);
          if (typeof cb === 'function') {
            if (resp) {
              resp = JSON.parse(resp);
              fileHandler.removeFiles(filePath);
              cb(resp);
            } else {
              logger.log('ERROR: updFileToSG:', resp);
              cb(false);
            }
          }
        });
      }).on('error', (e) => {
        logger.log('SG Error: ', e);
        fileHandler.removeOriginalFile(filePath);
      });

      fs.createReadStream(filePath).pipe(req);
    });
  } catch (e) {
    logger.log('ERROR: decrypt exception:', e, filePath);
  }
};

module.exports.uploadAttachmentByBase64 = function (docId, attchId, rev, data, mime, cb) {
  const options = getOptions('PUT', `${encodeURIComponent(docId)}/${encodeURIComponent(attchId)}?rev=${rev}`, {
    'Content-Type': mime,
    'Content-Transfer-Encoding': 'binary',
  });
  if (options && options.hostname) {
    // Remove http:// or https://
    const regex = /^(https?|http):\/\//;
    options.hostname = options.hostname.replace(regex, '');
  }
  logger.log('INFO: uploadAttachmentByBase64: Options: ', options.path);
  try{
    var resp = '';
    var req = http.request(options, (res) => {
      res.on('data', (chunk) => {
        resp += chunk;
      });
      res.on('end', () => {
        if (typeof cb == 'function') {
          if (resp) {
            try {
              logger.log("uploadComplete!");
              resp = JSON.parse(resp)
              cb(resp);
            } catch (e) {
              logger.error("uploadBase64 exception:", resp);
              cb(false);
            }
          } else {
            logger.log('ERROR: uploadAttachmentByBase64:', resp);
            cb(false);
          }
        }
      })
    })
    const bufferStream = new stream.PassThrough();
    bufferStream.end(Buffer.from(data, 'base64'));
    // bufferStream.end(new Buffer(data, "base64"));
    bufferStream.pipe(req);
  } catch(e) {
    console.log(e);
  }
    
  // axios.request({
  //   baseURL: options.hostname + ':' + options.port,
  //   url: options.path,
  //   method: 'put',
  //   auth: {
  //     username: commonUtil.getConfig('GATEWAY_USER'),
  //     password: commonUtil.getConfig('GATEWAY_PW'),
  //   },
  //   headers: {
  //     'Content-Type': 'application/pdf',
  //     'Content-Transfer-Encoding': 'binary'
  //   },
  //   data: data
  // }).then((response) => {
  //   cb({ success: true});
  // }).catch((error) => {
  //   cb({ success: false});
  // });
};

const updToSG = (method, name, mime, data, cb) => {
  syncGatewayUtil.updateDocToSG(data, (err, result) => {
    console.log('err:' + JSON.stringify(err));
    console.log('result:' + JSON.stringify(result));
    if (!err && result) {
      cb(result);
    }
  });
  // axios.request({
  //   baseURL: options.hostname + ':' + options.port,
  //   url:  options.path,
  //   method:  options.method,
  // })
  // const req = http.request(options, (res) => {
  //   let resp = '';
  //   res.setEncoding('utf8');
  //   res.on('data', (chunk) => {
  //     resp += chunk;
  //   });
  //   res.on('end', () => {
  //     if (cb && typeof cb === 'function') {
  //       if (resp) {
  //         try {
  //           resp = JSON.parse(resp);
  //           // logger.log('updata to SG result:', resp);
  //           if (resp.ok && !resp.error) {
  //             cb(resp);
  //           } else {
  //             logger.error('update CB error:', resp.error, options);
  //             cb(false);
  //           }
  //         } catch (e) {
  //           // calling error
  //           logger.error('update CB error:', e, resp, options);
  //           cb(false);
  //         }
  //       } else {
  //         cb(false);
  //       }
  //     }
  //   });
  // }).on('error', (e) => {
  //   logger.log('ERROR:: SG Error: ', e);
  // });
  // if (data) {
  //   if (typeof data === 'object') {
  //     data = JSON.stringify(data);
  //   }
  //   req.write(data);
  // }
  // req.end();
};

const queryFromSG = function (method, name, params, updateIndex, cb) {
  let paramStr = '';
  if (params) {
    for (const p in params) {
      if (params[p]) {
        paramStr += `${(paramStr ? '&' : '?') + p}=${params[p]}`;
      }
    }
  }
  // set stale to false, The index is updated before the query is executed
  paramStr += (paramStr ? '&' : '?') + (updateIndex ? 'stale=false' : 'stale=ok');
  const options = getOptions(method, name + (paramStr || ''), { 'Content-type': 'application/json' });
  const uid = RemoteUtils.SecureRandom(8);

  axios.request({
    baseURL: options.hostname + ':' + options.port,
    url: options.path,
    method: options.method,
    auth: {
      username: commonUtil.getConfig('GATEWAY_USER'),
      password: commonUtil.getConfig('GATEWAY_PW'),
    },
  }).then((response) => {
    cb(response.data);
  }).catch((error) => {
    if (error.response) {
      // The request was made and the server responded with a status code
      // that falls out of the range of 2xx
      console.log(error.response.data);
      console.log(error.response.status);
      console.log(error.response.headers);
    } else if (error.request) {
      // The request was made but no response was received
      // `error.request` is an instance of XMLHttpRequest in the browser and an instance of
      // http.ClientRequest in node.js
      console.log(error.request);
    } else {
      // Something happened in setting up the request that triggered an Error
      console.log('Error', error.message);
    }
    console.log(error.config);
    
  });

  // const startTime = new Date();
  // const req = http.request(options, (res) => {
  //   let resp = '';
  //   res.setEncoding('utf8');
  //   res.on('data', (chunk) => {
  //     resp += chunk;
  //   });
  //   res.on('end', () => {
  //     const elapsedTimeMs = new Date() - startTime;
  //     let dataLength = 0;
  //     try {
  //       dataLength = Math.round((resp.length / 1048576) * Math.pow(10, 4)) / Math.pow(10, 4);
  //     } catch (ex) {
  //       dataLength = 'error';
  //     }
  //     logger.log(`INFO: queryFromSG End: ${uid}: ${dataLength}MB: Elapsed ${elapsedTimeMs}ms`);
  //     if (cb && typeof cb === 'function') {
  //       if (resp) {
  //         try {
  //           resp = JSON.parse(resp);
  //         } catch (ex) {
  //           logger.log('ERROR: queryFromSG', ex);
  //           resp = false;
  //         }
  //         cb(resp);
  //       } else {
  //         logger.log('ERROR: queryFromSG:', resp);
  //         cb(false);
  //       }
  //     }
  //   });
  // }).on('error', (e) => {
  //   logger.log('SG Error: ', e);
  // });
  // req.end();
};

var getOptions = function (method, path, headers) {
  const options = {
    hostname: commonUtil.getConfig('GATEWAY_URL'),
    path: `/${commonUtil.getConfig('GATEWAY_DBNAME')}/${path}`,
    method,
    port: commonUtil.getConfig('GATEWAY_PORT')
  };
  if (commonUtil.getConfig('GATEWAY_USER') && commonUtil.getConfig('GATEWAY_PW')) {
    options.auth =  commonUtil.getConfig('GATEWAY_USER')+":"+commonUtil.getConfig('GATEWAY_PW')
  }
  if (headers) {
    options.headers = headers;
  }
  return options;
};
