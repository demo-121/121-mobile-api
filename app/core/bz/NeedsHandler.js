'use strict';
var logger = global.logger || console;

var nDao = require("./cbDao/needs");
var cDao = require("./cbDao/client");
var bDao = require("./cbDao/bundle");
const dao = require('./cbDaoFactory').create();
const EmailUtils = require('./utils/EmailUtils');
const DateUtils = require('../common/DateUtils');
var AppHandler = require("./handler/ApplicationHandler");
var PDFHandler = require('./PDFHandler');
const CommonFunctions = require('./CommonFunctions');
var companyDao = require('./cbDao/company');

const nwIds = ['assets', 'savAcc', 'fixDeposit', 'invest', 'cpfOa', 'cpfSa', 'cpfMs', 'srs', 'car', 'property', 'otherAsset', 'liabilities', 'mortLoan', 'motLoan', 'eduLoan', 'otherLiabilites', 'netWorth'];
const cfIds = ['mDisposeIncome', 'aDisposeIncome', 'netMIcncome', 'aBonus', 'totAExpense', 'totMExpense', 'totAIncome'];
const cf12Ids = ['mIncome', 'lessMCPF', 'householdExpenses', 'insurancePrem', 'otherExpense', 'regSav', 'personExpenses'];
const eikeys = ['aInsPrem', 'lifeInsProt','retirePol', 'ednowSavPol', 'eduFund', 'invLinkPol', 'disIncomeProt', 'ciPort', 'personAcc','caInsPrem','csInsPrem','mdInsPrem','fpInsPrem'];
const pks = ['aInsPrem', 'pAInsPrem', 'sAInsPrem', 'oAInsPrem'];
const hpckeys = ['provideHeadstart', 'typeofWard'];

const syncGatewayUtil = require('../../../app/utils/syncGateway.util');

var _  = require('lodash');

module.exports.savePDA = function(data, session, cb) {
  var {cid, pda, tiInfo, confirm} = data;
  let {agentCode: aid} = session.agent;
  delete pda.tiInfo;
  bDao.onSaveNeedItem(cid, pda, nDao.ITEM_ID.PDA, {tiInfo}).then((result)=>{
    if (confirm || result.code === bDao.CHECK_CODE.VALID) {
      bDao.invalidNeedItem(cid, session.agent, pda, nDao.ITEM_ID.PDA, result.errObj).then((resp)=>{
        let newId = _.get(resp, nDao.ITEM_ID.PDA);
        if (newId) {
          pda.newId = newId;
        }
        nDao.savePDA(session.agent, cid, pda, tiInfo).then((result2)=>{
          let hasTi = pda.lastStepIndex > 1 && pda.trustedIndividual === "Y";
          bDao.updateApplicationTrustedIndividual(cid, hasTi, tiInfo).then(()=>{
            // bDao.updateStatus(cid, bDao.BUNDLE_STATUS.HAVE_FNA, ()=>{
            bDao.updateStatus(cid, bDao.BUNDLE_STATUS.HAVE_FNA).then(()=>{
              nDao.showFnaInvalidFlag(cid).then(showFnaInvalidFlag => {
                cb({
                  success: true,
                  pda: result2.pda,
                  fe: result2.fe,
                  fna: result2.fna,
                  profile: result2.profile,
                  showFnaInvalidFlag
                });
              });
            }).catch((error)=>{
              logger.error('ERROR: savePDA :', error);
              cb({success: false});
            });
          });
        })
      });
    }
    else{
      cb({success: true, code: result.code});
    }
  })
}

module.exports.getPDA = function(data, session, cb) {
  var cid = data.cid;
  nDao.getItem(cid, nDao.ITEM_ID.PDA).then((pda)=>{
    delete pda.tiInfo;
    cb({
      success: pda? true: false,
      pda
    })
  })
}

module.exports.saveFE = function(data, session, cb) {
  var {cid, fe, confirm} = data;
  let {agentCode: aid} = session.agent;
  bDao.onSaveNeedItem(cid, fe, nDao.ITEM_ID.FE).then((result)=>{
    if (confirm || result.code === bDao.CHECK_CODE.VALID) {
      bDao.invalidNeedItem(cid, session.agent, fe, nDao.ITEM_ID.FE, result.errObj).then((resp)=>{
        let newId = _.get(resp, nDao.ITEM_ID.FE);
        if (newId) {
          fe.newId = newId;
        }
        nDao.saveFE(session.agent, cid, fe).then((result2)=>{
        // bDao.updateStatus(cid, bDao.BUNDLE_STATUS.HAVE_FNA, ()=>{
          bDao.updateStatus(cid, bDao.BUNDLE_STATUS.HAVE_FNA).then(()=>{
            cDao.getProfile(cid).then((profile)=>{
              nDao.showFnaInvalidFlag(cid).then(showFnaInvalidFlag => {
                cb({
                  success: true,
                  fe: result2.fe,
                  fna: result2.fna,
                  showFnaInvalidFlag,
                  profile
                });
              });
            });
          }).catch((error)=>{
            logger.error('ERROR: saveFE :', error);
            cb({success: false});
          });
        });
      });
    }
    else{
      cb({success: true, code: result.code});
    }
  })
}

module.exports.getFE = function(data, session, cb) {
  var cid = data.cid;
  nDao.getItem(cid, nDao.ITEM_ID.FE).then((fe)=>{
    cb({
      success: fe? true: false,
      fe
    })
  })
}

module.exports.saveFNA = function(data, session, cb) {
  var {cid, fna, confirm} = data;
  let {agentCode: aid} = session.agent;
  bDao.onSaveNeedItem(cid, fna, nDao.ITEM_ID.NA).then((result)=>{
    //remove warning message if not invalidate bundle
    if (result.code != bDao.CHECK_CODE.INVALID_BUNDLE && result.code != bDao.CHECK_CODE.INVALID_FNA_REPORT) {
      result.code = bDao.CHECK_CODE.VALID;
    }
    if (confirm || result.code === bDao.CHECK_CODE.VALID) {
      bDao.invalidNeedItem(cid, session.agent, fna, nDao.ITEM_ID.NA).then((resp)=>{
        let newId = _.get(resp, nDao.ITEM_ID.NA);
        if (newId) {
          fna.newId = newId;
        }
        nDao.saveFNA(session.agent, cid, fna).then((result2)=>{
          // bDao.updateStatus(cid, bDao.BUNDLE_STATUS.HAVE_FNA, ()=>{
          bDao.updateStatus(cid, bDao.BUNDLE_STATUS.HAVE_FNA).then(()=>{
            cDao.getProfile(cid).then((profile)=>{
              nDao.showFnaInvalidFlag(cid).then(showFnaInvalidFlag => {
                cb({
                  success: true,
                  fna: result2.fna,
                  showFnaInvalidFlag,
                  profile
                });
              });
            });
          }).catch((error)=>{
            logger.error('ERROR: saveFNA :', error);
            cb({success: false});
          });
        }).catch((e) => {
          logger.error(e);
        })
      });
    }
    else{
      cb({success: true, code: result.code});
    }
  })
}

module.exports.getFNA = function(data, session, cb) {
  var cid = data.cid;
  nDao.getItem(cid, nDao.ITEM_ID.NA).then((fna)=>{
    cb({
      success: fna? true: false,
      fna
    })
  })
}

var _getNeedForm = function() {
  return new Promise((resolve)=>{
    let promises = [];
    promises.push(nDao.getCNAForm());
    promises.push(nDao.getPdaForm());
    promises.push(nDao.getFeForm());
    promises.push(nDao.getFnaForm());
    Promise.all(promises).then((args)=>{
      resolve({
        cnaForm: args[0],
        pdaForm: args[1],
        feForm: args[2],
        fnaForm: args[3]
      })
    })
  })
}

module.exports.getNeedForm = function(data, session, cb) {
  _getNeedForm().then((result)=>{cb(result)});
}
var _initNeeds = function(data, session, cid) {
  return new Promise((resolve)=>{
    let promises = [];
    promises.push(nDao.getItem(cid, nDao.ITEM_ID.PDA));
    promises.push(nDao.getItem(cid, nDao.ITEM_ID.FE));
    promises.push(nDao.getItem(cid, nDao.ITEM_ID.NA));
    promises.push(new Promise((resolve3)=>{
      nDao.getNeedsSummary(resolve3);
    }))
    promises.push(cDao.getAllDependantsProfile(cid));
    promises.push(nDao.showFnaInvalidFlag(cid));

    Promise.all(promises).then((args) => {
      resolve({
        success: true,
        pda: args[0],
        fe: args[1],
        fna: args[2],
        needsSummary: args[3],
        dependantProfiles: args[4],
        showFnaInvalidFlag: args[5]
      })
    });
  })
}

module.exports.initNeeds = function(data, session, cb) {
  _initNeeds(data, session, data.cid).then((result)=>{
    cb(result)
  });
}

var handleProfileData = function(profile) {
  profile.name = profile.fullName;
  profile.mobile = profile.mobileNo? `${profile.mobileCountryCode} ${profile.mobileNo}`: '';
  profile.othMobile = profile.otherNo? `${profile.otherMobileCountryCode} ${profile.otherNo}`: '';
  profile.idDocType = profile.idDocType==='nric'?'NRIC': profile.idDocType === 'bcNo'?"Birth Certificate No.": profile.idDocType === 'passport'? 'Passport': profile.idDocType === 'fin'? 'FIN No.':  profile.idDocType === 'other'? `${profile.idDocType} - ${profile.idDocTypeOther}`:'';
  let dob = profile.dob? new Date(profile.dob): null;
  if (dob) {
    profile.dob = {
      day: dob.getDate(),
      month: dob.getMonth() + 1,
      year: dob.getFullYear()
    }
  }
  if (profile.occupation === 'O921' && !!profile.occupationOther){
    profile.occupation = getTitle('occupation', profile.occupation) + '-' + profile.occupationOther;
  }
  else {
    profile.occupation = getTitle('occupation', profile.occupation);
  }
  profile.nationality = getTitle('nationality', profile.occupation);
}

var getTitle = function(map, value) {
  return _.get(_.find(_.get(global.optionsMap,`${map}.options`), n=>n.value===value), 'title.en');
}

var handleCka = function(cka) {
  if (cka.course == "notApplicable") {
    cka.institution = "";
    cka.studyPeriodEndYear = "";
  }
  if (cka.collectiveInvestment != "Y") {
    cka.transactionType = "";
  }
  if (cka.insuranceInvestment != "Y") {
    cka.insuranceType = "";
  }
  if (!cka.profession == "notApplicable") {
    cka.years = null;
  }

  let {course,collectiveInvestment,insuranceInvestment,profession} = cka;

  cka.isValid = ((course && collectiveInvestment && insuranceInvestment && profession) && ((course!='notApplicable'?1:0)+(collectiveInvestment=='Y'?1:0)+(insuranceInvestment=='Y'?1:0)+(profession!='notApplicable'?1:0))>=1)? "Y": "N";
}

var handleDate = function(_d) {
  if (!_d) return "";

  let d = new Date(_d);
  let addZero = function(value) {
    return (value.toString().length == 1? "0": "") + value.toString();
  }
  return `${addZero(d.getDate())}/${addZero(d.getMonth() + 1)}/${d.getFullYear()}`;
}

var replaceNewLine = function(str = '') {
  return str.split('\n').join('[[@newLine]]');
}

var genFnaReportData = function(session, agent, profile, dependantProfiles, pda, fe, fna, skipEmailInReport, template, callback) {
  
 bDao.getCurrentBundle(profile.cid).then((_bundle)=>{
   try{
    let now = new Date();
    let printDate = _bundle.firstBiCreateTime;
    if (!printDate) {
      printDate = pda.lastUpd;
      if (printDate < fe.lastUpd) {
        printDate = fe.lastUpd;
      }
      if (printDate < fna.lastUpd) {
        printDate = fna.lastUpd;
      }
    }
    printDate = new Date(printDate);
    let printDateStr = `${printDate.getDate()}/${printDate.getMonth()+1}/${printDate.getFullYear()}`;
    let hasSpouse = pda.applicant === 'joint' ? true : false;
    let authorised = _.map(_.split(agent.authorisedDes, ","), _.trim);
    let hasILP = _.find(["Investment Linked Plan"], _id=>{return authorised.indexOf(_id) > -1})? "Y": "N";
    let hasLI = _.find(["Life Insurance"], _id=>{return authorised.indexOf(_id) > -1})? "Y": "N";
    let hasHI = _.find(["Health Insurance"], _id=>{return authorised.indexOf(_id) > -1})? "Y": "N";
    let hasUT = _.find(["Unit Trusts"], _id=>{return authorised.indexOf(_id) > -1})? "Y": "N";

    if (skipEmailInReport) {
      profile.email = '';
    }
    
    let reportData={
      root:{
        printDate: printDateStr,
        owner: profile,
        hasSpouse: hasSpouse ? "Y" : "N",
        agent: {
          name: agent.name,
          code: agent.agentCode,
          mobile: agent.mobile,
          hasILP,
          hasLI,
          hasHI,
          hasUT
        },
        personalData: {
          applicant: pda.applicant,
          ownerConsentMethod: pda.ownerConsentMethod,
          spouseConsentMethod: pda.spouseConsentMethod,
          dependants: []
        }
      }
    }, root = reportData.root;
    handleProfileData(root.owner);


    let spouse = _.find(profile.dependants, d=>d.relationship==='SPO');
    let spouseProfile;
    if (spouse) {
      spouseProfile = _.cloneDeep(dependantProfiles[spouse.cid]);
      if (hasSpouse) {
        root.spouse = _.cloneDeep(dependantProfiles[spouse.cid]);
        handleProfileData(root.spouse);
      }
    }

    //handle personal data acknowledgement
    let childs = [], sd=[];
    if (pda.dependants) {
      root.hasDependants = "Y";
      sd = pda.dependants.split(",");
      _.forEach(sd, d=>{
        let dependant = _.cloneDeep(dependantProfiles[d]) || {};
        //add into childs for later validation
        if (['SON', 'DAU'].indexOf(dependant.relationship)>-1) {
          childs.push(dependant.cid);
        }
        let targetSupYear = dependant.relationship==='SON'?25:dependant.relationship==='DAU'?22:dependant.gender==='M'?(80+8):(85+8);
        let dob = new Date(dependant.dob);

        let dAge = DateUtils.getNearestAge(printDate, dob);
        let supYrs = targetSupYear-dAge;
        if (1 >= dAge) {
          let pmon = printDate.getMonth(), dmon = dob.getMonth();
          if (printDate.getFullYear() > dob.getFullYear()) {
            pmon+=12;
          }
          if (pmon - dmon <=6) {
            if (pmon - dmon === 6) {
              if (printDate.getDate()>=dob.getDate()) {
                dAge = 1;
              }
              else {
                dAge = "<1";
              }
            }
            else{
              dAge = "<1";
            }
          }
          else{
            dAge = 1;
          }
        }
        let dobStr = `${dob.getDate()}/${dob.getMonth()+1}/${dob.getFullYear()}`

        let relationshipOther = '';
        let relationship = getRelationship(dependant.relationship);
        if (dependant.relationship ==='OTH'){
          let relationshipDependant = _.find(profile.dependants, d=>d.cid===dependant.cid);
          if (relationshipDependant && relationshipDependant.relationshipOther){
            relationshipOther = relationshipDependant.relationshipOther;
            relationship += '-' + relationshipOther;
          }
        }
        root.personalData.dependants.push({
          name: dependant.fullName,
          dob: dobStr,
          age: dAge,
          supYrs: supYrs>0?supYrs:0,
          relationship: relationship
        })
      })
    }
    for(let aa=3 - _.size(root.personalData.dependants); aa>0; aa-- ) {
      root.personalData.dependants.push({});
    }

    //handle prority
    let priorityValid = true;
    let _asts = _.split(fna.aspects, ",");
    let {sfAspects = [], spAspects = []} = fna;
    let priority = {otherSav:[], otherPro:[], psGoals: []};
    let pArr = _.concat(sfAspects, spAspects);
    //insert index
    _.forEach(pArr, (pObj, pIdx)=>{
      pObj.pIdx = pIdx + 1;
    });
    let _pArr = _.chain(pArr)
        .groupBy("aid")
        .toPairs()
        .map(function(currentItem) {
            return _.zipObject(["aid", "aspects"], currentItem);
        })
        .value();
    _.forEach(_pArr, pObj=>{
      if (!_.get(fna, `${pObj.aid}.isValid`)) {
        priorityValid = false;
      }
      else if (priorityValid) {
        if ('psGoals' === pObj.aid) {
          let _aArr = _.chain(pObj.aspects)
            .groupBy("title")
            .toPairs()
            .map(function(currentItem) {
                return _.zipObject(["title", "aspects"], currentItem);
            })
            .value();
          _.forEach(_aArr, _aObj=>{
            let a = '', ai = 1;
            _.forEach(_aObj.aspects, _a=>{
              let an = _a.cid === profile.cid? 'Myself': hasSpouse && _a.cid === _.get(spouseProfile, 'cid')? 'Spouse': _.get(dependantProfiles[_a.cid],'fullName');
              a += (a?", ": "") + `${_a.pIdx} - ${an}`;
            })
            priority.psGoals.push({
              title: _aObj.title,
              value: a,
              level: _.findIndex(pArr, _pObj=>{})
            });
          })
        }
        else if ('other' === pObj.aid) {
          let _aArr = _.chain(pObj.aspects)
            .groupBy("type")
            .toPairs()
            .map(function(currentItem) {
                return _.zipObject(["type", "aspects"], currentItem);
            })
            .value();
          
          _.forEach(_aArr, _aObj=>{
            let _tArr = _.chain(_aObj.aspects)
            .groupBy("title")
            .toPairs()
            .map(function(currentItem) {
                return _.zipObject(["title", "aspects"], currentItem);
            })
            .value();

            _.forEach(_tArr, _tObj=>{
              let a = '', ai = 1;
              _.forEach(_tObj.aspects, _t=>{
                let an = _t.cid === profile.cid? 'Myself': hasSpouse && _t.cid === spouseProfile.cid? 'Spouse': _.at(dependantProfiles[_t.cid],'fullName')[0];
                a += (a?", ": "") + `${_t.pIdx} - ${an}`;
              })
              if (_aObj.type === "SAVINGS") {
                priority.otherSav.push({
                  title: _tObj.title,
                  value: a
                })
              }
              else{
                priority.otherPro.push({
                  title: _tObj.title,
                  value: a
                })
              }
            })
          })
        } else if ('pcHeadstart' === pObj.aid) {
          _.forEach(['protection', 'critical'], aspectType => {
            const aspects = _.filter(pObj.aspects, aspect=>_.toLower(aspect.title).indexOf(aspectType) > -1);
            let a = '', ai = 1;
            _.forEach(aspects, aspect => {
              let cids = _.split(aspect.cid, ',');
              _.forEach(cids, (cid, index) => {
                cids[index] = 
                cids[index] === profile.cid ? 'Myself': 
                  hasSpouse && cids[index] === spouseProfile.cid ? 'Spouse' : 
                  _.get(dependantProfiles[cids[index]],'fullName');
              });
              let _cid = cids.join(", ");
              a += (a ? ', ' :  '') + `${aspect.pIdx} - ${_cid}`;
            });
            priority[(aspectType === 'protection' ? 'P' : 'C') + pObj.aid] = a;
          })
        } else {
          let a = '', ai = 1;
          _.forEach(pObj.aspects, _a=>{
            let cids = _.split(_a.cid, ",");
            for(let i in cids) {
              cids[i] = cids[i] === profile.cid? 'Myself': hasSpouse && cids[i] === spouseProfile.cid? 'Spouse': _.get(dependantProfiles[cids[i]],'fullName');
            }
            let _cid = cids.join(", ");
            a += (a?", ": "") + `${_a.pIdx} - ${_cid}`;
          })
          priority[pObj.aid] = a;
        }
      }
    });

    if (!_.size(priority.psGoals)) {
      priority.psGoals.push({})
    }
    if (!_.size(priority.otherSav)) {
      priority.otherSav.push({});
    }
    if (!_.size(priority.otherPro)) {
      priority.otherPro.push({});
    }

    if (priorityValid) {
      root.priority = priority;
    }
    else {
      root.priority = {otherSav:[{}], otherPro:[{}], psGoals: [{}]};
    }

    //handle networth
    let cnws = _.at(fe, ['owner.assets', 'owner.liabilities', 'owner.noALReason', 'owner.otherLiabilites', 'owner.otherLiabilitesTitle', 
      'spouse.otherLiabilites', 'spouse.otherLiabilitesTitle', 'owner.init', 'spouse.init', 'owner.otherAsset', 
      'owner.otherAssetTitle', 'owner.otherAsset', 'owner.otherAssetTitle']);
    let hasNoAlReason = !cnws[0] && !cnws[1];
    if (!((hasNoAlReason && !cnws[2]) || (cnws[3] && !cnws[4]) || (!cnws[3] && cnws[4]) || (cnws[9] && !cnws[10]) || (!cnws[9] && cnws[10]) 
      || cnws[7] || (hasSpouse && ((cnws[5] && !cnws[6]) || (!cnws[5] && cnws[6]) || (cnws[11] && !cnws[12]) || (!cnws[12] && cnws[11]) || cnws[8])))) {
      let netWorth = root.netWorth = {owner:{}, spouse: {}, valid: 'Y'};
      if (hasNoAlReason) {
        netWorth.noALReason = cnws[2];
      }

      if (hasSpouse) {
        _handleCurrency(netWorth.spouse, fe.spouse, nwIds, 0, true);
        netWorth.spouse.otherLiabilitesTitle = fe.spouse.otherLiabilitesTitle;
        netWorth.spouse.otherAssetTitle = fe.spouse.otherAssetTitle;
      }
      _handleCurrency(netWorth.owner, fe.owner, nwIds, 0, true);
      netWorth.owner.otherLiabilitesTitle = fe.owner.otherLiabilitesTitle;
      netWorth.owner.otherAssetTitle = fe.owner.otherAssetTitle;
    }

    //handle cash flow
    //check valid
    let valid = true;
    let incomeIds = ['mIncome', 'aBonus', 'netMIcncome'];
    let expenseIds = ['personExpenses', 'householdExpenses', 'insurancePrem', 'regSav', 'otherExpense'];
    let hasNoCFReason = 
      // no income provided
      _.findIndex(incomeIds, _s=>Number(_.get(fe.owner, _s, 0))) === -1 || 
      // no exspense provided
      _.findIndex(expenseIds, _s=>Number(_.get(fe.owner, _s, 0))) === -1;

    if (
       // input Other expense without other expense title
      (_.get(fe, 'owner.otherExpense') && !_.get(fe, 'owner.otherExpenseTitle')) ||
      (_.get(fe, 'spouse.otherExpense') && !_.get(fe, 'spouse.otherExpenseTitle')) ||
       // input Other expense title without other expense
      (!_.get(fe, 'owner.otherExpense') && _.get(fe, 'owner.otherExpenseTitle')) ||
      (!_.get(fe, 'spouse.otherExpense') && _.get(fe, 'spouse.otherExpenseTitle')) ||
      // no visit before
      _.get(fe, 'owner.init') || (hasSpouse && _.get(fe, 'spouse.init')) ||
      // without income / expense and reason
      (hasNoCFReason && !_.get(fe, 'owner.noCFReason'))
    ) {
      valid = false;
    }

    if (valid) {
      let cashFlow = root.cashFlow = {owner:{}, spouse: {}, valid: 'Y'};
      // check is it no input in UI
      if (hasNoCFReason) {
        root.cashFlow.noCFReason = _.get(fe, 'owner.noCFReason', '');
      }

      root.cashFlow.additionComment = _.get(fe, 'owner.additionComment', '');

      if (hasSpouse) {
        _handleCurrency(cashFlow.spouse, fe.spouse, cfIds, 0, true);  
        cashFlow.spouse.otherExpenseTitle = fe.spouse.otherExpenseTitle;
      }
      _handleCurrency(cashFlow.owner, fe.owner, cfIds, 0, true);
      cashFlow.owner.otherExpenseTitle = fe.owner.otherExpenseTitle;
      
      _.forEach(cf12Ids, _id=>{
        if (hasSpouse) {
          cashFlow.spouse[_id] = getCurrency(Number(_.get(fe, `spouse.${_id}`)) * 12, 0, true);
        }
        cashFlow.owner[_id] = getCurrency(Number(_.get(fe, `owner.${_id}`)) * 12, 0, true);
      })
    }

    //handle existing insurance portfolio
    var checkEiPorf = function(values={}, isOwner) {
      const { eiPorf, hospNSurg, aInsPrem, sInsPrem, noEIReason } = values;
      return (isOwner && !eiPorf && !hospNSurg && !aInsPrem && !sInsPrem && !noEIReason) || // no existing insurance Porfolio, annual insurance Premium and reason 
        ((values.eiPorf || values.hospNSurg) && !values.aInsPrem && !values.sInsPrem) || // has existing insurance Porfolio but no annual insurance Premium
        (!values.eiPorf &&  !values.hospNSurg && (values.aInsPrem || values.sInsPrem)) || // has annual insurance premium but no existing insurance porfolio
        values.init ? false : true
    }

    let eiValid = true;
    if (!checkEiPorf(fe.owner, true) || (hasSpouse && !checkEiPorf(fe.spouse))) {
      eiValid = false;
    }
    _.forEach(fe.dependants, d=>{
      if (childs.indexOf(d.cid)>-1 && !checkEiPorf(d)) {
        eiValid = false;
      }
    })

    if (eiValid) {
      let eo = {name: 'Myself'};
      let es = {name: 'Spouse', notShown: hasSpouse?'N':'Y'};
      _handleCurrency(eo, fe.owner, eikeys, 0, true);
      _handleString(eo, fe.owner, ['hospNSurg']);
      if (hasSpouse) {
        _handleCurrency(es, fe.spouse, eikeys, 0, true);
        _handleString(es, fe.spouse, ['hospNSurg']);
      }

      let oPayer = '';
      let oPayers = _.at(fe, [`owner.${pks[0]}`,`owner.${pks[1]}`,`owner.${pks[2]}`,`owner.${pks[3]}`]);
      if (oPayers[1]) oPayer = 'Proposer';
      if (profile.marital==='M' && oPayers[2]) oPayer += (oPayer?", ": "") + 'Spouse';
      if (oPayers[3]) oPayer += (oPayer?", ": "") + 'Others';
      eo.payer = oPayer;

      if (hasSpouse) {
        let sPayer = '';
        let sPayers = _.at(fe, [`spouse.${pks[1]}`,`spouse.${pks[2]}`,`spouse.${pks[3]}`]);
        if (sPayers[0]) sPayer = 'Proposer';
        if (sPayers[1]) sPayer += (sPayer ? ", " : "") + 'Spouse';
        if (sPayers[2]) sPayer += (sPayer ? ", " : "") + 'Others';
        es.payer = sPayer;
      }

      let eiPorf = root.eiPorf = {person:[{items:[eo, es]}]};
      if (!_.get(fe, `owner.eiPorf`) && !_.get(fe, `owner.hospNSurg`)) {
        eiPorf.noEIReason = _.get(fe, `owner.noEIReason`);
      }
      _.forEach(childs, (c, cidx)=>{
        let dependant = dependantProfiles[c];
        let d = _.find(fe.dependants, _d=>_d.cid === c);
        let dv = {name: dependant.fullName};
        _handleCurrency(dv, d, eikeys, 0, true);
        _handleString(dv, d, ['hospNSurg']);

        let otPayer = '';
        let otPayers = _.at(d, [pks[1],pks[2],pks[3]]);
        if (otPayers[0]) otPayer = 'Proposer';
        if (otPayers[1]) otPayer += (otPayer?", ": "") + 'Spouse';
        if (otPayers[2]) otPayer += (otPayer?", ": "") + 'Others';
        dv.payer = otPayer;
        if (eiPorf.person[eiPorf.person.length-1].items.length == 3) {
          eiPorf.person.push({items: []});
        }
        eiPorf.person[eiPorf.person.length - 1].items.push(dv);
      })
      if (eiPorf.person[0].items.length===2) {
        eiPorf.person[0].items.push({name: 'Child', notShown: "Y"})
      }
    }
    else {
      root.eiPorf = {person:[{items:[{name: 'Myself'}, {name: 'Spouse', notShown: hasSpouse?'N':'Y'}, {name: 'Child', notShown: 'Y'}]}]};
    }

    let aspects = _.split(_.get(fna, "aspects"), ",");

    //retire planning
    root.rPlanning = [{name: 'Myself', isActive: false}, {name: 'Spouse', isActive: false}];
    let rpac = 0;
    if (_.get(fna, 'rPlanning.isValid') && aspects.indexOf("rPlanning") > -1) {
      if (_.get(fna, 'rPlanning.owner.isActive')) {
        root.rPlanning[0] = handleRp(root, fna.rPlanning.owner, 'Myself', profile, template);
      }
      if (hasSpouse && spouseProfile && _.get(fna, 'rPlanning.spouse.isActive')) {
        root.rPlanning[1] = handleRp(root, fna.rPlanning.spouse, 'Spouse', spouseProfile, template);
      }
      _.forEach(root.rPlanning, _rp=>{
        rpac += _.size(_rp.assets);
      });
    }
    
    if (rpac === 0 && root.rPlanning[0]) {
      root.rPlanning[0].assets = [{}];
    }

    //income protection
    root.fiProtection = [{name: 'Myself'}, {name: 'Spouse'}];
    if (_.get(fna, 'fiProtection.isValid') && aspects.indexOf("fiProtection") > -1) {
      if (_.get(fna, 'fiProtection.owner.isActive')) {
        root.fiProtection[0] = handleFip(root, fna.fiProtection.owner, 'Myself', profile, template);
      }
      if (hasSpouse && spouseProfile && _.get(fna, 'fiProtection.spouse.isActive')) {
        root.fiProtection[1] = handleFip(root, fna.fiProtection.spouse, 'Spouse', spouseProfile, template);
      }
    }

    //critical illness protection
    let ccip = _.at(fna, ['ciProtection.isValid', 'ciProtection.owner.isActive', 'ciProtection.spouse.isActive']);
    let ciProtection = root.ciProtection = [];
    if (ccip[0] && aspects.indexOf("ciProtection") > -1) {
      if (ccip[1]) {
        addCiData(ciProtection, fna.ciProtection.owner, 'Myself');
      }
      else {
        ciProtection[0] = {items: [{name: 'Myself'}]};
      }

      if (hasSpouse && ccip[2]) {
        addCiData(ciProtection, fna.ciProtection.spouse, 'Spouse');
      }
      else {
        ciProtection[0].items.push({name: 'Spouse'});
      }
      
      if (ciProtection.length == 1 && ciProtection[0].items.length == 2) {
        ciProtection[0].items.push({name: 'Child'})
      }
    }
    if (!_.size(ciProtection)) {
      ciProtection[0] = {items:[{name: 'Myself'}, {name: 'Spouse'}, {name: 'Child'}]};
    }

    
    root.iarRateNotMatchReason = fe.isCompleted && fna.lastStepIndex > 0 && (fna.iarRate < 0 || fna.iarRate > 6)? fna.iarRateNotMatchReason: '';

    //Disability income protection
    let cdip = _.at(fna, ['diProtection.isValid', 'diProtection.owner.isActive', 'diProtection.spouse.isActive']);
    let diProtection = root.diProtection = [];
    if (cdip[0] && aspects.indexOf("diProtection") > -1) {
      if (cdip[1]) {
        addDiData(diProtection, fna.diProtection.owner, 'Myself');
      }
      else {
        diProtection[0] = {items: [{name: 'Myself'}]};
      }

      if (hasSpouse && cdip[2]) {
        addDiData(diProtection, fna.diProtection.spouse, 'Spouse');
      }
      else {
        diProtection[0].items.push({name: 'Spouse'});
      }

      _.forEach(childs, ch=>{
        let didp = _.find(fna.diProtection.dependants, fd=>{return fd.cid === ch});
        if (didp && didp.isActive) {
          addDiData(diProtection, didp, dependantProfiles[ch].fullName);
        }
      })
      if (_.size(diProtection[0].items) == 2) {
        diProtection[0].items.push({name: 'Child'});
      }

    }

    if (!_.size(root.diProtection)) {
      root.diProtection[0] = {items:[{name: 'Myself'}, {name: 'Spouse'}, {name: 'Child'}]};
    }

    //Personal accident protection
    let cpaip = _.at(fna, ['paProtection.isValid', 'paProtection.owner.isActive', 'paProtection.spouse.isActive']);
    let paProtection = root.paProtection = [];
    if (cpaip[0] && aspects.indexOf("paProtection") > -1) {
      if (cpaip[1]) {
        addPaData(paProtection, fna.paProtection.owner, 'Myself');
      }
      else {
        paProtection[0] = {items:[{name: 'Myself'}]};
      }

      if (hasSpouse && cpaip[2]) {
        addPaData(paProtection, fna.paProtection.spouse, 'Spouse');
      }
      else {
        paProtection[0].items.push({name: 'Spouse'});
      }

      _.forEach(childs, ch=>{
        let cadp = _.find(fna.paProtection.dependants, fd=>{return fd.cid === ch});
        if (cadp && cadp.isActive) {
          addPaData(paProtection, cadp, dependantProfiles[ch].fullName);
        }
      })
      if (_.size(paProtection[0].items) == 2) {
        paProtection[0].items.push({name: 'Child'});
      }
    }
    if (!_.size(paProtection)) {
      paProtection[0] = {items:[{name: 'Myself'}, {name: 'Spouse'}, {name: 'Child'}]};
    }

    //Planning for Children's Headstart
    let pcHeadstart = root.pcHeadstart = [];
    if (_.get(fna, 'pcHeadstart.isValid') && aspects.indexOf("pcHeadstart") > -1) {
      _.forEach(childs, ch=>{
        let pchdp = _.find(fna.pcHeadstart.dependants, fd=>{return fd.cid === ch});
        if (pchdp && pchdp.isActive) {
          addPchData(pcHeadstart, pchdp, dependantProfiles[ch].fullName);
        }
      })
    }
    if (!_.size(pcHeadstart)) {
      pcHeadstart[0] = {};
    }

    //education planning
    let ePlanning = root.ePlanning = [];
    if (_.get(fna, 'ePlanning.isValid') && aspects.indexOf("ePlanning") > -1) {
      _.forEach(childs, ch=>{
        let ep = _.find(fna.ePlanning.dependants, epd=>{return epd.cid === ch});
        if (ep && ep.isActive) {
          let dependant = dependantProfiles[ch];
          addEpData(ePlanning, ep, dependant.fullName, template);
        }
      })
    }
    if (!_.size(root.ePlanning)) {
      root.ePlanning[0] = {};
    }
    _.forEach(root.ePlanning, epi=>{
      if (!_.size(epi.assets)) {
        epi.assets = [{}];
      }
    })

    let hcProtection = root.hcProtection = [];
    let chcp = _.at(fna, ['hcProtection.isValid', 'hcProtection.owner.isActive', 'hcProtection.spouse.isActive']);
    if (chcp[0] && aspects.indexOf("hcProtection") > -1) {
      if (chcp[1]) {
        addHcpData(hcProtection, fna.hcProtection.owner, 'Myself');
      }
      else{
        hcProtection[0] = {items:[{name: 'Myself'}]};
      }

      let addHcSpuseData = false;
      if (hasSpouse && chcp[2]) {
        addHcpData(hcProtection, fna.hcProtection.spouse, 'Spouse');
      }
      else if (spouse && spouse.cid) {
        // add spouse data if spouse is dependant, not myself and spouse
        const hcpSData = _.find(fna.hcProtection.dependants, hcpd=>_.find(sd, d=>d === hcpd.cid) && hcpd.cid === spouse.cid && hcpd.isActive);
        if (hcpSData) {
          addHcSpuseData = true;
          addHcpData(hcProtection, hcpSData, 'Spouse');
        } else {
          hcProtection[0].items.push({name: 'Spouse'});
        }
      }

      _.forEach(sd, d=>{
        let hcpe = _.find(fna.hcProtection.dependants, hcpd=>{return hcpd.cid === d});
        let hcpDep = dependantProfiles[d];
        if (hcpe && hcpe.isActive) {
          if (addHcSpuseData && spouse && hcpe.cid === spouse.cid) {
            //don't add data if spouse data add before
          } else {
            addHcpData(hcProtection, hcpe, _.get(hcpDep, 'fullName'));
          }
        }
      })
      if (_.size(hcProtection[0].items) === 2) {
        hcProtection[0].items.push({name: 'Child'});
      }
    }
    if (!_.size(root.hcProtection)) {
      root.hcProtection[0] = {items:[{name: 'Myself'}, {name: 'Spouse'}, {name: 'Child'}]};
    }

    let psGoals = root.psGoals = [];
    if (_.get(fna, 'psGoals.isValid') && aspects.indexOf("psGoals") > -1) {
      addPsgData(psGoals, fna.psGoals.owner, 'Myself', template);
      if (!psGoals[0]) {
        psGoals[0] = {name: 'Myself'}
      }
      if (hasSpouse) {
        addPsgData(psGoals, fna.psGoals.spouse, 'Spouse', template)
      };
      if (!psGoals[1]) {
        psGoals[1] = {name: 'Spouse'}
      }
    }
    else {
      root.psGoals = [{name: 'Myself'}, {name: 'Spouse'}];
    }
    _.forEach(root.psGoals, _psg=>{
      if (!_.size(_psg.assets)) {
        _psg.assets = [{}];
      }
    })


    let otherNeed = root.otherNeed = [];
    if (_.get(fna, 'other.isValid') && aspects.indexOf("other") > -1) {
      if (_.get(fna, 'other.owner.isActive')) {
        addOData(otherNeed, fna.other.owner, 'Myself');
      }
      if (!_.size(otherNeed)) {
        otherNeed.push({items:[{name: 'Myself'}], goals: []})
      }
      
      if (hasSpouse && _.get(fna, 'other.spouse.isActive')) {
        addOData(otherNeed, fna.other.spouse, 'Spouse');
      }
      if (_.size(otherNeed[0].items) == 1) {
        otherNeed[0].items.push({name: 'Spouse'});
      }

      _.forEach(childs, ch=>{
        let fon = _.find(fna.other.dependants, fod=>{fod.cid === ch});
        if (fon && fon.isActive) {
          addOData(otherNeed, fon, dependantProfiles[ch].fullName);
        }
      })
      if (_.size(otherNeed[0].items) === 2) {
        otherNeed[0].items.push({name: 'Child'});
      }
    }
    else {
      root.otherNeed = [{items:[{name: 'Myself'}, {name: 'Spouse'}, {name: 'Child'}], goals: [{}]}];    
    }

    _.forEach(otherNeed, (o)=>{
      if (!_.size(o.goals)) {
        o.goals = [{}];
      }
    })
    
    //client choice budget
    
    if (fna.isCompleted && _.get(_bundle, "clientChoice.isBudgetCompleted") && !_.get(_bundle, "clientChoice.isAppListChanged")) {
      let clientChoiceBudget = {};
      let _ccbIds = [];
      let bundleChoice = _.get(_bundle, "clientChoice.budget") || {};
      _.forEach(["cpfMs", "cpfOa", "cpfSa", "rp", "sp", "srs"], (_id)=>{
        let compareId = `${_id}Compare`;
        _handleCurrency(clientChoiceBudget, bundleChoice, [`${_id}Budget`, compareId, `${_id}TotalPremium`]);
        // if (_.get(bundleChoice, compareId) < 0) {
        //   clientChoiceBudget[compareId] = "(" + clientChoiceBudget[compareId].replace("-", "") + ")";
        // }
      });
      _.forEach(["Less", "More"], (_id)=>{
        if (bundleChoice[`budget${_id}Choice`] === "Y") {
          clientChoiceBudget[`budget${_id}Reason`] = replaceNewLine(bundleChoice[`budget${_id}Reason`]);
        }
      })
      root.clientChoiceBudget = clientChoiceBudget;

    }

    //budget
    let bgValid = true;
    let hasBudget = false;
    let budget = {};
    if (fe.owner && !fe.owner.init) {
      let owner = fe.owner;
      _handleCurrency(budget, owner, ["singPrem", "aRegPremBudget", "cpfOaBudget", "cpfSaBudget", "cpfMsBudget", "srsBudget"], 2, true);
      if (owner.aRegPremBudget) {
        budget.hasRegPrem = "Y";
        hasBudget = true;
        if (!owner.aRegPremBudgetSrc || (owner.confirmBudget === 'Y' && !owner.confirmBudgetReason)) {
          bgValid = false;
        }
        else {
          _handleString(budget, owner, ["aRegPremBudgetSrc", "confirmBudget"])
          if (budget.confirmBudget === 'Y') {
            _handleString(budget, owner, ["confirmBudgetReason"])
          }
        }
      }

      if (owner.singPrem) {
        hasBudget = true;
        if (!owner.singPremSrc || (owner.confirmSingPremBudget === 'Y' && !owner.confirmSingPremBudgetReason)) {
          bgValid = false;
        }
        else {
          _handleString(budget, owner, ["singPremSrc", "confirmSingPremBudget"])
          if (budget.confirmSingPremBudget === 'Y') {
            _handleString(budget, owner, ["confirmSingPremBudgetReason"])
          }
        }
      }

      if (owner.cpfOaBudget) {
        hasBudget = true;
        if (!owner.cpfOa || owner.cpfOaBudget > owner.cpfOa) {
          bgValid = false;
        }
      }

      if (owner.cpfSaBudget) {
        hasBudget = true;
        if (!owner.cpfSa || owner.cpfSaBudget > owner.cpfSa) {
          bgValid = false;
        }
      }

      if (owner.cpfMsBudget) {
        hasBudget = true;
        if (!owner.cpfMs || owner.cpfMsBudget > owner.cpfMs) {
          bgValid = false;
        }
      }

      if (owner.srsBudget) {
        hasBudget = true;
        if (!owner.srs || owner.srsBudget > owner.srs) {
          bgValid = false;
        }
      }

      if (!owner.forceIncome || (owner.forceIncome === 'Y' && !owner.forceIncomeReason)) {
        bgValid = false;
      }
      if (owner.forceIncome) {
        _handleString(budget, owner, ["forceIncome"]);
        if (owner.forceIncome === 'Y') {
          _handleString(budget, owner, ["forceIncomeReason"]);
        }
      }
    }
    else {
      bgValid = false;
    }

    if (hasBudget && bgValid) {
      root.budget = budget;
    }

    //trusted individual
    if (pda && pda.isCompleted && (checkHasTi(profile) || (hasSpouse && checkHasTi(spouseProfile)))) {
      root.hasTi = pda.trustedIndividual;
      if (root.hasTi === 'Y') {
        let language = {
          "en": "English",
          "mandarin": "Mandarin",
          "malay": "Malay",
          "tamil": "Tamil"
        }
        let _idDocType = {
          "nric": "NRIC",
          "bcNo": "Birth Certificate No.",
          "fin": "FIN",
          "passport": "Passport"
        }

        let tiLang = _.get(_bundle, `formValues.${profile.cid}.declaration.TRUSTED_IND01`);
        tiLang = tiLang? tiLang == "other"? _.get(_bundle, `formValues.${profile.cid}.declaration.TRUSTED_IND02`): language[tiLang]: "";
        root.ti = _.cloneDeep(profile.trustedIndividuals);
        root.ti.relationship = getRelationship(root.ti.relationship);
        root.ti.language = tiLang || "";
        handleProfileData(root.ti);
      }
    }

    if (!root.ti) {
      root.ti = {idDocType: 'NRIC/Passport'};
    }
    
    let prodType = _.get(fna, "productType.prodType") || "";
    let hasCka = fe.isCompleted && fna.lastStepIndex >= 4 && _.get(fna, "ckaSection.isValid");
    if (hasCka && prodType.indexOf("investLinkedPlans") > -1) {
      root.cka = {};
      root.cka.owner = fna.ckaSection.owner || {};
      handleCka(root.cka.owner);
    }

    let hasRa = fe.isCompleted && fna.lastStepIndex >= 4 && _.get(fna, "raSection.isValid");
    if (hasRa && (prodType.indexOf("participatingPlans") > -1 || prodType.indexOf("investLinkedPlans") > -1)) {
      root.ra = {};
      root.ra.owner = fna.raSection.owner || {};
      let haveSelfSectLv = _.get(fna, "ckaSection.isValid") && _.get(fna, "ckaSection.owner.passCka") == "Y"?true: false;
      handleRa(root.ra.owner, haveSelfSectLv);
    }

    AppHandler.getAppListView({profile}, session, function(aList) {
      let {clientChoiceFinished, quotCheckedList} = aList;
      let declaration = _.get(_bundle, `formValues.${profile.cid}.declaration`);
      if (declaration && clientChoiceFinished && fna.isCompleted) {
        let tickActivity = _.get(declaration, "ROADSHOW01");
        if (tickActivity === "Y") {
          root.tickActivity = _.get(declaration, "ROADSHOW04");
          let tickDate = _.get(declaration, "ROADSHOW02");
          if (tickDate) {
            tickDate = new Date(tickDate);
            root.tickDate = `${tickDate.getDate()}/${tickDate.getMonth()+1}/${tickDate.getFullYear()}`;
            root.tickVenue = _.get(declaration, "ROADSHOW03")||"";
          }
        }
      }
      var quots = [], qidx = 1;
      var addQuot = function(quotId, isSelected, callback) {
        dao.getDoc(quotId, function(quot) {
          quot.isSelected = isSelected?"Y":"N";
          quots.push(quot);
          if (_.size(quots) === _.size(quotCheckedList)) {
            callback(quots);
          }
          else {
            addQuot(quotCheckedList[qidx].id, quotCheckedList[qidx++].checked, callback);
          }
        })
      }
      root.cmdProd = [];
      root.selectedPlan=[];
      root.unselectedPlan=[];
      root.choiceCompleted = clientChoiceFinished && fna.isCompleted? "Y": "N";
      if (aList.clientChoiceFinished && _.size(quotCheckedList) && fna.isCompleted) {
        //recommendation
        root.recommendProd = [];
        _.forEach(fna.sfAspects, sfa=>{
          let cids = _.split(sfa.cid, ",");
          for(var i in cids) {
            cids[i] = cids[i] === profile.cid?"Myself": (hasSpouse && spouseProfile && spouseProfile.cid===cids[i])? "Spouse": _.at(dependantProfiles[cids[i]], 'fullName')[0];
          }
          let cmdProd = {
            name: cids.join(", "),
            // special handling for disability benefit since the value changed
            aspect: _.toLower(sfa.title).indexOf('disability') > - 1 ? 'Disability benefit' : sfa.title
          }
          if (_.size(cids) === 1) {
            let aValues = _.at(fna, [`${sfa.aid}.owner`,`${sfa.aid}.spouse`,`${sfa.aid}.dependants`]);
            let aValue = profile.cid === sfa.cid? aValues[0]: (hasSpouse && spouseProfile.cid === sfa.cid)? aValues[1]: _.find(aValues[2], _av=>_av.cid === sfa.cid);
            if (aValue) {
              if (['psGoals', 'other'].indexOf(sfa.aid) > -1) {
                let _sValue = _.get(aValue, `goals[${sfa.index - 1}]`, {});
                cmdProd.totShortfall = _sValue.totShortfall;
                cmdProd.timeHorizon = _sValue.timeHorizon;
              } else if (['pcHeadstart'].indexOf(sfa.aid) > -1) {
                cmdProd.totShortfall =  _.toLower(sfa.title).indexOf('protection') > -1 ? aValue.pTotShortfall : aValue.ciTotShortfall;
              }
              else{
                cmdProd.totShortfall = aValue.totShortfall;
                cmdProd.timeHorizon = aValue.timeHorizon;
              }

            }
          }
          root.recommendProd.push(cmdProd);
        });
        
        addQuot(quotCheckedList[0].id, quotCheckedList[0].checked, function(_quots) {
          _.forEach(_quots, (_qu, quIdx)=>{
            let funds = _.get(_qu, "fund.funds") || [{}];
            let quotItems = [];
            let shieldInfo = {};
            if (_qu.quotType == 'SHIELD') {
              _handleCurrency(shieldInfo, _qu, ['totCashPortion', 'totMedisave', 'totPremium']);
              for(var i in _qu.insureds) {
                handleQuot(_qu.insureds[i], root, _qu.quotType);
                let { plans = [], iFullName = '' } = _.get(_qu, `insureds[${i}]`, {});
                let planNames = _.map(plans, 'covName.en', []);
                let covCodes = _.map(plans, 'covCode', []);
                let covClasses = _.map(plans, 'covClass', []);
                quotItems = _.concat(quotItems, plans);
                
                if (_qu.isSelected === "Y") {
                  //find if there is any plans selected with same plans and rider
                  //AXA Shield (Plan A) and AXA SHield (Plan B) will have same covCode but different covClass
                  let sameIdx = _.findIndex(root.selectedPlan, sPlan => {
                    return sPlan.quIdx === quIdx 
                      && !_.difference(sPlan.covCodes, covCodes).length && !_.difference(covCodes, sPlan.covCodes).length
                      && !_.difference(sPlan.covClasses, covClasses).length && !_.difference(covClasses, sPlan.covClasses).length
                  });
                  if (sameIdx === -1) {
                    root.selectedPlan.push({
                      quotType: _qu.quotType,
                      value: _.join(planNames, ', '),
                      name: iFullName,
                      covCodes,
                      covClasses,
                      quIdx
                    });
                  } else {
                    root.selectedPlan[sameIdx].name += ' / ' + iFullName;
                  }
                }
                else if (_qu.isSelected === "N") {
                  //find if there is any plans selected with same plans and rider
                  let sameIdx = _.findIndex(root.unselectedPlan, sPlan => {
                    return  sPlan.quIdx === quIdx && !_.difference(sPlan.covCodes, covCodes).length && !_.difference(covCodes, sPlan.covCodes).length
                  });
                  if (sameIdx === -1 ) {
                    root.unselectedPlan.push({
                      quotType: _qu.quotType,
                      value: _.join(planNames, ', '),
                      name: iFullName,
                      covCodes,
                      covClasses,
                      quIdx
                    });
                  } else {
                    root.unselectedPlan[sameIdx].name += ' / ' + iFullName;
                  }
                }
              }


              let ropBlock = _.get(_qu, "clientChoice.recommendation.rop_shield.ropBlock");
              if (ropBlock) {
                let hasRop = false;
                _qu.clientChoice.Q1 = "N";
                _qu.clientChoice.Q2 = "N";
                _qu.clientChoice.Q3 = "N";
                for(var i in ropBlock.iCidRopAnswerMap) {
                  if (ropBlock[ropBlock.iCidRopAnswerMap[i]] === "Y") {
                    _qu.clientChoice.Q1 = "Y";
                    _qu.clientChoice.Q2 = ropBlock.ropQ2;
                    _qu.clientChoice.Q3 = ropBlock.ropQ3;
                  }
                }
              }
            }
            else {
              handleQuot(_qu, root);
              quotItems = _qu.plans;
              if (_qu.isSelected === "Y") {
                root.selectedPlan.push({
                  value: _.join(_.map(_qu.plans, 'covName.en', []), ','),
                  name: _qu.iFullName
                });
              }
              else if (_qu.isSelected === "N") {
                root.unselectedPlan.push({
                  value: _.join(_.map(_qu.plans, 'covName.en', []), ','),
                  name: _qu.iFullName
                });
              }
            }

            // replace \n to br
            let recommendation = _.get(_qu, "clientChoice.recommendation");
            let isShield = _.get(_qu, 'quotType') === 'SHIELD';
            if (recommendation) {
              _.forEach(["benefit", "reason", "limitation"], rId => {
                recommendation[rId] = replaceNewLine(recommendation[rId]);
              })
              let rop = recommendation.rop;
              let shieldRop = recommendation.rop_shield;
              if (rop && !isShield) {
                _.forEach(["choiceQ1Sub3"], rId => {
                  rop[rId] = replaceNewLine(rop[rId]);
                })
              } else if (isShield && shieldRop) {
                rop['isConsultantExplanedRopinShield'] = _.get(recommendation, 'rop_shield.ropBlock.ropQ3', 'N');
                rop['choiceQ1Sub3'] = replaceNewLine(_.get(recommendation, 'rop_shield.ropBlock.ropQ1sub3', ''));
              }
            }
            
            root.cmdProd.push({
              type: _qu.quotType, 
              isSelected: _qu.isSelected, 
              items: quotItems, 
              funds, 
              choice:_qu.clientChoice,
              shieldInfo
            });
            
          })

          if (!_.size(root.cmdProd)) {
            root.cmdProd = [{}];
          }
          for(let i = 3 - _.size(root.selectedPlan); i > 0; i--) {
            root.selectedPlan.push({});
          }
          for(let i = 3 - _.size(root.unselectedPlan); i > 0; i--) {
            root.unselectedPlan.push({});
          }
          callback(reportData);
        })
      }
      else{
        root.cmdProd = [{}];
        for(let i = 3 - _.size(root.selectedPlan); i > 0; i--) {
          root.selectedPlan.push({});
        }
        for(let i = 3 - _.size(root.unselectedPlan); i > 0; i--) {
          root.unselectedPlan.push({});
        }
        callback(reportData);
      }
    })
   }
   catch(e) {
     logger.error("ERROR IN GENERATE FNA REPORT: ", e);
     callback(false);
   }
  })
}

var handleQuot = function(quot={}, root={}, quotType = "") {
  const baseProductCode = quot.baseProductCode || "";
  _.forEach(quot.plans, plan=>{
    let mode = quotType == "SHIELD" ? _.get(plan, "payFreq") : _.get(quot, "paymentMode");
    let frequency = mode === "A"? "Annual": mode === "S"? "Semi Annual": mode === "Q"? "Quarterly": mode === "M"? "Monthly": "-";
    plan.productLineTitle = _.get(global, `optionsMap.productLine.options.${quot.plans[0].productLine}.title.en`);
    plan.frequency = plan.covCode === 'IND'? '': frequency;
    plan.mode = handleQuotPlanPremiumMode(quot);
    plan.policyTerm = quotType == "SHIELD"? "-": plan.policyTerm === "999"? baseProductCode === "BAA"? "75": "99": plan.polTermDesc;
        // plan.policyTermYr ? plan.policyTermYr : plan.policyTerm;
    plan.premTermDesc = plan.premTermDesc || '-';
    plan.sumInsured = handleQuotPlanSumInsured(quotType, plan, baseProductCode);
    plan.premium = getCurrency(plan.premium);
    plan.iName = quot.iFullName;
    plan.ccy = quot.ccy;
    if (['LTT', 'LVT', 'ADBR', 'EPB', 'WPSR', 'WPTN', 'WPPT', 'LARB', 'CARB'].indexOf(plan.covCode) > -1) {
      plan.premium = 'unit deduction';
      plan.mode = '-';
      plan.frequency = '-';
    }
  })
  
}

const handleQuotPlanPremiumMode= function(quot) {
  const rpPaymentMethod = ['A', 'S', 'Q', 'M'];
  const spPaymentMethod = ['L'];
  const paymentMode = _.get(quot, 'paymentMode');
  if (quot.quotType === 'SHIELD') {
    return 'RP';
  } else if (rpPaymentMethod.indexOf(paymentMode) > -1) {
    return 'RP';
  } else if (spPaymentMethod.indexOf(paymentMode) > -1) {
    const lang = 'en';
    return _.get(quot, `policyOptionsDesc.paymentMethod.${lang}`, 'SP');
  } else {
    return 'RP';
  }
}

const handleQuotPlanSumInsured = function(quotType, plan, baseProductCode) {
  const isShield = quotType === 'SHIELD'
  if (isShield) {
    switch(_.get(plan, 'covClass')) {
      case 'A':
        return 'Plan A';
      case 'B':
        return 'Plan B';
      case 'C':
        return 'Standard Plan';
      default:
        return '-';
    }
  } else if (baseProductCode === 'IND') {
    const planSumInsured = _.get(plan, 'sumInsured', 0);
    return (planSumInsured === 0) ? '-' : getCurrency(planSumInsured);
  } else if (['AWT', 'PUL'].indexOf(baseProductCode) > -1) {
    return '-';
  } else if (plan.saViewInd == 'Y') {
    return getCurrency(_.get(plan, 'sumInsured', 0));
  } else {
    return '-';
  }
}

var handleRp = function(root, value={}, name, profile, template) {
  let result = {name, assets:[], hasData: 'Y'};
  _.forEach(value.assets, (g,gi)=>{
    let aItem = {title: getAssetName(g.key, template)};
    _handleCurrency(aItem, g, ["calAsset", "usedAsset"]);
    _handleDemical(aItem, g, ["return"]);
    result.assets.push(aItem);
  })
  _handleInteger(result, value, ["retireAge", "timeHorizon", "retireDuration"]);
  _handleDemical(result, value, ["avgInflatRate", "iarRate2"]);
  _handleCurrency(result, value, ["annualInReqRetirement", "compFv", "othRegIncome", "firstYrPMT", "compPv", "maturityValue"]);
  _handleString(result, value, ["unMatchPmtReason"]);
  result.inReqRetirement = getCurrency((value.inReqRetirement || 0)*12);
  _handleShortfall(result, value);
  if (value.inReqRetirement > profile.allowance) {
    if (name==='Myself') {
      root.oUnmatchIncome = value.unMatchPmtReason
    }
    else {
      root.sUnmatchIncome = value.unMatchPmtReason
    }
  }
  return result;
}

var _handleInteger = function(data, oValue, ids) {
  _.forEach(ids, (id)=>{
    let value = Number(oValue[id] || 0);
    data[id] = value < 0? `(${Math.abs(value)})`: value.toString();
  })
}

var _handleCurrency = function(data, oValue, ids, decimal = 2, showBlankIfZero) {
  _.forEach(ids, (id)=>{
    let value = oValue[id] || 0;
    data[id] = getCurrency(value, decimal, showBlankIfZero);
  })
}

var getCurrency = function(value = 0, decimal = 2, showBlankIfZero) {
  value = Number(value);
  
  // don't show 0 if user not input data
  if (showBlankIfZero && (value === 0 || !value)) { return ''; }
  
  let text = Math.abs(value).toFixed(decimal).toString();
    let temp = text.split(",");
    temp[0] = temp[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    return value < 0? `(${temp.join(".")})`: temp.join(".");
}

var _handleDemical = function(data, oValue, ids) {
  _.forEach(ids, (id)=>{
    let value = Number(oValue[id] || 0);
    let text = Math.abs(value).toFixed(2).toString();
    data[id] = value<0? `(${text})`: text;
  })
}

var _handleString = function(data, oValue, ids) {
  _.forEach(ids, (id)=>{
    data[id] = oValue[id] || "";
  })
}

var _handleShortfall = function(data, value, id = 'totShortfall') {
  data[id] = getCurrency(Math.abs(value[id] || 0));
  if (value[id] < 0) {
    data[id] = "(" + data[id] + ")";
  }
}

var handleFip = function(root, value={}, name, profile, template) {
  let result = {name};  
    let sum = 0;
    _.forEach(value.assets, (g,gi)=>{
      sum += g.calAsset;
    })
    result.totAsset = getCurrency(sum);
    _handleInteger(result, value, ["requireYrIncome", "timeHorizon", "retireDuration"]);
    _handleCurrency(result, value, ["pmt", "annualRepIncome", "lumpSum", "totLiabilities", "finExpenses", "totRequired", "existLifeIns", "maturityValue", "othFundNeeds"]);
    _handleDemical(result, value, ["iarRate2"]);
    _handleString(result, value, ["othFundNeedsName"]);
    if (result.othFundNeedsName == "") {
      delete result.othFundNeedsName;
    }
    _handleShortfall(result, value);

    if (value.pmt>profile.allowance) {
    if (name==='Myself') {
      root.oUnmatchPmt = value.unMatchPmtReason
    }
    else {
      root.sUnmatchPmt = value.unMatchPmtReason
    }
  }  

  return result;
}

var handleRa = function(values, hasSelectRLv) {
  let questions = _.at(values, ["riskPotentialReturn", "avgAGReturn", "smDroped", "alofLosses", "expInvTime" ,"invPref"]), totSource = 0, valid = true;
  _.forEach(questions, (q)=>{
    let source = Number(q);
    if (!source) {
      valid = false;
    }
    else {
      totSource += source;
    }
  })
  if (valid) {
    values.totSource = totSource;
  }
  if (!values.selfSelectedriskLevel || values.selfSelectedriskLevel == values.assessedRL || (!hasSelectRLv && valid)) {
    values.selfSelectedriskLevel = "";
    values.selfRLReasonRP = "";
  }
}
var checkHasTi = function(profile={}) {
  var cnt=0;

  if (profile.age > 62)
    cnt++;
  if (profile.education === 'below')
    cnt++;
  if (profile.language && profile.language.indexOf("en") == -1)
    cnt++;
  return cnt>1?true: false;
}

var getAspectName = function(aid, template) {
  let {fnaForm} = template;
  return _.at(_.find(_.at(fnaForm, 'items[0].items[0].options')[0], opt=>{return opt.value === aid}), 'title.en')[0];
}

var getAssetName = function(aid, template) {
  let {feForm} = template;
  return _.at(_.find(_.at(feForm, 'items[0].items[0].items[0].items[0].items')[0], opt=>{return opt.id === aid}), 'title.en')[0];
}

var addOData = function (oArr, value={}, name) {
  let item = {name, isActive: value.isActive? "Y": "N"};
  let position = oArr.length === 0 || oArr[oArr.length-1].items.length === 3? 1: oArr[oArr.length-1].items.length+1;
  if (value.isActive) {
    if (position === 1) {
      oArr.push({items: [], goals:[]});
    }
    oArr[oArr.length - 1].items.push(item);
    let {goalNo, goals} = value;
    let index = Number(goalNo);
    for(let i=0; i<index; i++) {
      let goal = goals[i];
      let oItem = {position};
      _handleCurrency(oItem, goal, ["needsValue", "extInsuDisplay"]);
      _handleString(oItem, goal, ["goalName"]);
      _handleShortfall(oItem, goal);
      oArr[oArr.length - 1].goals.push(oItem);
    }
  }
}

var addPsgData = function(psgArr, value={}, name, template) {
  let item = {name};
  if (value.isActive) {
    let items = [];
    let assets = [];
    let {goalNo=0, goals=[]} = value;
    let index = Number(goalNo);
    for(let i=0; i<index; i++) {
      let goal = goals[i] || {};
      let spgData = {name: goal.goalName};
      _handleCurrency(spgData, goal, ["compFv", "projMaturity"]);
      _handleInteger(spgData, goal, ["timeHorizon"]);
      _handleShortfall(spgData, goal);
      _.forEach(goal.assets, ga=>{
        let aItem ={title: getAssetName(ga.key, template), position: i+1};
        _handleCurrency(aItem, ga, ["calAsset", "usedAsset"]);
        _handleDemical(aItem, ga, ["return"]);
        assets.push(aItem);
      })
      items.push(spgData);
    }
    item.assets = assets;
    item.items = items;
  }
  psgArr.push(item);
}

var getRelationship = function(key) {
  syncGatewayUtil.getDocFromSG('relationship', (err, result) => {
    let options = result.options;
    return _.at(_.find(options, opt=>opt.value===key), 'title.en')[0];
  });
}

var addCiData = function(ciArr, value={}, name) {
  let ciData = {name};
  _handleCurrency(ciData, value, ["pmt", "annualLivingExp", "lumpSum", "mtCost", "totCoverage", "ciProt"]);
  _handleDemical(ciData, value, ["iarRate2"]);
  _handleInteger(ciData, value, ["requireYrIncome"])
  _handleShortfall(ciData, value);
  
  let totAsset = 0;
  _.forEach(value.assets, ca=>{
    totAsset += ca.usedAsset;
  })
  ciData.totAsset = getCurrency(totAsset);

  if (ciArr.length === 0 || ciArr[ciArr.length-1].items.length === 3) {
    ciArr.push({items:[ciData]});
  }
  else {
    ciArr[ciArr.length-1].items.push(ciData);
  }
}

var addDiData = function(diArr, value={}, name) {
  let diData = {name};
  _handleCurrency(diData, value, ["pmt", "annualPmt", "disabilityBenefit", "othRegIncome"])
  _handleInteger(diData, value, ["requireYrIncome"])
  _handleShortfall(diData, value);
  
  if (diArr.length === 0 || diArr[diArr.length-1].items.length === 3) {
    diArr.push({items:[diData]});
  }
  else {
    diArr[diArr.length-1].items.push(diData);
  }
}

var addPaData = function(paArr, value={}, name) {
  let paData = {name};
  _handleCurrency(paData, value, ["pmt", "extInsurance"]);
  _handleShortfall(paData, value);

  if (paArr.length === 0 || paArr[paArr.length-1].items.length === 3) {
    paArr.push({items:[paData]});
  }
  else {
    paArr[paArr.length-1].items.push(paData);
  }
}

var addPchData = function(pchArr, value={}, name) {
  let pchData = {name};
  if (_.indexOf(value.providedFor, 'P') > -1) {
    pchData.showProtection = 'Y';
    _handleCurrency(pchData, value, ["sumAssuredProvided", "extInsurance"]);
    _handleShortfall(pchData, value, 'pTotShortfall');
  }
  if (_.indexOf(value.providedFor, 'C') > -1) {
    pchData.showCriticalIllness = 'Y';
    _handleCurrency(pchData, value, ["sumAssuredCritical", "ciExtInsurance"]);
    _handleShortfall(pchData, value, 'ciTotShortfall');
  }

  if (pchArr.length === 0 || pchArr[pchArr.length-1].items.length === 3) {
    pchArr.push({items:[pchData]});
  }
  else {
    pchArr[pchArr.length-1].items.push(pchData);
  }
}

var addEpData = function(epArr, value={}, name, template) {
  let epPosition = (epArr.length === 0 || epArr[epArr.length-1].items.length === 3)?1: epArr.length + 1;
  let epData = {name};
  _handleInteger(epData, value, ["curAgeofChild", "yrtoSupport", "timeHorizon"]);
  _handleCurrency(epData, value, ["costofEdu", "estTotFutureCost", "maturityValue"]);
  _handleDemical(epData, value, ["avgEduInflatRate"]);
  _handleShortfall(epData, value);
  if (epPosition === 1) {
    epArr.push({items:[epData], assets: []});
  }
  else {
    epArr[epArr.length-1].items.push(epData);
  }

  _.forEach(value.assets, (g,gi)=>{
    let {calAsset = 0, return: _r = 0, usedAsset = 0} = g;
    let eItem = {title:getAssetName(g.key, template), position: epArr[epArr.length-1].items.length};
    _handleCurrency(eItem, g, ["calAsset", "usedAsset"]);
    _handleDemical(eItem, g, ["return"]);
    epArr[epArr.length - 1].assets.push(eItem);
  })
}

var addHcpData = function(hcpArr, value={}, name) {
  let hcpData = {name};
  _.forEach(hpckeys, hcpk=>{
    hcpData[hcpk] = value[hcpk];
  })

  if (hcpArr.length === 0 || hcpArr[hcpArr.length-1].items.length === 3) {
    hcpArr.push({items:[hcpData]});
  }
  else {
    hcpArr[hcpArr.length-1].items.push(hcpData);
  }
}

var generateFNAReportForSignDocDemo = (data, session, cb) => {
    var cid = data.cid;
    let reportData = {
      root: {
        id: data.profile.cid,
        name: data.profile.fullName
      }
    };

    nDao.getFNAReportTemplate().then((reportTemplate)=>{
     PDFHandler.getFnaReportPdf(reportData, reportTemplate, (pdf) => {
        cb ({
          success: true,
          fnaReport: pdf
        });
      })
    })
}

module.exports.generateFNAReportForSignDocDemo = generateFNAReportForSignDocDemo;

var _generateFNAReport = (data, session) => {
    var cid = data.cid;
    const DOC_ID = 'fnaReport';
    return new Promise((resolve)=>{
      bDao.getCurrentBundle(cid).then(bundle => {
        // if (bundle.status >= bDao.BUNDLE_STATUS.SIGN_FNA) {
        //   //generate token for image in mandDocs & optDoc
        //   let now = new Date().getTime();
        //   let tokensMap = {};
        //   let tokens = [];
        //   let token = createPdfToken(bundle.id, DOC_ID, now, session.loginToken);
        //   tokens.push(token);
        //   tokensMap[DOC_ID] = token.token;

        //   setPdfTokensToRedis(tokens, () => {
        //     dao.getAttachment(bundle.id, 'fnaReport', resp => { resolve({pdf: resp.data, token: token.token}); });
        //   });
        // } else {
          let promises = [];
          promises.push(cDao.getProfile(cid));
          promises.push(cDao.getAllDependantsProfile(cid));
          promises.push(nDao.getItem(cid, nDao.ITEM_ID.PDA));
          promises.push(nDao.getItem(cid, nDao.ITEM_ID.FE));
          promises.push(nDao.getItem(cid, nDao.ITEM_ID.NA));
          promises.push(nDao.getFeForm());
          promises.push(nDao.getFnaForm());
          promises.push(nDao.getFNAReportTemplate());
          Promise.all(promises).then((args)=>{
            let p = args[0], dp = args[1], pda = args[2], fe = args[3], 
              fna = args[4], feForm = args[5], fnaForm = args[6], reportTemplates = args[7];
              genFnaReportData(session, session.agent, p, dp, pda, fe, fna, data.skipEmailInReport, {feForm, fnaForm}, function(reportData) {
                if (!reportData) {
                  resolve({pdf: null});
                }
                else {
                  PDFHandler.getFnaReportPdf(reportData, reportTemplates, (pdf) => {
                    resolve({pdf});
                  })
                }
              });
          });
        //}
      });
    });
}

module.exports.generateFNAReport = function (data, session, callback) {
  _generateFNAReport(data, session).then((resp)=>{
    callback({
      success: resp.pdf? true: false,
      fnaReport: resp.pdf,
      token: resp.token
    })
  })
};

module.exports.getFNAEmailTemplate = function (data, session, callback) {
  nDao.getFNAEmailTemplate((result) => {
    if (result && !result.error) {
      callback({
        success: true,
        agentSubject: result.agentSubject,
        agentContent: result.agentContent,
        clientSubject: result.clientSubject,
        clientContent: result.clientContent,
        embedImgs: result.embedImgs
      });
    } else {
      callback({
        success: false
      });
    }
  });
};

var prepareFnaReportAtt = function (pdf, emailId, clientProfile, agentProfile, callback) {
  let password;
  if (emailId === 'agent') {
    let agentCode = agentProfile.agentCode;
    if (agentCode.length >= 6) {
      password = agentCode.replace(/ /g, '').substr(agentCode.length - 6, 6).toLowerCase();
    } else {
      password = agentCode.replace(/ /g, '').substr(0, agentCode.length).toLowerCase();
    }
    //password = agentCode.substr('000124' - 6, 6) + agentProfile.name.substr(0, 5).toLowerCase();
  } else {
    let dob = new Date(clientProfile.dob);
    password = dob.format('ddmmmyyyy').toUpperCase();
  }
  CommonFunctions.protectBase64Pdf(pdf, password, (data) => {
    callback({
      fileName: 'FNA_Report.pdf', // TODO file name
      data: data
    });
  });
  /**callback({
    fileName: 'FNA_Report.pdf', // TODO file name
    data: pdf
  });*/
};

module.exports.emailFnaReport = function (data, session, callback) {
  let {emails, profile, agentProfile, skipEmailInReport} = data;
  data.cid = profile.cid
  if (emails) {
    companyDao.getCompanyInfo((companyInfo) => {
      _generateFNAReport(data, session).then((attachment) => {
        let fnaReportpdfStr = attachment.pdf
        let promises = [];
        _.each(emails, (email, index, emails) => {
          promises.push(new Promise((resolve)=>{
            prepareFnaReportAtt(fnaReportpdfStr,email.id,profile,agentProfile,(att)=>{
              let attArr = [];
              attArr.push(att);
              _.each(email.embedImgs, (value, key) => {
                attArr.push({
                  fileName: key,
                  cid: key,
                  data: value
                });
              });
              EmailUtils.sendEmail({
                from: companyInfo.fromEmail, // TODO
                to:  _.join(email.to, ','),
                title: email.title,
                content: email.content,
                attachments: attArr
              }, resolve);
            })
          }))
        });
        Promise.all(promises).then(()=>{
          callback({success: true});
        })
      })
    })
  } else {
    callback({ success: true });
  }
};

/*
  tid: target id, can be proposer, spouse id or dependant's id
  p: profile, can get from client store
  pda: personal data acknowledgement, can get from needs store
  fna: need analysis data, can get from needs store
*/
module.exports.getSelectedNeeds = function (tid, p, pda, fna) {
  let dt = "";
  let hasSpouse = _.get(pda, "applicant") === "joint"? true: false;
  let sp = _.find(_.get(p, "dependants"), d=>d.relationship === "SPO");
  let sid = _.get(sp, "cid");

  if (tid === p.cid) {
    dt = "owner";
  }
  else if (hasSpouse && sid === tid) {
    dt = "spouse";
  }
  else if (_.find(_.get(pda, "dependants") && _.get(pda, "dependants").split(','), dCid => dCid === tid)) {
    dt = "dependants";
  }

  //if dt has no value, cid is invalid
  if (!dt) {
    return;
  }

  let aspects = _.split(_.get(fna, "aspects"), ",");
  let result = [];
  _.forEach(aspects, a=>{
    let v = _.get(fna, `${a}.${dt}`);
    if (dt === "dependants") {
      v = _.find(v, _v=>_v.cid === tid);
    }
    if (!_.get(v, "init") && _.get(v, "isActive")) {
      result.push(a);
    }
  })
  return result;
};
