const logger = global.logger || console;
const _ = require('lodash');

module.exports.getAppSysDocs = (application, docNamesList, isFACase, isShield) => {
  let result = {};
  let source = _.get(application, 'supportDocuments.values');
  // loop tabs of Supp Docs
  let pdfArray = (isFACase) ? ['proposal', 'appPdf'] : ['fnaReport', 'proposal', 'appPdf'];
  _.forEach(source, (tabs)=>{
    _.forEach(tabs, (section, keySection)=>{
      if (keySection === 'sysDocs') {
        let sysDocs = (isShield) ? getSysDocsFiles(section, docNamesList) : getShieldSysDocsFiles(section, docNamesList);
        _.each(sysDocs, (sysDoc, docKey) => {
          // Handle the multiple app form in shield
          if (isShield && (pdfArray.indexOf(docKey) > -1 || docKey.indexOf('appPdf') > -1)) {
            result[docKey] = sysDoc;
          } else if (pdfArray.indexOf(docKey) > -1) {
            result[docKey] = sysDoc;
          }
        });
      }
    });
  });
  return result;
};

module.exports.getAppMandDocs = (application, docNamesList, isShield) => {
  let result = {};
  let source = _.get(application, 'supportDocuments.values');
  // loop tabs of Supp Docs
  _.forEach(source, (tabs, cid)=>{
    let titlePostFix = (isShield) ? getShieldLAFullNameByCid(application, cid) : '';
    cid = (isShield) ? cid : '';
    _.forEach(tabs, (section, keySection)=>{
      if (keySection === 'mandDocs') {
        Object.assign(result, getMandDocsFiles(section, docNamesList, titlePostFix, cid));
      }
    });
  });
  return result;
};

module.exports.getAppOptionalDocs = (application, docNamesList, isShield) => {
  let result = {};
  let source = _.get(application, 'supportDocuments.values');
  // loop tabs of Supp Docs
  _.forEach(source, (tabs, cid)=>{
    let titlePostFix = (isShield) ? getShieldLAFullNameByCid(application, cid) : '';
    cid = (isShield) ? cid : '';
    _.forEach(tabs, (section, keySection)=>{
      if (keySection === 'optDoc') {
        Object.assign(result, getMandDocsFiles(section, docNamesList, titlePostFix, cid));
      }
    });
  });
  return result;
};

module.exports.getAppOtherDocs = (application, isShield) => {
  let result = {};
  let source = _.get(application, 'supportDocuments.values');
  // loop tabs of Supp Docs
  _.forEach(source, (tabs, cid)=>{
    let titlePostFix = (isShield) ? getShieldLAFullNameByCid(application, cid) : '';

    _.forEach(tabs, (section, keySection)=>{
      if (['sysDocs', 'mandDocs', 'optDoc'].indexOf(keySection) === -1) {
        Object.assign(result, getOtherDocsFiles(section, titlePostFix));
      }
    });
  });
  return result;
};

module.exports.getApprovalSysDocs = (application, approval, docNamesList) => {
  let result = {};
  if (approval) {
    let sysDocs = {};
    let source = _.get(application, 'supportDocuments.values');
    _.forEach(source, (tabs)=>{
      _.forEach(tabs, (section, keySection)=>{
        if (keySection === 'sysDocs') {
          Object.assign(sysDocs, getSysDocsFiles(section, docNamesList));
        }
      });
    });
    _.each(approval._attachments, (att, key) => {
      if (sysDocs[key]) {
        result[key] = sysDocs[key];
      }
    });
  }
  return result;
};

module.exports.getNonSysDocs = (application, excludeFilter) => {
  let result = [];
  let source = _.get(application, 'supportDocuments.values');
  // loop tabs of Supp Docs
  _.forEach(source, (tabs)=>{
    _.forEach(tabs, (section, keySection)=>{
      if (keySection === 'otherDoc') {
        _.forEach(_.get(section, 'values'), (documents, keyDocument)=>{
          _.forEach(documents, function(doc, index) {
            result.push(doc);
          });
        });
      } else if (keySection !== 'sysDocs') {
        _.forEach(section, function(attList, attKey) {
          _.forEach(attList, function(att, index) {
            if (excludeFilter.indexOf(att.fileType) < 0) {
              result.push(att);
            }
          });
        });
      }
    });
  });
  return result;
}

const getShieldSysDocsFiles = function(section, docsNameList) {
  let result = {};
  logger.log('INFO: getCurrentFilesList -- getSysDocsFiles');
  _.forEach(section, (file, keyFile)=>{
    result[keyFile] = {
      'items': [file],
      'title': file.title || _.get(docsNameList, keyFile + '.name'),
      'filename': (keyFile.indexOf('appPdf') > -1) ? file.title : _.get(docsNameList, keyFile + '.filename')
    };
  });
  return result;
};

const getSysDocsFiles = function(section, docsNameList) {
  let result = {};
  logger.log('INFO: getCurrentFilesList -- getSysDocsFiles');
  _.forEach(section, (file, keyFile)=>{
    result[keyFile] = {
      'items': [file],
      'title': file.title || _.get(docsNameList, keyFile + '.name'),
      'filename': _.get(docsNameList, keyFile + '.filename')
    };
  });
  return result;
};

// Mandatory Document and Optional Document have same structure, use this same function
const getMandDocsFiles = function(section, docsNameList, titlePostFix, cid) {
  let result = {};
  logger.log('INFO: getCurrentFilesList -- getMandDocsFiles');
  _.forEach(section, (document, keyDocument)=>{
    if (!_.isEmpty(document)) {
      result[`${keyDocument}${cid}`] = {
        'items' : document,
        'title' : _.get(docsNameList, keyDocument + '.name') + titlePostFix,
        'filename': _.get(docsNameList, keyDocument + '.filename') + titlePostFix
      };
    }
  });
  return result;
};

const getOtherDocsFiles = function(section, titlePostFix) {
  let result = {};
  logger.log('INFO: getCurrentFilesList -- getOtherDocsFiles');
  _.forEach(_.get(section, 'values'), (document, keyDocument)=>{
    result[keyDocument] = {
      'items': document,
      'title': _.get(
        _.find(_.get(section, 'template'), (tmplItem)=>{
            return tmplItem.id === keyDocument;
        }),
      'title') + titlePostFix
    };
  });
  return result;
};

const getShieldLAFullNameByCid = function(application, cid) {
  cid = (cid === 'proposer' || cid === 'policyForm') ? _.get(application, 'applicationForm.values.proposer.personalInfo.cid') : cid;
  return ` (${_.get(application, `quotation.insureds.${cid}.iFullName`)})`;
}
