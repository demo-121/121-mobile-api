const emailUtil = require('../../../utils/email.util');

// the file is included from main project
// e.g. /axa-sg-app/app/u
// import axiosWrapper from '../../../../../app/utilities/axiosWrapper';

module.exports.sendEmail = (email, cb) => {
  // TODO: Keep the code
  // callApi('/email/sendEmail', email, cb)
  
  emailUtil.send(email, (err, result) => {
    if (err) {
      cb(err, null);
    } else {
      cb(null, result);
    }
  });

  // axiosWrapper({
  //     url: "/email/send",
  //     data: email,
  //     method: "POST",
  //     headers: {
  //         Authorization:
  //             "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJjdXN0b21EYXRhIjp7InVpZCI6IkExIn0sImlhdCI6MTUzNjc1OTcyMH0.DbHpxbyi9YeMuFJzs_7H_rNcsbr7R2uvh7zjwuYYHjM"
  //     }
  // })
  // .then(cb)
  // .catch((e) => {
  //     console.log(e);
  // })
};
