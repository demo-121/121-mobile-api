"use strict"

// var Java = require("java");  // commented in core-api
var java = require("java");
var CryptoJS = require("crypto-js");
// var root = fs.realpathSync('.');
// var path = require('path');  // commented in core-api
const fs = require('fs');
const NativeUtils = require('../../../../app/utilities/NativeUtils');

// commented in core-api
// class JavaRunner {
//   constructor() {

//     var root = global.rootPath;
//     // this.config = {
//     //   dbuser: global.config.db.user,
//     //   dbpwd: global.config.db.password,
//     //   jdbc_url: global.config.db.jdbc_url
//     // };
//     Java.options.push("-Xss2m");
//     Java.options.push('-Djava.awt.headless=true'); // necessary for mac to merge pdf, DO NOT remove
//     // Java.options.push('-agentlib:jdwp=transport=dt_socket,address=8787,server=y,suspend=n');
    
//     // load all java libraries
//     Java.classpath.push(path.join(root, "java","NodeJSModule.jar"));
//     Java.classpath.push(path.join(root, "java","lib","commons-codec-1.9.jar"));
//     Java.classpath.push(path.join(root, "java","lib","commons-logging-1.2.jar"));
//     Java.classpath.push(path.join(root, "java","lib","couchbase-lite-java-1.2.0.jar"));
//     Java.classpath.push(path.join(root, "java","lib","couchbase-lite-java-core-1.2.0.jar"));
//     Java.classpath.push(path.join(root, "java","lib","couchbase-lite-java-forestdb-1.2.0.jar"));
//     Java.classpath.push(path.join(root, "java","lib","couchbase-lite-java-sqlcipher-1.2.0.jar"));
//     Java.classpath.push(path.join(root, "java","lib","couchbase-lite-java-sqlite-custom-1.2.0.jar"));
//     Java.classpath.push(path.join(root, "java","lib","gson-2.6.2.jar"));
//     Java.classpath.push(path.join(root, "java","lib","httpclient-4.5.2.jar"));
//     Java.classpath.push(path.join(root, "java","lib","httpcore-4.4.4.jar"));
//     Java.classpath.push(path.join(root, "java","lib","jackson-annotations-2.5.0.jar"));
//     Java.classpath.push(path.join(root, "java","lib","jackson-core-2.5.0.jar"));
//     Java.classpath.push(path.join(root, "java","lib","jackson-databind-2.5.0.jar"));
//     Java.classpath.push(path.join(root, "java","lib","json-20160212.jar"));
//     Java.classpath.push(path.join(root, "java","lib","ojdbc7.jar"));
//     Java.classpath.push(path.join(root, "java","lib","stateless4j-2.4.0.jar"));
//     Java.classpath.push(path.join(root, "java","lib","pdfbox-app-2.0.8.jar"));
//     Java.classpath.push(path.join(root, "java", "lib", "pdfreactor.jar"));
//     Java.classpath.push(path.join(root, "java","lib","xslt.jar"));

//     java = Java
//   }
module.exports.addWordsPdfs = function(base64Pdf, agentName, proposerName, x1, y1, x2, y2){
  return new Promise(resolve => {
    NativeUtils.addWordsPdfs(base64Pdf, agentName, proposerName, x1, y1, x2, y2, resolve);
  });
}

module.exports.setPdfPassword = function(base64Pdf, pass){
  return java.callStaticMethodSync("pdf.Pdf", "setPassword", base64Pdf, pass);
}

module.exports.mergePdfs = function(base64Pdfs, callback) {
  NativeUtils.mergePdfs(base64Pdfs, callback);
}

module.exports.convertHtml2Pdf = function(html, pdfOptions){
  return java.callStaticMethodSync("pdf.Pdf", "convertHtml2Pdf", html, pdfOptions);
}

module.exports.addPdfTitle = function(base64Pdf, title) {
  return java.callStaticMethodSync('pdf.Pdf', 'addTitle', base64Pdf, title);
}

module.exports.convertBase64Imgs2Pdf = function(base64Imgs) {
  return java.callStaticMethodSync('pdf.Pdf', 'convertBase64Images2Pdf', base64Imgs);
}

module.exports.compareEncryptPassword = function(plainPass, userName, privateKey, encryptedPass){
  privateKey = userName + privateKey;
  var keyHex = CryptoJS.enc.Utf8.parse(privateKey);
  var encrypted_P1 = CryptoJS.DES.encrypt(plainPass, keyHex, {
      mode: CryptoJS.mode.ECB,
      padding: CryptoJS.pad.Pkcs7
  });
    return (encrypted_P1==encryptedPass);
}

module.exports.createAesKey = function(){

  var aeskey = java.callStaticMethodSync("com.eab.Util", "GenerateRandomASCII", 32);
  return aeskey;
}

module.exports.createKeyStore = function(ccb){
  //var keyfile = require('eab');
  var keyfile =  __dirname + '/keystore/eab.keystore';
  //var keyfile =  'eab.keystore';

  var keyConfig = require('./keystore/keyConfig.js').keyConfig;
  var ksstatus = java.callStaticMethodSync("com.eab.Keystore", "Create", keyfile, keyConfig.keystore_pass);

  ccb();
}

module.exports.saveTripleDESToKeyStore = function(tripledeskey, cb){

  var keyfile =  __dirname + '/keystore/eab.keystore';
  var keyConfig = require('./keystore/keyConfig.js').keyConfig;
  var keystore_pass = keyConfig.keystore_pass;
  var keyalias_name = keyConfig.keyalias_name;
  var keyalias_password = keyConfig.keyalias_password;
  var savestatus = java.callStaticMethodSync("com.eab.Keystore", "ImportPassphrase", keyfile, keystore_pass, keyalias_name, tripledeskey, keyalias_password);


  cb(savestatus);
}

module.exports.getTripleDESFromKeyStore = function(){
  var keyfile =  __dirname + '/keystore/eab.keystore';
  var keyConfig = require('./keystore/keyConfig.js').keyConfig;
  var keystore_pass = keyConfig.keystore_pass;
  var keyalias_name = keyConfig.keyalias_name;
  var keyalias_password = keyConfig.keyalias_password;
  var key = java.callStaticMethodSync("com.eab.Keystore", "ExportPasspharase", keyfile, keystore_pass, keyalias_name, keyalias_password);
  return key;
}



//gen new tripleDES key
module.exports.createTripleDesKey = function(){
  var tripledeskey = java.callStaticMethodSync("com.eab.Util", "GenerateRandomASCII", 21);    // 24 chars = 192-bit
  return tripledeskey;
}

// AES Encryption - TripleDES Key to be Encrypted String
module.exports.enrtyptAesKey = function(tripledeskey, aes256key){
  var enkey = CryptoJS.AES.encrypt(tripledeskey, aes256key);
  return enkey;
}

// using this
module.exports.jsonToXml = function(json, callback){
  java.callStaticMethod("com.eab.xslt.XSLTTransform", "jsonToXml", json, function(err, result){
    if(err)
      callback(false);
    else
      callback(result);
  });
}

// using this
module.exports.XSLTTransformer = function(xml, xsl, callback){
  java.callStaticMethod("com.eab.xslt.XSLTTransform", "transformXml", xml, xsl, function(err, result) {
    callback(result);
  });
}

module.exports.XSLTTest = function(callback){
  java.callStaticMethod("com.eab.xslt.XSLTTransform", "test",  function(err, result) {
    callback(result);
  });
}

// exeSql(sql, data, callback) {
//   if (!callback && typeof data == 'function') {
//     callback = data;
//   }

//   java.callStaticMethod("com.eab.database.JDBC", "execute", this.config.dbuser, this.config.dbpwd, this.config.jdbc_url, sql, function(err, results) {
//     if (callback) {
//       callback(err, results)
//     }
//   });
// }

module.exports.initCBL = function(callback) {
  java.callStaticMethod("com.eab.database.CBLite", "main", [], function(err, result) {
    if(err) { callback(false); return; }
    if (callback) {
      callback(err, result)
    }
  });
}


module.exports.createView = function(ddname, view, callback) {
  java.callStaticMethod("com.eab.database.CBLite", "CreateView", ddname, view, function(err, doc) {
    if (callback) {
      callback(err, JSON.parse(doc));
    }
  })
}

module.exports.getViewRange = function(ddname, name, start, end, params, callback) {
  java.callStaticMethod("com.eab.database.CBLite", "GetView", ddname, name, start, end, params, function(err, doc) {
    if (callback) {
      callback(err, JSON.parse(doc));
    }
  })
}

module.exports.getDoc = function(docId, callback) {
  //Get document
  java.callStaticMethod("com.eab.database.CBLite", "GetDocument", docId, function(err, doc) {
      if(err) {
        callback(false);
        return;
      }
      callback(err, JSON.parse(doc));
  });
}

module.exports.getAttachment = function(docId, attName, callback) {
  //Get attachment
  java.callStaticMethod("com.eab.database.CBLite", "GetAttach", docId, attName, function(err, binary) {
      if (err || !binary) { callback(false); return; }
      callback(err, binary);
  });
}

module.exports.addDoc = function(docId, data, callback) {
  //Create new document
  java.callStaticMethod("com.eab.database.CBLite", "CreateDocument", docId, data, function(err, newRevId) {
      if(err || !newRevId) {
        callback(false);
        return;
      }
      callback(true);
  });
}

module.exports.updDoc = function(docId, data, callback) {
  //Create new document
  java.callStaticMethod("com.eab.database.CBLite", "UpdateDocument", docId, data, function(err, newRevId) {
      if(err || !newRevId) {
        callback(false);
        return;
      }
      callback(true);
  });
}

module.exports.delDoc = function(docId, callback) {
  //Create new document
  java.callStaticMethod("com.eab.database.CBLite", "DeleteDocument", docId, function(err, status) {
      if(err || !status) {
        callback(false);
        return;
      }
      callback(status);
  });
}

module.exports.setAttachment = function(docId, name, mime, data, callback) {
  //Update attachment to document
  java.callStaticMethod("com.eab.database.CBLite", "UpdateAttach", docId, name, mime, data, function(err, newRevId) {
      if(err || !newRevId) { callback(false); return; }
      callback(true);
  });
}


module.exports.delAttachment = function(docId, name, callback) {
  //Update attachment to document
  java.callStaticMethod("com.eab.database.CBLite", "DeleteAttach", docId, name, function(err, newRevId) {
      if(err || !newRevId) { callback(false); return; }
      callback(true);
  });
}
// }

// exports["default"] = JavaRunner;
// module.exports = exports["default"];

