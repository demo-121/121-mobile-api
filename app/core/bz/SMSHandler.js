var {
  callApi
} = require('./utils/RemoteUtils.js');
const smsController = require('../../utils/sms.util');

module.exports.sendSMS = (data, session, cb) => {
  console.log('data.sms:' + JSON.stringify(data.sms));
  smsController.send(data.sms.mobileNo, data.sms.smsMsg, (err, result) => {
    if(err) {
      cb("");
    } else {
      cb(result);
    }
  });
  // if (global.config.disableSMS){
  //   cb({});
  // } else {
  //   callApi('/sms/sendSMS',
  //     data.sms,
  //     cb);
  // }
};
