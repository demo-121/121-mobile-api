const crypto = require('crypto');

const dao = require('../cbDaoFactory').create();
const bundleDao = require('../cbDao/bundle/index');

const logger = global.logger || console;

const jobDetailsDocKey = 'invalidQuotByFundsJob';

module.exports.execute = () => {
  return new Promise((resolve) => {
    dao.getDoc(jobDetailsDocKey, (doc) => {
      if (doc && !doc.error) {
        if (!doc.handledBy) {
          doc.handledBy = crypto.randomBytes(12).toString('hex');
          logger.log('Jobs :: invalidateByFunds :: handling job:', doc.handledBy);
          dao.updDoc(jobDetailsDocKey, doc, (result) => {
            doc._rev = result.rev;
            resolve(doc);
          });
        } else {
          logger.log('Jobs :: invalidateByFunds :: job skipped:', doc.handledBy);
          resolve(false);
        }
      } else {
        logger.log('Jobs :: invalidateByFunds :: cannot find job details');
        resolve(false);
      }
    });
  }).then((jobDetails) => {
    if (!jobDetails) {
      return;
    }
    return bundleDao.invalidateApplicationsByFund(jobDetails.fundCodes).then(() => {
      jobDetails.completedDate = new Date().toISOString();
      dao.updDoc(jobDetailsDocKey, jobDetails, (result) => {
        logger.log('Jobs :: invalidateByFunds :: job completed');
      });
    });
  }).catch((err) => {
    logger.log('Jobs :: invalidateByFunds :: failed to invalidate by funds\n', err);
  });
};
