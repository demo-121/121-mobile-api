const crypto = require('crypto');
const _ = require('lodash');

const dao = require('../cbDaoFactory').create();
const bundleDao = require('../cbDao/bundle');
const cDao = require('../cbDao/client');
const nDao = require('../cbDao/needs');

const logger = global.logger || console;

const CI_ID = 'ciProtection';
const jobDetailsDocKey = 'invalidateCiKids';

const {getAllCustomer, getFilterAgent, insertNoticeToAgent} = require('./invalidateFunctions');


function hasCiKid(cid) {
    return cDao.getProfile(cid, true).then((profile = {}) => {
        if (!profile.cid) {
            return false;
        }
        return nDao.getItem(cid, nDao.ITEM_ID.PDA).then((pda = {}) => {
            return nDao.getItem(cid, nDao.ITEM_ID.NA).then((na = {}) => {
                if (!pda.id || !na.id) {
                    return false;
                }
                // check there are kids in profile dependants
                const kids = _.map(_.filter(profile.dependants, dependant => ['SON', 'DAU'].indexOf(dependant.relationship) > -1), 'cid');

                // filter the kids which is selected in personal data acknowledgement
                const selectedDependants = _.split(pda.dependants, ',');
                const selectedKids = _.filter(kids, kid => selectedDependants.indexOf(kid) > -1);

                // check user selected critical illness protection in fna and select kids
                const hasSelectCi = _.split(na.aspects, ',').indexOf(CI_ID) > -1;
                const ciData = _.get(na, `${CI_ID}.dependants`, []);
                const ciKids = _.filter(selectedKids, kid => _.get(_.find(ciData, data => data.cid === kid), 'isActive'));
                if (hasSelectCi && _.size(ciKids) && na.lastStepIndex >= 2) {
                    return ciKids;
                } else {
                    return false;
                }
            });
        });
    }).catch(e=> {
        return false;
    });
}

function existRider(basicIds = [], riderIds = [], basicCode = '', plans = []) {
    return (
        basicIds.indexOf(basicCode) > -1 &&
        _.find(riderIds, riderId => _.find(plans, plan => plan.covCode === riderId))
    );
}

function getDataFromApplicationObj(applicationObj = {}) {
    return new Promise(resolve => {
        if (applicationObj.applicationDocId) {
            dao.getDoc(
                applicationObj.applicationDocId, 
                (application = {}) => {
                    resolve({
                        quotation: application.quotation,
                        isProposalSigned: application.isProposalSigned
                    });
                }
            )
        }
        else {
            dao.getDoc(
                applicationObj.quotationDocId,
                (quotation = {}) => {
                    resolve({
                        quotation,
                        isProposalSigned: false
                    });
                }
            )
        }
    });
}

function checkIsBiInvalid(application = {}, cid = '', ciKids = []) {
    return getDataFromApplicationObj(application)
        .then((data = {}) => {
            const { quotation = {}, isProposalSigned = false } = data;
            // only check the life assured is child and proposal is not signed
            if (ciKids.indexOf(quotation.iCid) === -1 || isProposalSigned) {
                return false;
            } else {
                const plans = _.get(quotation, 'plans', []);
                const basicPlanCode = _.get(plans[0], 'covCode', '');
                if (existRider(['TPX', 'TPPX'], ['CIP', 'CRX', 'CIP_LMP'], basicPlanCode, plans)) {
                    return true;
                } if (existRider(['FPX', 'FSX'], ['LVT', 'LARB', 'CARB'], basicPlanCode, plans)) {
                    return true;
                } else {
                    return false;
                }
            }
        }).catch(e => {
            return false;
        });
}

function checkExistInvalidBIs(cid = '', ciKids = []) {
    return bundleDao.getCurrentBundle(cid).then((bundle = {}) => {
        let promises = [];
        
        let applications = _.filter(bundle.applications, application => !application.invalidateDate);

        // if no submitted / in progess BIs, reset FNA status
        if (!_.size(applications)) {
            return nDao.invalidSection(cid, nDao.ITEM_ID.NA).then(() => {
                return false;
            });
        }

        let inProgessApplications = _.filter(applications, application => application.appStatus === 'APPLYING' || !application.applicationDocId);

        _.forEach(inProgessApplications, application => {
            promises.push(checkIsBiInvalid(application, cid, ciKids));
        });

        return Promise.all(promises).then(args => {
            // start invalidation when there is impact BI not signed
            if (_.find(args, (arg, index) => arg)) {
                const bundleStatus = _.get(bundle, 'status');
                logger.log("INFLIGHT HANDLING - START INVALIDATE BIs FOR CI KIDS - ", cid);
                if (bundleStatus >= bundleDao.BUNDLE_STATUS.SUBMIT_APP) {
                    const agent = {
                        agentCode: bundle.agentCode,
                        channel: {
                            code: bundle.dealerGroup
                        },
                        compCode: bundle.compCode
                    };
                    return bundleDao.createNewBundle(cid, agent).then(() => { 
                        return true;
                    });
                } else {
                    return bundleDao.invalidateApplicationByClientId(cid).then(() => {
                        return true;
                    });
                }
            } else {
                return false;
            }
        })
    }).catch(e => {
        logger.log("ERROR in INFLIGHT CASE HANDLING: ", e);
        return false;
    });
}

function invalidateCustomer(cid = '') {
    return bundleDao.getCurrentBundle(cid).then((bundle = {}) => {
        // don't continue if no bundle id
        if (!bundle.id) {
            return false;
        } else {
            return hasCiKid(cid).then((ciKids = [])=> {
                if (!ciKids) {
                    return false;
                } else {                            
                    // check any immpact BIs
                    return checkExistInvalidBIs(cid, ciKids).then((hasInvalidateBi) => {
                        return cDao.getProfile(cid, true).then(profile => {
                            if (!hasInvalidateBi) {
                                return false;
                            } else {
                                return new Promise(resolve => {
                                    dao.updDoc(
                                        cid,
                                        profile,
                                        () => {
                                            // if client has ci kid, invalidate need analysis only
                                            nDao.invalidSection(cid, nDao.ITEM_ID.NA).then(() => {
                                                resolve(profile.agentCode);
                                            });
                                        }
                                    );
                                });
                            }
                        });
                    });
                }
            });
        }  
    }).catch(e => {
        logger.log("ERROR in INFLIGHT CASE HANDLING: ", e);
        return false;
    });
}

function recursive (cids = [], invalidateCid = [], result = [], index = 0) {
    return invalidateCustomer(cids[index]).then(agentCode => {
        if (agentCode) {
            invalidateCid.push(cids[index]);
        }
        logger.log("INFO: EXECUTE CI INFLIGHT CASE HANDLING PROCESS - " + (index + 1) + '/' + cids.length);
        result[index++] = agentCode;
        if (index < cids.length) {
            return recursive(cids, invalidateCid, result, index);
        } else {
            logger.log("INFO: COMPLETED CI INFLIGHT CASE HANDLING");
            return result;
        }
    }).catch(e => {
      return result; 
    });
}

function recursiveAgent (agents = [], index = 0) {
    logger.log('INFO: EXECUTE PUSHING NOTICE TO AGENT - ' + (index + 1) + '/' + agents.length);
    return insertNoticeToAgent(agents[index++], jobDetailsDocKey, 'Critical Illness Protection needs calculation method for children has been redesigned under FNA. If you have unsigned cases where there is CI needs for children, please re-compute the needs in FNA and generate a fresh BI').then(() => {
        if (index < agents.length) {
            return recursiveAgent(agents, index);
        } else {
            logger.log('INFO: COMPLETE PUSHING NOTICE TO AGENT');
            return;
        }
    }).catch(e => {
      return;
    });
}

function _execute(jobDetails) {
    return new Promise(resolve => {
        getAllCustomer().then((customers = [])=> {
            let invalidateCids = [];
            recursive(customers, invalidateCids).then((agentCodes = []) => {
                jobDetails.invalidateCids = invalidateCids;
                let impactAgents = _.uniq(_.compact(agentCodes));
                getFilterAgent(impactAgents).then((agentIds = []) => {
                    jobDetails.agentIds = agentIds;
                    recursiveAgent(agentIds).then(()=>{resolve(agentIds)});
                });
            });
        });
    });

}

module.exports.execute = () => {
    return new Promise((resolve) => {
        dao.getDoc(jobDetailsDocKey, (doc) => {
            if (doc && !doc.error) {
                if (!doc.handledBy) {
                    doc.handledBy = crypto.randomBytes(12).toString('hex');
                    logger.log('Jobs :: invalidateCiKids :: handling job:', doc.handledBy);
                    dao.updDoc(jobDetailsDocKey, doc, (result) => {
                        doc._rev = result.rev;
                        resolve(doc);
                    });
                } else {
                    logger.log('Jobs :: invalidateCiKids :: job skipped:', doc.handledBy);
                    resolve(false);
                }
            } else if (doc && doc.error === 'not_found') {
                let newDoc = {
                    handledBy: crypto.randomBytes(12).toString('hex')
                };

                logger.log('Jobs :: invalidateCiKids :: handling job:', newDoc.handledBy);
                dao.updDoc(jobDetailsDocKey, newDoc, (result) => {
                    newDoc._rev = result.rev;
                    resolve(newDoc);
                });
            } else {
                logger.log('Jobs :: invalidateCiKids :: cannot find job details');
                resolve(false);
            }
        });
    }).then((jobDetails) => {
        if (!jobDetails) {
            return;
        }
        return _execute(jobDetails).then(() => {
            jobDetails.completedDate = new Date().toISOString();
            dao.updDoc(jobDetailsDocKey, jobDetails, (result) => {
                logger.log('Jobs :: invalidateCiKids :: job completed');
            });
        });
    }).catch((err) => {
        logger.log('Jobs :: invalidateCiKids :: failed to invalidate by funds\n', err);
    });
};