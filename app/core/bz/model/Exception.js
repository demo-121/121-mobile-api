const ACTION_CONTINUE = 1;
const ROLL_BACK = 2;

let EmailException = function(message) {
  this.message = message;
  this.code = ACTION_CONTINUE;
}

let UpdateDocException = function(message){
  this.message = message;
  this.code = ROLL_BACK;
}

let CreateDocException = function(message){
  this.message = message;
  this.code = ROLL_BACK;
}

let GetTemplateException = function(message){
  this.message = message;
  this.code = ROLL_BACK;
}

let UnknownException = function(message){
  this.message = message;
  this.code = ROLL_BACK;
}

EmailException.prototype = Error.prototype;
CreateDocException.prototype = Error.prototype;
GetTemplateException.prototype = Error.prototype;
UpdateDocException.prototype = Error.prototype;
UnknownException.prototype = Error.prototype;

module.exports = {EmailException, CreateDocException, GetTemplateException, UpdateDocException, UnknownException, ACTION_CONTINUE, ROLL_BACK}