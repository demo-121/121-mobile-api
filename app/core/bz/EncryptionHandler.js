var CryptoJS = require("crypto-js");
var crypto = require('crypto');

var encryptPassword = function(email, pass, callback){
  //use hmac-sha3 encryption
  sha3Encryption(email, pass, function(encryptedPass){
    callback(encryptedPass)
  })
}

module.exports.sha3Encryption = function(email, pass, cb){
  var sha3Pass = CryptoJS.HmacSHA3(pass, email);
  var hashInBase64 = CryptoJS.enc.Base64.stringify(sha3Pass);
  cb(hashInBase64);
}


module.exports.idEncryption = function(id){
  var hash = crypto.createHash('md5').update(id).digest('hex');
  return hash;
}