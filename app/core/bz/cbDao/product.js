const _ = require('lodash');
const moment = require('moment');
const dao = require('../cbDaoFactory').create();
const {getAges} = require('../../common/DateUtils');
const {checkEntryAge, getProductId} = require('../../common/ProductUtils');
var logger = global.logger || console;

module.exports.queryBasicPlansByClient = function(compCode, insured, proposer, params) {
  return new Promise((resolve) => {
    if (!proposer) {
      proposer = insured;
    }
    const currentDate = new Date();
    const iAges = getAges(currentDate, new Date(insured.dob));
    const oAges = getAges(currentDate, new Date(proposer.dob));
    const now = moment();
    dao.getViewRange('main','products',
      '["' + compCode + '","B","0"]',
      '["' + compCode + '","B","ZZZ"]',
      params ? params : null,
      function(list){
        var filteredList = [];

        if (list && list.rows && list.rows.length) {
          logger.log('before filter :', list.rows.length);

          var pubDateMap = {};
          for (var i in list.rows) {
            var p = list.rows[i].value;

            if (!canViewProduct(p, insured, iAges, proposer, oAges, params.ccy, now)) {
              continue;
            }

            // set the doc id
            p._id = list.rows[i].id;

            if (pubDateMap[p.covCode]) {
              if (pubDateMap[p.covCode].version > p.version) {
                continue;
              } else {
                filteredList[pubDateMap[p.covCode].index] = p;
                pubDateMap[p.covCode] = {
                  version: p.version,
                  index: pubDateMap[p.covCode].index
                };
              }
            } else {
              pubDateMap[p.covCode] = {
                pubDate: p.pubDate,
                index: filteredList.length
              };
              filteredList.push(p);
            }
          }
        }
        resolve(filteredList);
      }
    );
  });
};

module.exports.getProductByVersion = function(compCode, covCode, version, callback) {
  dao.getDocFromCacheFirst(compCode + '_product_' + covCode + '_' + version, function(product) {
    callback(product);
  });
};

module.exports.getProduct = function(docId, callback) {
  return new Promise(resolve => {
    dao.getDoc(docId, function(product) {
      callback && callback(product);
      resolve(product);
    });
  });
};

module.exports.getAttachment = function(planId, attName, callback) {
  dao.getAttachment(planId, attName, callback);
};

module.exports.getAttachmentWithLang = function (productId, attName, lang, callback) {
  dao.getAttachment(productId, attName + (lang ? ('_' + lang) : ''), callback);
};

module.exports.getPlanByCovCode = function (compCode, covCode, planInd, getOldest) {
  return new Promise(resolve => {
    const now = moment().valueOf();
    dao.getViewRange('main', 'products',
      `["${compCode}","${planInd}","${covCode}"]`,
      `["${compCode}","${planInd}","${covCode}"]`,
      null,
      function (list) {
        if (list && list.rows) {
          let product = null;
          _.each(list.rows, row => {
            const p = row.value;
            if (!(moment(p.effDate).valueOf() <= now && moment(p.expDate).valueOf() >= now)) {
              return; // filter it if not match condition
            }
            if (!product || (getOldest && p.version < product.version) || (!getOldest && p.version > product.version)) {
              product = p;
            }
          });
          if (product) {
            dao.getDoc(getProductId(product), resolve);
          } else {
            resolve();
          }
        } else {
          resolve();
        }
      }
    );
  });
};

module.exports.canViewProduct = function (productId, proposer, insured, callback) {
  if (!insured) {
    insured = proposer;
  }
  dao.getDoc(productId, (product) => {
    const currentDate = new Date();
    const pAges = getAges(currentDate, new Date(proposer.dob));
    const iAges = getAges(currentDate, new Date(insured.dob));
    const now = moment();
    if (canViewProduct(product, insured, iAges, proposer, pAges, null, now)) {
      callback(product);
    } else {
      callback(false);
    }
  });
};

var canViewProduct = function (product, insured, iAges, proposer, pAges, ccy, now) {
  var p = product;

  if (!checkEntryAge(p, insured.residenceCountry, pAges, iAges)) {
    logger.log('Products :: canViewProduct :: product filtered as owner age:', p.covCode, p.entryAge);
    return false;
  }

  if (!(p.genderInd === '*' || p.genderInd === insured.gender)){
    logger.log('Products :: canViewProduct :: product filtered as gender:', p.genderInd);
    return false;
  }

  if (p.ctyGroup.indexOf('*') === -1 && p.ctyGroup.indexOf(insured.residenceCountry) === -1) {
    logger.log('Products :: canViewProduct :: product filtered as city group:', p.covCode, p.ctyGroup);
    return false;
  }

  if (p.smokeInd !== '*' && p.smokeInd !== insured.isSmoker) {
    logger.log('Products :: canViewProduct :: product filtered as smoke:', p.covCode, p.smokeInd);
    return false;
  }

  if (ccy && ccy !== 'ALL') {
    var currency = _.find(p.currencies, (c) => (c.country === '*' || c.country === insured.residenceCountry));
    if (!currency || !_.find(currency.ccy, (c) => c === ccy)) {
      logger.log('Products :: canViewProduct :: product filtered as ccy:', p.covCode, currency, ccy);
      return false;
    }
  }

  if (now < moment(p.effDate) || now > moment(p.expDate)) {
    logger.log('Products :: canViewProduct :: product filtered as not within date:', p.covCode, p.effDate, p.expDate, now);
    return false;
  }

  return true;
};

module.exports.getProductSuitability = () => {
  return new Promise((resolve) => {
    dao.getDocFromCacheFirst('productSuitability', resolve);
  });
};
