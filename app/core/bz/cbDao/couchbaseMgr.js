const _ = require('lodash');
const fs = require('fs');
const viewsFolder = './bz/cbDao/views/';

let cbDao = null;
const logger = global.logger || console;

const viewVersionNum = 2;
const init = function (callback) {
  if (!cbDao) {
    cbDao = require('../cbDaoFactory').create();
  }
  cbDao.init((resp) => {
    updateViews(callback);
  });
};

const generateViewJson = function (viewsMap) {
  const views = {};

  // default to main.js
  if (!viewsMap) {
    viewsMap = require('./views/main.js');
  }

  _.each(viewsMap, (viewFunc, viewId) => {
    views[viewId] = {
      map: viewFunc + ''
    };
  });
  
  return JSON.stringify({
    views: views
  });
};

const generateAllViewJsons = function() {
  const viewNames = getAllViews();
  let views = {};
  _.forEach(viewNames, viewName => {
    const view = require('./views/' + viewName + '.js');
    views[viewName] = generateViewJson(view);
  });

  return views;
}

const getAllViews = function () {
  const files = fs.readdirSync(viewsFolder);
  return _.map(files, file => file.replace('.js', ''));
}

const hasDiffView = function (cbViews) {
  return _.find(getViewsMap(), (viewFunc, viewId) => {
    return !cbViews[viewId] || cbViews[viewId].map.indexOf(viewFunc + '') === -1;
  });
};

const getViewsMap = function (viewName = 'main') {
  return require('./views/' + viewName + '.js');
}

const updateViews = function (callback) {
  cbDao.getDoc('_design/main', (doc) => {
    if (!doc || !doc.views || hasDiffView(doc.views)) {
      const views = generateViewJson();
      logger.log('CouchbaseMgr :: updateViews :: updating view');
      cbDao.createView('main', views, (res) => {
        logger.log('CouchbaseMgr :: updateViews :: complete:', res);
        if (typeof callback === "function") {
          callback(res);
        }
      });
    } else {
      if (typeof callback === "function") {
        callback(null);
      }
    }
  });
};

const exportViewsJson = function () {
  return generateViewJson();
};

module.exports = {
  viewVersionNum,
  init,
  exportViewsJson,
  generateAllViewJsons
};