const dao = require('../cbDaoFactory').create();
const bDao = require('./bundle');
const fileType = require('file-type');
const Buffer = require('buffer').Buffer;
const cUtils = require('../utils/CommonUtils');
const _ = require('lodash');
const cFunctions = require('../utils/CommonUtils');
const commonFunctions = require('../CommonFunctions');
const appPdfName = "appPdf";
const fnaPdfName = "fnaReport";
var logger = global.logger || console;

var genMasterAppId = function(agentNumber, seq){
  agentNumber = cUtils.getAgentIdForDoc(agentNumber);
  seq = cUtils.getSeqForDoc(seq);
  return 'SA' + agentNumber + '-' +  seq;
};

var genAppId = function(agentNumber, seq){
  agentNumber = cUtils.getAgentIdForDoc(agentNumber);
  seq = cUtils.getSeqForDoc(seq);
  return "NB" + agentNumber + "-" +  seq;
};

module.exports.createShieldApplicaitonNumbers = function(agentCode, numOfIds){
  let numOfSeq = numOfIds - 1 > 0 ? numOfIds - 1 : numOfIds;

  return cFunctions.getDocNumbers(agentCode, 'app', numOfSeq).then((results) => {
    let ids = [];
    ids.push(genMasterAppId(results.agentNumber, _.get(results, 'seqNos[0]')));

    _.forEach(results.seqNos, (seq, index) => {
      ids.push(genAppId(results.agentNumber, seq));
    });
    return ids;
  });
};

module.exports.createApplicaitonNumber = function(agentCode, callback){
  cFunctions.getDocNumber(agentCode, "app", function(agentNumber, seq){
    callback(genAppId(agentNumber, seq));
  })
}

module.exports.getAppListView = function (compCode, cid, bundleId) {
  // TODO: need to re-write, should not query view one by one, try use the applicationsView!
  return bDao.getApplicationIdsByBundle(bundleId).then((applications) => {
    let quotIds = [];
    let appIds = [];
    let applicationMap = {};
    _.each(applications, (app) => {
      if (app.type === 'quotation') {
        quotIds.push(app.id);
        applicationMap[app.id] = app;
      } else if (app.type === 'application' || app.type === 'masterApplication') {
        appIds.push(app.id);
        applicationMap[app.id] = app;
      }
    });
    var promises = [];
    promises.push(getQuotationsForSummary(compCode, cid, quotIds).then((apps) => {
      _.each(apps, (app) => {
        app.appStatus = applicationMap[app.id].status;
        app.invalidateReason = applicationMap[app.id].invalidateReason;
      });
      return apps;
    }).catch((error)=>{
      logger.error("Error in getAppListView->getQuotationsForSummary: ", error);
    }));
    promises.push(getApplicationsForSummary(compCode, cid, appIds).then((apps) => {
      _.each(apps, (app) => {
        app.appStatus = applicationMap[app.id].status;
        app.invalidateReason = applicationMap[app.id].invalidateReason;
      });
      return apps;
    }).catch((error)=>{
      logger.error("Error in getAppListView->getApplicationsForSummary: ", error);
    }));
    return Promise.all(promises).then((args) => {
      return _.reduce(args, (result, arg) => result.concat(arg || []));
    }).catch((error)=>{
      logger.error("Error in getAppListView->Promise.all: ", error);
    });
  });
};

var getQuotationsForSummary = function (compCode, cid, quotIds) {
  let keys = _.map(quotIds, quotId => '["' + compCode + '","' + cid + '","' + quotId + '"]');
  return dao.getViewByKeys('main', 'summaryQuots', keys, null).then((result) => {
    let quotations = [];
    if (result) {
      _.each(result.rows, (row) => {
        quotations.push(row.value);
      });
    }
    return quotations;
  }).catch((error)=>{
    logger.error("Error in getQuotationsForSummary->dao.getViewByKeys: ", error);
  });
};

var getApplicationsForSummary = function (compCode, cid, appIds) {
  let keys = _.map(appIds, appId => '["' + compCode + '","' + cid + '","' + appId + '"]');
  return dao.getViewByKeys('main', 'summaryApps', keys, null).then((result) => {
    let applications = [];
    if (result) {
      _.each(result.rows, (row) => {
        applications.push(row.value);
      });
    }
    return applications;
  }).catch((error)=>{
    logger.error("Error in getApplicationsForSummary->dao.getViewByKeys: ", error);
  });
};

module.exports.getFNATemplate = function(callback){
  var docId = "fnaTemplate";
  if (!global.documentCache[docId]) {
    dao.getDoc(docId, function(result) {
      //logger.log("needs: getData", result)
      if (result && (result._id || result.id || !result.error)) {
        global.documentCache[docId] = result;
        callback(result);
      } else {
        logger.log('getCNAForm error: fnaTemplate', result.error);
        callback(false);
      }
    })
  } else {
    callback(global.documentCache[docId]);
  }
}

module.exports.getAppPDFTemplate = function(tempId, callback){
  dao.getDoc(tempId, function(template){
    callback(template);
  });
};

var getApplication = function(appId, callback){
  dao.getDoc(appId, function(app){
    //callback(utils.decrypt(app, applicationEncryFields));
    callback(app);
  });
};
module.exports.getApplication = getApplication;

module.exports.deleteApplication = function(appId, rev, callback){
  dao.delDocWithRev(appId, rev, (result)=>{
      if (result && !result.error) {
        dao.updateViewIndex("main", "summaryApps");
        // dao.updateViewIndex("main", "contacts");
        callback({success:true})
      } else {
        callback({success:false})
      }
    });
};

module.exports.updApplication = function(appId, newApp, cb){
  let upd = function(result){
    if (cb) {
      cb(result);
    }
  };
  dao.updDoc(appId || newApp._id,  newApp, upd);
};

// unmask the application and upsert to db
const _upsertApplication = function(appId, newApp, callback, skipUpdateView) {
  getApplication(appId || newApp._id, function(app) {
    if (app && !app.error) {
      for (var key in app) {
        if (key.indexOf('_') == 0) {
          newApp[key] = app[key];
        }
      }
    }

    dao.updDoc(appId, newApp, function(err, result){
      if (!err || result.success) {
        newApp._rev = result.rev;
        if (!skipUpdateView) {
          dao.updateViewIndex("main", "summaryApps");
          // dao.updateViewIndex("main", "signatureExpire");
        }
        callback(result);
      }
    });
  });
};
module.exports.upsertApplication = _upsertApplication;
// module.exports._upsertApplication = function(appId, newApp, forms, callback) {
//   getApplication(appId || newApp._id, function(app) {
//     if (app && !app.error) {
//       // reset system key before update
//       for (var key in app) {
//         if (key.indexOf('_') == 0) {
//           newApp[key] = app[key];
//         }
//       }
//       if (forms) {
//         for (var f in forms) {
//           utils.unmask(forms[f], newApp.applicationForm.values, app.applicationForm.values);
//         }
//       }
//     }
//     // don't unmask new application as it was not masked at all
//     // unmask
//     var encrypted = utils.encrypt(newApp, applicationEncryFields)
//     dao.updDoc(appId || newApp._id, encrypted, function(result) {
//       if (!result.error || result.success) {
//         newApp._rev = result.rev;
//         // update view index
//         dao.updateViewIndex("main", "summaryApps");
//         callback(result)
//       }
//     })
//   })
// }

module.exports.upsertAppAttachments = function(appId, rev, attachments, callback) {
  logger.log("upsertAppAttachments starts");
  var keys = Object.keys(attachments);
  var _rev = rev;
  var uploadTask = function(index) {
    if (keys.length > index) {
      var att = attachments[keys[index]];
      var ftype = null;
      var mime = commonFunctions.base64MimeType(att);
      if(!mime){
        ftype = fileType(new Buffer(att, 'base64'));
        mime = (!!ftype) ? ftype.mime : 'application/pdf';
      }
      dao.uploadAttachmentByBase64(appId, keys[index], _rev, att, mime, function(result) {
        if (result && !result.error) {
          _rev = result.rev;
          uploadTask(index+1);
        } else {
          callback({success:false})
        }
      })
    } else {
      callback({success:true, rev: _rev})
    }
  }
  uploadTask(0);
};

module.exports.getAppAttachment = (appId, attId, callback) => {
  dao.getAttachment(appId, attId, (result) => {
    if (result && !result.error) {
      callback(result);
    } else {
      callback({success: false});
    }
  });
};

module.exports.deleteAppAttachments = function(appId, attIds, callback) {
  getApplication(appId, function(app){
    if (app && !app.error) {
      let attachments = app['_attachments'];
      _.forEach(attIds, (attId) => {
        delete attachments[attId];
      });
      app['_attachments'] = attachments;
      dao.updDoc(appId, app, function(result){
        if (!result.error || result.success) {
          // newApp._rev = result.rev;
          dao.updateViewIndex("main", "summaryApps");
          // dao.updateViewIndex("main", "signatureExpire");
          callback({
            success: true,
            application: app
          });
        } else {
          callback({success: false});
        }
      });
    } else {
      callback({success: false});
    }
  });
};

module.exports.deleteAppAttachment = function(appId, attId, session, callback) {
  getApplication(appId, function(app){
    if (app && !app.error) {
      let attachments = app['_attachments'];
      delete attachments[attId];
      app['_attachments'] = attachments;

      dao.updDoc(appId, app, function(result){
        if (!result.error || result.success) {
          // newApp._rev = result.rev;
          dao.updateViewIndex("main", "summaryApps");
          // dao.updateViewIndex("main", "signatureExpire");
          callback({success: true});
        } else {
          callback({success: false});
        }
      })
    } else {
      callback({success: false});
    }
  });
  // dao.delAttachment(appId, attId, (result)=>{
  //   if (result && !result.error) {
  //         callback({success:true})
  //       } else {
  //         callback({success:false})
  //       }
  // })
};

module.exports.getApplicationPDF = function(appId, callback) {
  //var docId = genQuotationDocID(appId);
  dao.getAttachment(appId, appPdfName, callback)
}

module.exports.getFnaReportPDF = function(appId, callback) {
  //var docId = genQuotationDocID(appId);
  dao.getAttachment(appId, fnaPdfName, callback)
}

module.exports.getQuotationPDF = function(appId, callback) {
  //var docId = genQuotationDocID(appId);
  dao.getAttachment(appId, fnaPdfName, callback)
}

module.exports.uploadAttachmentPDF = function(docId, attchId, rev, data, mime, callback) {
  dao.uploadAttachmentByBase64(docId, attchId, rev, data, mime, callback);
}

// var getFormFiller = function(cid, callback) {
//   var id = "cust_form_filler_" + cid;
//   dao.getDoc(id, function(ff){
//     callback(utils.decrypt(ff, formFillerEncryFields));
//   });
// };
// module.exports.getFormFiller = getFormFiller;

//var fillerFilters = ['iidNo'];

// module.exports.upsertFormFiller = function(cid, data, callback) {
//   if (cid) {
//     var id = "cust_form_filler_" + cid;
//     if (!data._rev) {
//       getFormFiller(cid, function(doc) {
//         if (doc && !doc.error) {

//           // TODO:: handle masked fields !!!

//           if (!_.isEqual(doc.applicationForm, data.applicationForm)) {
//             doc.applicationForm = utils._.cloneDeep(data.applicationForm);

//             var encrypted = utils.encrypt(doc, formFillerEncryFields);
//             dao.updDoc(id, encrypted, function(result) {
//               logger.log('update form filler:', result);
//               if (callback) {
//                 callback({success: true});
//               }
//             });
//           } else { // no changes
//             if (callback) {
//               callback({success: true});
//             }
//           }
//         } else {  // no exsiting file found
//           var formValue = Object.assign(data, {
//             type: 'formFiller',
//             agentCode: 'SYS',
//             cid: cid,
//             updateDate: (new Date()).getTime()
//           });
//           var encrypted = utils.encrypt(formValue, formFillerEncryFields);
//           dao.updDoc(id, encrypted, function(result) {
//             logger.log('update form filler:', result);
//             if (callback) {
//               callback({success: result && !result.error});
//             }
//           });
//         }
//       })
//     } else {
//       var encrypted = utils.encrypt(data, formFillerEncryFields);
//       dao.updDoc(id, encrypted, function(result) {
//         logger.log('update form filler:', result);
//         if (callback) {
//           callback({success: result && !result.error});
//         }
//       });
//     }
//   } else {
//     logger.log('ERROR: upsertFormFiller: undefined profile ID :', cid);
//     if (callback) {
//       callback({success: false});
//     }
//   }
// }

module.exports.getAppFormTemplate = function(formId, callback){
  formId = 'appFormTemplate';
  dao.getDoc(formId, function(template){
    callback(template);
  });
};

module.exports.getEmailTemplate = function (callback) {
  dao.getDoc('eApp_email_template', callback);
};

module.exports.getSubmissionEmailTemplate = function (isFAChannel, callback) {
  if (isFAChannel) {
    dao.getDoc('eSubmission_email_template_fa', callback);
  } else {
    dao.getDoc('eSubmission_email_template', callback);
  }
};

module.exports.getAppSumEmailAttKeys = function (session, callback){
  let channelType = _.get(session.agent, 'channel.type');
  if (channelType === 'FA') {
    dao.getDoc('eApp_fa_email_attachments', callback);
  } else {
    dao.getDoc('eApp_email_attachments', callback);
  }
};

module.exports.initSuppDocsViewedListInApp = function (applicationId, agent) {
  const funcName = 'initSuppDocsViewedListInApp';
  const loginAgentCode = _.get(agent, 'agentCode', '');

  return new Promise((resolve) => {
    getApplication(applicationId, (app) => {
      var viewedList = _.get(app, 'supportDocuments.viewedList');
      const hasInited = _.includes(_.keys(viewedList), loginAgentCode);
      if (hasInited) {
        console.log(funcName + ': Agent ' + loginAgentCode + 'has existing viewedList in ' + applicationId);
        resolve();
      } else {
        const isFAChannel = _.get(agent, 'channel.type') === 'FA';
        const sysDocsIds = ['appPdf', 'proposal'];
        const fnaReportIdArr = ['fnaReport'];
        const isShield = _.get(app, 'quotation.quotType') === 'SHIELD';
        let needViewDocsIdList = [];

        if (loginAgentCode !== app.agentCode) {
          needViewDocsIdList = _.keys(_.get(app, 'supportDocuments.viewedList.' + app.agentCode, {}));
        }
        needViewDocsIdList = _.union(needViewDocsIdList, sysDocsIds);

        if (isFAChannel) {
          // if in FA Channel, need to add cAck's files into ViewedList, as the documents of FNA in FA Channel
          const cAckFiles = _.get(app, 'supportDocuments.values.policyForm.mandDocs.cAck', []);
          needViewDocsIdList = _.union(needViewDocsIdList, _.map(cAckFiles, 'id'));
        } else {
          // if non-FA Channel, system documents contains FNA Report
          needViewDocsIdList = _.union(needViewDocsIdList, fnaReportIdArr);
        }

        if (isShield) {
          var shieldAppPdfArr = [];
          _.each(app.iCids, (iCid) => {
            shieldAppPdfArr.push('appPdf' + iCid);
          });
          needViewDocsIdList = _.union(needViewDocsIdList, shieldAppPdfArr);
        }

        _.each(needViewDocsIdList, (id) => {
          _.set(viewedList, loginAgentCode + '.' + id, false);
        });

        _upsertApplication(app.id, app, function(resp){
          if (resp) {
            console.log(funcName + ': Agent ' + loginAgentCode + 'viewedList has been created in ' + applicationId);
            resolve();
          } else {
            console.log(funcName + ': Agent ' + loginAgentCode + 'viewedList FAILED to be created in ' + applicationId);
            resolve();
          }
        });
      }
    });
  });
};


