const dao = require('../cbDaoFactory').create();
const nDao = require('./needs');
const bDao = require('./bundle');
const cFunctions = require('../utils/CommonUtils');
const utils = require('../utils/RemoteUtils');
const DateUtils = require('../../common/DateUtils');
const encryFields = ['email'];
var logger = global.logger || console;
var _ = require('lodash');

var genClientId = function(agentNumber, seq){
  agentNumber = cFunctions.getAgentIdForDoc(agentNumber);
  seq = cFunctions.getSeqForDoc(seq);
  return "CP" + agentNumber + "-" +  seq;
}

var _createClientNumber = function(agentCode, callback){
  cFunctions.getDocNumber(agentCode, "client", function(agentNumber, seq){
    callback(genClientId(agentNumber, seq));
  })
}
module.exports.createClientNumber = _createClientNumber;

module.exports.getContactList = function(compCode, agentId, callback){
  var result = [];
  var validbundleResult = [];
  dao.updateViewIndex("main", "contacts",(res) => {
    dao.updateViewIndex("main", "validbundleApplicationsByAgent",(res) => {
      dao.getViewRange(
        'main',
        'validbundleApplicationsByAgent',
        '["'+ compCode + '","' + agentId + '"]',
        '["'+ compCode + '","' + agentId + '"]',
        null,
        function(pList){
          if(pList && pList.rows){
            for(var i=0; i<pList.rows.length; i++){
              validbundleResult.push(pList.rows[i].value);
            }
          }
          dao.getViewRange(
            'main',
            'contacts',
            '["'+ compCode + '","' + agentId + '"]',
            '["'+ compCode + '","' + agentId + '"]',
            null,
            function(pList){
              var applicationCountResult = {};
              for(var i=0; i<validbundleResult.length; i++){
                let validbundleResultRow = validbundleResult[i];
                if(validbundleResultRow.applicationDocId){
                  let pCid = validbundleResultRow.pCid;
                  applicationCountResult[pCid] = (!applicationCountResult[pCid])? 1: applicationCountResult[pCid] + 1;
                }
              }
              if(pList && pList.rows){
                for(var i=0; i<pList.rows.length; i++){
                  let pCid = pList.rows[i].id;
                  pList.rows[i].value.applicationCount = (!applicationCountResult[pCid]) ? 0 : applicationCountResult[pCid];
                  result.push(pList.rows[i].value);
                }
              }
              callback(result);
            }
          )
        }
      );
    });
  });
};

var unlinkRelationship = function(cid, fid, callback){
  dao.getDoc(cid, function(exDoc){
    //remove relationship of client
    var cDependants = exDoc.dependants;
    var cDepLength = cDependants.length
    for(var i=cDepLength-1; i>=0; i--){
      if(cDependants[i].cid == fid){
        exDoc.dependants.splice(i,1);
      }
    }

    //update document
    dao.updDoc(cid, exDoc, function(firstResp){
      if(firstResp.ok){     
        /**
         * TODO - eric - 20180731
         * prevent the error <<You attempted to set the key '_rev' with the value ... 
         * on an object that is meant to be immutable and has been frozen>>
         */
        // exDoc._rev = firstResp.rev;

        let exDoc2 = _.cloneDeep(exDoc);
        exDoc2._rev = firstResp.rev;

        dao.getDoc(fid, function(exFDoc){
          //remove relationship of dependant
          var fDependants = exFDoc.dependants;
          var fDepLength = fDependants.length
          for(var i=fDepLength-1; i>=0; i--){
            if(fDependants[i].cid == cid){
              exFDoc.dependants.splice(i,1);
            }
          }

          //upload document
          dao.updDoc(fid, exFDoc, function(secondResp){
            callback(exDoc2);
          })
        })
      }else{
        callback({
          error: true
        })
      }
    })
  })
}

module.exports.unlinkRelationship = unlinkRelationship;

module.exports.getTrustIndividualLayout = function(data, callback) {
  dao.getDocFromCacheFirst("trustIndividualsEditDialog", callback)
}


var getProfileLayout = function(data, callback) {
  dao.getDocFromCacheFirst("profileLayout", callback)
}

module.exports.getProfileLayout = getProfileLayout;

module.exports.getProfileDisplayLayout = function(data, callback) {
  dao.getDocFromCacheFirst("profileDisplayLayout", callback)
}

module.exports.getFamilProfileLayout = function(data, callback) {
  dao.getDocFromCacheFirst("familyProfileLayout", callback)
}

module.exports.addProfile = function(profile, agent, photo, callback){
  let {agentCode: aid} = agent;
  _createClientNumber(aid, function(docId){
    //init profile
    profile.cid=docId,
    profile.type = "cust";
    profile.agentId = aid;
    profile.agentCode = aid;
    profile.compCode = agent.compCode;
    profile.dealerGroup = agent.channel;

    profile = _handleProfile(profile, docId, agent, function(_profile){
      dao.updDoc(docId, _profile, function(data){
        // trigger view update
        dao.updateViewIndex("main", "contacts");
        if(photo && photo.type && photo.value){
          dao.uploadAttachmentByBase64(docId, 'photo', data.rev, photo.value, photo.type, function(resp){
            callback(_profile);
          })
        }else{
          callback(_profile);
        }
      });
    });

  });
}

var getOppositeRelationship = function(value, gender){
  var relationships = global.optionsMap.relationship.options;
  for(var i in relationships){
    var relationship = relationships[i];
    if(value == relationship.value){
      return relationship.opposite[gender];
    }
  }
  return "";
}

var _saveFamilyMember = function(fid, cid, agent, _profile, photo, callback, updateView){
   if(!_profile.cid){
    _profile.cid = fid;
    _profile.type = "cust";
    _profile.agentId = agent.agentCode;
    _profile.agentCode = agent.agentCode;
    _profile.compCode = agent.compCode;
    _profile.dealerGroup = agent.channel;
  }
  let {relationship, relationshipOther} = _profile;
  _handleProfile(_profile, cid, agent, function(profile){

    //get client document
    var dependant = {
      cid: profile.cid,
      relationship,
      relationshipOther
    };

    dao.getDoc(cid, function(exDoc) {
      var dependants = exDoc.dependants;
      var found = false;
      //found and update dependant in client profile
      for(var i in dependants){
        if(dependants[i].cid == profile.cid){
          exDoc.dependants[i]=dependant;
          found = true;
        }
      }
      if(!found){
        exDoc.dependants.push(dependant);
      }

      //handle relationship
      var fDependants = profile.dependants;
      var _found = false;
      var spoExisted = false;
      var spoExistedCid;
      var fDependant = {
        "cid": cid,
        "relationship": getOppositeRelationship(relationship, exDoc.gender),
        "relationshipOther": relationship ==="OTH"?("You are " + relationshipOther + " of " + exDoc.fullName): null
      };
      for(var i in fDependants){
        if(fDependants[i].cid == cid){
          //check for rename other relationship or not
          if(fDependants[i].relationshipOther && fDependant.relationshipOther && fDependants[i].relationshipOther != fDependant.relationshipOther){
            fDependant.relationshipOther = fDependants[i].relationshipOther;
          }
          profile.dependants[i]=fDependant;
          _found = true;
        }

        else if (fDependant.relationship === 'SPO' && fDependants[i].relationship === 'SPO') {
          spoExisted = true;
          spoExistedCid = fDependants[i].cid;
          fDependants.splice(i,1);
        }
      }
      if(!_found){
        profile.dependants.push(fDependant);
      }

      let callSer = function(){
        dao.getDoc(fid, function(fmDoc) {
          profile._rev = fmDoc._rev;
          profile._attachments = fmDoc._attachments;
          profile.relationship = null;
          profile.relationshipOther = null;
          delete profile.relationship;
          delete profile.relationshipOther;
          profile.lastUpdateDate = new Date().toISOString();
          dao.updDoc(fid, profile, function(result){
            if(result.ok){
              dao.updateViewIndex("main", "contacts");
              dao.updDoc(cid, exDoc, function(nextResult){
                var newProfile = Object.assign({}, profile);
                newProfile.relationship = relationship;
                newProfile.relationshipOther = relationshipOther;
                if(result.ok){
                  if(photo && photo.type && photo.value){
                      dao.uploadAttachmentByBase64(fid, 'photo', result.rev, photo.value, photo.type, function(resp){
                      callback(exDoc, newProfile, fid);
                      if(updateView){
                        dao.updateViewIndex("main", "contacts");
                      }
                    })
                  }else{
                    callback(exDoc, newProfile, fid);
                  }

                }
              });

            }
          });
        });
      }


      if (spoExisted === true) {
        unlinkRelationship(fid, spoExistedCid, () => {
          callSer();
        })
      }else {
        callSer();
      }

    });
  });
}
module.exports.saveFamilyMember  = function(fid, cid, agent, profile, photo, callback){
  if(!fid){
    _createClientNumber(agent.agentCode, function(docId){
      _saveFamilyMember(docId, cid, agent, profile, photo, callback, true);
    });
  }
  else{
    _saveFamilyMember(fid, cid, agent, profile, photo, callback)
  }
}

var changeProfileRelationship = function(targetId, changedGenderId) {
  var relationships = global.optionsMap.relationship.options;
  return new Promise((resolve, reject) => {
     dao.getDoc(targetId, function(exDoc) {
      resolve(exDoc);
     });
  }).then(doc => {
    let dep = _.get(doc, 'dependants');
    _.forEach(dep, obj =>{
      if (obj.cid === changedGenderId){
        obj.relationship = _.get(_.filter(relationships, o=> {return o.value === obj.relationship}),'[0].oppositeValue') || obj.relationship;
      }
    })
    doc.dependants = dep;
    dao.updDoc(doc.cid, doc, function(result){
      return true;
    })
  }).catch((error)=>{
      logger.error("Error in changeProfileRelationship->new Promise: ", error);
  });
}

var changeOppositeGenderRelationship = function(profile, photo, tiPhoto, callback){
  let dep = _.get(profile, 'dependants');
  let spoExisted = false;
  let spoCid='';
  let promises = [];
  if (_.isArray(dep) && dep.length > 0){
    var relationships = global.optionsMap.relationship.options;
    _.forEach(dep, obj =>{
      if (obj.relationship === 'SPO'){
        spoExisted = true;
        spoCid = obj.cid;
      } else {
        promises.push(changeProfileRelationship(obj.cid, profile.cid));
      }
    })

  }
  Promise.all(promises).then(function(result) {
    if (spoExisted){
      unlinkRelationship(profile.cid, spoCid, (p) => {
        if (p && p.cid) {
          profile.dependants = p.dependants;
          updateProfile(profile, photo, tiPhoto, callback);
        }else {
            callback(profile);
        }

      })
    }else {
      updateProfile(profile, photo, tiPhoto, callback);
    }
  }).catch((error)=>{
      logger.error("Error in changeOppositeGenderRelationship->Promise.all: ", error);
  });
}
var updateProfile = function (profile, photo, tiPhoto, callback) {
  var docId = profile.cid;
  dao.getDoc(docId, (exDoc)=>{
    profile._rev = exDoc._rev;
    profile._attachments = exDoc._attachments;
    profile.lastUpdateDate = new Date().toISOString();
    dao.updDoc(docId, profile, function(result){
      if(result){
        dao.updateViewIndex("main", "contacts");
        if(photo && photo.type && photo.value){
          dao.uploadAttachmentByBase64(docId, 'photo', result.rev, photo.value, photo.type, function(resp){
            if(tiPhoto && tiPhoto.type && tiPhoto.value){
              dao.uploadAttachmentByBase64(docId, 'tiPhoto', result.rev, tiPhoto.value, tiPhoto.type, function(resp2){
                callback(profile);
              })
            }else{
              callback(profile);
            }
          })
        }else{
          if(tiPhoto && tiPhoto.type && tiPhoto.value){
              dao.uploadAttachmentByBase64(docId, 'tiPhoto', result.rev, tiPhoto.value, tiPhoto.type, function(resp2){
                callback(profile);
              })
            }else{
              callback(profile);
            }
        }
      }
    });
  })
}


module.exports.saveProfile = function(_profile, photo, tiPhoto, aid, callback){
  var docId = _profile.cid;
  _handleProfile(_profile, docId, aid, function(profile){
    _getProfile(docId, true).then((oProfile)=>{
      if (profile && oProfile && profile.gender !== oProfile.gender) {
        profile = changeOppositeGenderRelationship(profile, photo, tiPhoto, callback)
      }else {
        updateProfile(profile, photo, tiPhoto, callback);
      }
    }).catch((error)=>{
      logger.error("Error in saveProfile->_getProfile: ", error);
    });
  });
}

module.exports.deleteProfile = function(cid, callback){
  var removeDependants = function(did, callback){
    dao.getDoc(did, function(dDoc){
      for(var i in dDoc.dependants){
        if(dDoc.dependants[i].cid === cid){
          dDoc.dependants.splice(i, 1);
        }
      }
      dao.updDoc(did, dDoc, callback);
    });
  }
  dao.getDoc(cid, function(exDoc){
    for(var i in exDoc.dependants){
      removeDependants(exDoc.dependants[i].cid);
    }
    exDoc._delete = true;
    //dao.updDoc(cid, {_rev: exDoc._rev}, function(){ // TODO - eric cheung - 20180728 - this is true delete, not mark delete
    dao.delDoc(cid, function(){
      callback({success: true});
      dao.updateViewIndex("main", "contacts");
    });
  })
}

var _handleProfile = function(data, cid, agent, callback){
  data.type = 'cust';
  if(!data.dependants){
    data.dependants = [];
  }

  if(!data.referrals){
    data.referrals = "";
  }

  if(!data.fnaRecordIdArray){
    data.fnaRecordIdArray = "";
  }

  if(data.dob){
    data.nearAge = DateUtils.getNearestAge(new Date(), new Date(data.dob));
    data.age = cFunctions.calcAge(data.dob);
  }

  if(!data.bundle){
    bDao.createNewBundle(data.cid, agent).then((bundle)=>{
      data.bundle = bundle;
      callback(data);
    }).catch((error)=>{
      logger.error("Error in _handleProfile->createNewBundle: ", error);
    });
  }
  else {
    callback(data);
  }

}


var _getProfile = function(docId, isFreeze) {
  return new Promise((resolve)=>{
    dao.getDoc(docId, (profile)=>{
      if(!profile.cid){
        resolve();
      }
      else {
        if(!isFreeze){
          if(profile.age && profile.dob){
            let dob = new Date(DateUtils.formatDate(new Date(profile.dob)));
            let age = cFunctions.calcAge(dob);
            let nearAge = DateUtils.getNearestAge(new Date(DateUtils.formatDate(new Date())), dob);
            if(age != profile.age || nearAge !=profile.nearAge){
              profile.age = age;
              profile.nearAge = nearAge;
              dao.updDoc(docId, profile);
            }
          }
        }
        resolve(profile);
      }
    })
  })
}

module.exports.getProfile = _getProfile;

module.exports.getClientById = function(docId, callback) {
  dao.getDoc(docId, function(result) {
    // if (result) {
      callback(result);
    // }
  })
}

var _getAllDependantsProfile = function(cid, isFreeze){
  return new Promise((resolve)=>{
    _getProfile(cid, isFreeze).then((profile={})=>{
      let promises = [], {dependants} = profile, result={};
      _.forEach(dependants, (d)=>{
        let {cid: did, relationship, relationshipOther} = d;
        promises.push(new Promise((resolve2)=>{
          _getProfile(did, isFreeze).then((dProfile)=>{
            dProfile.relationship = relationship;
            dProfile.relationshipOther = relationshipOther;
            result[dProfile.cid] = dProfile;
            resolve2();
          }).catch((error)=>{
            logger.error("Error in _getAllDependantsProfile->_getProfile: ", error);
          });
        }));
      })
      Promise.all(promises).then((args)=>{
        resolve(result);
      }).catch((error)=>{
        logger.error("Error in _getAllDependantsProfile->Promise.all: ", error);
      });
    }).catch((error)=>{
      logger.error("Error in _getAllDependantsProfile->_getProfile: ", error);
    });
  })
}
module.exports.getAllDependantsProfile = _getAllDependantsProfile;

var getProfileById = function(docId, callback) {
  dao.getDoc(docId, function(result) {
    if (result && !result.error) {
      var ret = result;
      try {
        ret = utils.decrypt(result, encryFields);
      } catch (e) {
        logger.error(e);
      }
      callback(ret);
    } else {
      callback(result);
    }
  })
}
module.exports.getProfileById = getProfileById

module.exports.getAuditLog = function(docId, callback) {
  dao.getDoc(docId, function(result) {
    if (result) {
      callback(result);
    }
  })
}

module.exports.updateClientById = function(id, data, callback) {
  dao.updDoc(id, data, function(result) {
    if (result && !result.error) {
      dao.updateViewIndex("main", "contacts");
      data._rev = result.rev;
    }
    callback(result);
  });
}

module.exports.updateAuditLog = function(id, data, callback) {
  dao.updDoc(id, data, function(result) {
    if (result && !result.error) {
      data._rev = result.rev;
    }
    callback(result);
  });
}

module.exports.updProfileById = function(docId, newProfile, oldProfile, callback) {
  getProfileLayout({}, function(template) {
    getProfileById(docId, function(profile) {
      // check masked file and unmasked it if it is not changed.
      if (profile && !profile.error) {
        for (var key in profile) {
          if (key.indexOf('_') == 0) {
            newProfile[key] = profile[key];
          }
        }
        utils.unmask(template, newProfile, profile);
      } else {
        utils.unmask(template, newProfile, oldProfile);
      }

      dao.updDoc(docId, utils.encrypt(newProfile, encryFields), function(result) {
        if (result && !result.error) {
          newProfile._rev = result.rev;
        }
        result.template = template;
        callback(result);
      });
    })
  })
}

module.exports.setIsProfileCanDel = function(cid, callback) {
  dao.getDoc(cid, function(exDoc){
    exDoc.haveSignDoc = true;
    dao.updDoc(cid, exDoc, function(resp){
      exDoc._rev = resp.rev;
      callback({profile: exDoc});
    })
  });
}

module.exports.getAddressByPostalCode = function(postalCode, fileName, callback) {
    return new Promise((resolve, reject) =>{
      dao.getDoc('postal_code_cov' + fileName, function(covObj){
        let resultObj = covObj[postalCode];
        if (resultObj) {
          resolve(callback(Object.assign({success: true}, resultObj)));
        }else {
          resolve(callback(Object.assign({success: false})));
        }
      });
    }).catch((e)=>{
      logger.error(e);
      callback({success: false});
    });
};

module.exports.saveTrustedIndividual = function(cid, tiInfo, tiPhoto){
  return new Promise((resolve)=>{
    _getProfile(cid, false).then((profile)=>{
      profile.trustedIndividuals = tiInfo;
      updateProfile(profile, null, tiPhoto, ()=>{resolve({profile})})
    }).catch((error)=>{
      logger.error("Error in saveTrustedIndividual->_getProfile: ", error);
    });
  })
}

var _updateProfile = function(cid, profile){
  return new Promise((resolve)=>{
    dao.getDoc(cid, (exDoc)=>{
      for(let i in exDoc){
        if(i.indexOf("_") === 0 ){
          profile[i] = exDoc[i];
        }
      }
      dao.updDoc(cid, profile, ()=>{resolve(profile)});
    })
  })
}
module.exports.updateProfile = _updateProfile;
