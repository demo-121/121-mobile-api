const _ = require('lodash');

const dao = require('../cbDaoFactory').create();
const logger = global.logger || console;

const getMessageByIds = function(compCode, messageIds) {
  let keys = _.map(messageIds, messageId => `["${compCode}","messageId","${messageId}"]`);
  return new Promise(resolve => {
    logger.log(`INFO:: getMessageByIds`);
    dao.getViewByKeys('main', 'systemNotification', keys, null).then((result) => {
      let validMessage = [];
      _.each(result && result.rows, (row) => {
          if (row.value && row.value.messageId) {
            validMessage.push(row.value);
          }
      });
      resolve(validMessage);
    });
  });
};
module.exports.getMessageByIds = getMessageByIds;

const getValidStartNotification = function(compCode, currentDateTime) {
  return new Promise(resolve => {
    dao.getViewRange(
      'main',
      'systemNotification',
      `["${compCode}","startTime",${0}]`,
      `["${compCode}","startTime",${currentDateTime}]`,
      null,
      (viewResult) => {
        resolve(viewResult);
      }
    );
  });
};
module.exports.getValidStartNotification = getValidStartNotification;

const getValidEndNotification = function(compCode, currentDateTime, expiryDate) {
  return new Promise(resolve => {
    dao.getViewRange(
      'main',
      'systemNotification',
      `["${compCode}","endTime",${currentDateTime}]`,
      `["${compCode}","endTime",${expiryDate}]`,
      null,
      (viewResult) => {
        resolve(viewResult);
      }
    );
  })
};
module.exports.getValidEndNotification = getValidEndNotification;
