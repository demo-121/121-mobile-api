const dao = require('../cbDaoFactory').create();
var logger = global.logger || console;

module.exports.getCompanyInfo = function(callback) {
  // dao.getDocFromCacheFirst('companyInfo', function(result) {
  //   callback(result);
  // });
  dao.getDoc('companyInfo', (result) => {
    callback(result);
  });
};

module.exports.getTNCTemplate = function(callback) {
  dao.getDocFromCacheFirst('tncTemplate', function(result) {
    callback(result);
  });
};

