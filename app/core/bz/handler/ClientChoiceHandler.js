var _           = require('lodash');
var logger      = global.logger || console;

const moment    = require('moment');
const dao       = require('../cbDaoFactory').create();
const quotDao   = require('../cbDao/quotation');
const bDao      = require('../cbDao/bundle');
const nDao      = require('../cbDao/needs');
const appDao    = require('../cbDao/application');
const shieldApplicationFuncs = require('./application/shield/clientChoice');

const commonUtils = require('../../common/CommonUtils');
const DateUtils = require('../../common/DateUtils');
const commonAppHandler = require('./application/common');

//Template name
const fcRecommendationId                = 'fcRecommendation';
const ccRecommedationMenuItemId         = 'clientChoiceRecommendationMenuItemTmpl';
const ccRecommedationMenuItemId_Shield  = 'clientChoiceRecommendationMenuItemTmpl_Shield';
const ccBudgetId                        = 'clientChoiceBudgetTmpl';
const ccAcceptanceId                    = 'clientChoiceAcceptanceTmpl';
const ccAcceptanceChoiceItemId          = 'clientChoiceAcceptanceChoiceItemTmpl';
const ccyTmplId                         = 'ccy';

//Budget Compare Result
const BUDGET_UNDERUTILIZED  = 'Underutilized';
const BUDGET_EXCEEDED       = 'Exceeded';
const BUDGET_ALIGNED        = 'Aligned';

const productPaymentMapping = 'product_to_payment_method';

/** DO NOT REMOVE ccRecommendDefaultData */
const ccRecommendDefaultData = {
  recommendation: {
    reason: '',
    benefit: '',
    limitation: '',
    rop: {
      choiceQ1: '',
      choiceQ1Sub1: '',
      choiceQ1Sub2: '',
      choiceQ1Sub3: '',
      existLife: 0,
      existTpd: 0,
      existCi: 0,
      existPaAdb: 0,
      existTotalPrem: 0,
      replaceLife: 0,
      replaceTpd: 0,
      replaceCi: 0,
      replacePaAdb: 0,
      replaceTotalPrem: 0
    },
    rop_shield: {
      ropBlock: {
        shieldRopAnswer_0: '',
        shieldRopAnswer_1: '',
        shieldRopAnswer_2: '',
        shieldRopAnswer_3: '',
        shieldRopAnswer_4: '',
        shieldRopAnswer_5: '',
        iCidRopAnswerMap: {},
        ropQ2: '',
        ropQ3: '',
        ropQ1sub3: ''
      }
    }
  }
};

const _frozenTemplate = function (template) {
  let type = (template.type) ? template.type.toUpperCase() : '';
  if (['TEXTSELECTION', 'TEXTAREA', 'TEXT', 'QRADIOGROUP'].indexOf(type) >= 0) {
    template.disabled = true;
  } else if (template.items) {
    for (let i = 0; i < template.items.length; i++) {
      _frozenTemplate(template.items[i]);
    }
  }
};

module.exports.updateQuotationClientChoice = function(data, session, callback) {
  var values        = data.values;
  var stepperIndex  = data.stepperIndex;
  var isSaveOnly    = data.isSaveOnly;

  var newCcFlag = {
    isRecommendationCompleted: false,
    isBudgetCompleted: false,
    isAcceptanceCompleted: false,
    isClientChoiceCompleted: false,
    clientChoiceStep: 0
  };

  bDao.getCurrentBundle(data.clientId).then((bundle) => {
    if (bundle) {
      if (isSaveOnly) {
        newCcFlag.isRecommendationCompleted   = bundle.clientChoice.isRecommendationCompleted;
        newCcFlag.isBudgetCompleted           = bundle.clientChoice.isBudgetCompleted;
        newCcFlag.isAcceptanceCompleted       = bundle.clientChoice.isAcceptanceCompleted;
        newCcFlag.clientChoiceStep            = bundle.clientChoice.clientChoiceStep ? bundle.clientChoice.clientChoiceStep : 0;
      }

      if (stepperIndex == 0) {
        let upPromises = [];

        /** DO NOT REMOVE if condition */
        if (session.platform) {
          const quotList = _.concat(_.get(values, "recommendation.chosenList"), _.get(values, "recommendation.notChosenList"));
          _.forEach(quotList, quotId => {
            upPromises.push(new Promise((upResolve, upReject) => {
              let cValues       = _.get(values, `recommendation[${quotId}]`);
              let newQuotData   = {};
              let newCcData     = {};
  
              quotDao.getQuotation(quotId, function(quot) {
                if (quot && !quot.error) {
                  //Prepare Client Choice Recommendation data saved in each Quotation
                  newCcData = Object.assign({}, _.cloneDeep(ccRecommendDefaultData), {
                    isClientChoiceSelected: false
                  });
  
                  newCcData.isClientChoiceSelected = _.get(values, "recommendation.chosenList", []).indexOf(quotId) >= 0;
                  newCcData.recommendation.reason = _.get(values, `recommendation[${quotId}].reason`);
                  newCcData.recommendation.benefit = _.get(values, `recommendation[${quotId}].benefit`);
                  newCcData.recommendation.limitation = _.get(values, `recommendation[${quotId}].limitation`);
  
                  newCcData.recommendation.rop.choiceQ1 = _.get(values, `recommendation[${quotId}].rop.choiceQ1`);
                  newCcData.recommendation.rop.existLife = _.get(values, `recommendation[${quotId}].rop.existLife`);
                  newCcData.recommendation.rop.existTpd = _.get(values, `recommendation[${quotId}].rop.existTpd`);
                  newCcData.recommendation.rop.existCi = _.get(values, `recommendation[${quotId}].rop.existCi`);
                  newCcData.recommendation.rop.existPaAdb = _.get(values, `recommendation[${quotId}].rop.existPaAdb`);
                  newCcData.recommendation.rop.existTotalPrem = _.get(values, `recommendation[${quotId}].rop.existTotalPrem`);
  
                  newCcData.recommendation.rop.choiceQ1Sub1 = _.get(values, `recommendation[${quotId}].rop.choiceQ1Sub1`);
                  newCcData.recommendation.rop.choiceQ1Sub2 = _.get(values, `recommendation[${quotId}].rop.choiceQ1Sub2`);
                  newCcData.recommendation.rop.choiceQ1Sub3 = _.get(values, `recommendation[${quotId}].rop.choiceQ1Sub3`);
                  newCcData.recommendation.rop.replaceLife = _.get(values, `recommendation[${quotId}].rop.replaceLife`);
                  newCcData.recommendation.rop.replaceTpd = _.get(values, `recommendation[${quotId}].rop.replaceTpd`);
                  newCcData.recommendation.rop.replaceCi = _.get(values, `recommendation[${quotId}].rop.replaceCi`);
                  newCcData.recommendation.rop.replacePaAdb = _.get(values, `recommendation[${quotId}].rop.replacePaAdb`);
                  newCcData.recommendation.rop.replaceTotalPrem = _.get(values, `recommendation[${quotId}].rop.replaceTotalPrem`);
                  
                  if (_.get(quot, 'quotType') === 'SHIELD') {
                    let anyInsuredChoseROP = false;
                    let insuredRopValue = '';
                    for (let i = 0; i < 6; i++) {
                      insuredRopValue = _.get(values, `recommendation[${quotId}].ropBlock.shieldRopTable.shieldRopAnswer_${i}`, '');
                      if (insuredRopValue) {
                        newCcData.recommendation.rop_shield.ropBlock['shieldRopAnswer_' + i] = insuredRopValue;
                        if (insuredRopValue === 'Y') {
                          anyInsuredChoseROP = true;
                        }
                      }
                    }
  
                    newCcData.recommendation.rop_shield.ropBlock['iCidRopAnswerMap']
                      = _.get(values, `recommendation[${quotId}].ropBlock.shieldRopTable.iCidRopAnswerMap`, {});
  
                    if (anyInsuredChoseROP) {
                      newCcData.recommendation.rop_shield.ropBlock.ropQ2 = _.get(values, `recommendation[${quotId}].ropBlock.ropQ2`);
                      newCcData.recommendation.rop_shield.ropBlock.ropQ3 = _.get(values, `recommendation[${quotId}].ropBlock.ropQ3`);
                      newCcData.recommendation.rop_shield.ropBlock.ropQ1sub3 = _.get(values, `recommendation[${quotId}].ropBlock.ropQ1sub3`);
                    }
                  }
                  
                  newQuotData = _.cloneDeep(quot);
                  newQuotData.clientChoice = newCcData;
  
                  quotDao.upsertQuotation(quot._id, newQuotData, (upRes) => {
                    if (upRes && !upRes.error) {
                      _prepareReplacementOfPolicies(bundle, newQuotData).then(upResolve);
                    } else {
                      upReject(upRes);
                    }
                  });
                } else {
                  upReject(quot);
                }
              });
            }));
          });
        } else {
          // TODO: deprecate following values format, use the above format after web is updated
          for (let i = 0; i < values.menuIdList.length; i ++) {
            let menuId = values.menuIdList[i];

            upPromises.push(new Promise((upResolve, upReject) => {
              let cValues       = values[menuId];
              let newQuotData   = {};
              let newCcData     = {};

              quotDao.getQuotation(cValues.quotId, function(quot) {
                if (quot && !quot.error) {
                  //Prepare Client Choice Recommendation data saved in each Quotation
                  newCcData = Object.assign({}, JSON.parse(JSON.stringify(ccRecommendDefaultData)), {
                    isClientChoiceSelected: false
                  });

                  newCcData.isClientChoiceSelected            = cValues.clientChoiceSelect === 'Y';
                  newCcData.recommendation.reason             = cValues.recommendReason;
                  newCcData.recommendation.benefit            = cValues.recommendBenefit;
                  newCcData.recommendation.limitation         = cValues.recommendLimit;
                  newCcData.recommendation.rop.choiceQ1       = cValues.ropQ1;

                  newCcData.recommendation.rop.existLife      = cValues.policyTable.existLife;
                  newCcData.recommendation.rop.existTpd       = cValues.policyTable.existTpd;
                  newCcData.recommendation.rop.existCi        = cValues.policyTable.existCi;
                  newCcData.recommendation.rop.existPaAdb     = cValues.policyTable.existPaAdb;
                  newCcData.recommendation.rop.existTotalPrem = cValues.policyTable.existTotalPrem;

                  if (cValues.ropQ1 === 'Y') {
                    newCcData.recommendation.rop.choiceQ1Sub1     = cValues.ropQ1sub1;
                    newCcData.recommendation.rop.choiceQ1Sub2     = cValues.ropQ1sub2;
                    newCcData.recommendation.rop.choiceQ1Sub3     = cValues.ropQ1sub3;
                    newCcData.recommendation.rop.replaceLife      = cValues.policyTable.replaceLife ? cValues.policyTable.replaceLife : 0;
                    newCcData.recommendation.rop.replaceTpd       = cValues.policyTable.replaceTpd ? cValues.policyTable.replaceTpd : 0;
                    newCcData.recommendation.rop.replaceCi        = cValues.policyTable.replaceCi ? cValues.policyTable.replaceCi : 0;
                    newCcData.recommendation.rop.replacePaAdb     = cValues.policyTable.replacePaAdb ? cValues.policyTable.replacePaAdb : 0;
                    newCcData.recommendation.rop.replaceTotalPrem = cValues.policyTable.replaceTotalPrem ? cValues.policyTable.replaceTotalPrem : 0;
                  }

                  if (_.get(quot, 'quotType') === 'SHIELD') {
                    let anyInsuredChoseROP = false;
                    let insuredRopValue = '';
                    for (let i = 0; i < 6; i++) {
                      insuredRopValue = _.get(cValues, 'ropBlock.shieldRopTable.shieldRopAnswer_' + i, '');
                      if (insuredRopValue) {
                        newCcData.recommendation.rop_shield.ropBlock['shieldRopAnswer_' + i] = insuredRopValue;
                        if (insuredRopValue === 'Y') {
                          anyInsuredChoseROP = true;
                        }
                      }
                    }

                    newCcData.recommendation.rop_shield.ropBlock['iCidRopAnswerMap']
                      = _.get(cValues, 'ropBlock.shieldRopTable.iCidRopAnswerMap', {});

                    if (anyInsuredChoseROP) {
                      newCcData.recommendation.rop_shield.ropBlock.ropQ2      = _.get(cValues, 'ropBlock.ropQ2');
                      newCcData.recommendation.rop_shield.ropBlock.ropQ3      = _.get(cValues, 'ropBlock.ropQ3');
                      newCcData.recommendation.rop_shield.ropBlock.ropQ1sub3  = _.get(cValues, 'ropBlock.ropQ1sub3');
                    }
                  }
                  
                  newQuotData = _.cloneDeep(quot);
                  newQuotData.clientChoice = newCcData;

                  quotDao.upsertQuotation(quot._id, newQuotData, (upRes) => {
                    if (upRes && !upRes.error) {
                      _prepareReplacementOfPolicies(bundle, newQuotData).then(upResolve);
                    } else {
                      upReject(upRes);
                    }
                  });
                } else {
                  upReject(quot);
                }
              });
            }).catch((error) => {
              logger.error(error);
            }));
          }
        }

        Promise.all(upPromises).then((newData) => {
          //Change Flag in bundle
          if (!isSaveOnly) {
            newCcFlag.isRecommendationCompleted = true;
            newCcFlag.clientChoiceStep = 1;

            bundle.clientChoice = Object.assign(bundle.clientChoice, newCcFlag);
          }

          logger.log(`INFO: BundleUpdate - [cid:${data.clientId}; bundleId:${_.get(bundle, 'id')}; stepperIndex:${stepperIndex}; fn:updateQuotationClientChoice]`);
          bDao.updateBundle(bundle, function(resp){
            bDao.updateApplicationReplacementOfPolicies(data.clientId, newData).then(() => {
              if (resp && !resp.error) {
                // bDao.updateStatus(data.clientId, bDao.BUNDLE_STATUS.HAS_PRE_EAPP, (newBundle)=>{
                bDao.updateStatus(data.clientId, bDao.BUNDLE_STATUS.HAS_PRE_EAPP).then((newBundle)=>{
                  logger.log('INFO: updateQuotationClientChoice - end [RETURN=1]', data.clientId, stepperIndex);
                  /** DO NOT REMOVE _.has(values, "recommendation") ? { recommendation: values.recommendation } : {} */
                  callback(Object.assign({success: true}, newCcFlag, _.has(values, "recommendation") ? { recommendation: values.recommendation } : {}));
                }).catch((error)=>{
                  logger.error('ERROR: updateQuotationClientChoice [RETURN=-1] fails', data.clientId, stepperIndex, error);
                  callback({success: false});
                });
              } else {
                logger.log('INFO: updateQuotationClientChoice - end [RETURN=-1]', data.clientId, stepperIndex);
                callback({success: false});
              }
            }).catch((error) => {
              logger.error('ERROR: updateQuotationClientChoice - updateApplicationReplacementOfPolicies', data.clientId, error);
            });
          });
        }).catch((error) => {
          logger.error('ERROR: updateQuotationClientChoice - prepareReplacementOfPolicies', data.clientId, error);
        });
      } else if (stepperIndex == 1) {
        // Set client choice budget's data into bundle
        if (!bundle.clientChoice.hasOwnProperty('budget')) {
          bundle.clientChoice.budget = {};
          bundle.clientChoice.isBudgetCompleted = false;
        }

        let budget = bundle.clientChoice.budget;

        // Save Client Choice Budget data to bundle
        /** DO NOT REMOVE if condition */
        if (session.platform) {
          budget.spBudget           = values.budget.spBudget;
          budget.spTotalPremium     = values.budget.spTotalPremium;
          budget.spCompare          = values.budget.spCompare;
          budget.spCompareResult    = values.budget.spCompareResult;
          budget.rpBudget           = values.budget.rpBudget;
          budget.rpTotalPremium     = values.budget.rpTotalPremium;
          budget.rpCompare          = values.budget.rpCompare;
          budget.rpCompareResult    = values.budget.rpCompareResult;
          budget.cpfOaBudget        = values.budget.cpfOaBudget;
          budget.cpfOaTotalPremium  = values.budget.cpfOaTotalPremium;
          budget.cpfOaCompare       = values.budget.cpfOaCompare;
          budget.cpfOaCompareResult = values.budget.cpfOaCompareResult;
          budget.cpfSaBudget        = values.budget.cpfSaBudget;
          budget.cpfSaTotalPremium  = values.budget.cpfSaTotalPremium;
          budget.cpfSaCompare       = values.budget.cpfSaCompare;
          budget.cpfSaCompareResult = values.budget.cpfSaCompareResult;
          budget.srsBudget          = values.budget.srsBudget;
          budget.srsTotalPremium    = values.budget.srsTotalPremium;
          budget.srsCompare         = values.budget.srsCompare;
          budget.srsCompareResult   = values.budget.srsCompareResult;
          budget.cpfMsBudget        = values.budget.cpfMsBudget;
          budget.cpfMsTotalPremium  = values.budget.cpfMsTotalPremium;
          budget.cpfMsCompare       = values.budget.cpfMsCompare;
          budget.cpfMsCompareResult = values.budget.cpfMsCompareResult;
          budget.budgetMoreChoice   = values.budget.budgetMoreChoice;
          budget.budgetMoreReason   = values.budget.budgetMoreReason;
          budget.budgetLessChoice   = values.budget.budgetLessChoice;
          budget.budgetLessReason   = values.budget.budgetLessReason;
          budget.multiCcyNote       = values.budget.multiCcyNote;
        } else {
          // TODO: deprecate following values format, use the above format after web is updated
          budget.spBudget           = values.spBudget;
          budget.spTotalPremium     = values.spTotalPremium;
          budget.spCompare          = values.spCompare;
          budget.spCompareResult    = values.spCompareResult;
          budget.rpBudget           = values.rpBudget;
          budget.rpTotalPremium     = values.rpTotalPremium;
          budget.rpCompare          = values.rpCompare;
          budget.rpCompareResult    = values.rpCompareResult;
          budget.cpfOaBudget        = values.cpfOaBudget;
          budget.cpfOaTotalPremium  = values.cpfOaTotalPremium;
          budget.cpfOaCompare       = values.cpfOaCompare;
          budget.cpfOaCompareResult = values.cpfOaCompareResult;
          budget.cpfSaBudget        = values.cpfSaBudget;
          budget.cpfSaTotalPremium  = values.cpfSaTotalPremium;
          budget.cpfSaCompare       = values.cpfSaCompare;
          budget.cpfSaCompareResult = values.cpfSaCompareResult;
          budget.srsBudget          = values.srsBudget;
          budget.srsTotalPremium    = values.srsTotalPremium;
          budget.srsCompare         = values.srsCompare;
          budget.srsCompareResult   = values.srsCompareResult;
          budget.cpfMsBudget        = values.cpfMsBudget;
          budget.cpfMsTotalPremium  = values.cpfMsTotalPremium;
          budget.cpfMsCompare       = values.cpfMsCompare;
          budget.cpfMsCompareResult = values.cpfMsCompareResult;
          budget.budgetMoreChoice   = values.budgetMoreChoice;
          budget.budgetMoreReason   = values.budgetMoreReason;
          budget.budgetLessChoice   = values.budgetLessChoice;
          budget.budgetLessReason   = values.budgetLessReason;
          budget.multiCcyNote       = values.multiCcyNote;
        }
        
        //Change Flag in bundle
        if (!isSaveOnly) {
          newCcFlag.isRecommendationCompleted = true;
          newCcFlag.isBudgetCompleted = true;
          newCcFlag.clientChoiceStep = 2;

          bundle.clientChoice = Object.assign(bundle.clientChoice, newCcFlag);
        }

        logger.log(`INFO: BundleUpdate - [cid:${data.clientId}; bundleId:${_.get(bundle, 'id')}; stepperIndex:${stepperIndex}; fn:updateQuotationClientChoice]`);
        bDao.updateBundle(bundle, (resp) => {
          if (resp && !resp.error) {
            logger.log('INFO: updateQuotationClientChoice - end [RETURN=2]', data.clientId, stepperIndex);
            /** DO NOT REMOVE _.has(values, "budget") ? { budget: values.budget } : {} */
            callback(Object.assign({success: true}, newCcFlag, _.has(values, "budget") ? { budget: values.budget } : {}));
          } else {
            logger.log('INFO: updateQuotationClientChoice - end [RETURN=-2]', data.clientId, stepperIndex);
            callback({success: false});
          }
        });
      } else if (stepperIndex == 2) {
        //Change Flag in bundle
        newCcFlag.isRecommendationCompleted = true;
        newCcFlag.isBudgetCompleted         = true;
        newCcFlag.isAcceptanceCompleted     = true;
        newCcFlag.clientChoiceStep          = 2;
        newCcFlag.isClientChoiceCompleted   = true;

        bundle.clientChoice = Object.assign(bundle.clientChoice, newCcFlag, {
          isAppListChanged: false,
        });
        // updateStatus
        if (bundle.status < bDao.BUNDLE_STATUS.HAS_PRE_EAPP){
          bundle.status = bDao.BUNDLE_STATUS.HAS_PRE_EAPP;
        }

        logger.log(`INFO: BundleUpdate - [cid:${data.clientId}; bundleId:${_.get(bundle, 'id')}; stepperIndex:${stepperIndex}; fn:updateQuotationClientChoice]`);
        bDao.updateBundle(bundle, resp => {
          if (resp && !resp.error) {
            logger.log('INFO: updateQuotationClientChoice - end [RETURN=3]', data.clientId, stepperIndex);
            /** DO NOT REMOVE _.has(values, "acceptance") ? { acceptance: values.acceptance } : {} */
            callback(Object.assign({success: true}, newCcFlag, _.has(values, "acceptance") ? { acceptance: values.acceptance } : {}));
          } else {
            logger.log('INFO: updateQuotationClientChoice - end [RETURN=-3]', data.clientId, stepperIndex);
            callback({success: false});
          }
        });
      }
    } else {
      logger.log('INFO: updateQuotationClientChoice - end [RETURN=-4]', data.clientId, stepperIndex);
      callback({success: false});
    }
  });
};

// Prepare ROP object (contains new bundle & new application) for update later
var _prepareReplacementOfPolicies = function(bundle, newQuot) {
  logger.log('INFO: prepareReplacementOfPolicies - start', newQuot._id);
  return new Promise((resolve, reject)=>{
    let result = {
      bundle: {},
      application: {}
    };
    let { rop, rop_shield }  = newQuot.clientChoice.recommendation;

    //for normal product
    let newReplacePolicies = {
      replaceCi: 0,
      replaceLife: 0,
      replaceTpd: 0,
      replacePaAdb: 0,
      replaceTotalPrem: 0
    };
    if (rop && rop.choiceQ1 === 'Y') {
      newReplacePolicies = Object.assign(newReplacePolicies, {
        replaceCi: rop.replaceCi,
        replaceLife: rop.replaceLife,
        replaceTpd: rop.replaceTpd,
        replacePaAdb: rop.replacePaAdb,
        replaceTotalPrem: rop.replaceTotalPrem
      });
    }
    newReplacePolicies.isProslReplace = (newReplacePolicies.replaceLife > 0 ||
      newReplacePolicies.replaceTpd > 0 || newReplacePolicies.replaceCi > 0 ||
      newReplacePolicies.replacePaAdb > 0 || newReplacePolicies.replaceTotalPrem > 0) ? 'Y' : 'N';

    //for shield
    let newReplacePoliciesShield = {};
    let iCidRopAnswerMap = _.get(rop_shield, 'ropBlock.iCidRopAnswerMap', {})
    if (iCidRopAnswerMap) {
      for (let cid in iCidRopAnswerMap) {
        let iCidRopAnswerKey = iCidRopAnswerMap[cid];
        newReplacePoliciesShield[cid] = rop_shield.ropBlock[iCidRopAnswerKey];
      }
    }

    let appItem = bundle.applications.find(function (item) {
      return item.appStatus === 'APPLYING' && item.quotationDocId === newQuot._id;
    });

    if (appItem) {
      appDao.getApplication(appItem.applicationDocId, function(app) {
        logger.log('INFO: prepareReplacementOfPolicies - handle application', app._id);
        if (app && !app.error) {
          let {proposer, insured} = app.applicationForm.values;

          if (_.get(app, 'quotation.quotType') === 'SHIELD') {
            //for shield
            if (_.get(proposer, 'policies.ROP_01')) {
              let phRopAnswer = newReplacePoliciesShield[app.pCid];
              //set application
              proposer.policies.ROP_01 = phRopAnswer;
              if (phRopAnswer === 'N') {
                proposer.policies.ROP01_DATA = [];
                proposer.policies.ROP_DECLARATION_01 = '';
                proposer.policies.ROP_DECLARATION_02 = '';
              }

              //set bundle
              let phPolicies = _.get(bundle, `formValues.${app.pCid}.policies`);
              if (phPolicies.ROP_01) {
                phPolicies.ROP_01 = phRopAnswer;
                if (phRopAnswer === 'N') {
                  phPolicies.ROP01_DATA = [];
                  phPolicies.ROP_DECLARATION_01 = '';
                  phPolicies.ROP_DECLARATION_02 = '';
                }
                result.bundle[proposer.personalInfo.cid] = phPolicies;
              }
            }

            _.forEach(insured, (la, i) => {
              let iCid = _.get(app, `iCids[${i}]`, '');
              if (iCid && _.get(la, 'policies.ROP_01')) {
                let laRopAnswer = newReplacePoliciesShield[iCid];
                //set application
                la.policies.ROP_01 = laRopAnswer;
                if (laRopAnswer === 'N') {
                  la.policies.ROP01_DATA = [];
                  la.policies.ROP_DECLARATION_01 = '';
                  la.policies.ROP_DECLARATION_02 = '';
                }

                //set bundle
                let laPolicies = _.get(bundle, `formValues.${iCid}.policies`);
                if (laPolicies.ROP_01) {
                  laPolicies.ROP_01 = laRopAnswer;
                  if (laRopAnswer === 'N') {
                    laPolicies.ROP01_DATA = [];
                    laPolicies.ROP_DECLARATION_01 = '';
                    laPolicies.ROP_DECLARATION_02 = '';
                  }
                  result.bundle[iCid] = laPolicies;
                }
              }
            });
          } else {
            //for normal product
            if (insured.length === 0) {
              //set application
              proposer.policies = Object.assign(proposer.policies, newReplacePolicies);

              //set bundle
              let phPolicies = _.get(bundle, `formValues.${proposer.personalInfo.cid}.policies`);
              result.bundle[proposer.personalInfo.cid] = Object.assign(phPolicies, newReplacePolicies);

              logger.log('INFO: prepareReplacementOfPolicies - reset proposer', app._id);
            } else {
              for (let i = 0; i < insured.length; i ++) {
                //set application
                let la = insured[i];
                la.policies = Object.assign(la.policies, newReplacePolicies);

                //set bundle
                let laPolicies = _.get(bundle, `formValues.${la.personalInfo.cid}.policies`);
                result.bundle[la.personalInfo.cid] = Object.assign(laPolicies, newReplacePolicies);
              }
            }
          }


          result.application = app;
          logger.log('INFO: prepareReplacementOfPolicies - end [RETURN=1]', newQuot._id);
          resolve(result);
        } else {
          logger.log('INFO: prepareReplacementOfPolicies - end [RETURN=-1]', newQuot._id);
          reject(app);
        }
      });
    } else {
      logger.log('INFO: prepareReplacementOfPolicies - end [RETURN=2]', newQuot._id);
      resolve(result);
    }
  });
};
module.exports.prepareReplacementOfPolicies = _prepareReplacementOfPolicies;


var _getExistingPolicies = function(pCid, iCid) {
  logger.log('INFO: getExistingPolicies - start', pCid);
  return new Promise(function(resolve, reject) {
    nDao.getItem(pCid, nDao.ITEM_ID.FE).then((fe)=> {
      logger.log('INFO: getExistingPolicies - getFE', pCid);
      if (fe && !fe.error) {
        nDao.getExistingPoliciesFromFe(pCid, iCid, fe).then((ePolicies) =>{
          logger.log('INFO: getExistingPolicies - end [RETURN=1]', pCid);
          resolve(ePolicies);
        });
      } else {
        logger.log('INFO: getExistingPolicies - end [RETURN=-1]', pCid);
        reject(new Error('Fail to get FE for cid ' + pCid));
      }
    });
  });
};
module.exports.getExistingPolicies = _getExistingPolicies;


var genClientChoiceRecommendationsTemplateValues = function(bundle, quotList,
    quotSelectedMap, isReadOnly, template, values, cb) {
  const lang = 'en';

  let _getClientChoiceDefaultString = function(quotation) {
    const covCode       = _.get(quotation, 'baseProductCode');
    const payment       = _.get(quotation, 'policyOptions.paymentMethod', '');
    const plans         = _.get(quotation, 'plans', []);
    const isPhSameAsLa  = quotation.pCid === quotation.iCid;
    const basicBenefit  = _.get(quotation, 'policyOptions.basicBenefit', '');
    const planType      = _.get(quotation, 'policyOptions.planType', '');
    const deathBeneth   = _.get(quotation, 'policyOptions.deathBenefit', '');
    const payoutTerm    = _.get(quotation, 'policyOptions.payoutTerm', '');
    const payoutType    = _.get(quotation, 'policyOptions.payoutType', '');
    const proportiesObj = {covCode, payment, plans, isPhSameAsLa, basicBenefit, planType, deathBeneth, payoutTerm, payoutType};

    return new Promise(function(resolve, reject) {
      dao.getDocFromCacheFirst(fcRecommendationId, function(res){
        if (res && !res.error) {
          const resRiders = res.riders;
          let defaultRecommendBenefit = '';
          let defaultRecommendLimit = '';

          let basicPlanArray = _.filter(_.get(res, 'basicPlans'), (plan) => {
            return plan.covCode === covCode;
          });
          let foundBasicPlan;

          _.forEach(basicPlanArray, (resBasicPlan) => {
            if (resBasicPlan.conditions) {
              let copyProportiesObj = _.assign({}, proportiesObj, resBasicPlan.conditions);
              if (_.isEqual(proportiesObj, copyProportiesObj)) {
                foundBasicPlan = resBasicPlan;
                return;
              }
            } else {
              foundBasicPlan = resBasicPlan;
              return;
            }
          });

          defaultRecommendBenefit += _.get(foundBasicPlan, 'defaultText.benefits.' + lang, '');
          defaultRecommendLimit += _.get(foundBasicPlan, 'defaultText.limitations.' + lang, '');

          // Check Nearest Age
          if (_.get(foundBasicPlan, 'insuredAgeCheck.isNeedCheck')) {
            if (_.get(foundBasicPlan, 'insuredAgeCheck.byNearestAge')) {
              if (DateUtils.getNearestAge(moment().toDate(), moment(quotation.iDob).toDate()) === 0) {
                defaultRecommendLimit += _.get(foundBasicPlan, 'defaultText.additionalLimitations.' + lang, '');
              }
            }
          }

          // get riders
          _.forEach(plans, (plan, index) => {
            if (index === 0) {
              return;
            }

            const riderCovCodeValue = _.get(resRiders, plan.covCode);
            // const riderTextBlock = _.get(resRiders, plan.covCode);
            let riderTextBlock;

            if (!riderCovCodeValue) {
              return;
            }

            // For the case a rider can be used by several products
            if (riderCovCodeValue.baseProductCode) {
              if (_.get(riderCovCodeValue, 'baseProductCode.' + covCode)) {
                riderTextBlock = _.get(riderCovCodeValue, 'baseProductCode.' + covCode);
              } else {
                riderTextBlock = _.get(riderCovCodeValue, 'baseProductCode.default');
              }
            } else {
              riderTextBlock = riderCovCodeValue;
            }

            if (riderTextBlock.conditionsCheck) {
              _.forEach(riderTextBlock.conditionsCheck, (subTextBlock) => {
                if (_.get(subTextBlock, 'isPhSameAsLaCheck.isNeedCheck')) {
                  if (_.get(subTextBlock, 'isPhSameAsLaCheck.isPhSameAsLa') === isPhSameAsLa) {
                    defaultRecommendBenefit += _.get(subTextBlock, 'benefits.' + lang);
                    defaultRecommendLimit += _.get(subTextBlock, 'limitations.' + lang);
                  }
                }

                // Check Nearest Age
                if (_.get(subTextBlock, 'insuredAgeCheck.isNeedCheck')) {
                  if (_.get(subTextBlock, 'insuredAgeCheck.byNearestAge')) {
                    if (DateUtils.getNearestAge(moment().toDate(), moment(quotation.iDob).toDate()) === 0) {
                      defaultRecommendLimit += _.get(subTextBlock, 'additionalLimitations.' + lang, '');
                    }
                  } else if (_.get(subTextBlock, 'insuredAgeCheck.ageMethod') === 'attainedAge') {
                    if (_.get(subTextBlock, 'insuredAgeCheck.condition') === 'showIfSmaller') {
                      if (DateUtils.getAttainedAge(new Date(), new Date(_.get(quotation, 'iDob'))).year < _.get(subTextBlock, 'insuredAgeCheck.age')) {
                        defaultRecommendBenefit += _.get(subTextBlock, 'benefits.' + lang);
                        defaultRecommendLimit += _.get(subTextBlock, 'limitations.' + lang);
                      }
                    } else if (_.get(subTextBlock, 'insuredAgeCheck.condition') === 'showIfNotSmaller') {
                      if (DateUtils.getAttainedAge(new Date(), new Date(_.get(quotation, 'iDob'))).year >= _.get(subTextBlock, 'insuredAgeCheck.age')) {
                        defaultRecommendBenefit += _.get(subTextBlock, 'benefits.' + lang);
                        defaultRecommendLimit += _.get(subTextBlock, 'limitations.' + lang);
                      }
                    }
                  }
                }
              });
            } else {
              defaultRecommendBenefit += _.get(riderTextBlock, 'benefits.' + lang, '');
              defaultRecommendLimit += _.get(riderTextBlock, 'limitations.' + lang, '');
            }
          });

          logger.log('INFO: genClientChoiceRecommendationsTemplateValues - getClientChoiceDefaultString - end', covCode, payment, isPhSameAsLa);
          resolve({
            // hasClientChoice:hasClientChoice,
            defaultRecommendBenefit:_.trim(defaultRecommendBenefit),
            defaultRecommendLimit:_.trim(defaultRecommendLimit)
          });
        } else {
          logger.error('ERROR: genClientChoiceRecommendationsTemplateValues - getClientChoiceDefaultString - get fcRecommdation', res);
          reject(res);
        }
      });
    });
  };

  /** DO NOT REMOVE recommendationValues */
  let setMenuItemValues_nonShield = function(isShield, menuValues, quot, choiceUnchanged, recommendationValues) {
    menuValues.quotId = quot.id;
    menuValues.paymentMode = quot.paymentMode;
    // menuValues.clientChoiceSelect = (currClientChoiceSelected ? 'Y' : 'N');
    // Carlton TODO
    if (isShield) {
      /*
      let arrNames = [];
      _.forEach(_.get(quot, 'insureds', {}), (insured) => {
        arrNames.push(insured.iFirstName + ' ' + insured.iLastName);
        _.forEach(insured.plans, (plan, index) => {
          if (index > 0) {
            menuValues.ridersName.push(plan.covName);
          }
        });
      });
      menuValues.proposerAndLifeAssuredName = _.join(arrNames, ' / ');
      menuValues.basicPlanName = 'AXA Shield Plan';
      */

      let arrGroupList = [];

      _.forEach(_.get(quot, 'insureds', {}), (insured) => {
        let planCodeList = [];
        let covNameList = [];
        const insuredName = insured.iFirstName + ' ' + insured.iLastName;

        _.forEach(insured.plans, (plan) => {
          planCodeList.push(plan.planCode);
          covNameList.push(_.get(plan, 'covName.' + lang));
        });

        let foundGroup = _.find(arrGroupList, (group) => {
          return commonAppHandler.isTwoArraysEqual(group.planCodeList, planCodeList);
        });

        if (foundGroup) {
          foundGroup.insuredNameList.push(insuredName);
        } else {
          arrGroupList.push({
            insuredNameList: [insuredName],
            planCodeList: planCodeList,
            planNameList: covNameList
          });
        }
      });

      menuValues.shieldInsuredAndPlanGroupList = arrGroupList;

      /** DO NOT REMOVE recommendationValues */
      recommendationValues.extra.basicPlanName = "AXA Shield (RP)";
    } else {
      let basicPlan = _.find(quot.plans, (plan) => {
        return plan.covCode === quot.baseProductCode;
      });
      menuValues.proposerAndLifeAssuredName = quot.pFirstName + ' ' + quot.pLastName +
          (quot.pCid !== quot.iCid ? ' / ' + quot.iFirstName + ' ' + quot.iLastName : '');
      menuValues.basicPlanName = _.get(basicPlan, 'covName.' + lang);

      let plans = _.get(quot, 'plans', []);
      for (let i = 0; i < plans.length; i ++) {
        let plan = quot.plans[i];
        if (plan.covCode !== quot.baseProductCode) {
          menuValues.ridersName.push(plan.covName);
        }
      }

      /** DO NOT REMOVE recommendationValues */
      recommendationValues.extra.proposerAndLifeAssuredName = quot.pFirstName + ' ' + quot.pLastName + (quot.pCid !== quot.iCid ? ' / ' + quot.iFirstName + ' ' + quot.iLastName : '');
      recommendationValues.extra.basicPlanName = _.get(basicPlan, 'covName.' + lang);
      recommendationValues.extra.ridersName = menuValues.ridersName;
    }

    menuValues.isShield = isShield;
    menuValues.policyTable = JSON.parse(JSON.stringify(policyTableDefaultValues));

    /** DO NOT REMOVE recommendationValues */
    recommendationValues.extra.paymentMode = quot.paymentMode;
    recommendationValues.extra.isShield = isShield;

    //Load saved clientChoice if exist
    if (quot.hasOwnProperty('clientChoice')) {
      if (quot.clientChoice.hasOwnProperty('recommendation')) {
        let ccRecommendation = quot.clientChoice.recommendation;
        menuValues.recommendReason  = ccRecommendation.reason;
        menuValues.recommendBenefit = ccRecommendation.benefit;
        menuValues.recommendLimit   = ccRecommendation.limitation;

        /** DO NOT REMOVE recommendationValues */
        recommendationValues.reason = ccRecommendation.reason;
        recommendationValues.benefit = ccRecommendation.benefit;
        recommendationValues.limitation = ccRecommendation.limitation;

        if (choiceUnchanged && quot.clientChoice.recommendation.hasOwnProperty('rop')) {
          let ccRop = quot.clientChoice.recommendation.rop;
          menuValues.ropQ1        = ccRop.choiceQ1;
          menuValues.ropQ1sub1    = ccRop.choiceQ1Sub1;
          menuValues.ropQ1sub2    = ccRop.choiceQ1Sub2;
          menuValues.ropQ1sub3    = ccRop.choiceQ1Sub3;
          menuValues.policyTable  = Object.assign({},
            JSON.parse(JSON.stringify(policyTableDefaultValues)), {
            replaceLife:      ccRop.replaceLife ? ccRop.replaceLife : 0,
            replaceTpd:       ccRop.replaceTpd ? ccRop.replaceTpd : 0,
            replaceCi:        ccRop.replaceCi ? ccRop.replaceCi : 0,
            replacePaAdb:     ccRop.replacePaAdb ? ccRop.replacePaAdb : 0,
            replaceTotalPrem: ccRop.replaceTotalPrem ? ccRop.replaceTotalPrem : 0
          });

          /** DO NOT REMOVE recommendationValues */
          recommendationValues.rop.choiceQ1 = ccRop.choiceQ1;
          recommendationValues.rop.choiceQ1Sub1 = ccRop.choiceQ1Sub1;
          recommendationValues.rop.choiceQ1Sub2 = ccRop.choiceQ1Sub2;
          recommendationValues.rop.choiceQ1Sub3 = ccRop.choiceQ1Sub3;
          recommendationValues.rop.replaceCi = ccRop.replaceCi;
          recommendationValues.rop.replaceLife = ccRop.replaceLife;
          recommendationValues.rop.replacePaAdb = ccRop.replacePaAdb;
          recommendationValues.rop.replaceTotalPrem = ccRop.replaceTotalPrem;
          recommendationValues.rop.replaceTpd = ccRop.replaceTpd;
        }
      }
    }
  };

  var clientChoiceFlag = {
    clientChoiceChanged:        false,
    isRecommendationCompleted:  bundle.clientChoice.isRecommendationCompleted,
    isBudgetCompleted:          bundle.clientChoice.isBudgetCompleted,
    isAcceptanceCompleted:      bundle.clientChoice.isAcceptanceCompleted,
    isAppListChanged:           bundle.clientChoice.isAppListChanged,
    clientChoiceStep:           (bundle.clientChoice.clientChoiceStep ? bundle.clientChoice.clientChoiceStep : 0)
  };

  var recommendMenu = [];
  var recommendMenuCC = {
    'detailSeq': 1,
    'type': 'menusection',
    'title': 'Client Choice(s)',
    'items': []
  };
  var recommendMenuNS = {
    'detailSeq': 2,
    'type': 'menusection',
    'title': 'Not Selected Plan(s)',
    'items': []
  };

  var menuDefaultValues = {
    // applicationId: "",
    quotId: '',
    clientChoiceSelect: '',
    proposerAndLifeAssuredName: '',
    basicPlanName: '',
    ridersName: [],
    recommendBenefit: '',
    recommendLimit: '',
    recommendReason: '',
    ropQ1: '',
    ropQ1sub1: '',
    ropQ1sub2: '',
    ropQ1sub3: '',
    policyTable: {},
    recommendBenefitDefault: '',
    recommendLimitDefault: '',
    paymentMode: ''
  };

  var policyTableDefaultValues = {
    existLife: 0,
    existTpd: 0,
    existCi: 0,
    existPaAdb: 0,
    existTotalPrem: 0,
    replaceLife: 0,
    replaceTpd: 0,
    replaceCi: 0,
    replacePaAdb: 0,
    replaceTotalPrem: 0
  };

  let getRevisedPlanName = (quot, isShield) => {
    const rpPaymentMethod = ['A', 'S', 'Q', 'M'];
    const spPaymentMethod = ['L'];
    const lang = 'en';
    let { paymentMode } = quot;
    let nameLabel = 'RP';
    let basicPlanName = {en: ''};

    if (isShield) {
      basicPlanName = {
        en: 'AXA Shield Plan'
      };
    } else {
      let basicPlan = _.find(quot.plans, (plan) => {
        return plan.covCode === quot.baseProductCode;
      });
      basicPlanName = basicPlan.covName;
    }

    if (isShield || rpPaymentMethod.indexOf(paymentMode) > -1) {
      nameLabel = 'RP';
    } else if (spPaymentMethod.indexOf(paymentMode) > -1) {
      nameLabel = _.get(quot, `policyOptionsDesc.paymentMethod.${lang}`, 'SP');
    }

    _.forEach(basicPlanName, (value, key) => {
      basicPlanName[key] += ' (' + nameLabel + ')';
    });

    return basicPlanName;
  };

  let menuIdList = [];
  let menuIdQuotMap = {};
  logger.log('INFO: genClientChoiceRecommendationsTemplateValues - getDoc', ccRecommedationMenuItemId, bundle.id);

  let selectedCount = -1;
  let notSelectedCount = -1;
  _.forEach(quotList, (quot, i) => {
    let clientChoiceSelected = quotSelectedMap[quot.id];
    let menuId = (clientChoiceSelected ? 'CC' : 'NS') + '_' + (clientChoiceSelected ? ++selectedCount : ++notSelectedCount);
    menuIdList.push(menuId);
    menuIdQuotMap[quot.id] = menuId;
  });

  let promises = quotList.map((quot, i) => {
    const isShield = _.get(quot, 'quotType') === 'SHIELD';
    const templateDocId = isShield ? ccRecommedationMenuItemId_Shield : ccRecommedationMenuItemId;
    const menuId = menuIdQuotMap[quot.id];

    return new Promise((resolve, reject) => {
      if (quot.id) {
        logger.log('INFO: genClientChoiceRecommendationsTemplateValues - prepare Client Choice template for', bundle.id, quot.id);
        dao.getDocFromCacheFirst(templateDocId, (res) => {
          if (res && !res.error) {
            var currClientChoiceSelected = quotSelectedMap[quot.id];
            var prevClientChoiceSelectedStr = quot.hasOwnProperty('clientChoice') ? (quot.clientChoice.isClientChoiceSelected ? 'Y' : 'N') : '';
            var choiceUnchanged = (currClientChoiceSelected && prevClientChoiceSelectedStr === 'Y') || (!currClientChoiceSelected && prevClientChoiceSelectedStr !== 'Y');
            let iCidRopAnswerMap = {};

            clientChoiceFlag.clientChoiceChanged = clientChoiceFlag.clientChoiceChanged || !choiceUnchanged || prevClientChoiceSelectedStr === '';

            //Prepare each menu items
            var menuTemplateCopy = _.cloneDeep(res);
            menuTemplateCopy.id = menuId;
            menuTemplateCopy.detailSeq = i;

            if (currClientChoiceSelected) {
              recommendMenuCC.items.push(menuTemplateCopy);
            } else {
              recommendMenuNS.items.push(menuTemplateCopy);
            }
            // left side menu bar each item's title
            menuTemplateCopy.title = getRevisedPlanName(quot, isShield);

            if (isShield) {
              shieldApplicationFuncs.setMenuItemTemplate_shield(menuTemplateCopy, iCidRopAnswerMap, _.get(quot, 'insureds'));
            }

            if (isReadOnly) {
              _frozenTemplate(menuTemplateCopy);
            }

            // TODO deprecate menuValues, and use recommendationValues
            //Prepare default values for each menu
            logger.log('INFO: genClientChoiceRecommendationsTemplateValues - prepare Client Choice values for', bundle.id, quot.id);
            let menuValues = _.cloneDeep(menuDefaultValues);
            menuValues.clientChoiceSelect = (currClientChoiceSelected ? 'Y' : 'N');

            /** DO NOT REMOVE recommendationValues START */
            let recommendationValues = _.cloneDeep(ccRecommendDefaultData.recommendation);
            recommendationValues.extra = {
              lastUpdateDate: quot.lastUpdateDate
            };

            setMenuItemValues_nonShield(isShield, menuValues, quot, choiceUnchanged, recommendationValues);
            if (isShield) {
              shieldApplicationFuncs.setMenuItemValues_shield(menuValues, quot, recommendationValues);
              if (iCidRopAnswerMap) {
                _.set(menuValues, 'ropBlock.shieldRopTable.iCidRopAnswerMap', iCidRopAnswerMap);
                if (session.platform) {
                  _.set(recommendationValues, 'rop_shield.ropBlock.iCidRopAnswerMap', iCidRopAnswerMap);
                }
              }
            }

            resolve({ menuValues, recommendationValues });
            /** DO NOT REMOVE recommendationValues END */
          } else {
            logger.error('ERROR: genClientChoiceRecommendationsTemplateValues - end', bundle.id, _.get(res, 'error'));
            reject(new Error('Fail to get template ' + templateDocId));
          }
        });
      } else {
        logger.error('ERROR: genClientChoiceRecommendationsTemplateValues - unexpected error', quot.id);
        reject(new Error('wrong quotation data' + quot.id));
      }
    }).then(({ menuValues, recommendationValues }) => {
      /** DO NOT REMOVE recommendationValues */
      return new Promise((resolve) => {
        logger.log('INFO: genClientChoiceRecommendationsTemplateValues - prepare Client Choice Default String for', bundle.id, quot.id);
        if (isShield) {
          shieldApplicationFuncs.getClientChoiceDefaultString_shield(_.get(quot, 'insureds')).then(resolve);
        } else {
          _getClientChoiceDefaultString(quot).then(resolve);
        }
      }).then((sResult) => {
        menuValues.recommendBenefitDefault = sResult.defaultRecommendBenefit;
        menuValues.recommendLimitDefault = sResult.defaultRecommendLimit;
        /** DO NOT REMOVE recommendationValues START */
        recommendationValues.extra.recommendBenefitDefault = sResult.defaultRecommendBenefit;
        recommendationValues.extra.recommendLimitDefault = sResult.defaultRecommendLimit;

        // if (!sResult.hasClientChoice) {
        // quot.hasOwnProperty('clientChoice') means this quotation has clientChoice
        if (!quot.hasOwnProperty('clientChoice')) {
          menuValues.recommendBenefit = sResult.defaultRecommendBenefit;
          menuValues.recommendLimit = sResult.defaultRecommendLimit;
          recommendationValues.benefit = sResult.defaultRecommendBenefit;
          recommendationValues.limitation = sResult.defaultRecommendLimit;
        }
        /** DO NOT REMOVE recommendationValues END */

        logger.log('INFO: genClientChoiceRecommendationsTemplateValues - prepare Client Choice Existing Policies for', bundle.id, quot.id);

        return _getExistingPolicies(quot.pCid, quot.iCid);
      }).then((exResult) => {
        menuValues.policyTable = Object.assign(
          {},
          _.cloneDeep(menuValues.policyTable),
          exResult
        );

        /** DO NOT REMOVE recommendationValues START */
        recommendationValues.rop.existCi = _.get(menuValues.policyTable, "existCi");
        recommendationValues.rop.existLife = _.get(menuValues.policyTable, "existLife");
        recommendationValues.rop.existPaAdb = _.get(menuValues.policyTable, "existPaAdb");
        recommendationValues.rop.existTotalPrem = _.get(menuValues.policyTable, "existTotalPrem");
        recommendationValues.rop.existTpd = _.get(menuValues.policyTable, "existTpd");

        let mValues = {};
        // TODO deprecated menuId and use quot.id
        mValues[menuId] = menuValues;
        mValues[quot.id] = recommendationValues;
        /** DO NOT REMOVE recommendationValues END */
        return mValues;
      });
    });
  });

  Promise.all(promises).then((result) => {
    //reset stepper status if any change on client choice is detected
    if (clientChoiceFlag.clientChoiceChanged) {
      clientChoiceFlag.isRecommendationCompleted = false;
      clientChoiceFlag.isBudgetCompleted = false;
      clientChoiceFlag.isAcceptanceCompleted = false;
      clientChoiceFlag.clientChoiceStep = 0;
    }
    if (clientChoiceFlag.isAppListChanged) {
      clientChoiceFlag.isBudgetCompleted = false;
      clientChoiceFlag.isAcceptanceCompleted = false;
      if (clientChoiceFlag.clientChoiceStep > 0) {
        clientChoiceFlag.clientChoiceStep = 1;
      }
    }

    //prepare template
    recommendMenu.push(recommendMenuCC);
    if (recommendMenuNS.items.length > 0) {
      recommendMenu.push(recommendMenuNS);
    }
    template.items[0].items[0].items = recommendMenu;

    //prepare values
    for (let i = 0; i < result.length; i ++) {
      let mValues = result[i];
      for (let key in mValues) {
        values[key] = mValues[key];
      }
    }
    values.menuIdList = menuIdList;

    logger.log('INFO: genClientChoiceRecommendationsTemplateValues - end', bundle.id);
    cb(Object.assign({template:template, values:values, clientChoiceFlag:clientChoiceFlag}));
  }).catch((error) => {
    logger.error('ERROR: genClientChoiceRecommendationsTemplateValues', bundle.id, error);
  });

};

var getClientChoiceBudgetPremiumCompare = function(compare) {
  if (compare > 9.99) {
    return BUDGET_UNDERUTILIZED;
  }
  else if (compare < -9.99) {
    return BUDGET_EXCEEDED;
  }
  return BUDGET_ALIGNED;
};

const _getRpPremium = (quotation, freqMappingByLetter, paymentModeMag ) => {
  let calResult = 0;
  if (_.get(quotation, 'baseProductCode') === 'AWT') {
    return _.get(quotation, 'plans[0].yearPrem', 0) + (_.get(quotation, 'policyOptions.rspAmount', 0) * 12);
  } else {

    /***
     * original Code Carlton pleae help to review
     * let totalPremiumMag = quot.paymentMode === 'A' ? 1 : quot.paymentMode === 'S' ? 2 : quot.paymentMode === 'Q' ? 4 : quot.paymentMode === 'M' ? 12 : 1;
     * rpTotalPremium += quot.premium * totalPremiumMag + _.get(quot, 'totYearCashPortion', 0);
     *
     */

    const rspAmount = _.get(quotation, 'policyOptions.rspAmount') || _.get(quotation, 'policyOptions.rspAmt', 0);
    const rspPayFreq = _.get(quotation, 'policyOptions.rspPayFreq', 'A');
    const rspMag = freqMappingByLetter[rspPayFreq];

    // Flexi
    calResult += quotation.premium * paymentModeMag + rspAmount * rspMag;
    // Shield
    calResult += _.get(quotation, 'totYearCashPortion', 0);

    return calResult;
  }
};

var genClientChoiceBudgetTemplateValues = function(bundle, quotList, quotSelectedMap, isReadOnly, template, values, clientChoiceFlag, cb) {
  logger.log('INFO: genClientChoiceBudgetTemplateValues - start', bundle.id);

  let budgetDefaultValues = {
    spBudget:0,
    spTotalPremium:0,
    spCompare:0,
    spCompareResult:'',
    rpBudget:0,
    rpTotalPremium: 0,
    rpCompare:0,
    rpCompareResult:'',
    cpfOaBudget:0,
    cpfOaTotalPremium:0,
    cpfOaCompare:0,
    cpfOaCompareResult:'',
    cpfSaBudget:0,
    cpfSaTotalPremium:0,
    cpfSaCompare:0,
    cpfSaCompareResult:'',
    srsBudget:0,
    srsTotalPremium:0,
    srsCompare:0,
    srsCompareResult:'',
    cpfMsBudget:0,
    cpfMsTotalPremium:0,
    cpfMsCompare:0,
    cpfMsCompareResult:'',
    budgetMoreChoice:'',
    budgetMoreReason:'',
    budgetLessChoice:'',
    budgetLessReason:'',
    multiCcyNote: []
  };

  let setBudgetTemplate = function(template, ccBudgetTemplate, budget){
    var budgetTemplateCopy = Object.assign({}, ccBudgetTemplate);
    if (isReadOnly) {
      _frozenTemplate(budgetTemplateCopy);
    }

    let {
      spCompare, rpCompare, cpfOaCompare, cpfSaCompare, srsCompare, cpfMsCompare,
      spCompareResult, rpCompareResult, cpfOaCompareResult, cpfSaCompareResult,
      srsCompareResult, cpfMsCompareResult
    } = budget;

    let compareIndex = {
      spCompare: {},
      rpCompare: {},
      cpfOaCompare: {},
      cpfSaCompare: {},
      srsCompare: {},
      cpfMsCompare: {}
    };

    //find index of compare label
    _.forEach(compareIndex, (value, key) => {
      _.forEach(budgetTemplateCopy.items, (pItem, index) => {
        if (_.toUpper(pItem.type) === 'PAPER') {
          let column = _.findIndex(pItem.items, (item) => { return item.id === key; });
          if (column >= 0) {
            compareIndex[key].row = index;
            compareIndex[key].column = column;
            return false;
          }
        }
      });
    });

    budgetTemplateCopy.items[compareIndex.spCompare.row].items[compareIndex.spCompare.column].title = spCompareResult ? spCompareResult : getClientChoiceBudgetPremiumCompare(spCompare);
    budgetTemplateCopy.items[compareIndex.rpCompare.row].items[compareIndex.rpCompare.column].title = rpCompareResult ? rpCompareResult : getClientChoiceBudgetPremiumCompare(rpCompare);
    budgetTemplateCopy.items[compareIndex.cpfOaCompare.row].items[compareIndex.cpfOaCompare.column].title = cpfOaCompareResult ? cpfOaCompareResult : getClientChoiceBudgetPremiumCompare(cpfOaCompare);
    budgetTemplateCopy.items[compareIndex.cpfSaCompare.row].items[compareIndex.cpfSaCompare.column].title = cpfSaCompareResult ? cpfSaCompareResult : getClientChoiceBudgetPremiumCompare(cpfSaCompare);
    budgetTemplateCopy.items[compareIndex.srsCompare.row].items[compareIndex.srsCompare.column].title = srsCompareResult ? srsCompareResult : getClientChoiceBudgetPremiumCompare(srsCompare);
    budgetTemplateCopy.items[compareIndex.cpfMsCompare.row].items[compareIndex.cpfMsCompare.column].title = cpfMsCompareResult ? cpfMsCompareResult : getClientChoiceBudgetPremiumCompare(cpfMsCompare);

    template.items[1].items.push(budgetTemplateCopy);
  };

  let budgetValidationReset = function(bundle, newBudget) {
    logger.log('INFO: genClientChoiceBudgetTemplateValues - budgetValidationReset');
    let {budget} = bundle.clientChoice;
    if (budget) {
      if (budget.spCompareResult  !== newBudget.spCompareResult ||
        budget.rpCompareResult    !== newBudget.rpCompareResult ||
        budget.cpfOaCompareResult !== newBudget.cpfOaCompareResult ||
        budget.cpfSaCompareResult !== newBudget.cpfSaCompareResult ||
        budget.srsCompareResult   !== newBudget.srsCompareResult ||
        budget.cpfMsCompareResult !== newBudget.cpfMsCompareResult) {

        let resetBudgetMore = (budget.spCompareResult === BUDGET_EXCEEDED && newBudget.spCompareResult !== BUDGET_EXCEEDED) ||
          (budget.rpCompareResult     === BUDGET_EXCEEDED       && newBudget.rpCompareResult      !== BUDGET_EXCEEDED) ||
          (budget.cpfOaCompareResult  === BUDGET_EXCEEDED       && newBudget.cpfOaCompareResult   !== BUDGET_EXCEEDED) ||
          (budget.cpfSaCompareResult  === BUDGET_EXCEEDED       && newBudget.cpfSaCompareResult   !== BUDGET_EXCEEDED) ||
          (budget.srsCompareResult    === BUDGET_EXCEEDED       && newBudget.srsCompareResult     !== BUDGET_EXCEEDED) ||
          (budget.cpfMsCompareResult  === BUDGET_EXCEEDED       && newBudget.cpfMsCompareResult   !== BUDGET_EXCEEDED) ||
          (budget.spCompareResult     !== BUDGET_EXCEEDED       && newBudget.spCompareResult      === BUDGET_EXCEEDED) ||
          (budget.rpCompareResult     !== BUDGET_EXCEEDED       && newBudget.rpCompareResult      === BUDGET_EXCEEDED) ||
          (budget.cpfOaCompareResult  !== BUDGET_EXCEEDED       && newBudget.cpfOaCompareResult   === BUDGET_EXCEEDED) ||
          (budget.cpfSaCompareResult  !== BUDGET_EXCEEDED       && newBudget.cpfSaCompareResult   === BUDGET_EXCEEDED) ||
          (budget.srsCompareResult    !== BUDGET_EXCEEDED       && newBudget.srsCompareResult     === BUDGET_EXCEEDED) ||
          (budget.cpfMsCompareResult  !== BUDGET_EXCEEDED       && newBudget.cpfMsCompareResult   === BUDGET_EXCEEDED);

        let resetBudgetLess = (budget.spCompareResult === BUDGET_UNDERUTILIZED && newBudget.spCompareResult !== BUDGET_UNDERUTILIZED) ||
          (budget.rpCompareResult     === BUDGET_UNDERUTILIZED  && newBudget.rpCompareResult      !== BUDGET_UNDERUTILIZED) ||
          (budget.cpfOaCompareResult  === BUDGET_UNDERUTILIZED  && newBudget.cpfOaCompareResult   !== BUDGET_UNDERUTILIZED) ||
          (budget.cpfSaCompareResult  === BUDGET_UNDERUTILIZED  && newBudget.cpfSaCompareResult   !== BUDGET_UNDERUTILIZED) ||
          (budget.srsCompareResult    === BUDGET_UNDERUTILIZED  && newBudget.srsCompareResult     !== BUDGET_UNDERUTILIZED) ||
          (budget.cpfMsCompareResult  === BUDGET_UNDERUTILIZED  && newBudget.cpfMsCompareResult   !== BUDGET_UNDERUTILIZED) ||
          (budget.spCompareResult     !== BUDGET_UNDERUTILIZED  && newBudget.spCompareResult      === BUDGET_UNDERUTILIZED) ||
          (budget.rpCompareResult     !== BUDGET_UNDERUTILIZED  && newBudget.rpCompareResult      === BUDGET_UNDERUTILIZED) ||
          (budget.cpfOaCompareResult  !== BUDGET_UNDERUTILIZED  && newBudget.cpfOaCompareResult   === BUDGET_UNDERUTILIZED) ||
          (budget.cpfSaCompareResult  !== BUDGET_UNDERUTILIZED  && newBudget.cpfSaCompareResult   === BUDGET_UNDERUTILIZED) ||
          (budget.srsCompareResult    !== BUDGET_UNDERUTILIZED  && newBudget.srsCompareResult     === BUDGET_UNDERUTILIZED) ||
          (budget.cpfMsCompareResult  !== BUDGET_UNDERUTILIZED  && newBudget.cpfMsCompareResult   === BUDGET_UNDERUTILIZED);

        if (!resetBudgetMore) {
          newBudget.budgetMoreChoice = budget.budgetMoreChoice;
          newBudget.budgetMoreReason = budget.budgetMoreReason;
          logger.log('INFO: genClientChoiceBudgetTemplateValues - budgetValidationReset - load budget More reason', bundle.id);
        }

        if (!resetBudgetLess) {
          newBudget.budgetLessChoice = budget.budgetLessChoice;
          newBudget.budgetLessReason = budget.budgetLessReason;
          logger.log('INFO: genClientChoiceBudgetTemplateValues - budgetValidationReset - load budget Less reason', bundle.id);
        }
      } else {
        newBudget.budgetMoreChoice = budget.budgetMoreChoice;
        newBudget.budgetMoreReason = budget.budgetMoreReason;
        newBudget.budgetLessChoice = budget.budgetLessChoice;
        newBudget.budgetLessReason = budget.budgetLessReason;
        logger.log('INFO: genClientChoiceBudgetTemplateValues - budgetValidationReset - load all budget reason', bundle.id);
      }
    }
  };

  let getExchangeRateList = function() {
    return new Promise((resolve) => {
      let isMultiCcy = _.find(quotList, (quot) => {
        return quot.ccy && quot.ccy !== 'SGD';
      });

      if (isMultiCcy) {
        dao.getDoc(ccyTmplId, function(ccyTmpl) {
          const symbolList = ['AUD', 'GBP', 'USD', 'EUR'];
          const optionList = _.get(ccyTmpl, 'currencies.0.options', []);
          var rateList = [];
          _.forEach(symbolList, (symbol) => {
            let found = _.find(optionList, (option) => {
              return option.value === symbol;
            });
            rateList.push(found.exRate);
          });
          resolve(rateList);
        });
      } else {
        resolve();
      }
    }).catch((error) => {
      logger.error(error);
    });
  };

  dao.getDoc(ccBudgetId, function(res) {
    if (res && !res.error) {
      let {isAppListChanged, clientChoiceChanged} = clientChoiceFlag;
      if (isAppListChanged || clientChoiceChanged) {
        logger.log('INFO: genClientChoiceBudgetTemplateValues - recalulate premium budget compare', isAppListChanged, clientChoiceChanged);

        var spTotalPremium = 0;
        var rpTotalPremium = 0;
        var cpfOaTotalPremium = 0;
        var cpfSaTotalPremium = 0;
        var srsTotalPremium = 0;
        var cpfMsTotalPremium = 0;

        quotList.forEach(function(quot, index) {
          /**
           * Logics: 1. Inspire Duo is A Single Premium Product
           * Payment Method is cash, then goes to Budget Page - Single Premium;
           * If Payment Method is cpfissa, then goes to Budget Page - CPFISSA' etc;
           *
           * 2. Flexi Saver is A Regular Premium Product. It is cash method only.
           * So basicPlan + riders 's premium goes to Budget Page - Regular Premium
           * Top up premium goes to Budget Page - Single Premium;
           */
          const freqMappingByLetter = {A: 1,S: 2,Q: 4,M: 12};
          const paymentModeMag = freqMappingByLetter[_.get(quot, 'paymentMode')] || 0;
          const quotCcy = (_.get(quot, 'quotType') === 'SHIELD') ? 'SGD' : _.get(quot, 'ccy', 'SGD');
          const compCode = _.get(quot, 'compCode');
          const decimal = 2;

          // Term
          if (_.get(quot, 'baseProductCode') === 'TPX' || _.get(quot, 'baseProductCode') === 'TPPX') {
            if (_.includes(['A', 'S', 'Q', 'M'], _.get(quot, 'paymentMode'))) {
              rpTotalPremium += commonUtils.convertForeignCcytoSGD(_getRpPremium(quot, freqMappingByLetter, paymentModeMag), quotCcy, compCode, decimal);
            } else if (_.get(quot, 'paymentMode') === 'L') {
              spTotalPremium += commonUtils.convertForeignCcytoSGD(quot.premium, quotCcy, compCode, decimal);
            }
          } else {
            if (quotSelectedMap[quot.id] && quot.budgetRules.indexOf('SP') >= 0 && _.get(quot, 'paymentMode') === 'L') {
              const rspAmount = _.get(quot, 'policyOptions.rspAmount') || _.get(quot, 'policyOptions.rspAmt', 0);
              const rspPayFreq = _.get(quot, 'policyOptions.rspPayFreq', '');
              let rspAmountMag;
              if (rspPayFreq === 'annual' || rspPayFreq === 'A') {
                rspAmountMag = 1;
              } else if (rspPayFreq === 'semiAnnual' || rspPayFreq === 'S') {
                rspAmountMag = 2;
              } else if (rspPayFreq === 'quarterly' || rspPayFreq === 'Q') {
                rspAmountMag = 4;
              } else if (rspPayFreq === 'monthly' || rspPayFreq === 'M') {
                rspAmountMag = 12;
              } else {
                rspAmountMag = 1;
              }

              if (_.get(quot, 'policyOptions.paymentMethod') === 'cash') {
                spTotalPremium += commonUtils.convertForeignCcytoSGD((quot.premium + rspAmount * rspAmountMag), quotCcy, compCode, decimal);
              } else if (_.get(quot, 'policyOptions.paymentMethod') === 'cpfisoa') {
                cpfOaTotalPremium += commonUtils.convertForeignCcytoSGD((quot.premium + rspAmount * rspAmountMag), quotCcy, compCode, decimal);
              } else if (_.get(quot, 'policyOptions.paymentMethod') === 'cpfissa') {
                cpfSaTotalPremium += commonUtils.convertForeignCcytoSGD((quot.premium + rspAmount * rspAmountMag), quotCcy, compCode, decimal);
              } else if (_.get(quot, 'policyOptions.paymentMethod') === 'srs') {
                srsTotalPremium += commonUtils.convertForeignCcytoSGD((quot.premium + rspAmount * rspAmountMag), quotCcy, compCode, decimal);
              }
            } else if (quotSelectedMap[quot.id] && quot.budgetRules.indexOf('RP') >= 0) {
              rpTotalPremium += commonUtils.convertForeignCcytoSGD(_getRpPremium(quot, freqMappingByLetter, paymentModeMag), quotCcy, compCode, decimal);
            }
          }

          /**
           * Flexi and AWT
           * Top up amount is equal to single premium product
           */
          if (_.get(quot, 'policyOptions.topUpAmt', 0) && quotSelectedMap[quot.id]) {
            spTotalPremium += commonUtils.convertForeignCcytoSGD(_.get(quot, 'policyOptions.topUpAmt', 0), quotCcy, compCode, decimal);
          }

          // else if (quotSelectedMap.get(quot.id) && quot.budgetRules.indexOf('MED') >= 0) {
          if (quotSelectedMap[quot.id]) {
            // cpfMsTotalPremium += quot.premium;
            cpfMsTotalPremium += _.get(quot, 'totMedisave', 0);
          }

          if (index === quotList.length - 1) {
            logger.log('INFO: genClientChoiceBudgetTemplateValues - getFE', quot.pCid);
            nDao.getItem(quot.pCid, nDao.ITEM_ID.FE).then((fe)=> {
              if (fe) {
                var owner = fe.owner;
                if (owner) {
                  budgetDefaultValues.spBudget    = owner.singPrem;
                  budgetDefaultValues.rpBudget    = owner.aRegPremBudget;
                  budgetDefaultValues.cpfOaBudget = owner.cpfOaBudget;
                  budgetDefaultValues.cpfSaBudget = owner.cpfSaBudget;
                  budgetDefaultValues.srsBudget   = owner.srsBudget;
                  budgetDefaultValues.cpfMsBudget = owner.cpfMsBudget;
                }

                budgetDefaultValues.spTotalPremium    = spTotalPremium;
                budgetDefaultValues.rpTotalPremium    = rpTotalPremium;
                budgetDefaultValues.cpfOaTotalPremium = cpfOaTotalPremium;
                budgetDefaultValues.cpfSaTotalPremium = cpfSaTotalPremium;
                budgetDefaultValues.srsTotalPremium   = srsTotalPremium;
                budgetDefaultValues.cpfMsTotalPremium = cpfMsTotalPremium;

                budgetDefaultValues.spCompare     = budgetDefaultValues.spBudget    - budgetDefaultValues.spTotalPremium;
                budgetDefaultValues.rpCompare     = budgetDefaultValues.rpBudget    - budgetDefaultValues.rpTotalPremium;
                budgetDefaultValues.cpfOaCompare  = budgetDefaultValues.cpfOaBudget - budgetDefaultValues.cpfOaTotalPremium;
                budgetDefaultValues.cpfSaCompare  = budgetDefaultValues.cpfSaBudget - budgetDefaultValues.cpfSaTotalPremium;
                budgetDefaultValues.srsCompare    = budgetDefaultValues.srsBudget   - budgetDefaultValues.srsTotalPremium;
                budgetDefaultValues.cpfMsCompare  = budgetDefaultValues.cpfMsBudget - budgetDefaultValues.cpfMsTotalPremium;

                budgetDefaultValues.spCompareResult     = getClientChoiceBudgetPremiumCompare(budgetDefaultValues.spCompare);
                budgetDefaultValues.rpCompareResult     = getClientChoiceBudgetPremiumCompare(budgetDefaultValues.rpCompare);
                budgetDefaultValues.cpfOaCompareResult  = getClientChoiceBudgetPremiumCompare(budgetDefaultValues.cpfOaCompare);
                budgetDefaultValues.cpfSaCompareResult  = getClientChoiceBudgetPremiumCompare(budgetDefaultValues.cpfSaCompare);
                budgetDefaultValues.srsCompareResult    = getClientChoiceBudgetPremiumCompare(budgetDefaultValues.srsCompare);
                budgetDefaultValues.cpfMsCompareResult  = getClientChoiceBudgetPremiumCompare(budgetDefaultValues.cpfMsCompare);

                budgetValidationReset(bundle, budgetDefaultValues);

                // template.items[1].items.push(budgetTemplateCopy);
                setBudgetTemplate(template, res, budgetDefaultValues);

                getExchangeRateList().then(exchangeList => {
                  budgetDefaultValues.multiCcyNote = exchangeList;

                  /** DO NOT REMOVE { budget: budgetDefaultValues } */
                  var newValues = Object.assign({}, values, budgetDefaultValues, { budget: budgetDefaultValues });
                  logger.log('INFO: genClientChoiceBudgetTemplateValues - end [RETURN=3]', bundle.id);
                  cb({template:template, values:newValues});
                });

              } else {
                logger.log('INFO: genClientChoiceBudgetTemplateValues - end [RETURN=-1]', bundle.id);
                cb(false);
              }
            }).catch((error) => {
              logger.error('ERROR: genClientChoiceBudgetTemplateValues - getFE', error);
            });
          }
        });
      } else {
        // get data from bundle
        setBudgetTemplate(template, res, bundle.clientChoice.budget);
        let newValues = Object.assign({}, values, bundle.clientChoice.budget, { budget: bundle.clientChoice.budget });
        logger.log('INFO: genClientChoiceBudgetTemplateValues - end [RETURN=2]', bundle.id);
        cb({template:template, values:newValues});
      }
    } else {
      logger.error('ERROR: genClientChoiceBudgetTemplateValues - end [RETURN=-2]', ccBudgetId, ccBudgetId);
      cb(false);
    }
  });
};

var genClientChoiceAcceptanceTemplateValues = function(bundle, quotList, template, values, cb) {
  /** DO NOT REMOVE acceptance: [] */
  var acceptanceValues = {
    clientAcceptance: {
    },
    acceptance: []
  };

  logger.log('INFO: genClientChoiceAcceptanceTemplateValues - start', bundle.id);
  dao.getDoc(ccAcceptanceId, function(resMain) {
    if (resMain && !resMain.error) {
      var acceptTemplateCopy = Object.assign({}, resMain);
      dao.getDoc(ccAcceptanceChoiceItemId, function(resItem) {
        dao.getDoc('clientChoiceAcceptanceChoiceItemTmpl_Shield', function(resItemShield) {
          if (resItem && !resItem.error && resItemShield && !resItemShield.error) {
            values.menuIdList.forEach(function(menuId, index) {
              var choiceValues = values[menuId];

              if (choiceValues.clientChoiceSelect === 'Y') {
                var acceptItemTemplateCopy = _.cloneDeep(resItem);
                var acceptItemTemplateCopyShield = _.cloneDeep(resItemShield);

                if (choiceValues.isShield) {
                  const groupList = choiceValues.shieldInsuredAndPlanGroupList;

                  _.forEach(groupList, (group, listIndex) => {
                    if (listIndex > 0) {
                      acceptItemTemplateCopyShield.items = _.concat(acceptItemTemplateCopyShield.items, _.cloneDeep(resItemShield.items));
                    }

                    acceptItemTemplateCopyShield.items[listIndex * 3 + 0].id = resItemShield.items[0].id + index + '_' + listIndex;
                    acceptItemTemplateCopyShield.items[listIndex * 3 + 1].id = resItemShield.items[1].id + index + '_' + listIndex;

                    acceptanceValues.clientAcceptance[(acceptItemTemplateCopyShield.items[listIndex * 3 + 0].id)] = _.join(group.insuredNameList, ' / ');
                    acceptanceValues.clientAcceptance[(acceptItemTemplateCopyShield.items[listIndex * 3 + 1].id)] = _.join(group.planNameList, ', ');
                  });

                  acceptTemplateCopy.items[resMain.items.length - 1].items.push(acceptItemTemplateCopyShield);
                } else {
                  acceptItemTemplateCopy.items[0].id = resItem.items[0].id + index;
                  acceptItemTemplateCopy.items[1].id = resItem.items[1].id + index;
                  acceptItemTemplateCopy.items[2].id = resItem.items[2].id + index;

                  acceptanceValues.clientAcceptance[(acceptItemTemplateCopy.items[0].id)] = choiceValues.proposerAndLifeAssuredName;
                  acceptanceValues.clientAcceptance[(acceptItemTemplateCopy.items[1].id)] = choiceValues.basicPlanName;
                  acceptanceValues.clientAcceptance[(acceptItemTemplateCopy.items[2].id)] = choiceValues.ridersName;
                  
                  /** DO NOT REMOVE acceptanceValues.acceptance.push */
                  acceptanceValues.acceptance.push({
                    quotationDocId: choiceValues.quotId,
                    proposerAndLifeAssuredName: choiceValues.proposerAndLifeAssuredName,
                    basicPlanName: choiceValues.basicPlanName,
                    ridersName: choiceValues.ridersName
                  });

                  acceptTemplateCopy.items[resMain.items.length - 1].items.push(acceptItemTemplateCopy);
                }
              }
            });

            template.items[2].items.push(acceptTemplateCopy);
            var newValues = Object.assign({}, values, acceptanceValues);

            logger.log('INFO: genClientChoiceAcceptanceTemplateValues - end [RETURN=1]', bundle.id);
            cb({template:template, values:newValues});
          } else {
            logger.error('ERROR: genClientChoiceAcceptanceTemplateValues - end [RETURN=-1]', bundle.id, ccAcceptanceChoiceItemId);
            cb(false);
          }
        });
      });
    } else {
      logger.error('ERROR: genClientChoiceAcceptanceTemplateValues - end [RETURN=-2]', bundle.id, ccAcceptanceId);
      cb(false);
    }
  });
};

module.exports.clientChoice = function(data, session, cb) {
  var quotCheckedList = data.quotCheckedList;
  // var isReadOnly = data.isReadOnly;
  /** DO NOT REMOVE chosenList, notChosenList */
  const chosenList = [];
  const notChosenList = [];
  var isReadOnly = false;
  var quotSelectedMap = {};
  let quotIds = [];
  for (var i = 0; i < quotCheckedList.length; i ++) {
    var quotChecked = quotCheckedList[i];
    quotSelectedMap[quotChecked.id] = quotChecked.checked;
    quotIds.push(quotChecked.id);
  }

  logger.log('INFO: clientChoice - start', quotCheckedList.length, isReadOnly);

  var template = {
    'items': [
      {
        'type': 'step',
        'title': 'Recommendation',
        'detailSeq': 1,
        'styleClass': 'PanelBackgroundColor',
        'items': [
          {
            'type': 'menu',
            'items': []
          }
        ]
      },
      {
        'type': 'step',
        'title': 'Budget',
        'detailSeq': 2,
        'styleClass': 'ClientChoicePanel',
        'items': []
      },
      {
        'type': 'step',
        'title': 'Client\'s Acceptance',
        'detailSeq': 3,
        'styleClass': 'ClientChoicePanel',
        'style': {'height': '100vh'},
        'items': []
      },
    ]
  };

  var values = {};

  quotDao.getQuotationsByQuotIdList(quotIds, function(quotList) {
    //Sort Quot List by lastUpdateDate
    var sortedQuotList = quotList.slice();
    sortedQuotList.sort(function(quotA, quotB) {
      if (quotA.lastUpdateDate < quotB.lastUpdateDate) {
        return 1;
      }
      if (quotA.lastUpdateDate > quotB.lastUpdateDate) {
        return -1;
      }
      return 0;
    });

    /** DO NOT REMOVE chosenList, notChosenList */
    _.forEach(sortedQuotList, quot => {
      const quotId = quot.id;
      if (quotSelectedMap[quotId]) {
        chosenList.push(quotId);
      } else {
        notChosenList.push(quotId);
      }
    })

    bDao.getCurrentBundle(data.clientId).then((bundle) => {
      isReadOnly = bundle.isFnaReportSigned;
      bDao.updateQuotCheckedList(bundle, quotCheckedList).then(()=>{
        genClientChoiceRecommendationsTemplateValues(bundle, sortedQuotList,
            quotSelectedMap, isReadOnly, template, values, function(resRec) {
          if (resRec) {
            genClientChoiceBudgetTemplateValues(bundle, sortedQuotList,
                quotSelectedMap, isReadOnly, resRec.template, resRec.values, resRec.clientChoiceFlag, function(resBg) {
              if (resBg) {
                genClientChoiceAcceptanceTemplateValues(bundle, sortedQuotList, resBg.template, resBg.values, function(resAcc) {
                  if (resAcc) {
                    logger.log('INFO: clientChoice - end [RETURN=1]', quotCheckedList.length);

                    /** DO NOT REMOVE recommendation object */
                    const recommendation = {
                      chosenList,
                      notChosenList
                    }
                    _.forEach(_.concat(chosenList, notChosenList), quotId => {
                      recommendation[quotId] = resAcc.values[quotId];
                    })

                    /** DO NOT REMOVE isReadOnly, recommendation, budget and acceptance object */
                    cb({
                      success:true,
                      template:resAcc.template,
                      values:resAcc.values,
                      isRecommendationCompleted:resRec.clientChoiceFlag.isRecommendationCompleted,
                      isBudgetCompleted:resRec.clientChoiceFlag.isBudgetCompleted,
                      isAcceptanceCompleted:resRec.clientChoiceFlag.isAcceptanceCompleted,
                      clientChoiceStep: resRec.clientChoiceFlag.clientChoiceStep,
                      isReadOnly,
                      recommendation,
                      budget: resAcc.values.budget,
                      acceptance: resAcc.values.acceptance
                    });
                  } else {
                    logger.log('INFO: clientChoice - end [RETURN=-3]', quotCheckedList.length);
                    cb({success:false});
                  }
                });
              } else {
                logger.log('INFO: clientChoice - end [RETURN=-2]', quotCheckedList.length);
                cb({success:false});
              }
            });
          } else {
            logger.log('INFO: clientChoice - end [RETURN=-1]', quotCheckedList.length);
            cb({success:false});
          }
        });
      });
    }).catch((error) => {
      logger.error('ERROR: clientChoice', quotCheckedList.length, error);
    });
  });
};
/**FUNCTION ENDS: clientChoice*/

var _resetClientChoiceBudget = function(cid) {
  logger.log('INFO: resetClientChoiceBudget - start', cid);
  return new Promise((resolve)=>{
    bDao.getCurrentBundle(cid).then((bundle) => {
      bundle.clientChoice.isAppListChanged = true;
      bundle.clientChoice.isClientChoiceCompleted = false;
      bundle.clientChoice.isBudgetCompleted = false;
      bundle.clientChoice.isAcceptanceCompleted = false;
      bundle.clientChoice.clientChoiceStep = bundle.clientChoice.clientChoiceStep > 0 ? 1 : 0;
      //budget will be recalculated if isAppListChanged = true

      logger.log(`INFO: BundleUpdate - [cid:${cid}; bundleId:${_.get(bundle, 'id')}; fn:_resetClientChoiceBudget]`);
      bDao.updateBundle(bundle, (result) => {
        logger.log('INFO: resetClientChoiceBudget - end', cid);
        resolve(result);
      });
    }).catch((error) => {
      logger.error('ERROR: resetClientChoiceBudget', cid, error);
    });
  });
};
module.exports.resetClientChoiceBudget = _resetClientChoiceBudget;
