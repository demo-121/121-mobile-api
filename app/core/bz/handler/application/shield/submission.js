const commonApp = require('../common');
const dao = require('../../../cbDaoFactory').create();
const _ = require('lodash');
const _application = require('./application');
const _signature = require('./signature');
var async = require('async');
const logger = global.logger || console;
const appDao = require('../../../cbDao/application');
const bundleDao = require('../../../cbDao/bundle');
const approvalHandler = require('../../../ApprovalHandler');
const aEmaillHandler = require('../../../ApprovalNotificationHandler');
const PDFHandler = require('../../../PDFHandler');
const commonFunctions = require('../../../CommonFunctions');
const utils = require('../../../Quote/utils');

const _getSubmissionTemplate = function(frozen, callback){
  let result = {
    success: false,
    template: {}
  };

  return new Promise((resolve, reject) => {
    dao.getDocFromCacheFirst(commonApp.TEMPLATE_NAME.SUBMISSION_SHIELD, (tmpl) => {
      if (tmpl && !tmpl.error) {
        result.success = true;
        if (frozen) {
          commonApp.frozenTemplate(tmpl);
        }
        result.template = tmpl;
        resolve(result);
      } else {
        reject(new Error('Fail to get payment template ' + _.get(tmpl, 'error')));
      }
    });
  }).catch( error => {
    logger.error(`ERROR in _getSubmissionTemplate ${error}`);
  });
};

const _prepareSubmission = function(session, data){
  const { appId } = data;

  return _application.getApplication(appId).then((app) => {
    return new Promise((resolve, reject) => {
      async.waterfall([
        (callback) => {
          if (app.appStep < commonApp.EAPP_STEP.SUBMISSION) {
            app.appStep = commonApp.EAPP_STEP.SUBMISSION;
            app.agentChannelType = (session.agent.channel === 'FA' ? 'Y' : 'N');

            appDao.upsertApplication(app._id, app, (res)=>{
              if (res && !res.error) {
                app._rev = res.rev;
                callback(null, app);
              } else {
                callback('Fail to Update application');
              }
            });
          } else {
            callback(null, app);
          }
        }], (err, callbackApplication) => {
          if (err) {
            logger.error(`ERROR: _prepareSubmission: ${appId} ${err}`);
            resolve({success: false});
          } else {
            resolve(callbackApplication);
          }
      });
    }).catch(error => {
      logger.error(`ERROR: _prepareSubmission: ${appId} caught ${error}`);
    });
  });
};

const _saveSubmissionValuesPromise = (data, session) => {
  return new Promise((resolve, reject) => {
    _saveSubmissionValues(data, session, (result) => {
      if (result.success) {
        resolve(result);
      } else {
        resolve(undefined);
      }
    });
  }).catch(error => {
    logger.error(`ERROR in _saveSubmissionValuesPromise ${error}`);
  });
};
module.exports.saveSubmissionValuesPromise = _saveSubmissionValuesPromise;

const _saveSubmissionValues = function(data, session, cb) {
  let {appId, changedValues} = data;
  async.waterfall([
    (callback) => {
      _application.getApplication(appId).then((app) => {
        callback(null, app);
      }).catch(err => {callback((err));});
    }, (application, callback) => {
      application.submission = _.get(changedValues, 'submission') || {};
      commonApp.updateAndReturnApp(application, callback);
    }
  ], (err, application) => {
    if (err) {
      logger.error(`ERROR in _saveSubmissionValues, ${err}`);
      cb({success: false, application: {}});
    } else {
      cb({success: true, application});
    }
  });
};
module.exports.saveSubmissionValues = _saveSubmissionValues;

const _saveParentSubmissionValuestoChild = function(parentApplication, updateObj, callback) {
  let childIds = _.get(parentApplication, 'childIds');
  _application.getChildApplications(childIds).then(childApplications => {
      let updAppPromises = childApplications.map((app) => {
        app = _.assignIn(app, updateObj);

        // app.payment.trxStatus = _.get(parentApplication, 'payment.trxStatus', '');
        // app.payment.trxRemark = _.get(parentApplication, 'payment.trxRemark', '');
        // app.payment.trxAmount = _.get(parentApplication, 'payment.trxAmount', '');
        // app.payment.trxCcy = _.get(parentApplication, 'payment.trxCcy', '');
        // app.payment.trxTime = _.get(parentApplication, 'payment.trxTime', '');
        // app.payment.trxPayType = _.get(parentApplication, 'payment.trxPayType', '');
        // app.payment.receiptNo = _.get(parentApplication, 'payment.receiptNo', '');
        // app.payment.trxStatusRemark = _.get(parentApplication, 'payment.trxStatusRemark', '');
        // app.payment.trxEnquiryStatus = _.get(parentApplication, 'payment.trxEnquiryStatus', '');
        // app.payment.isInitialPaymentCompleted = _.get(parentApplication, 'payment.isInitialPaymentCompleted', '');
        
        // // Change the trxAmount from total in Parent Application to child Cash portion
        // app.payment.trxAmount = app.payment.cashPortion;
        return new Promise((resolve, reject) => {
          appDao.updApplication(app.id, app, (res) => {
            if (res && !res.error) {
              resolve(res);
            } else {
              reject(new Error('Fail to update application ' + app.id + ' ' + _.get(res, 'error')));
            }
          });
        });
      });

      return Promise.all(updAppPromises).then(results =>{
        callback(null, parentApplication);
      }, rejectReason => {
        callback(`ERROR in _saveParentPaymenttoChild with Reject Reason,  ${rejectReason}`);
      }).catch((error)=>{
        logger.error(`ERROR in _saveParentPaymenttoChild ${error}`);
        callback(`ERROR in _saveParentPaymenttoChild, ${error}`);
      });
    }
  ).catch(error => {
    logger.error(`ERROR in _saveParentPaymenttoChild: ${error}`);
  });
};

//=ApplicationHandler.genAppFormPdfByPaymentData
const _genAppFormPdfPaymentPageByData = function(app, lang, callback) {
  let trigger = {};
  let paymentValues = _.cloneDeep(app.payment);
  let ccy = _.get(app, 'applicationForm.values.proposer.extra.ccy');

  dao.getDoc(commonApp.TEMPLATE_NAME.PAYMENT_SHIELD, function(paymentTmpl) {
    logger.log('INFO: _genAppFormPdfPaymentPageByData - replaceAppFormValuesByTemplate start', app._id);
    commonApp.replaceAppFormValuesByTemplate(paymentTmpl, app.payment, paymentValues, trigger, [], lang, ccy);
    logger.log('INFO: _genAppFormPdfPaymentPageByData - replaceAppFormValuesByTemplate end', app._id);

    let ccySign = utils.getCurrencySign(ccy);
    if (paymentValues.totCPFPortion > 0) {
      paymentValues.totCPFPortion = utils.getCurrency(paymentValues.totCPFPortion, ccySign, 2);
    }
    if (paymentValues.totCashPortion > 0) {
      paymentValues.totCashPortion = utils.getCurrency(paymentValues.totCashPortion, ccySign, 2);
    }
    if (paymentValues.totMedisave > 0) {
      paymentValues.totMedisave = utils.getCurrency(paymentValues.totMedisave, ccySign, 2);
    }

    let reportData = {
      root: {
        reportData: paymentValues,
        originalData: app.payment
      }
    };

    reportData.root.reportData.pCid = app.pCid;

    new Promise((resolve, reject) => {
      dao.getDoc(commonApp.TEMPLATE_NAME.APP_FORM_MAPPING, (mapping)=> {
        if (mapping && !mapping.error && mapping[app.quotation.baseProductCode]) {
          resolve(mapping[app.quotation.baseProductCode].paymentTemplate);
        } else {
          reject(new Error('Fail to get ' + commonApp.TEMPLATE_NAME.APP_FORM_MAPPING));
        }
      });
    }).then((paymentTemplate) => {
      let tplFiles = [];
      commonApp.getReportTemplates(tplFiles, [paymentTemplate.main, ...paymentTemplate.template], 0, function() {
        if (tplFiles.length) {
          PDFHandler.getPremiumPaymentPdf(reportData, tplFiles, lang, function(pdfStr) {
            callback(pdfStr);
          });
        } else {
          logger.error('ERROR: _genAppFormPdfPaymentPageByData - end [RETURN=-101]', app._id);
          callback(false);
        }
      });
    }).catch((error) => {
      logger.error('ERROR: _genAppFormPdfPaymentPageByData - end [RETURN=-100]', app._id, error);
      callback(false);
    });
  });
};

const _preparePaymentPDf = function (app, lang, callback) {
  _genAppFormPdfPaymentPageByData(app, lang, (pdfStr) => {
    if (pdfStr && pdfStr.length > 0) {
      callback(null, pdfStr);
    } else {
      callback('Fail to generate Payment page for App Form');
    }
  });
};

const _mergeAppFormPdf = function(app, pdfStr, appformPdfStrs, callback) {
  logger.log('INFO: _mergeAppFormPdf - start', app._id);
  async.waterfall([
    (cb) => {
      // let getPdfPromise = [];
      // getPdfPromise.push(new Promise((resolve, reject) => {
      //   appDao.getAppAttachment(app._id, commonApp.PDF_NAME.eAPP, (appPdf) => {
      //     if (appPdf.success) {
      //       resolve(appPdf);
      //     } else {
      //       reject(new Error('Fail to get attachment for proposer'));
      //     }
      //   });
      // }));
      // getPdfPromise = getPdfPromise.concat(app.iCids.map((iCid, i) => {
      //   return new Promise((resolve, reject) => {
      //     appDao.getAppAttachment(app._id, _signature.getAppFormAttachmentName(iCid), (appPdf) => {
      //       if (appPdf.success) {
      //         resolve(appPdf);
      //       } else {
      //         reject(new Error('Fail to get attachment for insured ' + i + ' ' + iCid));
      //       }
      //     });
      //   });
      // }));

      // Promise.all(getPdfPromise).then((appPdfs) => {
      //   cb(null, appPdfs);
      // }).catch((error) => {
      //   logger.error('ERROR: _mergeAppFormPdf', app._id, error);
      //   cb(error);
      // });
      cb(null, appformPdfStrs);
    },
    (appPdfs, cb) => {
      let mergePdfPromise = [];
      let phAppPdf = appPdfs[0];

      mergePdfPromise.push(new Promise((resolve, reject) => {
        commonFunctions.mergePdfs([phAppPdf.data, pdfStr], function(mergedPdf) {
          if (mergedPdf) {
            logger.log('INFO: _mergeAppFormPdf - mergedPdf', '0', 'done', app._id);
            resolve(mergedPdf);
          } else {
            reject(new Error('Fail to merge pdf for proposer'));
          }
        });
      }));

      for (let i = 1; i < appPdfs.length; i ++) {
        let laAppPdf = appPdfs[i];
        mergePdfPromise.push(new Promise((resolve, reject) => {
          commonFunctions.mergePdfs([phAppPdf.data, laAppPdf.data, pdfStr], function(mergedPdf) {
            if (mergedPdf) {
              logger.log('INFO: _mergeAppFormPdf - mergedPdf', i, 'done', app._id);
              resolve(mergedPdf);
            } else {
              reject(new Error('Fail to merge pdf for insured ' + i));
            }
          });
        }));
      }

      Promise.all(mergePdfPromise).then((mergedPdfs) => {
        cb(null, mergedPdfs);
      }).catch((error) => {
        logger.error('ERROR: _mergeAppFormPdf', app._id, error);
        cb(error);
      });
    },
    (mergedPdfs, cb) => {
      let uploadInfoList = [];

      for (let i = 0; i < mergedPdfs.length; i ++) {
        let uploadInfo = {
          attachName: '',
          mergedPdf: mergedPdfs[i]
        };

        if (i === 0) {
          uploadInfo.attachName = commonApp.PDF_NAME.eAPP;
        } else {
          let iCid = app.iCids[i - 1];
          uploadInfo.attachName = _signature.getAppFormAttachmentName(iCid);
        }
        uploadInfoList.push(uploadInfo);
      }

      let _uploadAttachment = function(currApp, index, cbk) {
        let {attachName, mergedPdf} = uploadInfoList[index];
        dao.uploadAttachmentByBase64(currApp._id, attachName, currApp._rev, mergedPdf, 'application/pdf', function(res) {
          if (res && res.rev && !res.error) {
            currApp._rev = res.rev;
            cbk(null, currApp, index + 1);
          } else {
            cbk('Fail to upload Pdf attachment ' + index);
          }
        });
      };

      let uploadFunctions = [];
      _.forEach(mergedPdfs, (item, i) => {
        if (i > 0) {
          uploadFunctions.push(_uploadAttachment);
        }
      });

      async.waterfall([
        (cbk) => {
          _uploadAttachment(app, 0, cbk);
        },
        ...uploadFunctions
      ], (err, newApp, index) => {
        if (err) {
          cb('ERROR: _mergeAppFormPdf - _uploadAttachment ' + err);
        } else {
          cb(null, newApp);
        }
      });
    }
  ], (err, newApp) => {
    if (err) {
      logger.error('ERROR: _mergeAppFormPdf - end [RETURN=-100]', app._id, err);
      callback('Fail to merge AppForm Pdf');
    } else {
      logger.log('INFO: _mergeAppFormPdf - end [RETURN=100]', app._id);
      callback(null, newApp);
    }
  });
};

const _prepareSubmissionEmail = (data, app, session, cb) => {
  let receiveEmailFa = _.get(data, 'changedValues.submission.receiveEmailFa', 'N');
  let emailValues = Object.assign({}, data);
  let iCidMapping = _.get(app, 'iCidMapping');
  //Prepare Submission Email
  async.waterfall([
    (callback) => {
      appDao.getSubmissionEmailTemplate(session.agent.channel === 'FA', (result) => {
        if (result && !result.error) {
          // Check whether need to send email
          let now = Date.now();
          let filteredTemplate = _.filter(result.eSubmissionEmails, function (template) {
            return template.productCode.indexOf(app.quotation.baseProductCode) !== -1;
          });

          if (filteredTemplate && filteredTemplate.length !== 0) {
            logger.log('INFO: _prepareSubmissionEmail filteredTemplate', app.quotation.baseProductCode, session.agent.channel, app.id);
            let { attachmentKeys } = filteredTemplate[0];
            let appPdfAttachment = _.get(app, '_attachments');
            _.each(appPdfAttachment, (value, key) => {
              if (key.indexOf(commonApp.PDF_NAME.eAPP) > -1) {
                attachmentKeys.push(key);
              }
            });
            emailValues.agentName = _.get(data, 'agentProfile.name');
            emailValues.agentContactNum = _.get(data, 'agentProfile.mobile');
            emailValues.agentEmail = _.get(data, 'agentProfile.email');
            emailValues.agentTitle = '';
            emailValues.clientName = _.get(data, 'clientProfile.fullName');
            emailValues.clientEmail = _.get(data, 'clientProfile.email');
            emailValues.clientId = _.get(data, 'clientProfile.cid');
            emailValues.clientTitle = _.get(data, 'clientProfile.title');
            emailValues.emailContent = filteredTemplate[0].emailContent;
            emailValues.attKeys = attachmentKeys;
            emailValues.policyNumber = commonApp.getAllPolicyIdsStrFromMaster(iCidMapping);
            emailValues.submissionDate = commonFunctions.getFormattedDate(now);
            emailValues.quotId = app.quotation.id;
            emailValues.appId = app._id;
            emailValues.receiveEmailFa = session.agent.channel === 'FA' ? receiveEmailFa : 'Y';
            emailValues.isShield = true;
            callback(null, emailValues);
          } else {
            callback('Fail to get email template from product');
          }
        } else {
          callback('Fail to get email template');
        }
      });
    }, (eValues, callback) => {
      commonApp.prepareSubmissionEmails(eValues, session, (resp) => {
        if (resp && resp.success) {
           callback(null, app);
        } else {
          callback('Fail to prepareSubmissionEmails');
        }
      });
    }
  ], (err, callbackApplication) => {
    if (err) {
      cb(`Fail in _prepareSubmissionEmail, ${err}`);
    } else {
      cb(null, callbackApplication);
    }
  });
};

const _createApprovalCases = (app, session, callback) => {
  //Create Approval Cases
  let promises = [];
  let childIds = _.get(app, 'childIds');
  let currentTime = (new Date()).toISOString();
  //Create Master approval docs
  promises.push(_createMasterApprovalCase(app.id, session, currentTime));

  // Create child approval docs
  _.each(childIds, id => {
    promises.push(_createApprovalCase(id, session, currentTime));
  });

  return Promise.all(promises).then(results =>{
    callback(null, {success: true});
  }, rejectReason => {
    callback(`ERROR in _createApprovalCases with Reject Reason,  ${rejectReason}`);
  }).catch((error)=>{
    logger.error(`ERROR in _createApprovalCases ${error}`);
    callback(`ERROR in _createApprovalCases, ${error}`);
  });
};

const _createMasterApprovalCase = (masterId, session, currentTime) => {
  return new Promise((resolve, reject) => {
    approvalHandler.createApprovalCase({ids: [masterId], isShieldMaster: true, currentTime}, session, (eAppResult) =>{
      if (eAppResult.success) {
        resolve({success: true});
      } else {
        reject({success: false});
      }
    });
  });
};

const _createApprovalCase = (childAppId, session, currentTime) => {
  return new Promise((resolve, reject) => {
    approvalHandler.createApprovalCase({ids: [childAppId], isShield: true, currentTime}, session, (eAppResult) =>{
      if (eAppResult.success) {
        resolve({success: true});
      } else {
        reject({success: false});
      }
    });
  });
};

const _sendApprovalSubmittedCaseNotification = (app, session, callback) => {
  //Send email
  let masterEapprovalId = approvalHandler.getMasterApprovalIdFromMasterApplicationId(app.id);
  aEmaillHandler.SubmittedCaseNotification({id: masterEapprovalId}, session);
  callback(null, app);
};

const _submitApplication = (data, app, session, cb) => {
  let submissionValues = _.get(data, 'changedValues.submission');
  async.waterfall([
    (callback) => {
      // update bundle
      bundleDao.onSubmitApplication(app.pCid, app._id).then((result) => {
        if (result.ok) {
          // Success update bundle
          callback(null, {success: true});
        } else {
          callback('Fail to create/ update Bundle ');
        }
      }).catch(Err =>{
        logger.error('ERROR _submitApplication in update bundle promise', Err);
        callback(`Fail to create/ update Bundle ${Err}`);
      });
    }, (previousResult, callback) => {
      // update application

      if (_.get(session, 'agent.agentCode') !== _.get(session, 'agent.managerCode')) {
        commonApp.transformSuppDocsPolicyFormValues(app);
      }
      if (_.get(app, 'payment.initialPayment.paymentMethod') && _.get(_.get(app, 'supportDocuments.viewedList'), session.agentCode)) {
        app.supportDocuments.viewedList[session.agentCode][app.payment.initialPayment.paymentMethod] = false;
      }

      if (session.agent.channel === 'FA') {
        app.submission = submissionValues;
      }

      app.isSubmittedStatus = true;
      app.submitStatus = 'SUCCESS';
      app.applicationSubmittedDate = new Date().toISOString();
      _application.updateApplicationCompletedStep(app);

      // Declare which fields need to update to child
      let updateObjforChild = {
        submission: app.submission,
        isSubmittedStatus: app.isSubmittedStatus,
        submitStatus: app.submitStatus,
        applicationSubmittedDate: app.applicationSubmittedDate
      };

      appDao.upsertApplication(app._id, app, (res)=>{
        if (res && !res.error) {
          //isMandDocsAllUploaded may change to false in transformSuppDocsPolicyFormValues, always return isMandDocsAllUploaded = true to web in submission success case
          app.isMandDocsAllUploaded = true;
          callback(null, [app, updateObjforChild]);
        } else {
          callback('Fail to Update application');
        }
      });
    }, (result, callback) => {
      _saveParentSubmissionValuestoChild(result[0], result[1], callback);
    }, (pApplication, callback) => {
      bundleDao.updateStatus(pApplication.pCid, bundleDao.BUNDLE_STATUS.SUBMIT_APP).then((res)=>{
        logger.log('INFO: Shield appFormSubmission - update bundle', pApplication._id);
        callback(null, pApplication);
      }).catch((error)=>{
        logger.error('ERROR: Shield appFormSubmission - end [RETURN=1] fails :', pApplication._id, error);
        callback('Failed to update Bundle ');
      });
    }
  ], (error, modifiedApplication) => {
    if (error) {
      logger.error(`ERROR in _submitApplication ${error}`);
      cb(`Fail to _submitApplication ${error}`);
    } else {
      cb(null, modifiedApplication);
    }
  });
};

const startSubmission = function(data, app, session, cb) {
  logger.log('INFO: appFormSubmission - (2) doSubmission', app.id);
  let { appId } = data;
  async.waterfall([
    (callback) => {
      logger.info('Start Submission - Create Approval Cases');
      _createApprovalCases(app, session, callback);
    },(result, callback) => {
      //Update View
      logger.info('Start Submission - Update Views');
      dao.updateViewIndex('main', 'applicationsByAgent');
      dao.updateViewIndex('main', 'approvalDetails');
      callback(null, {success: true});
    }, (result, callback) => {
      // Submit the Case
      logger.info('Start Submission - Submit Application');
      _submitApplication(data, app, session, callback);
    }, (modifiedApplication, callback) => {
      // Send Approval Email
      logger.info('Start Submission - Send Approval Notification');
      _sendApprovalSubmittedCaseNotification(modifiedApplication, session, callback);
    }, (modifiedApplication, callback) => {
      // Send Submission Pos Email
      logger.info('Start Submission - Send Pos Email');
      _prepareSubmissionEmail(data, modifiedApplication, session, callback);
    }], (err, callbackApplication) => {
      if (err) {
        logger.error(`ERROR startSubmission: ${appId}, ${err}`);
        cb('Fail in start Submission');
      } else {
        // Return modified application
        cb(null, callbackApplication);
      }
  });
};

//=ApplicationHandler.appFormSubmission
module.exports.submission = function(data, session, cb){
  let { appId, submissionTemplate, changedValues } = data;
  return _application.getApplication(appId).then((app) => {
    let appformPdfstrs;
    let rollbackDoc;
    async.waterfall([
      (callback) => {
        // GetPdf Doc for rollback 
        // let getPdfPromise = [];
        // getPdfPromise.push(new Promise((resolve, reject) => {
        //   appDao.getAppAttachment(app._id, commonApp.PDF_NAME.eAPP, (appPdf) => {
        //     if (appPdf.success) {
        //       resolve(appPdf);
        //     } else {
        //       reject(new Error('Fail to get attachment for proposer'));
        //     }
        //   });
        // }));
        // getPdfPromise = getPdfPromise.concat(app.iCids.map((iCid, i) => {
        //   return new Promise((resolve, reject) => {
        //     appDao.getAppAttachment(app._id, _signature.getAppFormAttachmentName(iCid), (appPdf) => {
        //       if (appPdf.success) {
        //         resolve(appPdf);
        //       } else {
        //         reject(new Error('Fail to get attachment for insured ' + i + ' ' + iCid));
        //       }
        //     });
        //   });
        // }));

        // Promise.all(getPdfPromise).then((appPdfs) => {
        //   appformPdfstrs = appPdfs;
        //   callback(null, {success: true});
        // }).catch((error) => {
        //   logger.error('ERROR: prepare rollback in submission', app._id, error);
        //   callback(error);
        // });
        callback(null, {success: true});
      }, (result, callback) => {
        // Get application, bundle, approval json
        let docArr = [_.get(app, 'bundleId'), app.id];
        let promiseArr =[];
        docArr.forEach((obj) => {
            promiseArr.push(new Promise((resolve, reject)=>{
                dao.getDoc(obj, (doc)=>{
                    resolve(doc);
                });
            }));
        });
        Promise.all(promiseArr).then(getDocresult =>{
          rollbackDoc = getDocresult;
          callback(null, {success: true});
        }).catch((error)=>{
          logger.error("Error in getAllDoc->Promise.all: ", error);
          callback(`Error in getAllDoc for rollback ${error}`);
        });
      }, (result, callback) => {
        // Prepare the payment Pdf
        _preparePaymentPDf(app, 'en', callback);
      }, (pdfStr, callback) => {
        // merge to all AppForm Pdf
        _mergeAppFormPdf(app, pdfStr, appformPdfstrs, callback);
      }, (newApp, callback) => {
        // Start Submission
        startSubmission(data, newApp, session, callback);
      }], (err, callbackApplication) => {
        if (err) {
          logger.error(`ERROR submission: ${appId}, ${err}`);

          //TO-DO: handle rollback application, and fail message
          //msg 1: Supervisor detail not available. Case cannot be submitted.
          //msg 2: Submission Fail. Please try again later

          // Call Roll Back here
          _rollbackSubmission(app, submissionTemplate, appformPdfstrs, rollbackDoc, cb);
        } else {
          // Return template and modified application
          let frozenTemplate = _.cloneDeep(submissionTemplate);
          commonApp.frozenTemplate(frozenTemplate);
          cb({success: true, application: callbackApplication, template: frozenTemplate});
        }
    });
  }).catch(err => {
    logger.error(`ERROR submission: ${appId}, ${err}`);
    changedValues.submitStatus = 'FAIL';
    cb({success: true, application: changedValues, template: submissionTemplate});
  });
};

const _rollbackSubmission = (app, template, rollbackPdfs, rollbackDocs, cb) => {
  async.waterfall([
    (callback) => {
      // rollback pdf
      let cidMappingArr  = _.get(app, 'iCids');
      _rollbackPdf(app, rollbackPdfs, cidMappingArr, undefined, 0, callback);
    }, (result, callback) => {
      // rollback json
      _rollbackDoc(app, rollbackDocs, callback);
    }
  ], (error, result) =>{
    if (error) {
      cb(`ERROR in rollbacksubmission ${error}`);
    } else {
      app.submitStatus = 'FAIL';
      cb({success: false, application: app, template});
    }
  });  
}

const _rollbackDoc = (application, rollbackDocs, cb) => {
  async.waterfall([
    (callback) => {
      // Get application, bundle, approval json
      let docArrIds = [_.get(application, 'bundleId'), application.id];
      let deleteArrids = [];
      deleteArrids.push(approvalHandler.getMasterApprovalIdFromMasterApplicationId(application.id));
      _.each(_.get(application, 'iCidMapping'), (cidObj) => {
        _.each(cidObj, obj => {
          if (obj && obj.policyNumber) {
            deleteArrids.push(obj.policyNumber);
          }
        });
      });

      let promiseArr = [];
      // Rollback application
      docArrIds.map((id, index) => {
        promiseArr.push(
          new Promise((resolve, reject)=>{
            dao.getDoc(id, (doc)=>{
                resolve(doc);
            });
          }).then(doc => {
            return new Promise((resolve, reject) => {
              let updateDoc;
              _.each(rollbackDocs, obj => {
                if (obj._id === doc._id && doc.type === 'masterApplication') {
                  updateDoc = obj;
                  updateDoc.submitStatus = 'FAIL';
                  updateDoc.applicationSubmittedDate = new Date().toISOString();
                } else if (obj._id === doc._id) {
                  updateDoc = obj;
                }
              });
              updateDoc._rev = doc._rev;
              if (updateDoc) {
                dao.updDoc(doc._id, updateDoc, (result) => {
                  resolve(result);
                });
              } else {
                resolve({success: false});
              }
            });
          })
        );
      });

      //Delete Json
      deleteArrids.map(id => {
        promiseArr.push(
          new Promise((resolve, reject)=>{
            dao.getDoc(id, (doc)=>{
                resolve(doc);
            });
          }).then(doc => {
            return new Promise((resolve, reject) => {
              if (doc._id) {
                dao.delDocWithRev(doc._id, doc._rev, (result) => {
                  resolve(result);
                })
              } else {
                resolve({success: true});
              }
            });
          })
        );
      });

      Promise.all(promiseArr).then(getDocresult =>{
        callback(null, {success: true});
      }).catch((error)=>{
        logger.error(`Error in rollbackDoc for rollback ${error}`);
        callback(`Error in rollbackDoc for rollback ${error}`);
      });
    }
  ], (error, result) => {
    if (error) {
      cb(`ERROR in _rollbackdoc ${error}`);
    } else {
      cb(null, {success: true});
    }
  });
}

const _rollbackPdf = (application, base64StrsArr, cidMappingArr, rev, index, cb) => {
  async.waterfall([
    (callback) => {
      new Promise((resolve, reject)=>{
        if (rev === undefined) {
          _application.getApplication(application.id).then((app) => {
            resolve(app._rev);
          });
        } else {
            resolve(rev);
        }
      }).then(searchRev => {
        let base64Str = base64StrsArr[index];
        let attId = (index === 0) ? commonApp.PDF_NAME.eAPP : _signature.getAppFormAttachmentName(cidMappingArr[index - 1]);
        uploadAttachment(application.id, attId, searchRev, base64Str, 'application/pdf', (resultRev)=>{
          if (index + 1 !== base64StrsArr.length) {
            _rollbackPdf(application, base64StrsArr, cidMappingArr, resultRev, index + 1, callback);
          } else {
            callback(null, {success: true});
          }
        });
      }).catch((error)=>{
          logger.error("Error in rollbackPdf->new Promise: ", error);
          callback(`Error in rollbackPdf->new Promise:  ${error}`);
      });
    }
  ], (error, result) => {
    if (error) {
      cb(error);
    } else {
      cb(null, {success: true});
    }
  });
}

const uploadAttachment = function(id, attchId, rev, data, mime, callback){
  dao.uploadAttachmentByBase64(id, attchId, rev, data.data, mime, function(result) {
      if (result && !result.error) {
          callback(result.rev);
      } else {
        logger.error('ERROR:: uploadAttachment in rollback submission pdf', _.get(result, 'error'));
        callback({success:false});
      }
  });
};

module.exports.rollbackSubmission = _rollbackSubmission;
module.exports.getSubmissionTemplate = _getSubmissionTemplate;
module.exports.prepareSubmission = _prepareSubmission;
