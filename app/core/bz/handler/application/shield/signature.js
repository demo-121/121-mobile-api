const _ = require('lodash');
const moment = require('moment');
const logger = global.logger || console;

const tokenUtils = require('../../../utils/TokenUtils');
const commonApp = require('../common');
const dao = require('../../../cbDaoFactory').create();
const fDao = require('../../../cbDao/file');
const cDao = require('../../../cbDao/client');
const bDao = require('../../../cbDao/bundle');
const quotDao = require('../../../cbDao/quotation');
const appDao = require('../../../cbDao/application');
const _application = require('./application');

const APP_FORM_SIGN_BOX_WIDTH = {
  // TWO_BOXES: 0.188,
  // THREE_BOXES: 0.118
  TWO_BOXES: 0.147,
  THREE_BOXES: 0.092
};

const APP_FORM_SIGNDOC_SIGN_FIELD = {
  searchTag: '',
  // width: 0.188,
  // height: 0.039
  width: 0.147,
  height: 0.030
};

const FNA_SIGNDOC_SIGN_FIELD = {
  searchTag: '',
  // width: 0.139,
  // height: 0.0300
  width: 0.103,
  height: 0.026
};

const PROPOSAL_SIGNDOC_SIGN_FIELD = {
  searchTag: '',
  // width: 0.124,
  // height: 0.026,
  width: 0.108,
  height: 0.026,
  offsety: 0.006
};

var _getAppFormAttachmentName = function(iCid) {
  return commonApp.PDF_NAME.eAPP + iCid;
};

var _prepareFnaReportPdf = function (session, app, now, bundle) {
  // const SIGNDOC_SIGN_FIELD = {
  //   searchTag: '',
  //   width: 0.139,
  //   height: 0.0300
  // };

  const SIGNDOC_TEXT_FIELD = {
    searchTag: '',
    size: 12,
    width: 0.127,
    height: 0.0055
  };

  let result = {
    docid: '',
    pdfStr: '',
    attUrl: '',
    isSigned: false,
    auth: '',
    docts: 0,
    agentSignFields: [],
    clientSignFields: [],
    textFields: [],
    tokens: []
  };

  logger.log('INFO: _prepareFnaReportPdf', app._id);

  result.agentSignFields.push([
    Object.assign({}, FNA_SIGNDOC_SIGN_FIELD, {
      name:app.quotation.agent.name,
      searchTag:'FNAREPORT_AGENT_SIGNATURE',
      // height: 0.035
    })
  ]);
  result.textFields.push(
    [
      Object.assign({}, SIGNDOC_TEXT_FIELD, {
        searchTag: 'FNAREPORT_AGENT_NAME',
        value: 'agent'
      })
    ]
  );
  let clientSignFields = [
    Object.assign({}, FNA_SIGNDOC_SIGN_FIELD, {
      name:app.applicationForm.values.proposer.personalInfo.fullName,
      searchTag:'FNAREPORT_CLIENT_SIGNATURE'
    })
  ];

  result.tokens.push(tokenUtils.createPdfToken(bundle._id, commonApp.PDF_NAME.FNA, now, session.loginToken));

  return new Promise((resolve, reject) => {
    result.docid = app._id + '_' + commonApp.PDF_NAME.FNA;
    result.attUrl = global.config.signdoc.getPdf + '/' + result.tokens[0].token;
    result.isSigned = bundle.isFnaReportSigned;
    result.auth = commonApp.genSignDocAuth(app._id + '_' + commonApp.PDF_NAME.FNA, now);
    result.docts = now;

    //Check if Spouse exists
    cDao.getClientById(app.pCid, function(pClient) {
      if (pClient && !pClient.error) {
        let dependant = pClient.dependants.find(function(d) {
          return d.relationship === 'SPO';
        });
        // add SPOUSE name if dependant has relationship SPO
        if (dependant) {
          cDao.getClientById(dependant.cid, function(spouse) {
            if (spouse && !spouse.error) {
              clientSignFields.push(Object.assign({}, FNA_SIGNDOC_SIGN_FIELD, {
                name:spouse.fullName,
                searchTag:'FNAREPORT_SPOUSE_SIGNATURE'
              }));
              result.clientSignFields.push(clientSignFields);
              resolve(result);
            } else {
              reject(new Error('Fail to get spouse info in pCid=' + app.pCid + ' dependant.cid=' + dependant.cid + ' ' + _.get(spouse, 'error')));
            }
          });
        } else {
          result.clientSignFields.push(clientSignFields);
          resolve(result);
        }
      } else {
        reject(new Error('Fail to get client info in pCid=' + app.pCid + ' ' + _.get(pClient, 'error')));
      }
    });
  });
};

var _prepareProposalPdf = function(session, app, now, bundle) {
  // const SIGNDOC_SIGN_FIELD = {
  //   searchTag: '',
  //   width: 0.124,
  //   height: 0.026,
  //   offsety: 0.006
  // };
  const isFaChannel = session.agent.channel === 'FA';

  let result = {
    docid: '',
    pdfStr: '',
    attUrl: '',
    isSigned: false,
    auth: '',
    docts: 0,
    agentSignFields: [],
    clientSignFields: [],
    tokens: []
  };

  logger.log('INFO: _prepareProposalPdf', app._id);

  return new Promise((resolve, reject) => {
    fDao.getAttachment(app.quotationDocId, commonApp.PDF_NAME.BI, function(attProposal){
      if (attProposal.success) {
        resolve(attProposal);
      } else {
        reject(new Error('Fail to get attachment ' + app.quotationDocId + '/' + commonApp.PDF_NAME.BI + ' ' + _.get(attProposal, 'error')));
      }
    });
  }).then((attProposal) => {
    result.agentSignFields.push(
      [
        Object.assign({}, PROPOSAL_SIGNDOC_SIGN_FIELD, {
          name:app.quotation.agent.name,
          searchTag:'Signature of Financial Consultant'
        })
      ]
    );
    result.clientSignFields.push(
      [
        Object.assign({}, PROPOSAL_SIGNDOC_SIGN_FIELD, {
          name:app.applicationForm.values.proposer.personalInfo.fullName,
          searchTag:'Signature of Proposer'
        })
      ]
    );

    let isGenToken = app.isProposalSigned || bundle.isFnaReportSigned || isFaChannel;

    if (isGenToken) {
      result.tokens.push(tokenUtils.createPdfToken(app.quotationDocId, commonApp.PDF_NAME.BI, now, session.loginToken));
    }

    result.docid = app._id + '_' + commonApp.PDF_NAME.BI;
    result.pdfStr = (!app.isProposalSigned ? attProposal.data : '');
    result.attUrl = (isGenToken ? global.config.signdoc.getPdf + '/' + result.tokens[0].token : '');
    result.isSigned = app.isProposalSigned;
    result.auth = commonApp.genSignDocAuth(app._id + '_' + commonApp.PDF_NAME.BI, now);
    result.docts = now;

    return result;
  });
};

var _prepareAppFormProposerPdf = function(session, app, now, bundle) {

  let result = {
    docid: '',
    pdfStr: '',
    attUrl: '',
    isSigned: false,
    auth: '',
    docts: 0,
    agentSignFields: [],
    clientSignFields: [],
    tokens: []
  };

  logger.log('INFO: _prepareAppFormProposerPdf', app._id);
  // let isPhSameAsLa = app.applicationForm.values.proposer.extra.isPhSameAsLa === 'Y';
  let hasTrustedIndividual = app.applicationForm.values.proposer.extra.hasTrustedIndividual === 'Y';
  let signatureWidth = 0;
  //no signature for LA age < 18

  //Signature box width
  let numOfSignature = 2; //agent & proposor
  if (hasTrustedIndividual) {
    numOfSignature += 1;
  }
  switch (numOfSignature) {
    case 4:
      signatureWidth = 0.084;
      break;
    case 3:
      // signatureWidth = 0.118;
      signatureWidth = APP_FORM_SIGN_BOX_WIDTH.THREE_BOXES;
      break;
    default:
      // signatureWidth = 0.188;
      signatureWidth = APP_FORM_SIGN_BOX_WIDTH.TWO_BOXES;
  }


  return new Promise((resolve, reject) => {
    fDao.getAttachment(app._id, commonApp.PDF_NAME.eAPP, function(attAppForm){
      if (attAppForm.success) {
        resolve(attAppForm);
      } else {
        reject(new Error('Fail to get attachment ' + app._id + '/' + commonApp.PDF_NAME.eAPP + ' ' + _.get(attAppForm, 'error')));
      }
    });
  }).then((attAppForm) => {
    //prepare result
    result.agentSignFields.push(
      [
        Object.assign({}, APP_FORM_SIGNDOC_SIGN_FIELD, {
          name:app.quotation.agent.name,
          searchTag:'ADVISOR_SIGNATURE',
          width: signatureWidth
        })
      ]
    );
    let clientSignFields = [
      Object.assign({}, APP_FORM_SIGNDOC_SIGN_FIELD, {
        name: app.applicationForm.values.proposer.personalInfo.fullName,
        searchTag:'PROP_SIGNATURE',
        width: signatureWidth
      })
    ];

    if (hasTrustedIndividual) {
      clientSignFields.push(Object.assign({}, APP_FORM_SIGNDOC_SIGN_FIELD, {
        name: _.get(app, 'applicationForm.values.proposer.declaration.trustedIndividuals.fullName'),
        searchTag:'TI_SIGNATURE',
        width: signatureWidth
      }));
    }

    result.clientSignFields.push(clientSignFields);

    let isGenToken = app.isAppFormProposerSigned || app.isProposalSigned;
    if (isGenToken) {
      result.tokens.push(tokenUtils.createPdfToken(app._id, commonApp.PDF_NAME.eAPP, now, session.loginToken));
    }

    result.docid = app._id + '_' + commonApp.PDF_NAME.eAPP;
    result.pdfStr = (!app.isAppFormProposerSigned ? attAppForm.data : '');
    result.attUrl = (isGenToken ? global.config.signdoc.getPdf + '/' + result.tokens[0].token : '');
    result.isSigned = app.isAppFormProposerSigned;
    result.auth = commonApp.genSignDocAuth(app._id + '_' + commonApp.PDF_NAME.eAPP, now);
    result.docts = now;

    return result;
  });
};

var _prepareAppFormInsuredPdf = function(session, app, now, bundle, iCid) {

  const attName = _getAppFormAttachmentName(iCid);

  let result = {
    docid: '',
    pdfStr: '',
    attUrl: '',
    isSigned: false,
    auth: '',
    docts: 0,
    agentSignFields: [],
    clientSignFields: [],
    tokens: []
  };

  logger.log('INFO: _prepareAppFormInsuredPdf', app._id, '(' + iCid + ')');
  // let isPhSameAsLa = app.applicationForm.values.proposer.extra.isPhSameAsLa === 'Y';
  let iCids = _.get(app, 'iCids', []);
  let iCidIndex = iCids.indexOf(iCid);
  let isAppFormProposerSigned = _.get(app, 'isAppFormProposerSigned');
  let isAppFormInsuredSigned = _.get(app, `isAppFormInsuredSigned[${iCidIndex}]`);
  let isAppFormInsuredCompleted = _.get(app, `applicationForm.values.insured[${iCidIndex}].extra.isCompleted`, false);

  // let isSignedByLegalGuardian = app.applicationForm.values.insured.length > 0 && commonApp.checkIsLaSignedByProposer(app, iCidIndex);
  let hasTrustedIndividual = app.applicationForm.values.proposer.extra.hasTrustedIndividual === 'Y';
  let signatureWidth = 0;
  //no signature for LA age < 18

  //Signature box width
  let numOfSignature = 2; //agent & proposor
  if (hasTrustedIndividual) {
    numOfSignature += 1;
  }
  switch (numOfSignature) {
    case 4:
      signatureWidth = 0.084;
      break;
    case 3:
      // signatureWidth = 0.118;
      signatureWidth = APP_FORM_SIGN_BOX_WIDTH.THREE_BOXES;
      break;
    default:
      // signatureWidth = 0.188;
      signatureWidth = APP_FORM_SIGN_BOX_WIDTH.TWO_BOXES;
  }

  return new Promise((resolve, reject) => {
    logger.log('DEBUG: _prepareAppFormInsuredPdf', app._id, '(' + iCid + ')', iCidIndex, isAppFormInsuredCompleted);
    if (iCidIndex < 0 || isAppFormProposerSigned || !isAppFormInsuredCompleted) {
      resolve({});
    } else {
      fDao.getAttachment(app._id, attName, function(attAppForm){
        if (attAppForm.success) {
          resolve(attAppForm);
        } else {
          reject(new Error('Fail to get attachment ' + app._id + '/' + attName + ' ' + _.get(attAppForm, 'error')));
        }
      });
    }
  }).then((attAppForm) => {
    //prepare result
    result.agentSignFields.push(
      [
        Object.assign({}, APP_FORM_SIGNDOC_SIGN_FIELD, {
          name:app.quotation.agent.name,
          searchTag:'ADVISOR_SIGNATURE',
          width: signatureWidth
        })
      ]
    );
    let clientSignFields = [
      Object.assign({}, APP_FORM_SIGNDOC_SIGN_FIELD, {
        name: _.get(app, 'applicationForm.values.proposer.personalInfo.fullName', ''),
        searchTag:'PROP_SIGNATURE',
        width: signatureWidth
      })
    ];

    if (hasTrustedIndividual) {
      clientSignFields.push(Object.assign({}, APP_FORM_SIGNDOC_SIGN_FIELD, {
        name: _.get(app, 'applicationForm.values.proposer.declaration.trustedIndividuals.fullName'),
        searchTag:'TI_SIGNATURE',
        width: signatureWidth
      }));
    }

    clientSignFields.push(Object.assign({}, APP_FORM_SIGNDOC_SIGN_FIELD, {
      name: _.get(app, `applicationForm.values.insured[${iCidIndex}].personalInfo.fullName`, ''),
      searchTag:'LA_SIGNATURE',
      width: signatureWidth
    }));

    result.clientSignFields.push(clientSignFields);

    let isGenToken = isAppFormProposerSigned && isAppFormInsuredCompleted;
    if (isGenToken) {
      result.tokens.push(tokenUtils.createPdfToken(app._id, attName, now, session.loginToken));
    }

    result.docid = app._id + '_' + attName;
    result.pdfStr = (!isAppFormProposerSigned ? _.get(attAppForm, 'data', '') : '');
    result.attUrl = (isGenToken ? global.config.signdoc.getPdf + '/' + result.tokens[0].token : '');
    result.isSigned = isAppFormInsuredSigned;
    result.auth = commonApp.genSignDocAuth(app._id + '_' + attName, now);
    result.docts = now;

    return result;
  });
};

//=ApplicationHandler.getSignatureStatusFromCb
var _getSignatureInitUrl = function(session, appId){
  const isFaChannel = session.agent.channel === 'FA';
  const now = moment().valueOf();

  let result = {
    success: false,
    signature: {
      isFaChannel: isFaChannel,
      signingTabIdx: 0,
      isSigningProcess:[],
      numOfTabs: 0,
      pdfStr:[],
      attUrls:[],
      isSigned: [],
      agentSignFields:[],
      clientSignFields:[],
      signDocConfig: {
        auths:[],
        docts:[],
        ids:[],
        postUrl: global.config.signdoc.postUrl,
        resultUrl: global.config.signdoc.resultUrl,
        dmsId: global.config.signdoc.dmsid
      }
    },
    warningMsg: {
      msgCode: 0
    }
  };

  logger.log('INFO: _getSignatureInitUrl', appId);

  return _application.getApplication(appId).then((app) => {
    return bDao.getCurrentBundle(app.pCid).then((bundle) => {
      if (bundle && !bundle.error) {
        return {
          application: app,
          bundle: bundle
        };
      } else {
        throw new Error('Fail to get Bundle ' + _.get(bundle, 'id'), _.get(bundle, 'error'));
      }
    });
  }).then((cache) => {
    let { application, bundle } = cache;
    let preparePromise = [];
    let iCids = _.get(application, 'iCids');

    return _application.checkCrossAge(application).then((caRes) => {
      if (caRes.success && caRes.status !== commonApp.CROSSAGE_STATUS.NO_CROSSAGE) {
        result.warningMsg.msgCode = caRes.status;
      }

      if (_.get(caRes, 'crossedAgeCid.length', 0) > 0) {
        return caRes.crossedAgeCid;
      }

      return [];
    }).then((crossedAgeCid) => {
      if (crossedAgeCid.length === 0) {
        return;
      }

      let lastUpdateDate = moment().toISOString();
      return new Promise((resolve, reject) => {
        application.isCrossAge = true;
        application.lastUpdateDate = lastUpdateDate;
        appDao.upsertApplication(application.id, application, (resp) => {
          if (resp && !resp.error) {
            application._rev = resp.rev;
            resolve();
          } else {
            reject(new Error('Fail to update Application ' + result.application.id + ' ' + _.get(resp, 'error')));
          }
        });
      }).then(() => {
        let childIds = [];
        _.forEach(crossedAgeCid, (iCid) => {
          let mappingList = application.iCidMapping[iCid];
          _.forEach(mappingList, (mapping) => {
            childIds.push(mapping.applicationId);
          });
        });

        return _application.getChildApplications(childIds).then((childApps) => {
          return Promise.all(childApps.map((childApp) => {
            childApp.isCrossAge = true;
            childApp.lastUpdateDate = lastUpdateDate;

            return new Promise((resolve, reject) => {
              appDao.upsertApplication(childApp._id, childApp, (resp) => {
                if (resp && !resp.error) {
                  resolve();
                } else {
                  reject(new Error('Fail to update Application ' + childApp._id + ' ' + _.get(resp, 'error')));
                }
              });
            });
          }));
        });
      });
    }).then(() => {
      if (!isFaChannel) {
        preparePromise.push(_prepareFnaReportPdf(session, application, now, bundle));
      }
      preparePromise.push(_prepareProposalPdf(session, application, now, bundle));
      preparePromise.push(_prepareAppFormProposerPdf(session, application, now, bundle));
      _.forEach(iCids, (iCid) => {
        preparePromise.push(_prepareAppFormInsuredPdf(session, application, now, bundle, iCid));
      });

      return Promise.all(preparePromise).then((results) => {
        let stopUpdate = false;
        let numOfMandTabs = results.length - iCids.length;
        let tokens = [];

        _.forEach(results, (res, index) => {
          let isSigning = false;
          let iCidIndex = index - numOfMandTabs;
          let isCompleted = _.get(application, `applicationForm.values.insured[${iCidIndex}].extra.isCompleted`, false);

          if (index < numOfMandTabs || application.isAppFormProposerSigned && isCompleted) {
            isSigning = res.attUrl.length > 0 && !res.isSigned;
          }

          if (isSigning && !stopUpdate) {
            result.signature.signingTabIdx = index;
            stopUpdate = true;
          }
          result.signature.isSigningProcess.push(isSigning);
          result.signature.numOfTabs ++;
          result.signature.pdfStr.push(res.pdfStr);
          result.signature.attUrls.push(res.attUrl);
          result.signature.isSigned.push(res.isSigned);
          result.signature.agentSignFields = result.signature.agentSignFields.concat(res.agentSignFields);
          result.signature.clientSignFields = result.signature.clientSignFields.concat(res.clientSignFields);
          result.signature.signDocConfig.auths.push(res.auth);
          result.signature.signDocConfig.docts.push(res.docts);
          result.signature.signDocConfig.ids.push(res.docid);

          tokens = tokens.concat(res.tokens);
        });

        return new Promise((resolve) => {
          tokenUtils.setPdfTokensToRedis(tokens, () => {
            result.success = true;
            resolve(result);
          });
        });
      });
    });
  });
};

//=ApplicationHandler.getSignedPdfFromSignDocPost
module.exports.saveSignedPdfFromSignDoc = function(appId, attId, pdfData, callback) {
  let cache = {
    application: {},
    bundle: {}
  };

  logger.log('INFO: saveSignedPdfFromSignDoc - start', appId);

  _application.getApplication(appId).then((app) => {
    return bDao.getCurrentBundle(app.pCid).then((bundle) => {
      if (bundle && !bundle.error) {
        cache.application = app;
        cache.bundle = bundle;
        return bundle;
      } else {
        throw new Error('Fail to get Bundle ' + _.get(bundle, 'id'), _.get(bundle, 'error'));
      }
    });
  }).then((bundle) => {
    if (attId === commonApp.PDF_NAME.FNA) {
      //handle fnaReport
      return new Promise((resolve, reject) => {
        dao.uploadAttachmentByBase64(bundle._id, attId, bundle._rev, pdfData, 'application/pdf', function(res) {
          if (res && !res.error) {
            cache.bundle._rev = res.rev;
            resolve(bundle);
          } else {
            reject(new Error('Fail to update Fna Report Pdf to bundle ' + _.get(res, 'error')));
          }
        });
      }).then(() => {
        return bDao.updateStatus(cache.application.pCid, bDao.BUNDLE_STATUS.SIGN_FNA);
      }).then((newBundle)=>{
        cache.bundle = newBundle;
        return new Promise((resolve, reject) => {
          cDao.setIsProfileCanDel(cache.application.pCid, (res) => {
            if (res.profile && !res.profile.error) {
              cache.profile = res.profile;
              resolve(res.profile);
            } else {
              reject(new Error('Fail to update profile ' + _.get(res, 'error')));
            }
          });
        });
      }).then((profile) => {
        return new Promise((resolve, reject) => {
          let today = new Date();
          cache.bundle.profile = profile;
          cache.bundle.isFnaReportSigned = true;
          cache.bundle.fnaSignDate = `${today.getDate()}/${today.getMonth() + 1}/${today.getFullYear()}`;
          logger.log(`INFO: BundleUpdate - [cid:${_.get(cache.application, 'pCid')}; bundleId:${_.get(cache.bundle, 'id')}; fn:saveSignedPdfFromSignDoc]`);
          bDao.updateBundle(cache.bundle, function(res) {
            if (res && !res.error) {
              resolve(cache.application);
            } else {
              reject(new Error('Fail to update bundle ' + _.get(res, 'error')));
            }
          });
        });
      });
    } else if (attId === commonApp.PDF_NAME.BI) {
      //handle proposal
      return new Promise((resolve, reject) => {
        quotDao.getQuotation(cache.application.quotationDocId, (quot) => {
          if (quot && !quot.error) {
            resolve(quot);
          } else {
            reject(new Error('Fail to get quotation ' + _.get(quot, 'error')));
          }
        });
      }).then((quot) => {
        return new Promise((resolve, reject) => {
          quotDao.uploadSignedProposal(cache.application.quotationDocId, pdfData).then((res) => {
            if (res && !res.error) {
              resolve();
            } else {
              reject(new Error('Fail to update proposal pdf to quotation ' + _.get(res, 'error')));
            }
          });
        }).then(() => {
          return bDao.updateStatus(cache.application.pCid, bDao.BUNDLE_STATUS.SIGN_FNA).then((newBundle) => {
            return cache.application;
          });
        });
      });
    } else {
      //handle appForm
      return new Promise((resolve, reject) => {
        dao.uploadAttachmentByBase64(cache.application._id, attId, cache.application._rev, pdfData, 'application/pdf', function(res) {
          if (res && !res.error) {
            cache.application._rev = res.rev;
            resolve(cache.application);
          } else {
            reject(new Error('Fail to update App form Proposer Pdf to application ' + _.get(res, 'error')));
          }
        });
      }).then((app) => {
        if (attId === commonApp.PDF_NAME.eAPP) {
          //update bundle status to Fully Signed
          logger.log('INFO: saveSignedPdfFromSignDoc - update bundle status to fully signed', appId);
          return bDao.updateStatus(app.pCid, bDao.BUNDLE_STATUS.FULL_SIGN).then((newBundle)=>{
            _.forEach(newBundle.applications, (appItem) => {
              if (appItem.applicationDocId === app._id) {
                appItem.isFullySigned = true;
                return false;
              }
            });

            return new Promise((resolve, reject) => {
              logger.log(`INFO: BundleUpdate - [cid:${app.pCid}; bundleId:${_.get(newBundle, 'id')}; fn:saveSignedPdfFromSignDoc]`);
              bDao.updateBundle(newBundle, function(upRes) {
                if (upRes && !upRes.error) {
                  resolve(app);
                } else {
                  reject(new Error('Fail to update bundle to fully signed status'));
                }
              });
            });
          }).catch((error)=>{
            logger.error('ERROR: saveSignedPdfFromSignDoc - Fail to update bundle status', appId, error);
            throw error;
          });
        } else {
          return app;
        }
      });
    }
  }).then((app) => {
    let now = moment().toISOString();
    let childAppIds = [];

    let upPromise = [];
    if (attId === commonApp.PDF_NAME.FNA) {
      app.isStartSignature = true;
    } else if (attId === commonApp.PDF_NAME.BI) {
      // in FA channel, there is no FNA, that's why proposal also need to set isStartSignature
      app.isStartSignature = true;
      app.isProposalSigned = true;
      app.biSignedDate = now;
      childAppIds = _.get(app, 'childIds');
    } else if (attId === commonApp.PDF_NAME.eAPP) {
      app.isFullySigned = true;
      app.isAppFormProposerSigned = true;
      if (_.get(app, 'applicationForm.values.proposer.extra.isPhSameAsLa') === 'Y') {
        childAppIds = app.iCidMapping[app.pCid].map((mapping) => {
          return mapping.applicationId;
        });
      }
    } else if (attId.startsWith(commonApp.PDF_NAME.eAPP) && attId.length > commonApp.PDF_NAME.eAPP.length) {
      let cid = _.replace(attId, commonApp.PDF_NAME.eAPP, '');
      childAppIds = app.iCidMapping[cid].map((mapping) => {
        return mapping.applicationId;
      });

      let upIndex = app.iCids.indexOf(cid);
      if (upIndex >= 0) {
        app.isAppFormInsuredSigned[upIndex] = true;
      }
    }
    app.applicationSignedDate = now;
    app.lastUpdateDate = now;

    _application.updateApplicationCompletedStep(app);

    upPromise.push(new Promise((resolve, reject) => {
      appDao.upsertApplication(app._id, app, function(upRes) {
        if (upRes && !upRes.error) {
          resolve(upRes);
        } else {
          reject(new Error('Fail to update application ' + _.get(upRes, 'error')));
        }
      });
    }));

    _.forEach(childAppIds, (cAppId) => {
      upPromise.push(
        new Promise((resolve, reject) => {
          _application.getApplication(cAppId).then((cApp) => {
            _.union(cApp.parentAttachmentIds, [attId]);
            cApp.lastUpdateDate = now;
            if (attId === commonApp.PDF_NAME.BI) {
              cApp.biSignedDate = _.get(app, 'biSignedDate');
            } else if (attId === commonApp.PDF_NAME.eAPP || attId === _getAppFormAttachmentName(_.get(cApp, 'iCid'))) {
              cApp.applicationSignedDate = now;
            }
            cApp.isFullySigned = _.get(app, 'isFullySigned');
            appDao.upsertApplication(cAppId, cApp, (upRes) => {
              if (upRes && !upRes.error) {
                resolve(upRes);
              } else {
                reject(new Error('Fail to update child application ' + _.get(upRes, 'error')));
              }
            });
          });
        })
      );
    });

    return Promise.all(upPromise);
  }).then((results) => {
    logger.log('INFO: saveSignedPdfFromSignDoc - end [RETURN=100]', appId);
    callback({success: true, id: appId});
  }).catch((error) => {
    logger.error('ERROR: saveSignedPdfFromSignDoc - end [RETURN=-100]', appId);
    callback({success: false, id: appId});
  });
};

//=ApplicationHandler.getUpdatedAttachmentUrl
module.exports.getSignatureUpdatedUrl = function(data, session, callback) {
  const isFaChannel = session.agent.channel === 'FA';
  const { sdwebDocid, tabIdx } = data;
  const ids = commonApp.getIdFromSignDocId(sdwebDocid);
  const { docId, attId } = ids;
  const attUrl = global.config.signdoc.getPdf;
  const now = moment().valueOf();
  let nextTabIdx = tabIdx;
  let isSigned = [];
  let isSigningProcess = [];

  logger.log('INFO: getSignatureUpdatedUrl - start', docId);

  _application.getApplication(docId).then((app) => {
    return bDao.getCurrentBundle(app.pCid).then((bundle) => {
      if (bundle && !bundle.error) {
        return {
          application: app,
          bundle: bundle
        };
      } else {
        throw new Error('Fail to get Bundle ' + _.get(bundle, 'id'), _.get(bundle, 'error'));
      }
    });
  }).then((cache) => {
    let pdfTokens = [];
    let tokenIndexMap = {};
    let {application, bundle} = cache;

    let _setTokensArrayAndIndexMap = function(pdfToken, tokens, tokenMap, tabIndex) {
      tokens.push(pdfToken);
      tokenMap[pdfToken.token] = tabIndex;
    };

    if (!isFaChannel) {
      isSigned.push(bundle.isFnaReportSigned);
    }
    isSigned.push(application.isProposalSigned);
    isSigned.push(application.isAppFormProposerSigned);
    isSigned = isSigned.concat(application.isAppFormInsuredSigned);

    isSigningProcess = _.times(isSigned.length, _.constant(false));


    if (attId === commonApp.PDF_NAME.FNA) {
      nextTabIdx = tabIdx + 1;
      isSigningProcess[1] = true;

      _setTokensArrayAndIndexMap(tokenUtils.createPdfToken(application.bundleId, commonApp.PDF_NAME.FNA, now, session.loginToken), pdfTokens, tokenIndexMap, tabIdx);
      _setTokensArrayAndIndexMap(tokenUtils.createPdfToken(application.quotationDocId, commonApp.PDF_NAME.BI, now, session.loginToken), pdfTokens, tokenIndexMap, nextTabIdx);
    } else if (attId === commonApp.PDF_NAME.BI) {
      nextTabIdx = tabIdx + 1;
      isSigningProcess[(isFaChannel ? 1 : 2)] = true;

      _setTokensArrayAndIndexMap(tokenUtils.createPdfToken(application.quotationDocId, commonApp.PDF_NAME.BI, now, session.loginToken), pdfTokens, tokenIndexMap, tabIdx);
      _setTokensArrayAndIndexMap(tokenUtils.createPdfToken(application._id, commonApp.PDF_NAME.eAPP, now, session.loginToken), pdfTokens, tokenIndexMap, nextTabIdx);
    } else if (attId.startsWith(commonApp.PDF_NAME.eAPP)) {
      nextTabIdx = tabIdx;

      _setTokensArrayAndIndexMap(tokenUtils.createPdfToken(application._id, attId, now, session.loginToken), pdfTokens, tokenIndexMap, tabIdx);

      if (application.isAppFormInsuredSigned.length > 0 && application.isAppFormInsuredSigned.indexOf(false) >= 0) {
        let numOfMandTabs = isFaChannel ? 2 : 3;
        let skip = false;
        _.forEach(application.isAppFormInsuredSigned, (isInsuredSigned, i) => {
          let isCompleted = _.get(application, `applicationForm.values.insured[${i}].extra.isCompleted`, false);
          if (isCompleted && !isInsuredSigned) {
            let insuredTabIndex = numOfMandTabs + i;
            isSigningProcess[insuredTabIndex] = true;
            _setTokensArrayAndIndexMap(tokenUtils.createPdfToken(application._id, _getAppFormAttachmentName(application.iCids[i]), now, session.loginToken), pdfTokens, tokenIndexMap, insuredTabIndex);

            if (!skip) {
              nextTabIdx = insuredTabIndex;
              skip = true;
            }
          }
        });
      }
    }

    return new Promise((resolve) => {
      tokenUtils.setPdfTokensToRedis(pdfTokens, () => {
        let newAttUrls = [];
        _.forEach(pdfTokens, (pdfToken) => {
          let { token } = pdfToken;
          newAttUrls.push({
            index: tokenIndexMap[token],
            attUrl: attUrl + '/' + token
          });
        });
        resolve(newAttUrls);
      });
    });
  }).then((newAttUrls) => {
    callback({success: true, newAttUrls, nextTabIdx, isSigned, isSigningProcess, isFaChannel});
  }).catch((error) => {
    logger.error('ERROR: getSignatureUpdatedUrl - end [RETURN=-100]', docId, error);
    // callback({success: false});
  });
};

module.exports.getSignatureInitUrl = _getSignatureInitUrl;
module.exports.getAppFormAttachmentName = _getAppFormAttachmentName;
