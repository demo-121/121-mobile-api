const _               = require('lodash');

const {getProfile}    = require('./common');
const quotDao         = require('../../cbDao/quotation');
const needDao         = require('../../cbDao/needs');
const {
  formatDate,
  parseDate,
  formatDatetime,
  parseDatetime
} = require('../../../common/DateUtils');
const {getAgeByMethod, getOccupationClass, getProductId} = require('../../../common/ProductUtils');
const {getProductSuitability} = require('../../cbDao/product');

const getProposerFields = function (proposer, basicPlan, currentDate) {
  return {
    pCid:             proposer._id,
    pFullName:        proposer.fullName,
    pFirstName:       proposer.firstName,
    pLastName:        proposer.lastName,
    pGender:          proposer.gender,
    pDob:             proposer.dob,
    pAge:             getAgeByMethod(basicPlan.calcAgeMethod,
                        currentDate, parseDate(proposer.dob)),
    pResidence:       proposer.residenceCountry,
    pResidenceCity:   proposer.residenceCity,
    pSmoke:           proposer.isSmoker,
    pEmail:           proposer.email,
    pOccupation:      proposer.occupation,
    pOccupationClass: getOccupationClass(basicPlan, proposer.occupation)
  };
};

const getInsuredFields = function (insured, basicPlan, currentDate) {
  return {
    iCid:             insured._id,
    iFullName:        insured.fullName,
    iFirstName:       insured.firstName,
    iLastName:        insured.lastName,
    iGender:          insured.gender,
    iResidenceCity:   insured.residenceCity,
    iDob:             insured.dob,
    iAge:             getAgeByMethod(basicPlan.calcAgeMethod,
                        currentDate, parseDate(insured.dob)),
    iResidence:       insured.residenceCountry,
    iSmoke:           insured.isSmoker,
    iEmail:           insured.email,
    iOccupation:      insured.occupation,
    iOccupationClass: getOccupationClass(basicPlan, insured.occupation)
  };
};

const getCommonQuotFields = (basicPlan, agent, companyInfo, proposer, currentDate) => {
  return Object.assign({
    type: 'quotation',

    // reference fields, used in eBI/eApp
    baseProductCode:  basicPlan.covCode,
    baseProductId:    basicPlan._id,
    baseProductName:  basicPlan.covName,
    productLine:      basicPlan.productLine,
    budgetRules:      basicPlan.budgetRules,
    productVersion:   basicPlan.productVersion,

    compCode:         agent.compCode,
    agentCode:        agent.agentCode,
    dealerGroup:      agent.channel,
    agent: {
      agentCode:      agent.agentCode,
      name:           agent.name,
      dealerGroup:    agent.channel,
      company:        agent.channel.type === 'AGENCY' ?
                        companyInfo.compName : agent.company,
      tel:            agent.tel,
      mobile:         agent.mobile,
      email:          agent.email
    }
  }, getProposerFields(proposer, basicPlan, currentDate));
};

const getQuotFields = (basicPlan, agent, companyInfo, proposer, insured, ccy, currentDate) => {
  return Object.assign(getCommonQuotFields(basicPlan, agent, companyInfo, proposer, currentDate), {
    sameAs:           insured._id === proposer._id ? 'Y' : 'N',
    ccy:              getQuotCcy(basicPlan, insured, ccy),
    policyOptions:    {}
  }, getInsuredFields(insured, basicPlan, currentDate));
};

const genQuotation = (basicPlan, agent, companyInfo, proposer, insured, quickQuote, ccy, suitability, fna) => {
  const currentDate = new Date();
  return Object.assign(
    getQuotFields(basicPlan, agent, companyInfo, proposer, insured, ccy, currentDate), {
    quickQuote:       quickQuote,
    fund:             null, // available only for investment linked products
    extraFlags:       getQuotationFlags(insured, proposer, suitability, fna),
    isBackDate:       'N',
    riskCommenDate:   formatDate(currentDate),
    createDate:       formatDatetime(currentDate),
    lastUpdateDate:   formatDatetime(currentDate)
  });
};

const genBasicQuotation = (basicPlan, agent, companyInfo, proposer, insured) => {
  const currentDate = new Date();
  return getQuotFields(basicPlan, agent, companyInfo, proposer, insured, null, currentDate);
};

const genShieldQuotation = (basicPlan, agent, companyInfo, proposer, quickQuote) => {
  const currentDate = new Date();
  return Object.assign(
    getCommonQuotFields(basicPlan, agent, companyInfo, proposer, currentDate), {
    quickQuote:       quickQuote,
    quotType:         'SHIELD',
    insureds:         {},
    isBackDate:       'N',
    riskCommenDate:   formatDate(currentDate),
    createDate:       formatDatetime(currentDate),
    lastUpdateDate:   formatDatetime(currentDate)
  });
};

const updateClientFields = function (quotation, planDetails, requireFNA) {
  const bpDetail = planDetails[quotation.baseProductCode];
  const currentDate = new Date();
  if (quotation.quotType === 'SHIELD') {
    let promises = [];
    promises.push(getProfile(quotation.pCid));
    _.each(quotation.insureds, (subQuot) => {
      promises.push(getProfile(subQuot.iCid));
    });
    return Promise.all(promises).then((profiles) => {
      _.each(profiles, (profile) => {
        if (profile.cid === quotation.pCid) {
          Object.assign(quotation, getProposerFields(profile, bpDetail, currentDate));
        }
        if (quotation.insureds[profile.cid]) {
          Object.assign(quotation.insureds[profile.cid], getInsuredFields(profile, bpDetail, currentDate));
        }
      });
    });
  } else {
    let promises = [];
    promises.push(getProfile(quotation.pCid));
    promises.push(getProfile(quotation.iCid));
    promises.push(getProductSuitability());
    if (requireFNA) {
      promises.push(needDao.getItem(quotation.pCid, needDao.ITEM_ID.NA));
    }
    return Promise.all(promises).then(([proposer, insured, suitability, fna]) => {
      Object.assign(quotation,
        getProposerFields(proposer, bpDetail, currentDate),
        getInsuredFields(insured, bpDetail, currentDate),
        {
          extraFlags: getQuotationFlags(insured, proposer, suitability, fna)
        }
      );
    });
  }
};

const getQuotCcy = (basicPlan, insured, ccy) => {
  var quotCcy = '';
  if (basicPlan.ccy instanceof Array) {
    let currency = _.find(basicPlan.currencies,
      (c) => c.country === '*' || c.country === insured.residenceCountry);

    if (currency) {
      if (_.find(currency.ccy, (c) => c === ccy)) {
        quotCcy = ccy;
      }
    }
  }
  return quotCcy;
};

const getQuotationFlags = function (insured, proposer, suitability, fna) {
  var flags = {};
  var iNR = getNationalityCategory(insured) + getResidenceCategory(insured);
  var iResult = suitability.nationalityResidence[iNR];
  var pNR = getNationalityCategory(proposer) + getResidenceCategory(proposer);
  var pResult = suitability.nationalityResidence[pNR];

  flags.nationalityResidence = {
    iCategory: iNR,
    iRejected: (iResult && iResult.reject) ? 'Y' : 'N',
    pCategory: pNR,
    pRejected: (pResult && pResult.reject) ? 'Y' : 'N'
  };

  if (fna) {
    flags.fna = {};
    let ownerRa = _.get(fna, 'raSection.owner');
    if (ownerRa) {
      let passCka = _.get(fna, 'ckaSection.owner.passCka') === 'Y';
      if (passCka && ownerRa.selfSelectedriskLevel) {
        flags.fna.riskProfile = ownerRa.selfSelectedriskLevel;
      } else {
        flags.fna.riskProfile = ownerRa.assessedRL;
      }
    }
  }
  return flags;
};

const getNationalityCategory = function (profile) {
  if (profile.prStatus === 'Y') {
    return 'A';
  }
  var option = _.find(global.optionsMap.nationality.options,
    (opt) => opt.value === profile.nationality);
  return option && option.category;
};

const getResidenceCategory = function (profile) {
  var option = _.find(global.optionsMap.cityCategory.options,
    (opt) => opt.residency === profile.residenceCountry
    && (!opt.city || opt.city === profile.residenceCity));

    return option && option.category;
};

const checkNationalityResidence = function (suitability, role, nrKey) {
  var mapping = suitability.nationalityResidence[nrKey];
  if (mapping && mapping.showInQuot) {
    var messages = suitability.messages;
    if (messages[mapping.message + '_' + role]) {
      return messages[mapping.message + '_' + role];
    }
    return messages[mapping.message];
  }
  return null;
};

const getQuotationWarnings = function (basicPlan, quotation, suitability) {
  var warnings = [];
  if (quotation.isBackDate === 'N') {
    var currentDate = new Date();
    var pAgeCurr = getAgeByMethod(basicPlan.calcAgeMethod, currentDate,
                      parseDate(quotation.pDob));
    var iAgeCurr = getAgeByMethod(basicPlan.calcAgeMethod, currentDate,
                      parseDate(quotation.iDob));
    if (pAgeCurr !== quotation.pAge || iAgeCurr !== quotation.iAge) {
      warnings.push('Life assured\'s age has changed. This may result in increased Premium. System to auto-backdate the policy commencement date to Policy Illustration (PI) generation date.');
    }
  }

  if (quotation.extraFlags && quotation.extraFlags.nationalityResidence) {
    var warning = checkNationalityResidence(suitability, 'I',
          quotation.extraFlags.nationalityResidence.iCategory);

    if (warning) {warnings.push(warning);}

    if (quotation.iCid !== quotation.pCid) {
      warning = checkNationalityResidence(suitability, 'P',
          quotation.extraFlags.nationalityResidence.pCategory);
      if (warning) {
        warnings.push(warning);
      }
    }
  }
  let occupationClassByPlan = _.get(suitability, 'occupationClassByPlan');

  if (basicPlan.occupClassType && occupationClassByPlan[basicPlan.occupClassType]) {
      let occKeyObject = occupationClassByPlan[basicPlan.occupClassType];
      let matchedOccupation = _.find(global.optionsMap.occupation.options, option => option.value === quotation.iOccupation && option[basicPlan.occupClassType] === occKeyObject.messageKey);
      warning = (matchedOccupation) ? _.get(occKeyObject, 'warning') : undefined;

      if (warning) {
        warnings.push(warning);
      }
  }
  return warnings;
};

const hasCrossAge = function (quotation, bpDetail) {
  const currentDate = new Date();
  const pAgeCurr = getAgeByMethod(bpDetail.calcAgeMethod, currentDate, parseDate(quotation.pDob));
  if (quotation.quotType === 'SHIELD') {
    return pAgeCurr !== quotation.pAge || _.find(quotation.insureds, (subQuot) => {
      const iAgeCurr = getAgeByMethod(bpDetail.calcAgeMethod, currentDate, parseDate(subQuot.iDob));
      return iAgeCurr !== subQuot.iAge;
    });
  } else {
    const iAgeCurr = getAgeByMethod(bpDetail.calcAgeMethod, currentDate, parseDate(quotation.iDob));
    return pAgeCurr !== quotation.pAge || iAgeCurr !== quotation.iAge;
  }
};

const hasProfileUpdate = function (quotation, profiles) {
  const lastUpdateDate = parseDatetime(quotation.lastUpdateDate);
  return _.find(profiles, (profile) => parseDatetime(profile.lastUpdateDate) > lastUpdateDate);
};

const getFunds = function (compCode, fundCodes, paymentMethod, invOpt) {
  return new Promise((resolve) => {
    quotDao.queryFunds(compCode, fundCodes, paymentMethod, invOpt === 'mixedAssets', resolve);
  });
};

const transformShieldQuot = (quotation) => {
  const insureds = {};
  _.each(quotation.insureds, (subQuot, cid) => {
    if (subQuot.plans.length === 0) {
      insureds[cid] = subQuot;
    } else {
      insureds[cid] = [_.cloneDeep(subQuot), _.cloneDeep(subQuot)];
      const plans = subQuot.plans;
      insureds[cid][0].plans = [plans[0]];
      insureds[cid][1].plans = [_.slice(plans, 1)];
    }
  });
  const quot = _.cloneDeep(quotation);
  quot.insureds = insureds;
  return quot;
};

const getRelatedProfiles = (pCid) => {
  return getProfile(pCid).then((profile) => {
    let cids = [pCid];
    _.each(profile.dependants, (dependant) => {
      cids.push(dependant.cid);
    });
    return cids;
  }).then((dCids) => {
    return Promise.all(_.map(dCids, dCid => getProfile(dCid)));
  });
};

const getQuotClients = (quotation) => {
  let cids;
  if (quotation.quotType === 'SHIELD') {
    cids = [quotation.pCid];
    _.each(quotation.insureds, (subQuot) => {
      cids.push(subQuot.iCid);
    });
  } else {
    cids = [quotation.pCid, quotation.iCid];
  }
  return Promise.all(_.map(cids, (cid) => getProfile(cid)));
};

const updateQuot = function (quotation, planDetails, newQuot) {
  if (newQuot) {
    const currentDate = new Date();
    quotation.id = null;
    quotation.lastUpdateDate = formatDatetime(currentDate);
    quotation.createDate = formatDatetime(currentDate);
  }
  quotation.baseProductId = planDetails[quotation.baseProductCode]._id;

  // Remove campaign when requote and clone quotation 
  quotation.plans = _.map(quotation && quotation.plans, plan => {
    return _.omit(plan, ['campaignIds', 'campaignCodes']);
  });
  updateProductIds(quotation, planDetails);
};

const updateProductIds = function (quotation, planDetails) {
  let plans = quotation.plans;
  if (quotation.quotType === 'SHIELD') {
    plans = [];
    _.each(quotation.insureds, subQuot => {
      plans = plans.concat(subQuot.plans);
    });
  }
  _.each(plans, plan => {
    plan.productId = getProductId(planDetails[plan.covCode]);
  });
};

module.exports = {
  genQuotation,
  genBasicQuotation,
  genShieldQuotation,
  updateClientFields,
  getQuotationWarnings,
  hasCrossAge,
  hasProfileUpdate,
  getFunds,
  transformShieldQuot,
  getRelatedProfiles,
  getQuotClients,
  updateQuot,
  updateProductIds
};
