const config = require('config');

module.exports.getConfig = (key) => {
  if (process.env.NODE_ENV !== 'dev') {
    const result = process.env[key] ? process.env[key] : null;
    return result;
  }
  return config.get(key);
};
