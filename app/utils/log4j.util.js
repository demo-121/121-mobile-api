const log4js = require('log4js');
const uuid = require('uuid/v1');

const log4j = log4js.getLogger();

// ALL < TRACE < DEBUG < INFO < WARN < ERROR < FATAL < MARK < OFF
module.exports.log = (level, info, requestUUID) => {
  const errorUuidValue = uuid();
  switch (level.toLocaleLowerCase()) {
    case 'all':
      log4j.all(`${info} ${requestUUID ? '[uuid: ' : ''} ${requestUUID || ''} ${requestUUID ? ']' : ''} `);
      break;
    case 'trace':
      log4j.trace(`${info} ${requestUUID ? '[uuid: ' : ''} ${requestUUID || ''} ${requestUUID ? ']' : ''} `);
      break;
    case 'debug':
      log4j.debug(`${info} ${requestUUID ? '[uuid: ' : ''} ${requestUUID || ''} ${requestUUID ? ']' : ''} `);
      break;
    case 'info':
      log4j.info(`${info} ${requestUUID ? '[uuid: ' : ''} ${requestUUID || ''} ${requestUUID ? ']' : ''} `);
      break;
    case 'warn':
      log4j.warn(`${info} ${requestUUID ? '[uuid: ' : ''} ${requestUUID || ''} ${requestUUID ? ']' : ''} `);
      break;
    case 'error':
      log4j.error(`[error-uuid: ${errorUuidValue}]${info} ${requestUUID ? '[uuid: ' : ''} ${requestUUID || ''} ${requestUUID ? ']' : ''} `);
      break;
    case 'fatal':
      log4j.fatal(`${info} ${requestUUID ? '[uuid: ' : ''} ${requestUUID || ''} ${requestUUID ? ']' : ''} `);
      break;
    case 'mark':
      log4j.mark(`${info} ${requestUUID ? '[uuid: ' : ''} ${requestUUID || ''} ${requestUUID ? ']' : ''} `);
      break;
    case 'off':
      log4j.off(`${info} ${requestUUID ? '[uuid: ' : ''} ${requestUUID || ''} ${requestUUID ? ']' : ''} `);
      break;
    default:
      log4j.info(`${info} ${requestUUID ? '[uuid: ' : ''} ${requestUUID || ''} ${requestUUID ? ']' : ''} `);
  }
};
