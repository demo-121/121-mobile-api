const axios = require('axios');

const commonUtil = require('../utils/common.util');
const log4jUtil = require('../utils/log4j.util');

module.exports.setPasswordForPdf = (_base64Pdf, _password, cb) => {
  log4jUtil.log('debug', `baseURL: ${commonUtil.getConfig('INTERNAL_API_DOMAIN')}`);
  axios.request({
    url: '/api/mobile/setPasswordForPdf',
    method: 'post',
    baseURL: `${commonUtil.getConfig('INTERNAL_API_DOMAIN')}`,
    data: {
      base64Pdf: _base64Pdf,
      password: _password,
    },
  }).then((result) => {
    cb(null, result.data);
  }).catch((error) => {
    if (error.response) {
      // The request was made and the server responded with a status code
      // that falls out of the range of 2xx
      log4jUtil.log('error', `case2a,error.response.data:${JSON.stringify(error.response.data)}`);
      log4jUtil.log('error', `case2a,error.response.status:${error.response.status}`);
      log4jUtil.log('error', `case2a,error.response.headers:${JSON.stringify(error.response.headers)}`);
      cb({
        data: error.response.data,
        status: error.response.status,
        headers: error.response.headers,
      }, null);
    } else if (error.request) {
      // The request was made but no response was received
      // `error.request` is an instance of XMLHttpRequest in the browser and an instance of
      // http.ClientRequest in node.js
      log4jUtil.log('error', `case2b,error.request:${JSON.stringify(error.request)}`);
      cb(error.request, null);
    } else {
      // Something happened in setting up the request that triggered an Error
      log4jUtil.log('error', `case2c,error.message:${JSON.stringify(error.message)}`);
      cb(error.message, null);
    }
  });
};

module.exports.validate = (email) => {
  const regex = /[_A-Za-z0-9-\\+]+(\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\.[A-Za-z0-9]+)*(\.[A-Za-z]{2,})/;
  if (regex.test(email)) {
    return true;
  }
  return false;
};

module.exports.send = (emailBody, cb) => {
  axios.request({
    url: '/ease-api/email/sendEmail',
    method: 'post',
    maxContentLength: 52428890,
    baseURL: `${commonUtil.getConfig('INTERNAL_API_DOMAIN')}`,
    data: emailBody,
  }).then((response) => {
    log4jUtil.log('info', 'case1');
    if (response.status === 204 || response.status === 200) {
      cb(null, {});
    } else {
      cb(response, null);
    }
  }).catch((error) => {
    log4jUtil.log('info', 'case2');
    if (error.response) {
      // The request was made and the server responded with a status code
      // that falls out of the range of 2xx
      log4jUtil.log('error', `case2a,error.response.data:${JSON.stringify(error.response.data)}`);
      log4jUtil.log('error', `case2a,error.response.status:${error.response.status}`);
      log4jUtil.log('error', `case2a,error.response.headers:${JSON.stringify(error.response.headers)}`);
      cb({
        data: error.response.data,
        status: error.response.status,
        headers: error.response.headers,
      }, null);
    } else if (error.request) {
      // The request was made but no response was received
      // `error.request` is an instance of XMLHttpRequest in the browser and an instance of
      // http.ClientRequest in node.js
      log4jUtil.log('error', `case2b,error.request:${JSON.stringify(error.request)}`);
      cb(error.request, null);
    } else {
      // Something happened in setting up the request that triggered an Error
      log4jUtil.log('error', `case2c,error.message:${JSON.stringify(error.message)}`);
      cb(error.message, null);
    }
  });
};
