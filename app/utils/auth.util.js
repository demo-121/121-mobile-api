const passport = require('passport');
const jwksClient = require('jwks-rsa');
const jwt = require('jsonwebtoken');
const passportHttpBearer = require('passport-http-bearer').Strategy;
const uuid = require('uuid/v1');
const axios = require('axios');
const NodeRSA = require('node-rsa');
const CryptoJS = require('crypto-js');

const log4jUtil = require('../utils/log4j.util');
const encryptionUtil = require('../utils/encryption.util');
const commonUtil = require('../utils/common.util');
const syncGatewayUtil = require('../utils/syncGateway.util');

module.exports.decryptRequestBody = (req, cb) => {
  if (req.body.dk && req.body.d0) {
    log4jUtil.log('info', 'Start decrypt request body');
    axios.request({
      url: '/api/mobile/queryRSAKeyPair',
      method: 'post',
      baseURL: `${commonUtil.getConfig('INTERNAL_API_DOMAIN')}`,
      params: { agentId: req.user.id },
    }).then((response) => {
      if (response.status === 200) {
        if (response.data && response.data.privateKey) {
          log4jUtil.log('info', `Get private key successfully. Private key: ${response.data.privateKey}`);
          const key = new NodeRSA();
          key.setOptions({ encryptionScheme: 'pkcs1' });
          key.importKey(response.data.privateKey);
          const aes256KeyFromApp = key.decrypt(req.body.dk, 'utf8');
          const decrypted = CryptoJS.AES.decrypt(req.body.d0, aes256KeyFromApp);
          const decryptedStr = decrypted.toString(CryptoJS.enc.Utf8);
          req.body = JSON.parse(decryptedStr);
          req.aes256KeyFromApp = aes256KeyFromApp;
          log4jUtil.log('info', 'Decrypt request body successfully');
          cb(null, req);
        } else {
          log4jUtil.log('error', 'Public key is not exist on response body');
          cb('Public key is not exist on response body');
        }
      } else {
        log4jUtil.log('error', 'Query private key error');
        cb('Query private key error');
      }
    }).catch((error) => {
      if (error.response) {
        log4jUtil.log('error', `Query private key error, data::${JSON.stringify(error.response.data)}`);
        log4jUtil.log('error', `Query private key error, status:${error.response.status}`);
        log4jUtil.log('error', `Query private key error, headers:${JSON.stringify(error.response.headers)}`);
        // The request was made and the server responded with a status code
        // that falls out of the range of 2xx
        cb({
          data: error.response.data,
          status: error.response.status,
          headers: error.response.headers,
        }, null);
      } else if (error.request) {
        log4jUtil.log('error', `Query private key error, error.response: ${error.request}`);
        // The request was made but no response was received
        // `error.request` is an instance of XMLHttpRequest in the browser and an instance of
        // http.ClientRequest in node.js
        cb({
          request: error.request,
        }, null);
      } else {
        log4jUtil.log('error', `Query private key error, error.response: ${error.message}`);
        // Something happened in setting up the request that triggered an Error=
        cb({
          message: error.request,
        }, null);
      }
    });
  } else {
    log4jUtil.log('info', 'No need decrypt request body');
    cb(null, req);
  }
};

module.exports.verifyToken = () => {
  const uuidValue = uuid();
  log4jUtil.log('info', 'Initializing authentication method', uuidValue);
  passport.use(new passportHttpBearer.Strategy((token, done) => {
    log4jUtil.log('info', '[1]Start verify token', uuidValue);
    log4jUtil.log('info', `[2]token:${token}`, uuidValue);
    log4jUtil.log('info', '[3]Start check token format', uuidValue);
    if (token && /\w+\.\w+\.\w+/.test(token)) {
      log4jUtil.log('info', '[3a]Check token format result: success', uuidValue);
      try {
        log4jUtil.log('info', '[3a1]Start decrypt', uuidValue);
        const firstDotIndex = token.indexOf('.');
        const jwtHeader = encryptionUtil.decryptByBase64(token.substring(0, firstDotIndex)).replace(/\0/g, '');
        const secondDotIndex = token.lastIndexOf('.');
        const jwtPayload = encryptionUtil.decryptByBase64(token.substring(firstDotIndex + 1, secondDotIndex)).replace(/\0/g, '');
        log4jUtil.log('info', '[3a2]Start decrypt', uuidValue);
        const agentId = JSON.parse(jwtPayload.toString()).sub;
        log4jUtil.log('info', `[4]Agent Id:${agentId}`, uuidValue);
        if (agentId) {
          // Only for dev fake env.
          // if (agentId === 'A1') {
          //   done(null, { id: 'AG90001D' });
          // } else if (agentId === 'A2') {
          //   done(null, { id: 'AG90003M' });
          // } else if (agentId === 'A3') {
          //   done(null, { id: 'AG90006A' });
          // } else {
          //   done(null, { id: 'S2195576J' });
          // }
          syncGatewayUtil.getDocFromSG(`U_${agentId}`, (err, result) => {
            if (err) {
              log4jUtil.log('error', `agent not exist, error: ${JSON.stringify(uuidValue)}`);
              done(null, false, { err: `agent not exist, error: ${JSON.stringify(uuidValue)}` });
            } else {
              log4jUtil.log('info', JSON.stringify(result));
              done(null, { id: agentId });
            }
          });
        } else {
          const kidValue = JSON.parse(jwtHeader.toString()).kid;
          const algorithm = JSON.parse(jwtHeader.toString()).alg;
          log4jUtil.log('info', `[5]Start request jwk_url: ${commonUtil.getConfig('MAAM_JWKS_URL')}`, uuidValue);
          const client = jwksClient({
            strictSsl: true,
            jwksUri: commonUtil.getConfig('MAAM_JWKS_URL'),
          });
          client.getSigningKey(kidValue, (err, result) => {
            if (err || !result) {
              log4jUtil.log('info', `[5a]Request jwk_url result: error, error: ${JSON.stringify(err)}`, uuidValue);
              done(null, false, err);
            } else {
              log4jUtil.log('info', `[5b]Request jwk_url result: success, result: ${JSON.stringify(result)}`, uuidValue);
              const signingKey = result.publicKey || result.rsaPublicKey;
              log4jUtil.log('info', '[6]Start verify token');
              jwt.verify(token, signingKey, {
                algorithms: [algorithm],
              }, (jwtVerifyError, jwtVerifyResult) => {
                if (jwtVerifyError) {
                  log4jUtil.log('info', `[6a]Verify token result: error, error: ${JSON.stringify(jwtVerifyError)}`, uuidValue);
                  done(null, false, err);
                } else if (jwtVerifyResult.sub) {
                  log4jUtil.log('info', `[6b]Verify token result: success, result:${JSON.stringify(jwtVerifyResult)}`, uuidValue);
                  done(null, { id: jwtVerifyResult.sub, requestUuid: uuidValue });
                } else {
                  log4jUtil.log('info', '[6c]Verify token result: error, error: Can not get sub', uuidValue);
                  done(null, false, { message: 'Can not get sub' });
                }
              });
            }
          });
        }
      } catch (error) {
        log4jUtil.log('info', `[3a3]Decrypt error: ${JSON.stringify(error)}`, uuidValue);
        done(null, false, error);
      }
    } else {
      log4jUtil.log('info', '[3b]Check token format result: error', uuidValue);
      done(null, false, { err: 'token not exists or format error' });
    }
  }));
};
