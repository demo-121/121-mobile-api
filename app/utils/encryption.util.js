const crypto = require('crypto-js');

module.exports.decryptByBase64 = (base64Text) => {
  const words = crypto.enc.Base64.parse(base64Text);
  const text = crypto.enc.Utf8.stringify(words);
  return text;
};
