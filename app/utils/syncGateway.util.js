const axios = require('axios');
const _ = require('lodash');

const commonUtil = require('./common.util');
const log4jUtil = require('../utils/log4j.util');

const getDocFromSG = (docId, cb) => {
  if (docId && typeof docId === 'string') {
    const url = `${commonUtil.getConfig('GATEWAY_URL')}:${commonUtil.getConfig('GATEWAY_PORT')}/${commonUtil.getConfig('GATEWAY_DBNAME')}/${docId}`;
    axios
      .get(url, {
        auth: {
          username: commonUtil.getConfig('GATEWAY_USER'),
          password: commonUtil.getConfig('GATEWAY_PW'),
        },
      })
      .then((response) => {
        if (response.status !== 200) {
          cb({
            status: 'error',
            exception: response,
          }, null);
        } else {
          cb(null, response.data);
        }
      })
      .catch((error) => {
        if (error.response) {
          // The request was made and the server responded with a status code
          // that falls out of the range of 2xx
          log4jUtil.log('error', `DocId: ${docId}`);
          log4jUtil.log('error', JSON.stringify(error.response.data));
          log4jUtil.log('error', error.response.status);
          log4jUtil.log('error', JSON.stringify(error.response.headers));
          if (error.response && error.response.status === 404) {
            cb({
              status: 'error',
              exception: `Doc (${docId}) is not exists.`,
            }, null);
          } else {
            cb({
              data: error.response.data,
              status: error.response.status,
              headers: error.response.headers,
            });
          }
        } else if (error.request) {
          // The request was made but no response was received
          // `error.request` is an instance of XMLHttpRequest in the browser and an instance of
          // http.ClientRequest in node.js
          log4jUtil.log('error', JSON.stringify(error.request));
          cb({ request: error.request }, null);
        } else {
          // Something happened in setting up the request that triggered an Error
          log4jUtil.log('error', 'Error', JSON.stringify(error.message));
          cb({ message: error.message }, null);
        }
      });
  } else {
    cb({
      status: 'error',
      message: 'Doc Id not exists or doc id is not a string',
    }, null);
  }
};

module.exports.getDocFromSG = getDocFromSG;

module.exports.updateDocToSG = (docData, cb) => {
  const id = '_id';
  if (docData && ((docData.type === 'approval' || docData.type === 'masterApproval') ? (docData.approvalCaseId || docData.masterApproval) : docData[id])) {
    const doc = _.cloneDeep(docData);
    doc.lstChgDate = new Date().getTime();
    let tempUrl = '';
    if (docData.type === 'approval' || docData.type === 'masterApproval') {
      tempUrl = `/${commonUtil.getConfig('GATEWAY_DBNAME')}/${docData.approvalCaseId}`;
    } else {
      tempUrl = `/${commonUtil.getConfig('GATEWAY_DBNAME')}/${docData[id]}`;
    }
    axios
      .request({
        baseURL: `${commonUtil.getConfig('GATEWAY_URL')}:${commonUtil.getConfig('GATEWAY_PORT')}`,
        url: tempUrl,
        method: 'put',
        auth: {
          username: commonUtil.getConfig('GATEWAY_USER'),
          password: commonUtil.getConfig('GATEWAY_PW'),
        },
        data: doc,
      })
      .then((response) => {
        if (response.data.ok) {
          // console.log('here,data:' + JSON.stringify(response.data));
          cb(null, response.data);
        } else {
          cb(response.data, null);
        }
      })
      .catch((error) => {
        if (error.response) {
          log4jUtil.log('error', {
            data: JSON.stringify(error.response.data),
            status: error.response.status,
            headers: JSON.stringify(error.response.headers),
          });
          // The request was made and the server responded with a status code
          // that falls out of the range of 2xx
          if (error.response.status === 409) {
            if (docData.type === 'approval' || docData.type === 'masterApproval') {
              getDocFromSG(docData.approvalCaseId, (err, result) => {
                cb(null, result);
              });
            } else {
              getDocFromSG(docData[id], (err, result) => {
                cb(null, result);
              });
            }
          } else {
            cb({
              data: error.response.data,
              status: error.response.status,
              headers: error.response.headers,
            }, null);
          }
        } else if (error.request) {
          log4jUtil.log('error', JSON.stringify(error.request));
          // The request was made but no response was received
          // `error.request` is an instance of XMLHttpRequest in the browser and an instance of
          // http.ClientRequest in node.js
          cb(error.request, null);
        } else {
          log4jUtil.log('error', JSON.stringify(error.message));
          // Something happened in setting up the request that triggered an Error
          cb(error.message, null);
        }
      });
  } else {
    cb({
      status: 'error',
      message: 'data id not exists.',
    }, null);
  }
};

module.exports.getView = (designName, viewName, params, callback) => {
  axios.request({
    url: `/${commonUtil.getConfig('GATEWAY_DBNAME')}/_design/${designName}/_view/${viewName}?key=${params}`,
    method: 'get',
    baseURL: `${commonUtil.getConfig('GATEWAY_URL')}:${commonUtil.getConfig('GATEWAY_PORT')}`,
    auth: {
      username: commonUtil.getConfig('GATEWAY_USER'),
      password: commonUtil.getConfig('GATEWAY_PW'),
    },
  }).then((response) => {
    if (response && response.data) {
      callback(null, response.data);
    } else {
      callback(`Get view ${designName} ${viewName} error`, null);
    }
  });
};

const getViewRange = (ddname, vname, start, end, params, callback) => {
  const query = {
    startkey: start || null,
    endkey: end || null,
  };

  if (params) {
    _.forEach(params, (p) => {
      query[p] = params[p];
    });
  }

  // get session
  try {
    const updateIndex = false; // ddname == 'quotation' || ddname == 'application';
    axios.request({
      url: `/${commonUtil.getConfig('GATEWAY_DBNAME')}/_design/${ddname}/_view/${vname}?${updateIndex ? 'stale=false' : 'stale=ok'}`,
      method: 'get',
      baseURL: `${commonUtil.getConfig('GATEWAY_URL')}:${commonUtil.getConfig('GATEWAY_PORT')}`,
      auth: {
        username: commonUtil.getConfig('GATEWAY_USER'),
        password: commonUtil.getConfig('GATEWAY_PW'),
      },
    }).then((response) => {
      callback(response.data);
    });
  } catch (ex) {
    callback(false);
  }
};

module.exports.getViewRange = getViewRange;

module.exports.updateViewIndex = (ddname, vname, callback) => {
  axios.request({
    url: `/${commonUtil.getConfig('GATEWAY_DBNAME')}/_design/${ddname}/_view/${vname}]`,
    method: 'get',
    baseURL: `${commonUtil.getConfig('GATEWAY_URL')}:${commonUtil.getConfig('GATEWAY_PORT')}`,
    auth: {
      username: commonUtil.getConfig('GATEWAY_USER'),
      password: commonUtil.getConfig('GATEWAY_PW'),
    },
  }).then((response) => {
    if (response.status === 200 && response.data && response.data.total_rows === 1) {
      callback(null, response.data.rows[0].value);
    } else {
      callback('Get agent error', null);
    }
  });
};

module.exports.getViewByKeys = (ddname, vname, keys, params, batchSize = 40) => {
  const rows = [];
  let promise = Promise.resolve();
  for (let i = 0; i < keys.length; i += batchSize) {
    const batchKeys = _.slice(keys, i, i + batchSize);
    const batchParams = Object.assign({}, params, {
      keys: `[${_.join(batchKeys, ',')}]`,
    });
    promise = promise.then(() => new Promise((resolve) => {
      getViewRange(ddname, vname, null, null, batchParams, (result) => {
        if (result && !result.error) {
          rows.push(result.rows);
        }
        resolve();
      });
    }));
  }
  return promise.then(() => ({
    total_rows: rows.length,
    rows,
  }));
};

module.exports.getAttachmentByBase64 = (docId, attachemtnId, cb) => {
  axios.request({
    baseURL: `${commonUtil.getConfig('GATEWAY_URL')}:${commonUtil.getConfig('GATEWAY_PORT')}`,
    url: `/${commonUtil.getConfig('GATEWAY_DBNAME')}/${docId}/${attachemtnId}`,
    method: 'GET',
    responseType: 'arraybuffer',
    auth: {
      username: commonUtil.getConfig('GATEWAY_USER'),
      password: commonUtil.getConfig('GATEWAY_PW'),
    },
  }).then((response) => {
    const data = Buffer.from(response.data, 'binary').toString('base64');
    cb(null, data);
  }).catch((error) => {
    cb(error, null);
  });
};
