const axios = require('axios');

const log4jUtil = require('../utils/log4j.util');
const commonUtil = require('../utils/common.util');

module.exports.send = (mobileNumber, content, cb) => {
  if (mobileNumber && content) {
    axios.request({
      url: '/ease-api/sms/sendSMS',
      method: 'post',
      baseURL: `${commonUtil.getConfig('INTERNAL_API_DOMAIN')}`,
      data: {
        mobileNo: mobileNumber,
        smsMsg: content,
      },
    }).then((response) => {
      if (response.status === 200) {
        log4jUtil.log('info', 'Send sms successfully');
        cb(null, {});
      } else {
        log4jUtil.log('info', `Send sms error, response.data ${JSON.stringify(response.data)}`);
        cb(response, null);
      }
    }).catch((error) => {
      if (error.response) {
        // The request was made and the server responded with a status code
        // that falls out of the range of 2xx
        log4jUtil.log('info', `Send sms error, response.data:${JSON.stringify(error.response.data)}`);
        log4jUtil.log('info', `Send sms error, response.status:${error.response.status}`);
        log4jUtil.log('info', `Send sms error, response.headers:${JSON.stringify(error.response.headers)}`);
        cb({
          data: error.response.data,
          status: error.response.status,
          headers: error.response.headers,
        }, null);
      } else if (error.request) {
        // The request was made but no response was received
        // `error.request` is an instance of XMLHttpRequest in the browser and an instance of
        // http.ClientRequest in node.js
        log4jUtil.log('info', `Send sms error,error.request:${error.request}`);
        cb(error.request, null);
      } else {
        // Something happened in setting up the request that triggered an Error
        log4jUtil.log('info', `Send sms error,error.message:${error.message}`);
        cb(error.message, null);
      }
    });
  } else {
    cb('mobileNo and smsMsg required', null);
  }
};
